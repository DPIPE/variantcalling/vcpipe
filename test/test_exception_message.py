import unittest
import pytest


# Test exception handling (ref src/executor/engine/iterators.py) and AMG-3027
class ExceptionMessage(unittest.TestCase):

    def test_access_to_exception_message(self):
        e = KeyError("missing key")
#        print(e.msg)
        print(e)
        self.assertEqual(True, True)

    @pytest.mark.skip
    def test_throw_exception_and_print_message(self):
        try:
            _ = {}['unknown']
        except Exception as e:
            # fails as these are non-existing fields
            print(e.message)
            print(e.msg)

            self.assertEqual(True, True)

    def test_use_exception_message_in_string(self):
        e = KeyError("missing key")
#        print("An error occurred {}".format(e.msg))
#        print("An error occurred {}".format(e.message))
        print("An error occurred {}".format(str(e)))
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
