BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
PIPELINE_ID ?= $(shell git rev-parse --abbrev-ref HEAD)
MOUNTROOT ?= $(shell pwd)/test-folders
WEBUI_PORT ?= 8000
SINGULARITY_LOCATION ?= /storage/gitlab-runner/singularity
TESTDATA_LOCATION ?= /gitlab-runner/shared-cache/pipeline-testdata
REFDATA_LOCATION ?= /gitlab-runner/shared-cache/pipeline-refdata
ANNO_LOCATION = /storage/anno
ANNO_DATA_LOCATION = /storage/ci-testing/ella-anno-cache/data
SCRATCH_LOCATION ?= /scratch
EXECUTOR_SETTINGS ?=
COMMAND_NAME ?= /vcpipe/run.sh
CONTAINER_NAME ?= vcp-$(BRANCH)-$(USER)
CONTAINER_NAME_EXECUTOR_CI =  $(PIPELINE_ID)-vcpipe-executor-$(USER)
BASE_IMAGE_NAME := registry.gitlab.com/dpipe/variantcalling/vcpipe

ifneq ($(IMAGE_TAG),)
IMAGE_TAG := $(IMAGE_TAG)
else
IMAGE_TAG := local
endif
IMAGE_NAME := $(BASE_IMAGE_NAME):$(IMAGE_TAG)

.PHONY: help


help:
	@echo ""
	@echo "-- TEST COMMANDS --"
	@echo "STEP 1: set enviroment variables 'CAPTURE_KIT', 'GENE_PANEL', e.g.:"
	@echo "export CAPTURE_KIT=agilent_sureselect_v05 GENE_PANEL=PU_v02"
	@echo "STEP 2: run regression tests, e.g.:"
	@echo "make basepipe-integration   - build image local/<your_branch>, then run basepipe integration with CAPTURE_KIT and GENE_PANEL you supplied"
	@echo "make basepipe-regression    - build image local/<your_branch>, then run basepipe regression with CAPTURE_KIT and GENE_PANEL you supplied"
	@echo "make triopipe-integration   - build image local/<your_branch>, then run triopipe integration with CAPTURE_KIT and GENE_PANEL you supplied"
	@echo "make triopipe-regression    - build image local/<your_branch>, then run triopipe regression with CAPTURE_KIT and GENE_PANEL you supplied"
	@echo "make annopipe-integration   - build image local/<your_branch>, then run annopipe integration with CAPTURE_KIT and GENE_PANEL you supplied"
	@echo "make annopipe-regression    - build image local/<your_branch>, then run annopipe regression with CAPTURE_KIT and GENE_PANEL you supplied"
	@echo ""
	@echo "make lint local-test        - run linting tests"
	@echo "make unit local-test        - run python tests"
	@echo ""
	@echo "-- DEV COMMANDS --"
	@echo "make build		- build image $(IMAGE_NAME)"
	@echo "make dev		    - start an execution environment (no applications will be run) :: various variables can/should be set"
	@echo "make kill		- stop and remove the execution environment / apps"
	@echo "make shell		- get a bash shell into the environment"
	@echo "make logs		- tail logs apps running in the environment"
	@echo "make restart		- restart the environment"
	@echo "make any			- can be prepended to target the first container with pattern vcp-.*-$(USER), e.g. make any kill"

#---------------------------------------------
# DEVELOPMENT
#---------------------------------------------
.PHONY: any build run dev kill shell logs restart executor-ci-logs executor-ci-kill executor-ci-shell executor-ci-clean-import-area

any:
	$(eval CONTAINER_NAME = $(shell docker ps | awk '/vcpipe-.*-$(USER)/ {print $$NF}'))
	@true


build-release:
	docker build -t $(IMAGE_RELEASE_NAME) .

dev: # start container with a noop init process
ifdef EXECUTOR_SETTINGS
	$(eval EXECUTOR_SETTINGS_ENV = -e SETTINGS=$(EXECUTOR_SETTINGS))
endif
	docker run -d \
	--restart=always \
	--name $(CONTAINER_NAME) \
	--hostname $(CONTAINER_NAME) \
	--privileged \
	--user=$(shell id -u):$(shell id -g) \
	-e HOSTUID=$(shell id -u) \
	-e HOSTGID=$(shell id -g) \
	-p $(WEBUI_PORT):5000 \
	$(VCP_OPTS) \
	$(EXECUTOR_SETTINGS_ENV) \
	-v $(shell pwd):/vcpipe \
	-v $(SCRATCH_LOCATION):/tmp \
	-v $(SINGULARITY_LOCATION):/singularity \
	$(IMAGE_NAME) \
	$(COMMAND_NAME)

.docker-dev-start: # start container use as playground for Dockerfile instructions
	docker run -d \
	--name $(CONTAINER_NAME) \
	--hostname $(CONTAINER_NAME) \
	--privileged \
	-v $(shell pwd):/vcpipe \
	$(IMAGE_NAME) \
	$(COMMAND_NAME)

docker-dev: .docker-dev-start # enter playground container
	@docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti $(CONTAINER_NAME)  bash


kill:
	docker stop $(CONTAINER_NAME)
	docker rm $(CONTAINER_NAME)

executor-ci-kill:
	docker stop $(CONTAINER_NAME_EXECUTOR_CI)
	docker rm $(CONTAINER_NAME_EXECUTOR_CI)

shell:
	@docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti $(CONTAINER_NAME)  bash

executor-ci-shell:
	@docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti $(CONTAINER_NAME_EXECUTOR_CI)  bash

executor-ci-logs:
	docker logs -f $(CONTAINER_NAME_EXECUTOR_CI)

executor-ci-clean-import-area:
	@docker exec $(CONTAINER_NAME_EXECUTOR_CI)  bash -c 'rm -rf /analyses/executor/*'

restart:
	docker restart $(CONTAINER_NAME)


#---------------------------------------------
# TESTING
#---------------------------------------------
.PHONY: test ci test-build unit lint basepipe-integration annopipe-integration test-executor executor-ci-setup

build:
	docker build --cache-from ${BASE_IMAGE_NAME}:dev --cache-from ${BASE_IMAGE_NAME}:latest --cache-from ${IMAGE_NAME} -t $(IMAGE_NAME) .

push:
	docker push $(IMAGE_NAME)

test-singularity:
	docker run --privileged --rm $(IMAGE_NAME) \
	 /bin/sh -c 'singularity --version; echo "Exit code $$?"' && echo OK

test-singularity-vcpipe-essentials:
	VCPIPE_ESSENTIALS=$(shell jq '."vcpipe-essentials".filename' config/analysis/singularity.json) \
	&& docker run --privileged --rm  -v $(SINGULARITY_LOCATION):/singularity $(IMAGE_NAME)\
	 /bin/sh -c "singularity test /singularity/vcpipe-essentials/$$VCPIPE_ESSENTIALS"

test-singularity-vcpipe-r-cnv:
	VCPIPE_R_CNV=$(shell jq '."vcpipe-r-cnv".filename' config/analysis/singularity.json) \
	&& docker run --privileged --rm  -v $(SINGULARITY_LOCATION):/singularity $(IMAGE_NAME)\
	 /bin/sh -c "singularity test /singularity/vcpipe-r-cnv/$$VCPIPE_R_CNV"

test-singularity-happy:
	HAPPY=$(shell jq '.happy.filename' config/analysis/singularity.json) \
	&& docker run --privileged --rm  -v $(SINGULARITY_LOCATION):/singularity $(IMAGE_NAME)\
	 /bin/sh -c "singularity exec /singularity/happy/$$HAPPY hap.py --version"

# Start a container running a 'noop' command. Actual testing would be done using 'docker exec' on this container.
# Note! Some volume mounts are not relevant for all testing, but supplied for simplicity.
start-test-container: ci
# Add when testing locally:
#   -p $(WEBUI_PORT):5000
ifdef EXECUTOR_SETTINGS
	$(eval EXECUTOR_SETTINGS_ENV = -e SETTINGS=$(EXECUTOR_SETTINGS))
endif
	docker run \
	 --tty \
	 -d \
	--name $(CONTAINER_NAME) \
	--hostname $(CONTAINER_NAME) \
	--privileged \
	$(VCP_OPTS) \
	$(EXECUTOR_SETTINGS_ENV) \
	-v $(SINGULARITY_LOCATION):/singularity \
	-v $(TESTDATA_LOCATION):/run-testdata \
	-v $(REFDATA_LOCATION):/run-refdata \
	-v $(GENEPANELS_LOCATION):/genepanels \
	-v $(shell pwd)/ops/test/fake-sensitive-db:/fake-sensitive-db \
	$(IMAGE_NAME) \
	/vcpipe/run.sh


exec-in-running-container:
	docker exec $(CONTAINER_NAME) $(COMMAND_NAME)

start-and-exec: start-test-container
	docker exec $(CONTAINER_NAME) $(COMMAND_NAME)

local-test: test-build
	docker run \
	--privileged \
	$(VCP_OPTS) \
	-v $(SINGULARITY_LOCATION):/singularity \
	$(IMAGE_NAME) \
	$(COMMAND_NAME)


ci:
	$(shell mkdir -p $(MOUNTROOT)/tmp)
	$(shell mkdir -p $(MOUNTROOT)/result)
	$(shell mkdir -p $(MOUNTROOT)/output)
	$(shell mkdir -p $(MOUNTROOT)/analyses)
	$(shell mkdir -p $(MOUNTROOT)/preprocessed)
	$(shell chmod 777 -R $(MOUNTROOT))
	$(eval VCP_OPTS = $(VCP_OPTS) \
	  -v $(MOUNTROOT)/result:/result \
	  -v $(MOUNTROOT)/tmp:/tmp \
	  -v $(MOUNTROOT)/output:/output \
	  -v $(MOUNTROOT)/analyses:/analyses \
	  -v $(MOUNTROOT)/preprocessed:/preprocessed \
	  -v $(shell pwd):/vcpipe \
	  -e NXF_CI=yes)
	@echo "VCP_OPTS=$(VCP_OPTS)"
	@true

test-build:
#	$(eval BRANCH = test)
	sed 's/# ADD/ADD/' Dockerfile > Dockerfile.test
	docker build --target python-libs -t $(IMAGE_NAME) -f Dockerfile.test .
	rm Dockerfile.test


tar-python-modules:
	$(eval VCP_OPTS = ${VCP_OPTS} --rm )
	$(eval COMMAND_NAME = /vcpipe/ops/test/tar-python-modules)
	$(eval CONTAINER_NAME = $(PIPELINE_ID)-vcpipe-test-$(USER) )
	@true

unit:
	$(eval VCP_OPTS = ${VCP_OPTS} --rm )
	$(eval COMMAND_NAME = /vcpipe/ops/test/unit-test)
	$(eval CONTAINER_NAME = $(PIPELINE_ID)-vcpipe-test-$(USER) )
	@true

nextflow:
	$(eval VCP_OPTS = ${VCP_OPTS} --rm )
	$(eval COMMAND_NAME = /vcpipe/ops/test/run-nxf-test)
	$(eval CONTAINER_NAME = $(PIPELINE_ID)-vcpipe-nxf-test-$(USER) )
	@true

lint:
	$(eval VCP_OPTS = ${VCP_OPTS} --rm )
	$(eval COMMAND_NAME = /vcpipe/ops/test/lint)
	$(eval CONTAINER_NAME = $(PIPELINE_ID)-vcpipe-lint-test-$(USER) )
	@true

executor-ci-setup: test-build
# start container and have executor and webui for CI testing purposes
	EXECUTOR_SETTINGS="/vcpipe/config/settings-docker_executor_ci.json" \
	CONTAINER_NAME=$(CONTAINER_NAME_EXECUTOR_CI) \
	 make start-test-container

test-executor:
	docker exec $(CONTAINER_NAME_EXECUTOR_CI) /bin/sh -c 'chmod a+w /tmp'
	docker exec $(CONTAINER_NAME_EXECUTOR_CI) /bin/sh -c 'supervisord -c /vcpipe/ops/dev/supervisor-rootprocess.cfg'
	docker exec $(CONTAINER_NAME_EXECUTOR_CI) mkdir -p /output/logs
	docker exec $(CONTAINER_NAME_EXECUTOR_CI) /bin/sh -c 'while ! pg_isready --dbname=postgres --username=postgres; do sleep 5; done; sleep 2; supervisorctl -c /vcpipe/ops/dev/supervisor-rootprocess.cfg start vcpipe:'
	docker exec $(CONTAINER_NAME_EXECUTOR_CI) /vcpipe/ops/test/executor.sh

# intercative cleanup when running the tests yourself on focus:
cleanup:
	$(eval ANS = $(shell bash -c 'read -p "Remove $(MOUNTROOT) [yN]?: " answer; echo $${answer:-N}'))
	@if [[ $(ANS) == "y" ]];	then rm -rf $(MOUNTROOT); echo Done;	else  echo "Skipping";	fi

# cleanup after running tests in CI (tomato):
cleanup-ci-storage:
	rm -rf $$(dirname $$(readlink  -f $(MOUNTROOT)))


# Templates for reusable sequences of commands
# see https://www.gnu.org/software/make/manual/html_node/Canned-Recipes.html
define base-and-triopipe-template
$(eval CONTAINER_NAME_TEST := $(PIPELINE_ID)-$(SCRIPT)-$(SPECIALIZED_PIPELINE)-$(CAPTURE_KIT)-$(USER))
@echo Running $(SCRIPT) in $(CONTAINER_NAME_TEST)
@echo To clean up afterwards, run 'MOUNTROOT=$(MOUNTROOT) make cleanup'
docker run --privileged --rm \
	--name $(CONTAINER_NAME_TEST) \
	--user=$(shell id -u):$(shell id -g) \
	-e HOSTUID=$(shell id -u) \
	-e HOSTGID=$(shell id -g) \
	$(VCP_OPTS) \
	-v $(shell pwd)/refdata.json:/refdata.json \
	-v $(shell pwd)/ops/test/fake-sensitive-db:/fake-sensitive-db \
	-v $(CI_PROJECT_DIR)/ci-input:/input \
	-v $(TESTDATA_LOCATION):/run-testdata \
	-v $(REFDATA_LOCATION):/run-refdata \
	-v $(SINGULARITY_LOCATION):/singularity \
	-v /etc/passwd:/etc/passwd \
	$(IMAGE_NAME) \
	/vcpipe/ops/test/$(SCRIPT) $(CAPTURE_KIT) $(SPECIALIZED_PIPELINE)
endef

define annopipe-template
$(eval CONTAINER_NAME_TEST := $(PIPELINE_ID)-$(SCRIPT)-$(CAPTURE_KIT)-$(GENE_PANEL)-$(USER))
@echo Running $(SCRIPT) in $(CONTAINER_NAME_TEST)
@echo To clean up afterwards, run 'MOUNTROOT=$(MOUNTROOT) make cleanup'
docker run --privileged --rm \
	--name $(CONTAINER_NAME_TEST) \
	--user=$(shell id -u):$(shell id -g) \
	-e HOSTUID=$(shell id -u) \
	-e HOSTGID=$(shell id -g) \
	$(VCP_OPTS) \
	-v $(shell pwd)/refdata.json:/refdata.json \
	-v $(shell pwd)/ops/test/fake-sensitive-db:/fake-sensitive-db \
	-v $(CI_PROJECT_DIR)/ci-input:/input \
	-v $(GENEPANELS_LOCATION):/genepanels \
	-v $(TESTDATA_LOCATION):/run-testdata \
	-v $(REFDATA_LOCATION):/run-refdata \
	-v $(SINGULARITY_LOCATION):/singularity \
	-v $(ANNO_LOCATION):/anno \
	-v $(ANNO_DATA_LOCATION):/anno-data \
	-v /etc/passwd:/etc/passwd \
	$(IMAGE_NAME) \
	/vcpipe/ops/test/$(SCRIPT) $(CAPTURE_KIT) $(GENE_PANEL)
endef

noop-test: ci
	$(eval SCRIPT := noop-test)
	$(base-and-triopipe-template)


basepipe-integration: ci
	$(eval SCRIPT := basepipe-integration)
	$(base-and-triopipe-template)

basepipe-regression: ci
	$(eval SCRIPT := basepipe-regression)
	$(base-and-triopipe-template)

triopipe-integration: ci
	$(eval SCRIPT := triopipe-integration)
	$(base-and-triopipe-template)

triopipe-regression: ci
	$(eval SCRIPT := triopipe-regression)
	$(base-and-triopipe-template)

annopipe-integration: ci
	$(eval SCRIPT := annopipe-integration)
	$(annopipe-template)

annopipe-regression: build ci
	$(eval SCRIPT := annopipe-regression)
	$(annopipe-template)
