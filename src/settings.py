import json
import os
from pathlib import Path


class Settings(dict):

    def __init__(self):
        self.loaded = False
        self.settings_dir = Path(__file__).parent.parent / "config"
        super(Settings, self).__init__()


    def get(self, *args, **kwargs):
        if not self.loaded:
            self._load()
            self.loaded = True
        return super(Settings, self).get(*args, **kwargs)


    def __getitem__(self, key):
        if not self.loaded:
            self._load()
            self.loaded = True
        return super(Settings, self).__getitem__(key)


    def __contains__(self, key):
        if not self.loaded:
            self._load()
            self.loaded = True
        return super(Settings, self).__contains__(key)


    def _load(self):
        if 'SETTINGS' not in os.environ:
            raise RuntimeError(
              "The path to the settings file is not defined. "
              "Please, set the environment variable 'SETTINGS'."
            )

        settings_path = os.environ['SETTINGS']
        self.settings_dir = os.path.dirname(os.path.abspath(settings_path))

        self.update(self.loadJson(settings_path))


    def loadJson(self, path):
        if not os.path.exists(path):
            raise RuntimeError("Path doesn't exist: {}".format(path))

        with open(path) as fd:
            data = json.load(fd)
            return data


    def getBundle(self):
        bundle_path = self['vcpipe_bundle']
        if not os.path.exists(bundle_path):
            raise RuntimeError("Bundle path doesn't exist: {}".format(bundle_path))

        bundle_config_path = os.path.join(bundle_path, 'bundle.json')
        return self.loadJson(bundle_config_path)


    def concatBundlePath(self, rel_path):
        return os.path.join(self['vcpipe_bundle'], rel_path)


    def get_settings(self, capturekit):
        qc_settings_path = os.path.join(self.settings_dir, 'analysis', capturekit + '.json')
        if not os.path.exists(qc_settings_path):
            if capturekit not in self:
                raise RuntimeError("Invalid capturekit name ({}). Config file {} not found".format(
                    capturekit, qc_settings_path)
                )
            else:
                return self[capturekit]

        return self.loadJson(qc_settings_path)


settings = Settings()

