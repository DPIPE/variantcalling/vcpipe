#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-

import os
import configparser
from util.coveragedata import CoverageData
from .coverage_report import Style
from .coverage_report import Table
from .coverage_report import MyFPDF

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))

REPORT_TEXT = {
        'title': 'Vedlegg til rapport ved high throughput sequencing (HTS) analyse',
        'sample': 'Prøvenummer: {}',
        'genepanel': 'Genpanel: {}',
        'coverage_table_title': 'Genliste med dekningsgrad:',
        'coverage_table_title_threshold': 'Genliste med gener under {0:.0f}% dekningsgrad:',
        'coverage_low_table_title': 'Sekvensområder lest mindre enn {} ganger ved HTS:',
        'coverage_table_description': """(1) bp = basepar; + 26 bp = -20 og + 6 bp i intron for å dekke konservert spleisesete (basert på Refseqs fra UCSC refGene tabell av mars 2015, GRCh37/hg19)\n(2) Andel sekvens lest minst {} ganger\n""",
        'limitation_title': 'Metodebegrensninger',
        'limitation_description': """Enkelte genomiske regioner er dupliserte i genomet, noe som fører til begrenset sensitivitet i disse regionene. Disse regionene er ikke inkludert i våre analyser. Hvilke regioner dette gjelder, finnes i oversikt over gener som inngår i genpanelet (se https://www.genetikkportalen.no eller https://oslo-universitetssykehus.no/avdelinger/\nklinikk-for-laboratoriemedisin/avdeling-for-medisinsk-genetikk/enhet-for-hts-diagnostikk). Dekningsgrad i tabellen nedenfor gjelder det området som er inkludert i analysen."""
}

# keys matches data from CoverageData module
COVERAGE_FIELDS = {
    'gene': {
        'label': 'Gen',
        'align': 'L',
        'width': 7
    },
    'transcript': {
        'label': 'Transkript',
        'align': 'L',
        'width': 10
    },
    'phenotype': {
        'label': 'Sykdom',
        'align': 'L',
        'width': 30
    },
    'inheritance': {
        'label': 'Arvegang',
        'align': 'C',
        'width': 10
    },
    'bp': {
        'label': 'Antall kodende\n bp + 26 bp (1)',
        'align': 'R',
        'width': 8
    },
    'percent': {
        'label': 'Dekningsgrad\n (% bp) (2)',
        'align': 'R',
        'width': 8
    }
}

COVERAGE_TABLE_FIELDS = [
    'gene',
    'transcript',
    'phenotype',
    'inheritance',
    'bp',
    'percent'
]
COVERAGE_TABLE_SORT = ('gene', 'transcript')
COVERAGE_TABLE_ROW_HEIGHT = 4

# keys matches data from CoverageData module
COVERAGE_LOW_FIELDS = {
    'chromosome': {
        'label': 'Kromosom',
        'align': 'C',
        'width': 8
    },
    'start': {
        'label': 'Startposisjon',
        'align': 'C',
        'width': 10
    },
    'end': {
        'label': 'Stopposisjon',
        'align': 'C',
        'width': 10
    },
    'start_hgvsg': {
        'label': 'Startposisjon (HGVSg)',
        'align': 'C',
        'width': 14
    },
    'end_hgvsg': {
        'label': 'Stopposisjon (HGVSg)',
        'align': 'C',
        'width': 14
    },
    'gene': {
        'label': 'Gen',
        'align': 'L',
        'width': 8
    },
    'transcript': {
        'label': 'Transkript',
        'align': 'L',
        'width': 10
    },
    'exon': {
        'label': 'Ekson',
        'align': 'C',
        'width': 5
    },
    'reads': {
        'label': 'x dekning',
        'align': 'R',
        'width': 10
    }
}

# COVERAGE_LOW_TABLE_FIELDS is constructed dynamically due to taking options
COVERAGE_LOW_TABLE_SORT = ('gene', 'transcript')


PAGE_BREAK_Y = 275


class CoverageReport(object):

    def __init__(self,
                 coverage_path,
                 low_coverage_path,
                 config_path,
                 transcripts_path,
                 phenotypes_path,
                 title,
                 min_no_read,
                 cov_threshold,
                 gp_suffix,
                 low_coverage_exon,
                 low_coverage_hgvsg):
        self.pdf = MyFPDF()
        self.coverage_path = coverage_path
        self.low_coverage_path = low_coverage_path
        self.config_path = config_path
        self.transcripts_path = transcripts_path
        self.phenotypes_path = phenotypes_path
        self.title = title
        self.min_no_read = min_no_read
        self.cov_threshold = cov_threshold
        self.gp_suffix = gp_suffix
        self.low_coverage_exon = low_coverage_exon
        self.low_coverage_hgvsg = low_coverage_hgvsg
        self.coverage_data = CoverageData(self.transcripts_path, self.phenotypes_path)
        self._loadConfig()

    def _loadConfig(self):
        self.config = configparser.ConfigParser()
        self.config.read(self.config_path)
        if self.config.has_option('DEFAULT', 'coverage_threshold'):
            self.cov_threshold = float(self.config.get('DEFAULT', 'coverage_threshold'))

    def _setup_pdf(self):
        self.pdf.alias_nb_pages()
        self.pdf.add_page()
        self.pdf.add_font('freesans', '', os.path.join(SCRIPT_DIR, 'font', 'FreeSans.ttf'), uni=True)
        self.pdf.add_font('freesans', 'B', os.path.join(SCRIPT_DIR, 'font', 'FreeSansBold.ttf'), uni=True)
        self.pdf.set_font('freesans', '', 7)

    def _get_low_coverage_fields(self):
        header = list()
        if self.low_coverage_hgvsg:
            header.append('start_hgvsg')
            header.append('end_hgvsg')
        else:
            header.append('chromosome')
            header.append('start')
            header.append('end')
        header.append('gene')
        header.append('transcript')
        if self.low_coverage_exon:
            header.append('exon')
        header.append('reads')
        return header

    def _write_header(self):
        self.pdf.set_y(10)
        font_style = {
            'font_style': 'B',
            'font_size': 12
        }
        title = REPORT_TEXT['title']
        with Style(self.pdf, font_style):
            self.pdf.multi_cell(183, 10, title, border='TLR', align='C')

        l_text = REPORT_TEXT['sample'].format(self.title)
        r_text = REPORT_TEXT['genepanel'].format(self.config.get('DEFAULT', 'title'))
        if self.gp_suffix:
            r_text += ' ({})'.format(self.gp_suffix)
        font_style = {
            'font_size': 9
        }
        with Style(self.pdf, font_style):
            self.pdf.set_y(20)
            self.pdf.multi_cell(70, 10, l_text, border="LB", align="C")
            self.pdf.set_xy(70, 20)
            self.pdf.multi_cell(123, 10, r_text, border="RB", align="C")

    def _write_not_covered(self):
        self.pdf.inc_x(-1)
        self.pdf.inc_y(4)
        style = {'font_style': 'B', 'font_size': 9}
        with Style(self.pdf, style):
                self.pdf.multi_cell(200, 1, REPORT_TEXT['limitation_title'])
        self.pdf.inc_y(2)
        text = REPORT_TEXT['limitation_description']
        self.pdf.multi_cell(w=0, h=3, txt=text, align='L')

    def _write_coverage_table(self):
        self.pdf.inc_x(-1)
        self.pdf.inc_y(4)
        style = {'font_style': 'B', 'font_size': 9}
        with Style(self.pdf, style):
            if self.cov_threshold is not None:
                self.pdf.multi_cell(200, 1, REPORT_TEXT['coverage_table_title_threshold'].format(self.cov_threshold))
            else:
                self.pdf.multi_cell(200, 1, REPORT_TEXT['coverage_table_title'])

        t = Table(self.pdf)
        rows = self._create_table_data(COVERAGE_TABLE_FIELDS, rewrite_inheritance=True)
        t.write_rows(rows, 10, self.pdf.y + 3, line_height=COVERAGE_TABLE_ROW_HEIGHT)

    def _write_coverage_desc(self):
        self.pdf.inc_xy(-1, 2)
        text = REPORT_TEXT['coverage_table_description'].format(self.min_no_read)
        if self.config.has_option('DEFAULT', 'coverage_description'):
            text += self.config.get('DEFAULT', 'coverage_description').replace('<br>', '\n')
        self.pdf.multi_cell(200, 3, text)

    def _write_low_coverage_table(self):
        x, y = self.pdf.x, self.pdf.y
        self.pdf.set_xy(x-1, y+5)
        style = {'font_style': 'B', 'font_size': 9}
        with Style(self.pdf, style):
            self.pdf.multi_cell(200, 1, REPORT_TEXT['coverage_low_table_title'].format(self.min_no_read))
        # It is important to read position again, as page wrapping will screw around with positions
        y = self.pdf.y
        t = Table(self.pdf)
        coverage_low_fields = self._get_low_coverage_fields()
        t.write_rows(self._create_table_low_data(coverage_low_fields), 10, y+3, line_height=COVERAGE_TABLE_ROW_HEIGHT)

    def _write_mlpa(self):
        if not self.config.has_option('DEFAULT', 'mlpa'):
            return

        with Style(self.pdf, {'font_size': 10, 'font_style': 'B'}):
            self.pdf.inc_xy(0, 5)
            self.pdf.multi_cell(200, 1, 'Utfyllende analyser:')

        self.pdf.inc_y(2)
        self.pdf.multi_cell(170, 3, 'Den utførte analysen påviser ikke større strukturelle avvik som insersjoner, delesjoner og duplikasjoner. Slike genforandringer kan påvises med MLPA (Multiplex Ligation-dependent Probe Amplification). Ved mistanke om enkelte spesifikke diagnoser er det viktig også å utføre MLPA av ett eller flere gener. Dersom ikke annet er nevnt, er MLPA ikke utført i forbindelse med denne analysen. Eventuell MLPA må rekvireres separat på ny rekvisisjon. Ny blodprøve er ikke nødvendig.')

        with Style(self.pdf, {'font_style': 'B'}):
            self.pdf.inc_y(3)
            self.pdf.multi_cell(170, 1, 'Laboratoriet har tilbud om MLPA for følgende av panelets gener (se for øvrig www.genetikkportalen.no):')

        text = self.config.get('DEFAULT', 'mlpa').replace('<br>', '\n')
        self.pdf.inc_y(2)
        self.pdf.multi_cell(180, 2, text)

    def _create_table_data(self, fields, rewrite_inheritance=False):

        # Setup header row
        rows = list()
        header_row = list()
        for i, field in enumerate(fields):
            field_options = COVERAGE_FIELDS[field]
            header_row.append({
                'text': field_options['label'],
                'w': field_options['width'],
                'style': {
                    'font_style': 'B',  # Header is always bold
                    'align': field_options['align']
                }
            })

        rows.append(header_row)

        coverage_data = self.coverage_data.loadCoverage(self.coverage_path)

        # Filter on coverage if given a threshold
        if self.cov_threshold is not None:
            coverage_data = [v for v in coverage_data if float(v['percent'].strip('%')) < self.cov_threshold]

        # Wrote data rows
        for crow in coverage_data:
            row = list()
            for field in fields:
                if field == 'inheritance':
                    inheritance = crow.get('inheritance', ['n/a'])
                    if rewrite_inheritance:
                        inheritance = CoverageData.rewrite_inheritance(inheritance)
                    value = '\n'.join(inheritance)
                elif field == 'phenotype':
                    value = '\n'.join(crow.get('phenotype', ['n/a']))
                else:
                    value = crow[field]

                field_options = COVERAGE_FIELDS[field]
                row.append({
                    'text': value,
                    'w': field_options['width'],
                    'style': {
                        'align': field_options['align']
                    }
                })

            rows.append(row)
        return rows

    def _create_table_low_data(self, fields):

        low_coverage_data = self.coverage_data.loadLowCoverage(self.low_coverage_path, hgvsg=True)
        rows = list()

        header_row = list()
        for field in fields:
            field_options = COVERAGE_LOW_FIELDS[field]
            header_row.append({
                'text': field_options['label'],
                'w': field_options['width'],
                'style': {
                    'align': field_options['align'],
                    'font_style': 'B',
                }
            })

        rows.append(header_row)

        for test_row in low_coverage_data:
            row = list()
            for field in fields:
                field_options = COVERAGE_LOW_FIELDS[field]
                row.append({
                    'text': test_row[field],
                    'w': field_options['width'],
                    'style': {
                        'align': field_options['align']
                    }
                })
            rows.append(row)
        return rows

    def create_pdf(self, path='coverage-report.pdf'):
        self._setup_pdf()
        self._write_header()
        self._write_not_covered()
        self._write_coverage_table()
        self._write_coverage_desc()
        self._write_low_coverage_table()
        self._write_mlpa()
        self.pdf.output(path, 'F')

    def create_markdown(self, path='coverage-report.md'):
        table_data = self._create_table_data(COVERAGE_TABLE_FIELDS)
        low_coverage_fields = self._get_low_coverage_fields()
        low_cov_table_data = self._create_table_low_data(low_coverage_fields)

        md_text = ''
        if self.cov_threshold is not None:
            md_text += '### ' + REPORT_TEXT['coverage_table_title_threshold'].format(self.cov_threshold) + '\n'
        else:
            md_text += '### ' + REPORT_TEXT['coverage_table_title'] + '\n'

        # Gene coverage table
        md_text += '\n'
        for row_idx, row in enumerate(table_data):
            cols = [c['text'].replace('\n', '<br>') for c in row]
            md_text += '|{}|'.format('|'.join(cols))
            md_text += '\n'
            if row_idx == 0:  # Extra header row
                md_text += '|{}|'.format('|'.join(['---'] * len(cols))) + '\n'
        md_text += '\n'
        md_text += REPORT_TEXT['coverage_table_description'].format(self.min_no_read)
        md_text += '\n'

        # Low coverage table
        md_text += "### " + REPORT_TEXT['coverage_low_table_title'].format(self.min_no_read)
        md_text += '\n'
        for row_idx, row in enumerate(low_cov_table_data):
            cols = [c['text'].replace('\n', '<br>') for c in row]
            md_text += '|{}|'.format('|'.join(cols))
            md_text += '\n'
            if row_idx == 0:  # Extra header row
                md_text += '|{}|'.format('|'.join(['---'] * len(cols))) + '\n'

        with open(path, 'w') as f:
            f.write(md_text)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Create coverage PDF report from coverage and genepanel files.')
    parser.add_argument('--coverage-transcript', required=True, help='coverageReport-transcript file', dest='coverage')
    parser.add_argument('--coverage-low', required=True, help='lowCoverage.bed file', dest='low_coverage')
    parser.add_argument('--transcripts', required=True, help='genepanel transcripts path', dest='transcripts')
    parser.add_argument('--phenotypes', help='genepanel phenotypes path', dest='phenotypes')
    parser.add_argument('--config', required=True, help='report config path', dest='config')
    parser.add_argument('--title', required=True, help='Title for the header (usualy sample ID)', dest='title')
    parser.add_argument('--pdf', help='Output pdf file (default: coverage-report.pdf)', dest='pdf')
    parser.add_argument('--markdown', help='Output markdown file (default: coverage-report.md)', dest='md')
    parser.add_argument('--minimal-no-reads', help='Minimal number of reads to define a well covered site', default=10, dest='min_no_read')
    parser.add_argument('--coverage-threshold', help='Only show gene coverage for coverage below number. Default is to show all genes. Can also be set in config.', default=None, dest='cov_threshold')
    parser.add_argument('--gp-suffix', help='Suffix to add to the genepanel on right side', default='', dest='gp_suffix')
    parser.add_argument('--low-coverage-hgvsg', action='store_true', help='Show positions in low coverage table in HGVSg format', dest='low_cov_hgvsg')
    parser.add_argument('--low-coverage-exon', action='store_true', help='Show exon in low coverage table', dest='low_cov_exon')

    args = parser.parse_args()

    cov_threshold = None
    if args.cov_threshold:
        try:
            cov_threshold = float(args.cov_threshold)
        except ValueError:
            raise RuntimeError('Argument --coverage-threshold must be valid float value')

    filelist = [args.coverage, args.config, args.transcripts, args.low_coverage]
    for p in filelist:
        if not os.path.exists(p):
            raise RuntimeError("File {} does not exist".format(p))

    if not args.pdf and not args.md:
        raise RuntimeError("You need to specify an output, please add argument --pdf or --markdown")

    cr = CoverageReport(
        args.coverage,
        args.low_coverage,
        args.config,
        args.transcripts,
        args.phenotypes,
        args.title,
        args.min_no_read,
        cov_threshold,
        args.gp_suffix,
        args.low_cov_exon,
        args.low_cov_hgvsg
    )
    if args.pdf:
        cr.create_pdf(path=args.pdf)
    if args.md:
        cr.create_markdown(path=args.md)
