#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-

import os
import argparse
import glob
import pandas as pd
import re
from settings import settings


"""
Aggregate the coverage files in one Excel sheet, for use in EKG

usage: coverage_report_aggregate_xls.py [-h] -p PROJECT_PATH
                                        [--minimal-no-reads MIN_NO_READ]
                                        [--coverage-threshold COV_THRESHOLD]
Example:
$ coverage_report_aggregate_xls.py -p /ess/p22/data/diag/interpretations/Diag-EKG171002

Note: the VCPIPE environment must be loaded, so that the `bundle.json` is available.
The script makes use of
`vcpipe-bundle/clinicalGenePanels/{genepanel}/{genepanel}.phenotypes.csv` files.
"""

TITLE = "Genliste med gener under 100% dekningsgrad: (Dekningsgrad: gener)"
COV = [  # (column_name, show, translation)
    ('Project number', True, 'Prosjekt nummer'),
    ('#sampleName', True, 'Provenummer'),
    ('Gene panel', True, 'Genpanel'),
    ('gene', True, 'Gen'),
    ('transcript', True, 'Transkript'),
    ('phenotype', True, 'Sykdom'),
    ('inheritance', True, 'Arvegang'),
    ('nucleotides in total', True, 'Antall kodende bp + 4 bp (1)'),
    ('fraction covered', True, 'Dekningsgrad (% bp) (2)'),
    ('nucleotides covered', False, None),
    ('gene symbol', False, None),
 ]
COMMENT1 = "(1) bp = basepar; + 4 bp = -2 og + 2 bp i intron for a dekke konservert spleisesete (basert pa Refseqs fra UCSC refGene tabell av mars 2015, GRCh37/hg19)"
COMMENT2 = "(2) Andel sekvens lest minst 20 ganger"

TITLE2 = "Sekvensomrader lest mindre enn 20 ganger ved HTS: (Dekningsgrad: omrader)"
LOW = [  # (column_name, show, translation)
    ('Project number', True, 'Prosjekt nummer'),
    ('sample_name', True, 'Provenummer'),
    ('Gene panel', True, 'Genpanel'),
    ('chrom', False, None),
    ('start',  False, None),
    ('stop',  False, None),
    ('hgvs_start', True, 'Startposisjon (HGVSg)'),
    ('hgvs_stop', True, 'Stopposisjon (HGVSg)'),
    ('gene', True, 'Gen'),
    ('transcript', True, 'Transkript'),
    ('exon', True, 'Ekson'),
    ('coverage', True, 'x dekning'),
    ('descr', False, None),
    ('strand', False, None)
]

class Phenotype(object):
    """
    Return disease phenotype from gene panel and gene symbol arguments.
    """

    def __init__(self):
        self.phenotype_df = dict()

    def _get_phenotype_dataframe(self, gene_panel):
        """
        Load the phenotype file from the relevant clinicalGenePanel as described
        in the `bundle.json` into a Pandas dataframe. The dataframe is then stored
        in a dictionary under the "genePanel" key, in order to avoid reloading
        multiple times.
        """
        if gene_panel not in self.phenotype_df:
            phenotype_file = settings.concatBundlePath(settings.getBundle()['clinicalGenePanels'][gene_panel]['phenotypes'])
            phenotype_df = pd.read_csv(phenotype_file, sep='\t', skiprows=1)
            phenotype_df = phenotype_df.rename(columns={col: col.lstrip('#') for col in phenotype_df.columns})  # get rid of potential '#'
            self.phenotype_df[gene_panel] = phenotype_df[['gene symbol', 'inheritance', 'phenotype']]
        return self.phenotype_df[gene_panel]

    def get_phenotype(self, gene_panel, gene_symbol):
        phenotype_df = self._get_phenotype_dataframe(gene_panel)
        phenotype_string = '; '.join(phenotype_df[phenotype_df['gene symbol'] == gene_symbol]['phenotype'].tolist())
        return phenotype_string

    def get_inheritance(self, gene_panel, gene_symbol):
        phenotype_df = self._get_phenotype_dataframe(gene_panel)
        inheritance_string = '; '.join(phenotype_df[phenotype_df['gene symbol'] == gene_symbol]['inheritance'].tolist())
        return inheritance_string


def get_gene_coverage(project_path, cov_threshold=100):
    """
    Return concatenated pandas dataframe from *gene* coverage files.
    The dataframe is mangled for display and filtered.
    """
    coverageReport_transcripts = glob.glob(
        os.path.join(project_path, '**/*_coverageReport_transcript'))

    ph = Phenotype()
    df_list = list()
    for coverageReport_transcript in coverageReport_transcripts:
        df = pd.read_csv(coverageReport_transcript, sep='\t')
        regex = r'^.*\/Diag-(?P<project>\w*)-(?P<sample>\w*)-(?P<genepanel>\w*)[\-_]+(?P<version>v\d*)\/.*_coverageReport_transcript$'
        pattern = re.compile(regex)
        match = re.search(pattern, coverageReport_transcript)
        if match:
            match = match.groupdict()
            df['Project number'] = match['project']
            df['#sampleName'] = match['sample']
            df['Gene panel'] = match['genepanel'] + ' (' + match['version'] + ')'
            gp = '_'.join([match['genepanel'], match['version']])
            df['phenotype'] =  df.apply(lambda row: ph.get_phenotype(gp, row['gene']), axis=1)
            df['inheritance'] =  df.apply(lambda row: ph.get_inheritance(gp, row['gene']), axis=1)
            df_list += [df]
    pd_cov = pd.concat(df_list)

    pd_cov_filtered = pd_cov[pd_cov['fraction covered'].apply(lambda x: float(x.strip('%'))) < cov_threshold]  # filter data
    pd_cov_filtered = pd_cov_filtered[[c[0] for c in COV if c[1]]]  # sort and select columns to display
    pd_cov_filtered = pd_cov_filtered.sort(['Project number', '#sampleName', 'gene'])  # sort rows
    pd_cov_filtered = pd_cov_filtered.rename(columns=dict([(c[0], c[2]) for c in COV]))  # rename columns

    return pd_cov_filtered


def get_low_coverage(project_path, min_no_read=20):
    """
    Return concatenated pandas dataframe from *low* coverage files.
    The dataframe is mangled for display and filtered.
    """
    lowCoverages = glob.glob(os.path.join(project_path, '**/*_lowCoverage.bed'))
    df_list = list()
    for lowCoverage in lowCoverages:
        df = pd.read_csv(lowCoverage, sep='\t', header=None, names=['chrom',
                         'start', 'stop', 'descr', 'coverage', 'strand',
                         'sample_name', 'gene', 'transcript', 'exon'],
                         dtype=str)
        regex = r'^.*\/Diag-(?P<project>\w*)-(?P<sample>\w*)-(?P<genepanel>\w*)[\-_]+(?P<version>v\d*)\/.*_lowCoverage.bed$'
        pattern = re.compile(regex)
        match = re.search(pattern, lowCoverage)
        if match:
            match = match.groupdict()
            df['Project number'] = match['project']
            df['sample_name'] = match['sample']
            df['Gene panel'] = match['genepanel'] + ' (' + match['version'] + ')'
            df_list += [df]

    pd_low = pd.concat(df_list)

    # The exact nucleotide change is not relevant here, and we use N>N
    pd_low['hgvs_start'] = pd_low.apply(lambda row: 'chr' + row['chrom'] + ':g.' + row['start'] + 'N>N', axis=1)
    pd_low['hgvs_stop'] = pd_low.apply(lambda row: 'chr' + row['chrom'] + ':g.' + row['stop'] + 'N>N', axis=1)

    pd_low_filtered = pd_low[pd_low['coverage'].apply(lambda x: float(x)) < min_no_read]  # filter data
    pd_low_filtered = pd_low_filtered[[c[0] for c in LOW if c[1]]]  # sort and select columns to display
    pd_low_filtered = pd_low_filtered.sort(['Project number', 'sample_name', 'hgvs_start', 'gene'])  # sort rows
    pd_low_filtered = pd_low_filtered.rename(columns=dict([(c[0], c[2]) for c in LOW]))  # rename columns

    return pd_low_filtered


def write_excel(df, df2, output_filename):
    """
    Write dataframe to Excel workbook, and beautify
    """
    writer = pd.ExcelWriter(output_filename, engine='xlsxwriter')

    workbook = writer.book
    worksheet = workbook.add_worksheet('Dekningsgrad')
    writer.sheets['Dekningsgrad'] = worksheet

    worksheet.set_column('A:A', 12, None)
    worksheet.set_column('B:E', 18, None)
    worksheet.set_column('F:F', 30, None)
    worksheet.set_column('G:G', 18, None)
    worksheet.set_column('H:I', 10, None)

    # Add formats
    header_format = workbook.add_format({
        'bold': True,
        'text_wrap': True,
        'valign': 'top',
        'fg_color': '#A6A6A6',
        'font_color': '#FFFFFF',
        'border': 1})
    title_format = workbook.add_format({
        'bold': True,
        'text_wrap': False,
        'font_size': 15})
    comment_format = workbook.add_format({
        'font_size': 8,
        'text_wrap': False})
    table_format = workbook.add_format({'border': 1})

    # Write the column headers with the defined format.
    for col_num, value in enumerate(df.columns.values):
        worksheet.write(2, col_num, value, header_format)

    df.to_excel(writer, sheet_name='Dekningsgrad', startrow=3,
                header=False, index=False)

    worksheet.write(0, 0, TITLE, title_format)
    worksheet.write(len(df) + 3, 0, COMMENT1, comment_format)
    worksheet.write(len(df) + 4, 0, COMMENT2, comment_format)
    idx = len(df) + 7

    df2.to_excel(writer, sheet_name='Dekningsgrad', startrow=idx+3,
                 header=False, index=False)
    # Write the column headers with the defined format.
    for col_num, value in enumerate(df2.columns.values):
        worksheet.write(idx+2, col_num, value, header_format)
    worksheet.write(idx, 0, TITLE2, title_format)

    writer.save()

    #
    # beautify excel sheet
    #
    from openpyxl import Workbook
    from openpyxl import load_workbook
    from openpyxl.styles import Border, Color

    def set_border(ws, cell_range):
        rows = ws.range(cell_range)
        for row in rows:
            for c in row:
                c.style.borders.left.border_style = Border.BORDER_THIN
                c.style.borders.right.border_style = Border.BORDER_THIN
                c.style.borders.top.border_style = Border.BORDER_THIN
                c.style.borders.bottom.border_style = Border.BORDER_THIN

    def set_colors(ws, cell_range, color):
        rows = ws.range(cell_range)
        for row in rows:
            for c in row:
                c.style.font.color.index = color

    wb = load_workbook(output_filename)
    ws = wb['Dekningsgrad']
    set_border(ws, 'A4:I{}'.format(len(df)+3))
    set_colors(ws, 'A4:C{}'.format(len(df)+3), Color.RED)
    set_border(ws, 'A{}:I{}'.format(idx+4, idx+len(df2)+3))
    set_colors(ws, 'A{}:C{}'.format(idx+4, idx+len(df2)+3), Color.RED)

    wb.save(output_filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create aggregate coverage XLS report from coverage and genepanel files')
    parser.add_argument('-p', '--project', required=True,
                        help='path to project', dest='project_path')
    parser.add_argument('-o', '--output', required=False,
                        help='Output XLS file (default: coverage-report.xlsx)', default='coverage-report.xlsx', dest='output')
    parser.add_argument('--minimal-no-reads', required=False, type=int,
                        help='Minimal number of reads to define a well covered site',
                        default=20, dest='min_no_read')
    parser.add_argument('--coverage-threshold', required=False, type=int,
                        help='Only show gene coverage for coverage below number. Default is to show below 100%',
                        default=100, dest='cov_threshold')
    args = parser.parse_args()

    gene_coverage = get_gene_coverage(args.project_path, args.cov_threshold)
    low_coverage = get_low_coverage(args.project_path, args.min_no_read)
    write_excel(gene_coverage, low_coverage, args.output)

"""
project_path = 'test/'
cov_threshold = 100
min_no_read = 20
output_filename = 'coverage-report.xlsx'
gene_coverage = get_gene_coverage(project_path, cov_threshold)
low_coverage =  get_low_coverage(project_path, min_no_read)
write_excel(gene_coverage, low_coverage, output_filename)
"""
