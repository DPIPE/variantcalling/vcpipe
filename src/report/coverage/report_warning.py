#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-

import os
import json
import configparser

def ReportWarning(qc_result, output):
    with open(qc_result, 'r') as fd:
        qc_data = json.load(fd)
    failed_regions = qc_data['LowCoverageRegions'][1].get('failed')
    warning = ''
    if failed_regions:
        warning += '{} regions have too low coverage. Check `coverage-report.md` for more information.\n'.format(len(failed_regions))
    if warning:
        with open(output, 'w') as f:
            f.write(warning)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Create a text file warning users if QC has failed LowCoverageRegions.')
    parser.add_argument('--qc-result', required=True, help='qc-result.json', dest='qc_result')
    parser.add_argument('-o', '--output', required=True, help='Output filename', dest='output')

    args = parser.parse_args()

    ReportWarning(**vars(args))
