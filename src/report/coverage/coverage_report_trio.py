#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-

import os
import configparser
from util.coveragedata import CoverageData
from .coverage_report import Style
from .coverage_report import Table
from .coverage_report import MyFPDF


SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))

REPORT_TEXT = {
    'title': 'Vedlegg til rapport ved high throughput sequencing (HTS) analyse',
    'sample': 'Prøvenummer: {}',
    'genepanel': 'Genpanel: {}',
    'coverage_table_title_threshold': 'Gener dekket mindre enn {}%',
    'coverage_low_table_title': 'Sekvensområder lest mindre enn {} ganger ved HTS:',
    'coverage_table_description': """(1) bp = basepar; + 26 bp = -20 og + 6 bp i intron for å dekke konservert spleisesete (basert på Refseqs fra UCSC refGene tabell av mars 2015, GRCh37/hg19)\n(2) Andel sekvens lest minst {} ganger\n""",
    'limitation_title': 'Metodebegrensninger',
    'limitation_description': """Enkelte genomiske regioner er dupliserte i genomet, noe som fører til begrenset sensitivitet i disse regionene. Disse regionene er ikke inkludert i våre analyser. Hvilke regioner dette gjelder, finnes i oversikt over gener som inngår i genpanelet (se https://www.genetikkportalen.no eller https://oslo-universitetssykehus.no/avdelinger/\nklinikk-for-laboratoriemedisin/avdeling-for-medisinsk-genetikk/enhet-for-hts-diagnostikk). Dekningsgrad i tabellen nedenfor gjelder det området som er inkludert i analysen."""
}


COVERAGE_HEADER = [
    ('Gen', 'L'),
    ('Transkript', 'L'),
    ('Antall kodende\n bp + 26 bp (1)', 'R'),
    ('Proband\n (% bp) (2)', 'R'),
    ('Mor\n (% bp) (2)', 'R'),
    ('Far\n (% bp) (2)', 'R')
]
    

COVERAGE_COLUMN_SIZE = [7, 10, 8, 8, 8, 8]
COVERAGE_ROW_HEIGHT = 4

PAGE_BREAK_Y = 275


class CoverageReport(object):
    def __init__(self,
                proband_coverage,
                mother_coverage,
                father_coverage,
                config,
                transcripts,
                title,
                min_no_read,
                gp_suffix,
                cov_threshold):
        self.pdf = MyFPDF()
        self.proband_coverage_path = proband_coverage
        self.mother_coverage_path = mother_coverage
        self.father_coverage_path = father_coverage
        self.config_path = config
        self.transcripts_path = transcripts
        self.title = title
        self.min_no_read = min_no_read
        self.gp_suffix = gp_suffix
        self.cov_threshold = cov_threshold

        self.proband_coverage_data = CoverageData(self.transcripts_path)
        self.mother_coverage_data = CoverageData(self.transcripts_path)
        self.father_coverage_data = CoverageData(self.transcripts_path)
        self._loadConfig()

    def _loadConfig(self):
        self.config = configparser.ConfigParser()
        self.config.read(self.config_path)
        if not self.cov_threshold and self.config.has_option('DEFAULT', 'coverage_threshold'):
            self.cov_threshold = float(self.config.get('DEFAULT', 'coverage_threshold'))
        else:
            self.cov_threshold = 100.0

    def _setup_pdf(self):
        self.pdf.alias_nb_pages()
        self.pdf.add_page()
        self.pdf.add_font('freesans', '', os.path.join(SCRIPT_DIR, 'font', 'FreeSans.ttf'), uni=True)
        self.pdf.add_font('freesans', 'B', os.path.join(SCRIPT_DIR, 'font', 'FreeSansBold.ttf'), uni=True)
        self.pdf.set_font('freesans', '', 7)

    def _write_header(self):
        self.pdf.set_xy(15, 10)
        font_style = {
            'font_style': 'B',
            'font_size': 12
        }
        with Style(self.pdf, font_style):
            self.pdf.multi_cell(180, 10, REPORT_TEXT['title'], border='TLR', align='C')

        l_text = REPORT_TEXT['sample'].format(self.title)
        r_text = REPORT_TEXT['genepanel'].format(self.config.get('DEFAULT', 'title'))

        if self.gp_suffix:
            r_text += ' ({})'.format(self.gp_suffix)
        font_style = {
            'font_size': 9
        }
        with Style(self.pdf, font_style):
            self.pdf.set_xy(15, 20)
            self.pdf.multi_cell(90, 10, l_text, border="LB", align="C")
            self.pdf.set_xy(85, 20)
            self.pdf.multi_cell(110, 10, r_text, border="RB", align="C")

    def _write_not_covered(self):
        self.pdf.inc_x(-1)
        self.pdf.inc_y(4)
        style = {'font_style': 'B', 'font_size': 9}
        with Style(self.pdf, style):
            self.pdf.multi_cell(200, 1, REPORT_TEXT['limitation_title'])
        self.pdf.inc_y(2)
        text = REPORT_TEXT['limitation_description']
        self.pdf.multi_cell(w=0, h=3, txt=text, align='L')

    def _write_coverage_table(self):
        self.pdf.inc_x(-1)
        self.pdf.inc_y(3)
        style = {'font_style': 'B', 'font_size': 9}
        with Style(self.pdf, style):
            self.pdf.multi_cell(200, 1, REPORT_TEXT['coverage_table_title_threshold'].format(self.cov_threshold))

        t = Table(self.pdf)
        t.write_rows(self._create_table_data(), 10, self.pdf.y + 3, line_height=COVERAGE_ROW_HEIGHT)

    def _write_coverage_desc(self):
        self.pdf.inc_xy(-1, 2)
        text = REPORT_TEXT['coverage_table_description'].format(self.min_no_read)
        if self.config.has_option('DEFAULT', 'coverage_description'):
            text = self.config.get('DEFAULT', 'coverage_description').replace('<br>', '\n')
        self.pdf.multi_cell(200, 3, text)

    def _create_table_data(self):
        proband_coverage_data = self.proband_coverage_data.loadCoverage(self.proband_coverage_path)
        mother_coverage_data = self.mother_coverage_data.loadCoverage(self.mother_coverage_path)
        father_coverage_data = self.father_coverage_data.loadCoverage(self.father_coverage_path)

        coverage_data = dict()
        for item in proband_coverage_data:
            coverage_data[item['transcript']] = [
                item['gene'],
                item['transcript'],
                item['bp'],
                item['percent']
            ]
        for item in mother_coverage_data:
            coverage_data[item['transcript']].append(item['percent'])
        for item in father_coverage_data:
            coverage_data[item['transcript']].append(item['percent'])

        # serialize, filter, sort
        coverage_data = [v for v in list(coverage_data.values()) if float(v[-3].strip('%')) < self.cov_threshold or \
                                                              float(v[-2].strip('%')) < self.cov_threshold or \
                                                              float(v[-1].strip('%')) < self.cov_threshold]
        coverage_data.sort()

        rows = list()
        header_row = list()
        for i, cell in enumerate(COVERAGE_HEADER):
            header_row.append({
                'text': cell[0],
                'w': COVERAGE_COLUMN_SIZE[i],
                'style': {
                    'font_style': 'B',
                    'align': cell[1]
                }
            })

        rows.append(header_row)
        for coverage_row in coverage_data:
            row = list()
            for i, cell in enumerate(coverage_row):
                cell = {
                    'text': cell,
                    'w': COVERAGE_COLUMN_SIZE[i],
                    'style': {
                        'align': COVERAGE_HEADER[i][1]
                    }
                }
                if i == 0:
                    cell['style'].update({
                        'font_style': 'B'
                    })
                if i == 6:
                    cell['style'].update({
                        'font_style': 'B',
                    })
                row.append(cell)
            rows.append(row)
        return rows

    def create(self, path='coverage-report.pdf'):
        self._setup_pdf()
        self._write_header()
        self._write_not_covered()
        self._write_coverage_table()
        self._write_coverage_desc()
        self.pdf.output(path, 'F')


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Create coverage PDF report from coverage and genepanel files.')
    parser.add_argument('--coverage-transcript-proband', required=True, help='coverageReport-transcript file', dest='proband_coverage')
    parser.add_argument('--coverage-transcript-mother', required=True, help='coverageReport-transcript file', dest='mother_coverage')
    parser.add_argument('--coverage-transcript-father', required=True, help='coverageReport-transcript file', dest='father_coverage')
    parser.add_argument('--transcripts', required=True, help='genepanel transcripts path', dest='transcripts')
    parser.add_argument('--config', required=True, help='report config path', dest='config')
    parser.add_argument('--title', required=True, help='Title for the header (usualy sample ID)', dest='title')
    parser.add_argument('--minimal-no-reads', help='Minimal number of reads to define a well covered site', default=10, dest='min_no_read')
    parser.add_argument('-o', required=True, help='Output file (default: coverage-report.pdf)', dest='output')
    parser.add_argument('--gp-suffix', help='Suffix to add to the genepanel on right side', default='', dest='gp_suffix')
    parser.add_argument('--coverage-threshold', help='Only show gene coverage for coverage below number. Default is to show all genes. Can also be set in config.', default=None, dest='cov_threshold')
    
    args = parser.parse_args()

    cov_threshold = None
    if args.cov_threshold:
        try:
            cov_threshold = float(args.cov_threshold)
        except ValueError:
            raise RuntimeError('Argument --coverage-threshold must be valid float value')

    path_list = [args.proband_coverage, args.mother_coverage, args.father_coverage,
                 args.config, args.transcripts]

    for p in path_list:
        if not os.path.exists(p):
            raise RuntimeError("File {} does not exist".format(p))

    cr = CoverageReport(proband_coverage=args.proband_coverage,
                        mother_coverage=args.mother_coverage,
                        father_coverage=args.father_coverage,
                        config=args.config,
                        transcripts=args.transcripts,
                        title=args.title,
                        min_no_read=args.min_no_read,
                        gp_suffix=args.gp_suffix,
                        cov_threshold=cov_threshold)
    cr.create(path=args.output)
