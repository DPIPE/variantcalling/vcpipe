import csv
import os
import glob
import logging

from collections import OrderedDict

log = logging.getLogger(__name__)


class GenepanelReader(object):
    """
    Parses a transcripts.csv file from a genepanel and returns the result.
    If a phenotypes.csv file is available, the result is merged into the result.
    """

    def _load_trancripts(self, transcripts_path):
        transcripts = OrderedDict()
        with open(transcripts_path) as fd:
            reader = csv.reader(fd, delimiter='\t')
            header = None
            for idx, r in enumerate(reader):
                # Skip first header
                if idx == 0:
                    continue
                if idx == 1:
                    # Split away first '#'
                    r[0] = r[0][1:]
                    header = r
                    continue
                if not r:
                    continue
                t = dict(list(zip(header, r)))
                transcripts[t['refseq']] = {
                    'chrom': t['chromosome'],
                    'tx_start': int(t['txStart']),
                    'tx_end': int(t['txEnd']),
                    'gene': t['geneSymbol'],
                    'gene_alias': t.get('geneAlias', '').split(','),
                    'transcript': t['refseq'],
                    'ensembl_transcript': t['eTranscriptID'],
                    'chromosome': t['chromosome'],
                }
                if 'inheritance' in t:
                    transcripts[t['refseq']]['inheritance'] = [t['inheritance']]
                if 'disease' in t:
                    transcripts[t['refseq']]['phenotype'] = [t['disease']]

        return transcripts

    def _load_phenotypes(self, phenotypes_path):
        if not phenotypes_path:
            return dict()
        phenotypes = dict()
        with open(phenotypes_path) as fd:
            reader = csv.reader(fd, delimiter='\t')
            header = None
            for idx, r in enumerate(reader):
                # Skip first header
                if idx == 0:
                    continue
                if idx == 1:
                    # Split away first '#'
                    if r[0][0] == '#':
                        r[0] = r[0][1:]
                    header = r
                    continue
                if not r:
                    continue
                t = dict(list(zip(header, r)))

                # Any additional columns will be join by commas and returned as a single dict entry
                extra_phenotypes_header = [h for h in header if h not in ['gene symbol', 'HGNC', 'phenotype', 'inheritance',
                    'omim_number', 'pmid', 'inheritance info', 'comment']]
                if t['gene symbol'] not in phenotypes:
                    phenotypes[t['gene symbol']] = {
                        'gene': t['gene symbol'],
                        'phenotype': [t['phenotype']],
                        'inheritance': [t['inheritance']],
                        'omim_number': [t['omim_number']],
                        'inheritance info': [t['inheritance info']],
                        'HGNC': [t.get('HGNC', '')],
                        'extra_phenotypes': [','.join([t[eph] for eph in extra_phenotypes_header if t.get(eph)])]
                    }
                else:
                    for field in ['phenotype', 'inheritance', 'omim_number', 'inheritance info', 'HGNC']:
                        phenotypes[t['gene symbol']][field].append(t.get(field, ''))
                    phenotypes[t['gene symbol']]['extra_phenotypes'].append(','.join([t[eph] for eph in extra_phenotypes_header if t.get(eph)]))


        return phenotypes

    def _merge_transcripts_phenotypes(self, transcripts, phenotypes):
        for transcript_name in transcripts:
            gene_symbol = transcripts[transcript_name]['gene']
            if gene_symbol in phenotypes:
                transcripts[transcript_name].update(phenotypes[gene_symbol])

        return transcripts

    def get_display_inheritance(self, transcript):
        """
        Cleans the inheritance of the transcript, renaming some codes
        and gives out the sorted, distinct set of codes separated by comma.

        Codes that doesn't start with 'AR', 'AD' or 'X' are filtered out.
        """
        display_codes = list()
        try:
            for code in transcript['inheritance']:
                display_codes += code.replace('/', ';').replace('XD', 'X').replace('XR', 'X').replace(',', ';').split(';')
                display_codes = [c.strip() for c in display_codes]
        except KeyError:
            display_codes = ['N/A']
            log.error('No inheritance code found for {}'.format(transcript['gene']))
        display_codes = sorted(list(set(display_codes)))
        # For some old panels we have stuff like 'AD (imprinted)' and similar, that we need to keep
        return ','.join([c for c in display_codes for s in ['AR', 'AD', 'X', 'N/A'] if c.startswith(s)])

    def get_transcripts(self, transcripts_path, phenotypes_path=None):
        """
        :param transcripts_path: Path to genepanel's transcripts file.
        :param phenotypes_path: Path to genepanel's phenotype file. If None, try to autodetect it's presence.
        :returns: OrderedDict of format: {'NM_123.1': {data}, 'NM_125.1': {data}, ...}

        Phenotype file autodetection work by looking for the pattern *.phenotypes.csv in the same
        folder as the input transcripts file's path.
        """

        transcripts = self._load_trancripts(transcripts_path)

        if phenotypes_path is None:
            # Try to find phenotypes files
            base_path = os.path.split(transcripts_path)[0]
            matches = glob.glob(os.path.join(base_path, '*.phenotypes.csv'))
            if len(matches) > 1:
                raise RuntimeError("Found more than one phenotypes, I don't know which one to choose.")
            elif len(matches) == 1:
                phenotypes_path = matches[0]
            else:
                log.warn("Found no phenotypes file, assuming old-style genepanel.")

        if not phenotypes_path and not any('phenotype' in t for t in list(transcripts.values())):
            log.warn("Found no phenotype file and no phenotype information present in transcripts file.")

        if phenotypes_path:
            phenotypes = self._load_phenotypes(phenotypes_path)
            transcripts = self._merge_transcripts_phenotypes(transcripts, phenotypes)

        for t in list(transcripts.values()):
            t['display_inheritance'] = self.get_display_inheritance(t)

        return transcripts


if __name__ == '__main__':
    import argparse
    import json

    logging.basicConfig()

    parser = argparse.ArgumentParser(description='Loads a genepanel transcripts file and prints the result as json')
    parser.add_argument('--transcripts', required=True, help='Path to genepanel transcripts file', dest='transcripts')
    parser.add_argument('--phenotypes', required=False, help='Path to genepanel phenotypes file (will try to find automatically if not provided)', dest='phenotypes')

    args = parser.parse_args()

    print(json.dumps(GenepanelReader().get_transcripts(args.transcripts, phenotypes_path=args.phenotypes), indent=4))
