import logging
try:
    import xlsxwriter
except ImportError:
    print("You need to install the openpyxl module.")
    raise

from report.interpretation.metadata import VEP_CONSEQUENCE, CHECKLIST, CHECKLIST_COMMENT, PHENOTYPE_CHECKLIST, COVERAGE_TEMPLATE_HEADER, COVERAGE_TEMPLATE, PSEUDOGENES
from report.interpretation.excelwriter import Worksheet, RowWriter


log = logging.getLogger(__name__)

INTERPRETATION_OPTIONS = [
    # (code, title, color)
    (1, 'Class 1', '#4fbf74'),
    (2, 'Class 2', '#82de86'),
    (3, 'Class 3', '#ffc14f'),
    (4, 'Class 4', '#ff857d'),
    (5, 'Class 5', '#ff6266'),
    ('T', 'Technical variant', '#aea5ff'),
    ('U', 'Unclassified variant', '#a4d3e9'),
    ('ir', 'Ikke Rapportert (the class number is appended to the code, eg. "ir3")', '#ffff00'),
    ('?', 'Unlikely candidate variant, check more likely candidates first', '#f5f5dc')
]

INTERPRETATION_COLUMN = 'Klasse'


class VariantRowWriter(RowWriter):
    """
    Custom RowWriter for writing out variants.

    rows = [
        OrderedDict([(column1, value1), (column2, value2)]),
        OrderedDict([(column1, value3), (column2, value4)])
    ]
    All rows must have matching column names
    and same number of entries.

    It adds colors to (VEP) Consequence field,
    handles URLs and adds logic for the INTERPRETATION_COLUMN column.
    """

    def __init__(self, rows, color_interpretation=False):
        super(VariantRowWriter, self).__init__(rows)
        self.formats = dict()
        self.color_interpretation = color_interpretation

    def _create_formats(self):
        base_fmt = {
            'border': 1
        }
        self.formats['base'] = self.add_format(base_fmt)

        for key, value in VEP_CONSEQUENCE.items():
            fmt = dict(base_fmt)
            fmt.update({'bg_color': value['color']})
            self.formats[key] = self.add_format(fmt)

        for key, _, color in INTERPRETATION_OPTIONS:
            fmt = dict(base_fmt)
            fmt.update({'bg_color': color})
            self.formats[key] = self.add_format(fmt)

        for key, value in PSEUDOGENES.items():
            if 'color' in value:
                fmt = dict(base_fmt)
                fmt.update({'bg_color': value['color']})
                self.formats[key] = self.add_format(fmt)

        fmt = dict(base_fmt)
        fmt.update({'bold': True, 'align': 'center'})
        self.formats['Klasse'] = self.add_format(fmt)

    def get_index(self, name):
        """
        Get index of the column matching the provided name.
        """
        return list(self.rows[0].keys()).index(name)

    def write(self, ws, start_row_idx, start_col_idx=0):
        # Pre-create our formats (must be done after setup())
        self._create_formats()
        self._write_header(ws, start_row_idx=start_row_idx, start_col_idx=start_col_idx)

        # +1 due to header
        row_idx, col_idx = start_row_idx+1, start_col_idx
        for row in self.rows:
            col_idx = start_col_idx
            for key, value in row.items():
                write_fmt = self.formats.get('base')
                if not (hasattr(value, 'isdigit') and value.isdigit()):
                    value = str(value).decode('utf-8')

                # Write VEP colors
                if key == 'Consequence':
                    if value in VEP_CONSEQUENCE:
                        write_fmt = self.formats.get(value)
                    ws.write(row_idx, col_idx, value, write_fmt)

                # Write URL with title in cell
                elif key in ['ExAC_URL', 'HGMD_URL', 'OMIM_URL', 'CLINVAR_URL', 'GNOMAD_URL']:
                    if not value == 'N/A':
                        value, url = value.split(' | ', 1)
                        ws.write_url(row_idx, col_idx, url, write_fmt, value)

                elif key == 'Klasse':
                    ws.write(row_idx, col_idx, value, self.formats.get('Klasse'))

                elif key == 'pseudogenes':
                    # Assign color of the worst (ie higher up) category
                    for pseudo in list(PSEUDOGENES.keys()):
                        if pseudo in value.split('|'):
                            write_fmt = self.formats.get(pseudo)
                            break
                    ws.write(row_idx, col_idx, value, write_fmt)

                else:
                    ws.write(row_idx, col_idx, value, write_fmt)

                if key.startswith('ExAC_'):
                    self.column_widths[col_idx] = 5
                else:
                    if isinstance(value, str):
                        self.column_widths[col_idx] = len(value)
                    else:
                        self.column_widths[col_idx] = len(str(value))
                col_idx += 1

            # Color the line according to value of column INTERPRETATION_COLUMN
            if self.color_interpretation:
                trigger_col = start_col_idx + self.get_index(INTERPRETATION_COLUMN)
                for key, _, _ in INTERPRETATION_OPTIONS:
                    # Range is everything, except Consequence, to avoid overwriting its color. Split to two conditions
                    for fmt_range in [xlsxwriter.utility.xl_range(row_idx, start_col_idx, row_idx, start_col_idx + self.get_index('Consequence')-1),
                                      xlsxwriter.utility.xl_range(row_idx, self.get_index('Consequence')+1, row_idx, start_col_idx + len(self.rows[0]))]:
                        # Set criteria to INTERPRETATION_COLUMN column, on current row
                        criteria_row_col = xlsxwriter.utility.xl_rowcol_to_cell(row_idx, trigger_col, True, True)
                        ws.conditional_format(
                            fmt_range,
                            {
                                'type': 'formula',
                                'criteria': '{}={}'.format(criteria_row_col, str(key)) if
                                isinstance(key, int) else
                                '=ISNUMBER(SEARCH("{}",{}))'.format(key, criteria_row_col),
                                'format': self.formats[key]
                            }
                        )

            row_idx += 1

        return row_idx, start_col_idx


class CnvRowWriter(VariantRowWriter):
    def __init__(self, rows, color_interpretation=False):
        super(CnvRowWriter, self).__init__(rows)


class VariantHelpWorksheet(Worksheet):

    def __init__(self, title):
        super(VariantHelpWorksheet, self).__init__(title)

    def write(self, ws):

        row = 0
        ws.write(row, 1, 'Classifications:', self.wb.add_format({'bold': True}))
        row += 1
        for interpretation, title, color in INTERPRETATION_OPTIONS:
            ws.write(row, 1, '[{}] {}'.format(interpretation, title), self.wb.add_format({'bg_color': color}))
            row += 1

        row += 2
        ws.write(row, 1, 'VEP Consequence (shown in order of severity (more severe to less severe) as estimated by Ensembl):', self.wb.add_format({'bold': True}))
        row += 1
        for data in VEP_CONSEQUENCE.items():
            ws.write(row, 0, '', self.wb.add_format({'bg_color': data[1]['color']}))
            ws.write(row, 1, data[0])
            ws.write(row, 2, data[1]['description'])
            row += 1
        ws.set_column(0, 0, 1)
        ws.set_column(1, 1, 35)
        ws.set_column(2, 2, 250)

        row += 2
        ws.write(row, 1, 'Pseudogenes ', self.wb.add_format({'bold': True}))
        row += 1
        ws.write(row, 1, "Mandelker et al., (2016). Navigating highly homologous genes in a molecular diagnostic setting: a resource for clinical next-generation sequencing. Genetics in Medicine, 18(12), 1282-1289. https://doi.org/10.1038/gim.2016.58")
        row += 1
        for data in PSEUDOGENES.items():
            if 'color' in data[1]:
                ws.write(row, 0, '', self.wb.add_format({'bg_color': data[1]['color']}))
            ws.write(row, 1, data[0])
            ws.write(row, 2, data[1]['description'])
            row += 1


class VersionWorksheet(Worksheet):
    """
    Reads in version information from vcpipe and print it in a worksheet.
    """
    def __init__(self, title):
        super(VersionWorksheet, self).__init__(title)

    def _load_versions(self):

        from util import LocalVersion
        return LocalVersion().getVersion()

    def write(self, ws):

        try:
            version = self._load_versions()
        except Exception:
            # Don't break whole script if we fail to get version info...
            log.error("Failed to load version information. Version information will not be written.")
            return

        info_rows = [
            # title, info key
            ('Production', 'production'),
            ('Revision', 'revision'),
            ('Tags', 'tag'),
        ]

        row = 0
        ws.write(row, 0, 'Version information:', self.wb.add_format({'bold': True}))
        row += 2

        for project_name, info in version.items():

            ws.write(row, 0, project_name, self.wb.add_format({'bold': True}))

            ws.write(row, 0, project_name, self.wb.add_format({'bold': True}))
            row += 1
            for info_row in info_rows:
                ws.write(row, 0, info_row[0])
                ws.write(row, 1, str(info[info_row[1]]))
                row += 1

            row += 1
            ws.set_column(0, 0, 20)
            ws.set_column(1, 1, 50)


class CoverageTemplateWorksheet(Worksheet):
    """
    Prints a worksheet with a template for pasting coverage data.
    """
    def __init__(self, title):
        super(CoverageTemplateWorksheet, self).__init__(title)

    def write(self, ws):

        row = 0
        ws.write(row, 0, COVERAGE_TEMPLATE_HEADER.decode('utf-8'), self.wb.add_format({'bold': True}))
        row += 2

        for idx, header in enumerate(COVERAGE_TEMPLATE):
            ws.write(row, idx, header[0].decode('utf-8'), self.wb.add_format({'bold': True}))
            if idx < 6:
                ws.write(row + 1, idx, 'lim inn fra PDF-fil', self.wb.add_format({'italic': True}))
            ws.set_column(idx, idx, header[1])


class ChecklistWriter(RowWriter):

    def __init__(self, offset=5, comment_offset=11, table=CHECKLIST, comment=CHECKLIST_COMMENT):
        super(ChecklistWriter, self).__init__(None)
        self.offset = offset  # Cell count offset before writing table
	self.table = table
	self.comment = comment
	self.comment_offset = comment_offset

    def write(self, worksheet, start_row_idx=0, start_col_idx=0):
        """
        Writes a checklist for the lab at the bottom of the filtered tab.
        """

        row_idx = start_row_idx
        for row in self.table:
            for col_idx, cell in enumerate(row):
                text = cell[0].decode('utf-8')
                col_idx = col_idx + start_col_idx + self.offset
                default_fmt = {'top': 1, 'bottom': 1, 'left': 1, 'right': 1}
                if cell[1]:
                    default_fmt.update(cell[1])
                fmt = self.add_format(default_fmt)
                worksheet.write(row_idx, col_idx, text, fmt)
                # Update max width
                self.column_widths[col_idx] = len(text)
            row_idx += 1

        row_idx2 = start_row_idx
        for comment in self.comment:
            comment = comment.decode('utf-8')
            worksheet.write(row_idx2, start_col_idx + self.comment_offset, comment)
            row_idx2 += 1

        return row_idx, start_col_idx
