import numbers
import vcfiterator
from vcfiterator.processors import VEPInfoProcessor
from util.csqallele import CSQAllele
from report.util.genepanelreader import GenepanelReader
import json
import base64


class SangerVerification(object):

    def check_criteria(self, line, allele, sample_key=None):
        criteria_check = dict()

        # check it is a SNP
        ref = line['REF']
        alt = line['ALT']
        if len(alt) == 1:
            if ref != '.' and len(ref) == 1 and alt[0] != '.' and len(alt[0]) == 1:
                criteria_check['SNP'] = True
            else:
                criteria_check['SNP'] = False
        elif len(alt) > 1:
            if alt[0] != '.' and len(alt[0]) == 1 and alt[1] != '.' and len(alt[1]) == 1:
                criteria_check['SNP'] = True
            else:
                criteria_check['SNP'] = False
        else:
            print("DEBUG: No alternative allele")

        # FILTER = PASS
        if line['FILTER'] == 'PASS':
            criteria_check['FILTER'] = True
        else:
            criteria_check['FILTER'] = False

        # QUAL > 300
        if isinstance(line['QUAL'], numbers.Real) and line['QUAL'] > 300:
            criteria_check['QUAL'] = True
        else:
            criteria_check['QUAL'] = False

        if sample_key is None:
            sample_data = list(line['SAMPLES'].values())[0]
        else:
            sample_data = line['SAMPLES'][sample_key]

        # DP > 20 x
        try:
            if sample_data['DP'] > 20:
                criteria_check['DP'] = True
            else:
                criteria_check['DP'] = False
        except ValueError:
            criteria_check['DP'] = False
        except KeyError:
            criteria_check['DP'] = False

        # alternative allele frequency: HET: between 0.3-0.6, HOM: > 0.9. (Depth of ALT)/(Total Depth)
        try:
            gt = [int(g) for g in sample_data['GT'].split('/')]

            alleleDepth = sample_data['AD']

            def hom_criteria(ratio):
                return ratio > 0.9

            def het_criteria(ratio):
                return ratio > 0.3 and ratio < 0.6

            criteria_func = hom_criteria if gt[0] == gt[1] else het_criteria

            if gt[0] == gt[1] == 0:
                # 0/0 occurs in de novo variants for trios.
                # the AD criterion is not applicable
                pass
            elif not sum(alleleDepth):
                # Fail the criteria when no allele depth
                # (it does happen sometimes for some reason)
                criteria_check['AD'] = False
            else:
                # Procedure specifies to only check the AD (allele depth)
                # for the requested allele. Ref is not included.
                # Get total index (REF + ALT), since that's what GT is
                # specified from
                gt_idx = ([line['REF']] + line['ALT']).index(allele)
                ratio = float(alleleDepth[gt_idx]) / sum(alleleDepth)
                criteria_check['AD'] = criteria_func(ratio)

        # Catches GT: './.'
        except ValueError:
            criteria_check['AD'] = False
        except KeyError:
            criteria_check['AD'] = False

        return criteria_check

    def process(self, line, allele):
        criteria_check = self.check_criteria(line, allele)
        return {'sanger_verify': 'No' if all(criteria_check.values()) else 'Yes'}


class Frequencies(object):

    EXAC_REGIONS = [
        'Adj',
        'AFR',
        'AMR',
        'EAS',
        'FIN',
        'NFE',
        'OTH',
        'SAS'
    ]
    GNOMAD_REGIONS = [
        'AFR',
        'AMR',
        'ASJ',
        'EAS',
        'FIN',
        'NFE',
        'OTH',
        'SAS'
    ]

    def __init__(self, transcripts):
        self.transcripts = transcripts

    def _get_exac_freqs_totals(self, line, allele_idx):

        counts = dict()
        totals = dict()
        for field, value in line['INFO']['ALL'].items():
            if field.startswith('EXAC__AC_'):
                region = field.split('EXAC__AC_', 1)[1]
                counts[region] = value[allele_idx]
            elif field.startswith('EXAC__AN_'):
                region = field.split('EXAC__AN_', 1)[1]
                totals[region] = value

        freqs = dict()
        for region in Frequencies.EXAC_REGIONS:
            if all(region in f for f in [counts, totals]) and totals[region] > 0:
                freqs[region] = float(counts[region]) / totals[region]

        return freqs, totals

    def _get_gnomad_freqs_totals(self, kind, line, allele_idx):

        totals = dict()
        freqs = dict()

        assert kind.lower() in ['exomes', 'genomes'], "kind must be 'exomes' or 'genomes'"

        freq_fn = 'GNOMAD_' + kind.upper() + '__AF_'
        tot_fn = 'GNOMAD_' + kind.upper() + '__AN_'

        for field, value in line['INFO']['ALL'].items():
            if field.startswith(freq_fn):
                region = field.split(freq_fn, 1)[1]
                freqs[region] = value[allele_idx]
            elif field.startswith(tot_fn):
                region = field.split(tot_fn, 1)[1]
                totals[region] = value

        return freqs, totals

    @staticmethod
    def _get_maf(data, freq, allele):
        # VEP doesn't include the REF for insertion, split it out
        if len(allele) > 1:
            allele = allele[1:]
        if freq not in data:
            return 'N/A'
        freqs = list()
        for freq_allele, freq_number in data[freq].items():
            if freq_allele == allele:
                freqs.append(Frequencies._format_freq(freq_number))
            else:
                freqs.append(str(freq_allele) + ':' + Frequencies._format_freq(freq_number))
        if freqs:
            return ','.join(freqs)
        return 'N/A'

    @staticmethod
    def _format_freq(value):
        if isinstance(value, float):
            return '{}'.format(value)
        return str(value)

    @staticmethod
    def _format_indb_indication(line, pop_suffix='OUSWES', max_samples=10):
        '''Return the indications if less than 10 samples have that variant.'''
        try:
            hom = int(line['INFO']['ALL'].get('_'.join(['inDB__Hom', pop_suffix]), [None])[0])
            het = int(line['INFO']['ALL'].get('_'.join(['inDB__Het', pop_suffix]), [None])[0])
        except TypeError:
            return line['INFO']['ALL'].get('inDB__indications_' + pop_suffix, 'N/A')
        if hom + het < max_samples:
            return line['INFO']['ALL'].get('inDB__indications_' + pop_suffix, 'N/A')
        else:
            return '({} indications)'.format(hom + het)

    def process(self, vcf_line, allele):

        csq_allele = CSQAllele(vcf_line['INFO'][allele]['CSQ'], self.transcripts)

        data = {
            'ExAC': Frequencies._format_freq(vcf_line['INFO']['ALL'].get('EXAC__AF', ['N/A'])[0]),
            'gnomAD_exomes_TOT': Frequencies._format_freq(vcf_line['INFO']['ALL'].get('GNOMAD_EXOMES__AF', ['N/A'])[0]),
            'gnomAD_genomes_TOT': Frequencies._format_freq(vcf_line['INFO']['ALL'].get('GNOMAD_GENOMES__AF', ['N/A'])[0]),
            'gnomAD_exomes_TOT_AN': vcf_line['INFO']['ALL'].get('GNOMAD_EXOMES__AN', 'N/A'),
            'gnomAD_genomes_TOT_AN': vcf_line['INFO']['ALL'].get('GNOMAD_GENOMES__AN', 'N/A'),
            '1000g': Frequencies._get_maf(csq_allele.get_filtered_items()[0], 'GMAF', allele),
            'ESP6500_EA': Frequencies._get_maf(csq_allele.get_filtered_items()[0], 'EA_MAF', allele),  # CSQ EA_MAF - ESP6500 allele frequency from EA population
            'ESP6500_AA': Frequencies._get_maf(csq_allele.get_filtered_items()[0], 'AA_MAF', allele),  # CSQ AA_MAF - ESP6500 allele frequency from AA population
            'ExAC_URL': 'ExAC | http://exac.broadinstitute.org/variant/{}-{}-{}-{}'.format(
                vcf_line['CHROM'],
                vcf_line['POS'],
                vcf_line['REF'],
                allele
            ),
            'GNOMAD_URL': 'GNOMAD | http://gnomad.broadinstitute.org/variant/{}-{}-{}-{}'.format(
                vcf_line['CHROM'],
                vcf_line['POS'],
                vcf_line['REF'],
                allele
            ),
            'OMIM_URL': 'OMIM | https://www.omim.org/search/?index=entry&search=' + '+OR+'.join(csq_allele.get_omim_numbers()) if csq_allele.get_omim_numbers() else 'N/A'
        }
        if 'inDB__AC_OUSWES' in vcf_line['INFO']['ALL']:
            data.update({
                'AC_OUSWES': vcf_line['INFO']['ALL'].get('inDB__AC_OUSWES', ['N/A'])[0],
                'Hom_OUSWES': vcf_line['INFO']['ALL'].get('inDB__Hom_OUSWES', ['N/A'])[0],
                'Het_OUSWES': vcf_line['INFO']['ALL'].get('inDB__Het_OUSWES', ['N/A'])[0],
                'AN_OUSWES': vcf_line['INFO']['ALL'].get('inDB__AN_OUSWES', 'N/A'),
                'AF_OUSWES': vcf_line['INFO']['ALL'].get('inDB__AF_OUSWES', ['N/A'])[0],
                'indications_OUSWES': Frequencies._format_indb_indication(vcf_line, 'OUSWES'),
                'filter_OUSWES': vcf_line['INFO']['ALL'].get('inDB__filter_OUSWES', 'N/A'),
            })
        if 'inDB__AC_OUST1' in vcf_line['INFO']['ALL']:
            data.update({
                'AC_OUST1': vcf_line['INFO']['ALL'].get('inDB__AC_OUST1', ['N/A'])[0],
                'Hom_OUST1': vcf_line['INFO']['ALL'].get('inDB__Hom_OUST1', ['N/A'])[0],
                'Het_OUST1': vcf_line['INFO']['ALL'].get('inDB__Het_OUST1', ['N/A'])[0],
                'AN_OUST1': vcf_line['INFO']['ALL'].get('inDB__AN_OUST1', 'N/A'),
                'AF_OUST1': vcf_line['INFO']['ALL'].get('inDB__AF_OUST1', ['N/A'])[0],
                'indications_OUST1': Frequencies._format_indb_indication(vcf_line, 'OUST1'),
                'filter_OUST1': vcf_line['INFO']['ALL'].get('inDB__filter_OUST1', 'N/A'),
            })

        allele_idx = vcf_line['ALT'].index(allele)
        exac_freqs, exac_totals = self._get_exac_freqs_totals(vcf_line, allele_idx)
        for region in Frequencies.EXAC_REGIONS:
            region_name = 'ExAC_' + region if not region == 'Adj' else 'ExAC_TOT'
            data[region_name] = self._format_freq(exac_freqs.get(region, 'N/A'))
            data[region_name + '_AN'] = exac_totals.get(region, 'N/A')

        gnomad_exomes_freqs, gnomad_exomes_totals = self._get_gnomad_freqs_totals('exomes', vcf_line, allele_idx)
        gnomad_genomes_freqs, gnomad_genomes_totals = self._get_gnomad_freqs_totals('genomes', vcf_line, allele_idx)
        for region in Frequencies.GNOMAD_REGIONS:
            e_region_name = 'gnomAD_exomes_' + region
            g_region_name = 'gnomAD_genomes_' + region
            data[e_region_name] = self._format_freq(gnomad_exomes_freqs.get(region, 'N/A'))
            data[g_region_name] = self._format_freq(gnomad_genomes_freqs.get(region, 'N/A'))
            data[e_region_name + '_AN'] = gnomad_exomes_totals.get(region, 'N/A')
            data[g_region_name + '_AN'] = gnomad_genomes_totals.get(region, 'N/A')
        return data


class CSQ(object):

    def __init__(self, transcripts):
        self.transcripts = transcripts

    def process(self, vcf_line, allele):

        csq_allele = CSQAllele(vcf_line['INFO'][allele]['CSQ'], self.transcripts)
        return {
            'Gene': csq_allele.get_gene(),
            'Inheritance': csq_allele.get_inheritance(),
            'HGVSc': csq_allele.get_field(lambda x: x.get('HGVSc', 'N/A')),
            'HGVSp': csq_allele.get_field(lambda x: x.get('HGVSp', 'N/A')),
            'Consequence': csq_allele.format_worst_consequence(),
            'dbSNP': ','.join([a for a in csq_allele.get_filtered_items()[0].get('Existing_variation', []) if a.startswith('rs') or a == 'N/A']),
            'extra_phenotypes': csq_allele.get_extra_phenotypes(),
            'Phenotype': csq_allele.get_phenotype(),
        }


class VcfFields(object):

    def process(self, vcf_line, allele):
        sample = list(vcf_line['SAMPLES'].values())[0]
        amg_gene_panel = vcf_line['INFO']['ALL'].get('amg_gene_panel', 'N/A')
        if type(amg_gene_panel) is list:
            amg_gene_panel = ','.join(amg_gene_panel)
        return {
            'CHR': vcf_line['CHROM'],  # VCF CHROM
            'POS': vcf_line['POS'],  # VCF POS
            'REF': vcf_line['REF'],  # VCF REF
            'ALT': allele,  # VCF ALT
            'Genotype': sample['GT'],
            'VCF_FILTER': vcf_line['FILTER'],
            'VCF_QUAL': vcf_line['QUAL'],
            'VCF_AD_AlleleDepth': sample.get('AD', 'N/A'),
            'VCF_DP_depth': sample.get('DP', '0'),
            'AlamutColumn': ''.join(["Chr", vcf_line['CHROM'], "(GRCh37):g.", str(vcf_line['POS']), vcf_line['REF'], ">", allele]),  # Alamut column:  only to SNP sites
            'HGMD_URL': 'HGMD | https://portal.biobase-international.com/hgmd/pro/mut.php?accession={}'.format(
                vcf_line['INFO']['ALL']['HGMD__acc_num'],
            ) if 'HGMD__acc_num' in vcf_line['INFO']['ALL'] else 'N/A',
            'repeatMasker': vcf_line['INFO']['ALL'].get('repeatMasker', 'N/A'),
            'amg_gene_panel': amg_gene_panel,
            'CLINVAR_URL': self.get_clinvar_url(vcf_line),
            'pseudogenes': self.get_pseudogenes(vcf_line)
        }

    def get_clinvar_url(self, vcf_line):
        deserializer = lambda x: json.loads(base64.b16decode(x))

        if 'CLINVARJSON' not in vcf_line['INFO']['ALL']:
            return 'N/A'
        clinvarjson = vcf_line['INFO']['ALL']['CLINVARJSON']
        clinvar = deserializer(clinvarjson)
        url = 'CLINVAR | https://www.ncbi.nlm.nih.gov/clinvar/variation/{}/'.format(
            clinvar['variant_id']
        ) if 'variant_id' in clinvar else 'N/A'
        return url

    def get_pseudogenes(self, vcf_line):
        keys_exon = [
            "pseudo_exon_ngs_dead_zone",
            "pseudo_exon_ngs_problem_high",
            "pseudo_exon_ngs_problem_low"
        ]
        keys_gene = [
            "pseudo_gene_ngs_dead_zone",
            "pseudo_gene_ngs_problem_high",
            "pseudo_gene_ngs_problem_low",
        ]
        keys = keys_exon + keys_gene
        annotations = [key for key in keys if key in vcf_line['INFO']['ALL']]

        # keep only the most severe annotation for each of exon and gene categories:
        # dead_zone > problem_high > problem_low
        for it in range(2):
            if keys_exon[it] in annotations:
                annotations = [key for key in annotations if key not in keys_exon[it+1:]]
            if keys_gene[it] in annotations:
                annotations = [key for key in annotations if key not in keys_gene[it+1:]]

        return "|".join(annotations)


class TrioFields(object):

    def __init__(self, pedigree):
        """
            pedigree = {
                "father": "NA12341",
                "mother": "NA14232",
                "proband": "NA12434"
            }
        """
        self.pedigree = pedigree

    def process(self, vcf_line, allele):
        data = dict()
        info = vcf_line['INFO']['ALL']
        info_fields = {
            'TRIO_TYPE': lambda x: ','.join(x.split('|', 1)),
            'TRIO_DENOVO_P': lambda x: x,
            'TRIO_ALT_CHILD_PCT': lambda x: x,
            'TRIO_ALT_FATHER_PCT': lambda x: x,
            'TRIO_ALT_MOTHER_PCT': lambda x: x,
        }

        for key, func in info_fields.items():
            if key in info:
                data[key] = func(info[key])
            else:
                data[key] = None

        # Sample specific data
        sample_fields = ['GT', 'DP', 'AD']
        for field in sample_fields:
            data['{}_FATHER'.format(field)] = vcf_line['SAMPLES'][self.pedigree['father']][field]
            data['{}_MOTHER'.format(field)] = vcf_line['SAMPLES'][self.pedigree['mother']][field]
            data['{}_PROBAND'.format(field)] = vcf_line['SAMPLES'][self.pedigree['proband']][field]

        # Get sangerverification column
        sv = SangerVerification()
        sv_data = dict()
        for p in ['proband', 'father', 'mother']:
            sv_data[p] = sv.check_criteria(vcf_line, allele, sample_key=self.pedigree[p])

        data['TRIO_Sanger_verify'] = ','.join([k for k, v in sv_data.items() if not all(v.values())])

        return data


class VcfReader(object):

    def __init__(self, vcf_path, transcripts_path, phenotypes_path=None):
        self.vcf_path = vcf_path
        self.transcripts_path = transcripts_path
        self.vcfiterator = vcfiterator.VcfIterator(vcf_path)
        self.vcfiterator.addInfoProcessor(VEPInfoProcessor)
        self.phenotypes_path = phenotypes_path
        self.transcripts = self._load_transcripts()

    def _load_transcripts(self):
        return GenepanelReader().get_transcripts(self.transcripts_path, self.phenotypes_path)

    def get_data(self, include_trio=False, trio_pedigree=None):

        processors = [
            Frequencies(self.transcripts),
            CSQ(self.transcripts),
            VcfFields()
        ]

        if include_trio:
            if not trio_pedigree:
                raise RuntimeError("Missing pedigree input while include_trio is True")
            processors.append(
                TrioFields(trio_pedigree)
            )

        # For now normal sanger verification check only works when we have one sample
        if len(self.vcfiterator.getSamples()) == 1:
            processors.append(SangerVerification())

        for vcf_line in self.vcfiterator.iter():
            for allele in vcf_line['ALT']:
                data = dict()
                for p in processors:
                    data.update(p.process(vcf_line, allele))

                yield data
