import re

HGVS_INTRON_RE = re.compile(r'(?P<transcript>NM_[0-9,\.]+):c\.[0-9]+?([\-\+])([0-9]+)')
HGVS_UPSTREAM_ATG_RE = re.compile(r'(?P<transcript>NM_[0-9,\.]+):c\.-[0-9]+?([\-\+])([0-9]+)')
HGVS_5PRIME_INTRON_RE = re.compile(r'(?P<transcript>NM_[0-9,\.]+):c\.-([0-9]+)')
HGVS_3PRIME_INTRON_RE = re.compile(r'(?P<transcript>NM_[0-9,\.]+):c\.\*([0-9]+)')


def is_intronic(entry):
    """
    Returns True if variant is intronic and outside +6/-20 bp.
    """
    match = re.match(HGVS_INTRON_RE, entry['HGVSc'])
    criteria = {
        '+': 6,
        '-': 20
    }
    if match:
        _, sign, num = match.groups()
        num = int(num)
        return sign in criteria and num > criteria[sign]
    return False


def is_upstream_ATG(entry):
    """
    Returns True if variant is upstream of start codon and outside +6/-20 bp.
    """
    if not any([c in entry.get('Consequence', '') for c in ['5_prime_UTR_variant', 'intron_variant']]):
        return False
    match = re.match(HGVS_UPSTREAM_ATG_RE, entry['HGVSc'])
    criteria = {
        '+': 6,
        '-': 20
    }
    if match:
        _, sign, num = match.groups()
        num = int(num)
        return sign in criteria and num > criteria[sign]
    return False

def is_5_prime_intronic(entry):
    """
    Returns True if variant is 5' and more than 20 bp upstream.
    """
    if '5_prime_UTR_variant' not in entry.get('Consequence', ''):
        return False
    match = re.match(HGVS_5PRIME_INTRON_RE, entry['HGVSc'])
    if match:
        _, num = match.groups()
        num = int(num)
        return num > 20
    return False

def is_3_prime_intronic(entry):
    """
    Returns True if variant is 3' and more than 20 bp downstream.
    """
    if '3_prime_UTR_variant' not in entry.get('Consequence', ''):
        return False
    match = re.match(HGVS_3PRIME_INTRON_RE, entry['HGVSc'])
    if match:
        _, num = match.groups()
        num = int(num)
        return num > 20
    return False

def has_unclassified_vep_consequence(entry):
    """
    Returns True if each and everyone of the VEP consequences should be unclassified
    """
    unclassified_consequences = [
        '5_prime_UTR_variant',
        '3_prime_UTR_variant',
        'intron_variant',
        'upstream_gene_variant'
    ]
    vep_consequences = entry.get('Consequence', '').split('|')
    return all([any([u_csq in csq for u_csq in unclassified_consequences]) for csq in vep_consequences])

def check_class_unclassified(entry):
    """
    Returns True if a variant should be marked `u` (unclassified):
    - variant is intronic OR upsteam ATG OR 5' OR 3'
    - variant has one VEP consequence
    """
    rules = [
        is_intronic(entry),
        is_upstream_ATG(entry),
        is_5_prime_intronic(entry),
        is_3_prime_intronic(entry)
    ]
    return all([any(rules), has_unclassified_vep_consequence(entry)])