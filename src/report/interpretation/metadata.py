# -*- coding: utf-8 -*-

from collections import OrderedDict

VEP_CONSEQUENCE = OrderedDict(
    [('transcript_ablation', {
        'color': 'ff0000',
        'description': 'A feature ablation whereby the deleted region includes a transcript feature'
    }),
    ('splice_acceptor_variant', {
        'color': 'FF581A',
        'description': "A splice variant that changes the 2 base region at the 3' end of an intron"
    }),
    ('splice_donor_variant', {
        'color': 'FF581A',
        'description': "A splice variant that changes the 2 base region at the 5' end of an intron"
    }),
    ('stop_gained', {
        'color': 'ff0000',
        'description': 'A sequence variant whereby at least one base of a codon is changed, resulting in a premature stop codon, leading to a shortened transcript'
    }),
    ('frameshift_variant', {
        'color': '9400D3',
        'description': 'A sequence variant which causes a disruption of the translational reading frame, because the number of nucleotides inserted or deleted is not a multiple of three'
    }),
    ('stop_lost', {
        'color': 'ff0000',
        'description': 'A sequence variant where at least one base of the terminator codon (stop) is changed, resulting in an elongated transcript'
    }),
    ('initiator_codon_variant', {
        'color': 'ffd700',
        'description': 'A codon variant that changes at least one base of the first codon of a transcript'
    }),
    ('transcript_amplification', {
        'color': 'ff69b4',
        'description': 'A feature amplification of a region containing a transcript'
    }),
    ('inframe_insertion', {
        'color': 'ff69b4',
        'description': 'An inframe non synonymous variant that inserts bases into in the coding sequence'
    }),
    ('inframe_deletion', {
        'color': 'ff69b4',
        'description': 'An inframe non synonymous variant that deletes bases from the coding sequence'
    }),
    ('missense_variant', {
        'color': 'ffd700',
        'description': 'A sequence variant, that changes one or more bases, resulting in a different amino acid sequence but where the length is preserved'
    }),
    ('protein_altering_variant', {
        'color': 'ff0080',
        'description': 'A sequence_variant which is predicted to change the protein encoded in the coding sequence'
    }),
    ('splice_region_variant', {
        'color': 'ff7f50',
        'description': 'A sequence variant in which a change has occurred within the region of the splice site, either within 1-3 bases of the exon or 3-8 bases of the intron'
    }),
    ('incomplete_terminal_codon_variant', {
        'color': 'ff00ff',
        'description': 'A sequence variant where at least one base of the final codon of an incompletely annotated transcript is changed'
    }),
    ('stop_retained_variant', {
        'color': '76ee00',
        'description': 'A sequence variant where at least one base in the terminator codon is changed, but the terminator remains'
    }),
    ('synonymous_variant', {
        'color': '76ee00',
        'description': 'A sequence variant where there is no resulting change to the encoded amino acid'
    }),
    ('coding_sequence_variant', {
        'color': '458b00',
        'description': 'A sequence variant that changes the coding sequence'
    }),
    ('mature_miRNA_variant', {
        'color': '458b00',
        'description': 'A transcript variant located with the sequence of the mature miRNA'
    }),
    ('5_prime_UTR_variant', {
        'color': '7ac5cd',
        'description': "A UTR variant of the 5' UTR"
    }),
    ('3_prime_UTR_variant', {
        'color': '7ac5cd',
        'description': "A UTR variant of the 3' UTR"
    }),
    ('non_coding_transcript_exon_variant', {
        'color': '32cd32',
        'description': 'A sequence variant that changes non-coding exon sequence in a non-coding transcript'
    }),
    ('intron_variant', {
        'color': '02599c',
        'description': 'A transcript variant occurring within an intron'
    }),
    ('NMD_transcript_variant', {
        'color': 'ff4500',
        'description': 'A variant in a transcript that is the target of NMD'
    }),
    ('non_coding_transcript_variant', {
        'color': '32cd32',
        'description': 'A transcript variant of a non coding RNA gene'
    }),
    ('upstream_gene_variant', {
        'color': 'a2b5cd',
        'description': "A sequence variant located 5' of a gene"
    }),
    ('downstream_gene_variant', {
        'color': 'a2b5cd',
        'description': "A sequence variant located 3' of a gene"
    }),
    ('TFBS_ablation', {
        'color': 'a52a2a',
        'description': 'A feature ablation whereby the deleted region includes a transcription factor binding site'
    }),
    ('TFBS_amplification', {
        'color': 'a52a2a',
        'description': 'A feature amplification of a region containing a transcription factor binding site'
    }),
    ('TF_binding_site_variant', {
        'color': 'a52a2a',
        'description': 'A sequence variant located within a transcription factor binding site'
    }),
    ('regulatory_region_ablation', {
        'color': 'a52a2a',
        'description': 'A feature ablation whereby the deleted region includes a regulatory region'
    }),
    ('regulatory_region_amplification', {
        'color': 'a52a2a',
        'description': 'A feature amplification of a region containing a regulatory region'
    }),
    ('regulatory_region_variant', {
        'color': 'a52a2a',
        'description': 'A sequence variant located within a regulatory region'
    }),
    ('feature_elongation', {
        'color': '7f7f7f',
        'description': 'A sequence variant that causes the extension of a genomic feature, with regard to the reference sequence'
    }),
    ('feature_truncation', {
        'color': '7f7f7f',
        'description': 'A sequence variant that causes the reduction of a genomic feature, with regard to the reference sequence'
    }),
    ('intergenic_variant', {
        'color': '636363',
        'description': 'A sequence variant located in the intergenic region, between genes'
    }),
    ('not_available', {
        'color': '636363',
        'description': 'VEP could not annotate this variant'
    })]
)

HEADER_COLOR = "#bbbbbb"
OPTIONAL_COLOR = "#d7d7d7"
DISABLED_COLOR = "#888888"
LIGHTBLUE_COLOR = "#b3daff"

HEADER_STYLE_TOP = {'bg_color': HEADER_COLOR, 'bold': True, 'top': 2}
HEADER_STYLE_BOTTOM = {'bg_color': HEADER_COLOR, 'bold': True, 'bottom': 2}
HEADER_STYLE = {'bg_color': LIGHTBLUE_COLOR, 'bold': True, 'top': 2, 'bottom': 2}
OPTIONAL_STYLE = {'bg_color': OPTIONAL_COLOR}
TITLE_STYLE = {'bold': True, 'left': 0, 'right': 0, 'top': 0, 'bottom': 0, 'font_size': 14}

CHECKLIST = (
    # ('text', {style})
    (('', HEADER_STYLE_TOP), ('Resultatvurdering', HEADER_STYLE_TOP), ('Kontroll av resultatvurdering', HEADER_STYLE_TOP), ('', HEADER_STYLE_TOP)),
    (('', HEADER_STYLE_BOTTOM), ('signaturkode_ddmmyyyyy', {'bold': True, 'bottom': 2}), ('signaturkode_ddmmyyyy', {'bold': True, 'bottom': 2}), ('Kommentar', HEADER_STYLE_BOTTOM)),
    (('Normalvariant til rapport (eks. POLG i EEogPU)', {'bg_color': HEADER_COLOR}), ('', None), ('', None), ('', OPTIONAL_STYLE)),
    (('Sett etter i mutasjonstabeller og variantvurderingsskjema', {'bg_color': HEADER_COLOR}), ('', None), ('', {'bg_color': DISABLED_COLOR}), ('', OPTIONAL_STYLE)),
    (('CNV til rapport?', {'bg_color': HEADER_COLOR}), ('', None), ('', OPTIONAL_STYLE), ('', OPTIONAL_STYLE)),
    (('Konklusjon: Resultater til rapport', {'bg_color': HEADER_COLOR, 'bold': True, 'top': 2, 'bottom': 2}), ('', {'top': 2, 'bottom': 2}), ('', {'top': 2, 'bottom': 2}), ('', {'bg_color': OPTIONAL_COLOR, 'top': 2, 'bottom': 2})),
    (('Sangerverifisering (registrer i skjema hvis ja)', {'bg_color': HEADER_COLOR}), ('', None), ('', OPTIONAL_STYLE), ('', OPTIONAL_STYLE)),
    (('Vedlegg til rapport i SWL med riktig prøvenummer', {'bg_color': HEADER_COLOR}), ('', None), ('', None), ('', OPTIONAL_STYLE)),
    (('Evt. andre analyser', {'bg_color': HEADER_COLOR}), ('', None), ('', OPTIONAL_STYLE), ('', OPTIONAL_STYLE)),
    (('Resultat og svar i SWL', {'bg_color': HEADER_COLOR}), ('', None), ('', OPTIONAL_STYLE), ('', OPTIONAL_STYLE)),
    (('Signatur og data i Clarity LIMS', {'bg_color': HEADER_COLOR}), ('', None), ('', OPTIONAL_STYLE), ('', OPTIONAL_STYLE)),
    (('Teknisk frigitt', {'bg_color': HEADER_COLOR}), ('', {'bg_color': DISABLED_COLOR}), ('', OPTIONAL_STYLE), ('', OPTIONAL_STYLE)),
    (('Levert til enhet', {'bg_color': HEADER_COLOR, 'bottom': 2}), ('', {'bg_color': OPTIONAL_COLOR, 'bottom': 2}), ('', {'bg_color': DISABLED_COLOR, 'bottom': 2}), ('', {'bg_color': OPTIONAL_COLOR, 'bottom': 2}))
)

CHECKLIST_COMMENT = [
    'Sjekklisten benyttes som følger:',
    'Hvite ruter er obligatoriske og skal alltid fylles inn.',
    'Lysegrå ruter er valgfrie å fylle inn og benyttes etter behov.',
    'Mørkegrå ruter skal ikke fylles inn.'
]

PHENOTYPE_CHECKLIST = (
    # ('text', {style})
    (('Fenotypesjekk', TITLE_STYLE), ('', TITLE_STYLE)),
    (('gen', HEADER_STYLE), ('beskrivelse av variant', HEADER_STYLE), ('lablege sign', HEADER_STYLE), ('skrives ut (ja/nei)', HEADER_STYLE), ('sjekket DIPS', HEADER_STYLE), ('begrunnelse', HEADER_STYLE)),
    (('', {'bg_color': LIGHTBLUE_COLOR}), ('', {'bg_color': LIGHTBLUE_COLOR}), ('', None), ('', None), ('', None), ('', None)),
    (('', {'bg_color': LIGHTBLUE_COLOR}), ('', {'bg_color': LIGHTBLUE_COLOR}), ('', None), ('', None), ('', None), ('', None)),
    (('', {'bg_color': LIGHTBLUE_COLOR}), ('', {'bg_color': LIGHTBLUE_COLOR}), ('', None), ('', None), ('', None), ('', None)),
    (('', {'bg_color': LIGHTBLUE_COLOR, 'bottom': 2}),  ('', {'bg_color': LIGHTBLUE_COLOR,'bottom': 2}), ('', {'bottom': 2}), ('', {'bottom': 2}), ('', {'bottom': 2}), ('', {'bottom': 2})),
    (('', TITLE_STYLE), ('', TITLE_STYLE )),
    (('fylles inn av ingeniør', {'bg_color': LIGHTBLUE_COLOR, 'left': 2, 'top': 2, 'bottom': 2, 'right': 0}), ('', {'bg_color': LIGHTBLUE_COLOR, 'left': 0, 'top': 2, 'bottom': 2, 'right': 2}))
)

COVERAGE_TEMPLATE_HEADER = 'Sekvensområder lest mindre enn 40 ganger ved HTS:'
COVERAGE_TEMPLATE = [
    ('Startposisjon (HGVSg)', 24),
    ('Stopposisjon (HGVSg)', 24),
    ('Gen', 8),
    ('Transkript', 20),
    ('Ekson', 15),
    ('x dekning', 15),
    ('Amplikon', 15),
    ('Om-plate', 15),
    ('Signaturkode', 15)
]

PSEUDOGENES = OrderedDict([
    ("pseudo_exon_ngs_dead_zone", {
        'color': '#ff6266',
        'description': "Table S1 (List1) NGS Dead Zone exon level"
    }),
    ("pseudo_exon_ngs_problem_high", {
        'color':  '#82de86',
        'description': "Table S2 (List2) NGS Problem List High Stringency exon level"
    }),
    ("pseudo_exon_ngs_problem_low", {
        'color': '#ffc14f',
        'description': "Table S3 (List3) NGS Problem List Low Stringency exon level"
    }),
    ("pseudo_gene_ngs_dead_zone", {
        'description': "Table S5 (List1) NGS Dead Zone gene level"
    }),
    ("pseudo_gene_ngs_problem_high", {
        'description': "Table S7 (List2) NGS Problem List High Stringency gene level"
    }),
    ("pseudo_gene_ngs_problem_low", {
        'description': "Table S9 (List3) NGS Problem List Low Stringency gene level"
    }),
])
