# -*- coding: utf-8 -*-

try:
    import xlsxwriter
except ImportError:
    print("You need to install the openpyxl module.")
    raise


COLUMN_PADDING_RATIO = 1.2  # Sets the ratio for the columns widths. >1 means larger than text.


class MaxDict(dict):
    """
    Keeps tracks of max value for a key. If one tries to set a value less than existing, it is ignored.
    """
    def __setitem__(self, key, val):
        if key not in self:
            super(MaxDict, self).__setitem__(key, val)
        else:
            if val > self[key]:
                super(MaxDict, self).__setitem__(key, val)


class RowWriter(object):

    def __init__(self, rows):
        self.rows = rows
        self.column_widths = MaxDict()  # Tracks column width {idx(int): width(int)}

    def setup(self, workbook):
        self.wb = workbook

    def add_format(self, fmt):
        return self.wb.add_format(fmt)

    def get_column_widths(self):
        return self.column_widths

    def _write_header(self, ws, start_row_idx=0, start_col_idx=0):
        header_fmt = self.add_format({'bold': True, 'bottom': 1})

        # Write header
        if not self.rows:
            return

        for idx, value in enumerate(self.rows[0].keys()):
            self.column_widths[idx] = len(str(value))
            ws.write(start_row_idx, start_col_idx + idx, value, header_fmt)

    def write(self, ws, start_row_idx, start_col_idx=1):
        # rows = [OrderedDict({'Header title 1': 'Value 1', ..}), OrderedDict(..)]
        row_idx, col_idx = start_row_idx, start_col_idx
        for row in self.rows:
            col_idx = start_col_idx
            for key, value in row.items():
                if not (hasattr(value, 'isdigit') and value.isdigit()):
                    value = str(value).decode('utf-8')
                    self.column_widths[col_idx] = len(value)
                else:
                    self.column_widths[col_idx] = len(str(value))
                ws.write(row_idx, col_idx, value)
                col_idx += 1
            row_idx += 1


class Worksheet(object):

    def __init__(self,
                 title,
                 active=False,  # Whether worksheet should be activated
                 freeze_panes=None,  # Column range to freeze (int, int)
                 grouping_col=None):

        self.title = title
        self.wb = None
        self.writer = None
        self.active = active
        self.freeze_panes = freeze_panes
        self.grouping_col = grouping_col

    def setup(self, workbook):
        self.wb = workbook

    def set_writer(self, writer):
        self.writer = writer
        writer.setup(self.wb)

    def adjust_columns(self, worksheet, writer):
        """
        Given a list of writer(s), adjust the column widths of the worksheet.
        It gets the column width by calling the get_column_widths method
        on the writer(s)
        """
        if not isinstance(writer, list):
            writer = [writer]

        # Gather all max widths and merge them
        final = MaxDict()
        for w in writer:
            col_widths = w.get_column_widths()
            for col_idx, width in col_widths.items():
                final[col_idx] = width

        # Adjust in excel file
        for col_idx, width in final.items():
            worksheet.set_column(col_idx, col_idx, int(width * COLUMN_PADDING_RATIO))

    def postprocess(self, worksheet):
        if self.freeze_panes:
            worksheet.freeze_panes(*self.freeze_panes)

        if self.grouping_col:
            for sel in self.grouping_col:
                s, e, l = sel
                while s <= e:
                    worksheet.set_column(s, s, 25, None, {'level': l, 'hidden': True})
                    s += 1

        if self.active:
            worksheet.activate()

    def write(self, worksheet):
        self.writer.setup(self.wb)
        self.writer.write(worksheet, 0, 0)

        # Adjust column widths
        self.adjust_columns(worksheet, self.writer)

        self.postprocess(worksheet)


class SectionedWorksheet(Worksheet):

    def __init__(self, title, **kwargs):
        super(SectionedWorksheet, self).__init__(title, **kwargs)
        self.sections = list()

    def add_section(self, title, writer):
        self.sections.append((title, writer))

    def write(self, worksheet):

        row_idx, col_idx = 0, 0
        for section_name, writer in self.sections:
            writer.setup(self.wb)

            # Add section header
            if section_name:
                worksheet.write(row_idx, col_idx, section_name, self.wb.add_format({'bold': True, 'size': 14}))
                worksheet.set_row(row_idx, 20)

            row_idx += 1
            new_coords = writer.write(worksheet, row_idx, col_idx)
            if not len(new_coords) == 2:
                raise RuntimeError("Writer must return a list/tuple of new row/column coordinates. Got {} instead.".format(new_coords))
            row_idx, col_idx = new_coords

            # Add spacing between sections
            row_idx += 1

        # Adjust column widths
        self.adjust_columns(worksheet, [s[1] for s in self.sections])

        self.postprocess(worksheet)


class ExcelWriter(object):

    def __init__(self, path):

        self.wb = xlsxwriter.Workbook(path)
        self.worksheets = list()

    def add_worksheet(self, worksheet):
        """
        Adds a worksheet object to internal list, queuing it for writing.
        """
        self.worksheets.append(worksheet)

    def write(self):
        """
        Write all the added worksheets to the workbook. This is done by calling
        the write() method on the worksheets.
        """

        for worksheet in self.worksheets:
            added_ws = self.wb.add_worksheet(worksheet.title)
            worksheet.setup(self.wb)
            worksheet.write(added_ws)

    def save(self):
        self.write()
        self.wb.close()
