import copy
import pytest
from ..vcfreader import SangerVerification
from ..vcfreader import TrioFields


@pytest.fixture
def pass_data():
    # Base data where all criteria should be met
    return {
        'REF': 'A',
        'ALT': ['T'],
        'FILTER': 'PASS',
        'QUAL': 400,
        'SAMPLES': {
            'TESTSAMPLE': {
                'AD': [53, 50],
                'DP': 103,
                'GT': '0/1',
            }
        }
    }

pedigree = {
    "father": "NA12341",
    "mother": "NA14232",
    "proband": "NA12434"
}

@pytest.fixture
def trio_pass_data():
    # Base data where all criteria should be met
    return {
        'REF': 'T',
        'ALT': ['G'],
        'FILTER': 'PASS',
        'QUAL': 400,
        'INFO': {
            'ALL': {
                'TRIO_TYPE': ',',
                'TRIO_DENOVO_P': None,
                'TRIO_ALT_CHILD_PCT': None,
                'TRIO_ALT_FATHER_PCT': None,
                'TRIO_ALT_MOTHER_PCT': None,
                },
            },
        'SAMPLES': {
            'NA12434': {
                'AD': [50, 52],
                'DP': 102,
                'GT': '0/1',
            },
            'NA14232': {
                'AD': [95, 1],
                'DP': 96,
                'GT': '0/0',
            },
            'NA12341': {
                'AD': [98, 0],
                'DP': 98,
                'GT': '0/0',
            }
        }
    }


class TestSangerVerification(object):

    def test_pass(self, pass_data):
        criteria = SangerVerification().check_criteria(pass_data, 'T')
        assert all(criteria.values())

    def test_filter_fail(self, pass_data):
        pass_data['FILTER'] = "."
        criteria = SangerVerification().check_criteria(pass_data, 'T')
        assert not criteria['FILTER']

    def test_qual_string_fail(self, pass_data):
        pass_data['QUAL'] = "STRING"
        criteria = SangerVerification().check_criteria(pass_data, 'T')
        assert not criteria['QUAL']

    def test_qual_low_fail(self, pass_data):
        pass_data['QUAL'] = 100
        criteria = SangerVerification().check_criteria(pass_data, 'T')
        assert not criteria['QUAL']

    def test_DP_fail(self, pass_data):
        pass_data['SAMPLES']['TESTSAMPLE']['DP'] = 10
        criteria = SangerVerification().check_criteria(pass_data, 'T')
        assert not criteria['DP']

    def test_indel_fail(self, pass_data):
        # Test should fail if not a snp
        ref_indel = copy.deepcopy(pass_data)
        ref_indel['REF'] = 'TT'
        criteria = SangerVerification().check_criteria(ref_indel, 'T')
        assert not criteria['SNP']

        alt_indel = copy.deepcopy(pass_data)
        alt_indel['ALT'] = ['GC']
        criteria = SangerVerification().check_criteria(alt_indel, 'GC')
        assert not criteria['SNP']

        both_indel = copy.deepcopy(pass_data)
        both_indel['REF'] = ['TT']
        both_indel['ALT'] = ['AA']
        criteria = SangerVerification().check_criteria(both_indel, 'AA')
        assert not criteria['SNP']

    def test_allele_depth(self, pass_data):

        hetero_high = copy.deepcopy(pass_data)
        hetero_high['SAMPLES']['TESTSAMPLE'] = {
            'AD': [1, 100],
            'DP': 103,
            'GT': '0/1',
        }
        criteria = SangerVerification().check_criteria(hetero_high, 'T')
        assert not criteria['AD']

        hetero_low = copy.deepcopy(pass_data)
        hetero_low['SAMPLES']['TESTSAMPLE'] = {
            'AD': [150, 1],
            'DP': 103,
            'GT': '0/1',
        }
        criteria = SangerVerification().check_criteria(hetero_low, 'T')
        assert not criteria['AD']

        homo_low = copy.deepcopy(pass_data)
        homo_low['SAMPLES']['TESTSAMPLE'] = {
            'AD': [500, 10],
            'DP': 103,
            'GT': '1/1',
        }
        criteria = SangerVerification().check_criteria(homo_low, 'T')
        assert not criteria['AD']

        # Multi-allelic homo, ratio = 0.5 -> FAIL
        multi_hetero_high = copy.deepcopy(pass_data)
        multi_hetero_high['ALT'] = ['G', 'T']
        multi_hetero_high['SAMPLES']['TESTSAMPLE'] = {
            'AD': [5, 15, 20],
            'DP': 103,
            'GT': '2/2',
        }
        criteria = SangerVerification().check_criteria(multi_hetero_high, 'T')
        assert not criteria['AD']

        # Multi-allelic hetero, ratio = 0.6 -> FAIL
        multi_hetero_high = copy.deepcopy(pass_data)
        multi_hetero_high['ALT'] = ['G', 'T']
        multi_hetero_high['SAMPLES']['TESTSAMPLE'] = {
            'AD': [5, 15, 30],
            'DP': 103,
            'GT': '1/2',
        }
        criteria = SangerVerification().check_criteria(multi_hetero_high, 'T')
        assert not criteria['AD']

        # Test zero AD [0, 0]
        zero_ad = dict(pass_data)
        zero_ad['SAMPLES']['TESTSAMPLE'] = {
            'AD': [0, 0],
            'DP': 31,
            'GT': '0/1',
        }
        criteria = SangerVerification().check_criteria(zero_ad, 'T')
        assert not criteria['AD']

        # Test GT ./.
        gt_dot = dict(pass_data)
        gt_dot['SAMPLES']['TESTSAMPLE'] = {
            'AD': [0, 0],
            'DP': 31,
            'GT': './.',
        }
        criteria = SangerVerification().check_criteria(gt_dot, 'T')
        assert not criteria['AD']


class TestTrioSangerVerification(object):

    def test_pass(self, trio_pass_data):

        criteria = TrioFields(pedigree).process(trio_pass_data, 'G')
        print(criteria)
        assert not criteria['TRIO_Sanger_verify']
