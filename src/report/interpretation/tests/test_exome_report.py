from ..exome_report import ExomeReport
from ..vcfreader import Frequencies
from ..report_common import is_3_prime_intronic, is_5_prime_intronic, is_intronic, is_upstream_ATG, has_unclassified_vep_consequence


class TestCheckIntronicHGVS(object):

    def test_intron_hgvs(self):
        _ = ExomeReport(None, None, None, None)

        intronic = [
            {'HGVSc': 'NM_000816.3:c.548+81delA'},
            {'HGVSc': 'NM_020699.2:c.729+21T>A'},
            {'HGVSc': 'NM_020988.2:c.161+110_161+111insCA'},
            {'HGVSc': 'NM_000833.3:c.1498-34_1498-33delCT'},
            {'HGVSc': 'NM_018328.4:c.2845+58C>A'},
            {'HGVSc': 'NM_018328.4:c.2519-21delT'},
            {'HGVSc': 'NM_004992.3:c.26+51C>T'},
            {'HGVSc': 'NM_004992.3:c.26+53G>T'},
            {'HGVSc': 'NM_001135659.1:c.931+23184_931+23187delAATT'},
            {'HGVSc': 'NM_002547.2:c.832+160delA'},
            {'HGVSc': 'NM_015192.3:c.863+7delA'},
            {'HGVSc': 'NM_018129.3:c.364-21delT'}
        ]

        for i in intronic:
            assert is_intronic(i)

        non_intronic = [
            {'HGVSc': 'NM_004933.2:c.1750A>C'},
            {'HGVSc': 'NM_004933.2:c.2364C>A'},
            {'HGVSc': 'NM_004933.2:c.174C>T'},
            {'HGVSc': 'NM_004933.2:c.264T>C'},
            {'HGVSc': 'NM_004933.2:c.1971A>G'},
            {'HGVSc': 'NM_015192.3:c.863+6delA'},
            {'HGVSc': 'NM_018129.3:c.364-20delT'}
        ]

        for ni in non_intronic:
            assert not is_intronic(ni)


class TestUnclassifiedHGVS(object):

    def test_5_prime_upstream(self):
        _ = ExomeReport(None, None, None, None)

        unclassified = [
            {'Consequence': '5_prime_UTR_variant', 'HGVSc': 'NM_030665.3:c.-16-13763G>A'},
            {'Consequence': '5_prime_UTR_variant', 'HGVSc': 'NM_025243.3:c.-2-76delA'},
            {'Consequence': 'intron_variant', 'HGVSc': 'NM_000833.3:c.-210+52delG'},
            {'Consequence': 'intron_variant', 'HGVSc': 'NM_000833.3:c.-19+36G>A'}
        ]
        for u in unclassified:
            assert is_upstream_ATG(u)

        notunclassified = [
            {'Consequence': '5_prime_UTR_variant', 'HGVSc': 'NM_030665.3:c.-16-3G>A'},
        ]
        for nu in notunclassified:
            assert not is_upstream_ATG(nu)

    def test_5_prime_intronic(self):
        _ = ExomeReport(None, None, None, None)

        unclassified = [
            {'Consequence': '5_prime_UTR_variant', 'HGVSc': 'NM_015284.3:c.-62G>C'},
            {'Consequence': '5_prime_UTR_variant', 'HGVSc': 'NM_032531.3:c.-194G>C'},
        ]
        for u in unclassified:
            assert is_5_prime_intronic(u)

        notunclassified = [
            {'Consequence': '5_prime_UTR_variant', 'HGVSc': 'NM_005629.3:c.-5A>G'},
            {'Consequence': '5_prime_UTR_variant', 'HGVSc': 'NM_000834.3:c.-15G>A'},
        ]
        for nu in notunclassified:
            assert not is_5_prime_intronic(nu)

    def test_3_prime_intronic(self):
        _ = ExomeReport(None, None, None, None)

        unclassified = [
            {'Consequence': '3_prime_UTR_variant', 'HGVSc': 'NM_015284.3:c.*3262A>G'},
        ]
        for u in unclassified:
            assert is_3_prime_intronic(u)

        notunclassified = [
            {'Consequence': '3_prime_UTR_variant', 'HGVSc': 'NM_015284.3:c.*3A>G'},
        ]
        for nu in notunclassified:
            assert not is_3_prime_intronic(nu)

class TestUnclassifiedVEPConsequence(object):

    def test_unclassified_vep_consequence(self):
        _ = ExomeReport(None, None, None, None)

        unclassified = [
            {'Consequence': '5_prime_UTR_variant'},
            {'Consequence': '3_prime_UTR_variant'},
            {'Consequence': 'intron_variant'},
            {'Consequence': 'upstream_gene_variant'},
            {'Consequence': '3_prime_UTR_variant (NM_138736.2) | intron_variant (NM_020988.2)'},
            {'Consequence': '5_prime_UTR_variant (NM_001194995.1) | intron_variant (NM_152269.4)'},
            {'Consequence': 'splice_region_variant,intron_variant'},  # We rely on the TestCheckIntronicHGVS for this case 
            {'Consequence': 'splice_region_variant,intron_variant (NM_001242784.1) | intron_variant (NM_000411.6)'} # Here too..         
        ]
        for u in unclassified:
            assert has_unclassified_vep_consequence(u)

        notunclassified = [
            {'Consequence': 'missense_variant (NM_199460.2) | intron_variant (NM_000719.6)'},
            {'Consequence': 'frameshift_variant (NM_170695.3) | intron_variant (NM_173208.2)'},
            {'Consequence': 'synonymous_variant (NM_001144915.1) | 3_prime_UTR_variant (NM_000141.4)'},
            {'Consequence': 'missense_variant (NM_013976.3) | 3_prime_UTR_variant (NM_000159.3)'}
        ]
        for nu in notunclassified:
            assert not has_unclassified_vep_consequence(nu)


class TestFilterLine(object):

    """
    We test current criteria, both for code correctness and that they are what we expect

        # AD criteria
        [('Inheritance', lambda x: x == 'AD'), ('AF_OUSWES', lambda x: float(x) >= 0.05)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_NFE', lambda x: float(x) >= 0.005), ('gnomAD_exomes_NFE_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_FIN', lambda x: float(x) >= 0.005), ('gnomAD_exomes_FIN_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_AFR', lambda x: float(x) >= 0.005), ('gnomAD_exomes_AFR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_AMR', lambda x: float(x) >= 0.005), ('gnomAD_exomes_AMR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_EAS', lambda x: float(x) >= 0.005), ('gnomAD_exomes_EAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_SAS', lambda x: float(x) >= 0.005), ('gnomAD_exomes_SAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_OTH', lambda x: float(x) >= 0.005), ('gnomAD_exomes_OTH_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_NFE', lambda x: float(x) >= 0.005), ('gnomAD_genomes_NFE_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_FIN', lambda x: float(x) >= 0.005), ('gnomAD_genomes_FIN_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_AFR', lambda x: float(x) >= 0.005), ('gnomAD_genomes_AFR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_AMR', lambda x: float(x) >= 0.005), ('gnomAD_genomes_AMR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_EAS', lambda x: float(x) >= 0.005), ('gnomAD_genomes_EAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_SAS', lambda x: float(x) >= 0.005), ('gnomAD_genomes_SAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_OTH', lambda x: float(x) >= 0.005), ('gnomAD_genomes_OTH_AN', lambda x: int(x) >= 5000)],

        # non-AD criteria
        [('Inheritance', lambda x: x != 'AD'), ('AF_OUSWES', lambda x: float(x) >= 0.05)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_NFE', lambda x: float(x) >= 0.01), ('gnomAD_exomes_NFE_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_FIN', lambda x: float(x) >= 0.01), ('gnomAD_exomes_FIN_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_AFR', lambda x: float(x) >= 0.01), ('gnomAD_exomes_AFR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_AMR', lambda x: float(x) >= 0.01), ('gnomAD_exomes_AMR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_EAS', lambda x: float(x) >= 0.01), ('gnomAD_exomes_EAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_SAS', lambda x: float(x) >= 0.01), ('gnomAD_exomes_SAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_OTH', lambda x: float(x) >= 0.01), ('gnomAD_exomes_OTH_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_NFE', lambda x: float(x) >= 0.01), ('gnomAD_genomes_NFE_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_FIN', lambda x: float(x) >= 0.01), ('gnomAD_genomes_FIN_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_AFR', lambda x: float(x) >= 0.01), ('gnomAD_genomes_AFR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_AMR', lambda x: float(x) >= 0.01), ('gnomAD_genomes_AMR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_EAS', lambda x: float(x) >= 0.01), ('gnomAD_genomes_EAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_SAS', lambda x: float(x) >= 0.01), ('gnomAD_genomes_SAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_OTH', lambda x: float(x) >= 0.01), ('gnomAD_genomes_OTH_AN', lambda x: int(x) >= 5000)],
    """

    def test_above_but_not_used(self):
        # ExAC_TOT above but not used
        # gnomAD_exomes_TOT above but not used
        # gnomAD_genomes_TOT above but not used

        #AD
        line = {
            'Inheritance': 'AD',
            'AF_OUSWES': 0.01,  # below
            '1000g': 0.0001,
            'ExAC_TOT': 0.009,  # above for AD but not used for filtering
            'ExAC_TOT_AN': 5000,
            'ExAC_NFE': 0.009,    # above for AD but not used for filtering
            'ExAC_NFE_AN': 5000,
            'ExAC_FIN': 0.8,
            'ExAC_FIN_AN': 1,
            'ExAC_AFR': 0.8,  # above for AD but not used for filtering
            'ExAC_AFR_AN': 5000,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,  # too low AN
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.009,  # above for AD but not used for filtering
            'gnomAD_exomes_TOT_AN': 5000,
            'gnomAD_exomes_NFE': 0.8,
            'gnomAD_exomes_NFE_AN': 1,  # too low AN
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.009,  # above for AD but not used for filtering
            'gnomAD_genomes_TOT_AN': 5000,
            'gnomAD_genomes_NFE': 0.8,
            'gnomAD_genomes_NFE_AN': 1,  # too low AN
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)



        #non-AD
        line = {
            'Inheritance': 'AD,AR',
            'AF_OUSWES': 0.01,  # below
            '1000g': 0.0001,
            'ExAC_TOT': 0.02,  # above for non-AD but not used for filtering
            'ExAC_TOT_AN': 5000,
            'ExAC_NFE': 0.8,
            'ExAC_NFE_AN': 1,
            'ExAC_FIN': 0.02,  # above for non-AD but not used for filtering
            'ExAC_FIN_AN': 5000,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8, # above for non-AD but not used for filtering
            'ExAC_OTH_AN': 5000,
            'gnomAD_exomes_TOT': 0.02,  # above for non-AD but not used for filtering
            'gnomAD_exomes_TOT_AN': 5000,
            'gnomAD_exomes_NFE': 0.8,
            'gnomAD_exomes_NFE_AN': 1,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.02,  # above for non-AD but not used for filtering
            'gnomAD_genomes_TOT_AN': 5000,
            'gnomAD_genomes_NFE': 0.8,
            'gnomAD_genomes_NFE_AN': 1,
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

    def test_above(self):
        #AD
        line = {
            'Inheritance': 'AD',
            'AF_OUSWES': 0.01,  # below
            '1000g': 0.0001,
            'ExAC_TOT': 0.009,  # above for AD but not used for filtering
            'ExAC_TOT_AN': 5000,
            'ExAC_NFE': 0.8,    # above for AD but not used for filtering
            'ExAC_NFE_AN': 5000,
            'ExAC_FIN': 0.8,
            'ExAC_FIN_AN': 1,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.009,  # above for AD but not used for filtering
            'gnomAD_exomes_TOT_AN': 5000,
            'gnomAD_exomes_NFE': 0.8, # above for AD, to be filtered
            'gnomAD_exomes_NFE_AN': 5000,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.009,  # above for AD but not used for filtering
            'gnomAD_genomes_TOT_AN': 5000,
            'gnomAD_genomes_NFE': 0.8,
            'gnomAD_genomes_NFE_AN': 1,  # too low AN
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert ExomeReport._filter_line(line)



        #non-AD
        line = {
            'Inheritance': 'AD,AR',
            'AF_OUSWES': 0.01,  # below
            '1000g': 0.0001,
            'ExAC_TOT': 0.02,  # above for non-AD but not used for filtering
            'ExAC_TOT_AN': 5000,
            'ExAC_NFE': 0.8,
            'ExAC_NFE_AN': 1,
            'ExAC_FIN': 0.8,   # above for non-AD but not used for filtering
            'ExAC_FIN_AN': 5000,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.02,  # above for non-AD but not used for filtering
            'gnomAD_exomes_TOT_AN': 5000,
            'gnomAD_exomes_NFE': 0.02, # above for non-AD, to be filtered
            'gnomAD_exomes_NFE_AN': 5000,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.02,  # above for non-AD but not used for filtering
            'gnomAD_genomes_TOT_AN': 5000,
            'gnomAD_genomes_NFE': 0.8,
            'gnomAD_genomes_NFE_AN': 1,
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert ExomeReport._filter_line(line)

    def test_1000g(self):
        # Test 1000g above but not used for filtering
        #AD
        line = {
            'Inheritance': 'AD',
            'AF_OUSWES': 0.01,
            '1000g': 0.8, # above but not used for filtering
            'ExAC_TOT': 0.8,
            'ExAC_TOT_AN': 1,
            'ExAC_NFE': 0.8,
            'ExAC_NFE_AN': 1,
            'ExAC_FIN': 0.8,
            'ExAC_FIN_AN': 1,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.8,
            'gnomAD_exomes_TOT_AN': 1,
            'gnomAD_exomes_NFE': 0.8,
            'gnomAD_exomes_NFE_AN': 1,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.8,
            'gnomAD_genomes_TOT_AN': 1,
            'gnomAD_genomes_NFE': 0.8,
            'gnomAD_genomes_NFE_AN': 1,
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

        #not-AD
        line = {
            'Inheritance': 'AD,AR,X',
            'AF_OUSWES': 0.01,
            '1000g': 0.8, # above but not used for filtering
            'ExAC_TOT': 0.8,
            'ExAC_TOT_AN': 1,
            'ExAC_NFE': 0.8,
            'ExAC_NFE_AN': 1,
            'ExAC_FIN': 0.8,
            'ExAC_FIN_AN': 1,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.8,
            'gnomAD_exomes_TOT_AN': 1,
            'gnomAD_exomes_NFE': 0.8,
            'gnomAD_exomes_NFE_AN': 1,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.8,
            'gnomAD_genomes_TOT_AN': 1,
            'gnomAD_genomes_NFE': 0.8,
            'gnomAD_genomes_NFE_AN': 1,
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

    def test_at_threshold_but_not_used(self):
        # Test at threshold but not used for filtering
        #AD
        line = {
            'Inheritance': 'AD',
            'AF_OUSWES': 0.01,  # below
            '1000g': 0.0001,
            'ExAC_TOT': 0.005,   # at threshold for AD but not used for filtering
            'ExAC_TOT_AN': 5000,
            'ExAC_NFE': 0.005,  # at threshold for AD but not used for filtering
            'ExAC_NFE_AN': 5000,  # at threshold
            'ExAC_FIN': 0.8,
            'ExAC_FIN_AN': 1,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.005,  # at threshold for AD but not used for filtering
            'gnomAD_exomes_TOT_AN': 5000,
            'gnomAD_exomes_NFE': 0.8,
            'gnomAD_exomes_NFE_AN': 1,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.005,  # at threshold for AD but not used for filtering
            'gnomAD_genomes_TOT_AN': 5000,
            'gnomAD_genomes_NFE': 0.8,
            'gnomAD_genomes_NFE_AN': 1,
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

        #non-AD
        line = {
            'Inheritance': 'AD,AR',
            'AF_OUSWES': 0.01,  # below
            '1000g': 0.0001,
            'ExAC_TOT': 1,
            'ExAC_TOT_AN': 1,
            'ExAC_NFE': 0.01,    # at threshold for non-AD but not used for filtering
            'ExAC_NFE_AN': 5000,  # at threshold
            'ExAC_FIN': 0.8,
            'ExAC_FIN_AN': 1,
            'ExAC_AFR': 0.01,    # at threshold for non-AD but not used for filtering
            'ExAC_AFR_AN': 5000,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.01,    # at threshold for non-AD but not used for filtering
            'gnomAD_exomes_TOT_AN': 5000,
            'gnomAD_exomes_NFE': 0.8,
            'gnomAD_exomes_NFE_AN': 1,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.01,    # at threshold for non-AD but not used for filtering
            'gnomAD_genomes_TOT_AN': 5000,
            'gnomAD_genomes_NFE': 0.8,
            'gnomAD_genomes_NFE_AN': 1,
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

    def test_below(self):
        # Test below threshold
        #AD
        line = {
            'Inheritance': 'AD',
            'AF_OUSWES': 0.01,
            '1000g': 0.001,
            'ExAC_TOT': 0.8,
            'ExAC_TOT_AN': 1,
            'ExAC_NFE': 0.001,
            'ExAC_NFE_AN': 5000,
            'ExAC_FIN': 0.002,
            'ExAC_FIN_AN': 5000,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.8,
            'gnomAD_exomes_TOT_AN': 1,
            'gnomAD_exomes_NFE': 0.001,
            'gnomAD_exomes_NFE_AN': 5000,
            'gnomAD_exomes_FIN': 0.002,
            'gnomAD_exomes_FIN_AN': 5000,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.8,
            'gnomAD_genomes_TOT_AN': 1,
            'gnomAD_genomes_NFE': 0.001,
            'gnomAD_genomes_NFE_AN': 5000,
            'gnomAD_genomes_FIN': 0.002,
            'gnomAD_genomes_FIN_AN': 5000,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

        line = {
            'Inheritance': 'AD,AR',
            'AF_OUSWES': 0.0499,
            '1000g': 0.0099,
            'ExAC_TOT': 0.0099,
            'ExAC_TOT_AN': 2500,
            'ExAC_NFE': 0.0099,
            'ExAC_NFE_AN': 5000,
            'ExAC_FIN': 0.8,
            'ExAC_FIN_AN': 1,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.0099,
            'gnomAD_exomes_TOT_AN': 2500,
            'gnomAD_exomes_NFE': 0.0099,
            'gnomAD_exomes_NFE_AN': 5000,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.0099,
            'gnomAD_genomes_TOT_AN': 2500,
            'gnomAD_genomes_NFE': 0.0099,
            'gnomAD_genomes_NFE_AN': 5000,
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

    def test_string(self):
        # Test strings (will happend when freq is not given for current allele)
        line = {
            'Inheritance': 'AD,AR',
            'AF_OUSWES': 0.0499,
            '1000g': 'G: 0.30',
            'ExAC_TOT': 0.0099,
            'ExAC_TOT_AN': 2500,
            'ExAC_NFE': 0.0099,
            'ExAC_NFE_AN': 5000,
            'ExAC_FIN': 0.8,
            'ExAC_FIN_AN': 1,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.0099,
            'gnomAD_exomes_TOT_AN': 2500,
            'gnomAD_exomes_NFE': 0.0099,
            'gnomAD_exomes_NFE_AN': 5000,
            'gnomAD_exomes_FIN': 0.8,
            'gnomAD_exomes_FIN_AN': 1,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.0099,
            'gnomAD_genomes_TOT_AN': 2500,
            'gnomAD_genomes_NFE': 0.0099,
            'gnomAD_genomes_NFE_AN': 5000,
            'gnomAD_genomes_FIN': 0.8,
            'gnomAD_genomes_FIN_AN': 1,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

    def test_qual_match(self):
        # Test qualitative filter
        # (frequency is below thresholds)
        line = {
            'Inheritance': 'AD',
            'AF_OUSWES': 0.01,
            '1000g': 0.001,
            'ExAC_TOT': 0.8,
            'ExAC_TOT_AN': 1,
            'ExAC_NFE': 0.001,
            'ExAC_NFE_AN': 5000,
            'ExAC_FIN': 0.002,
            'ExAC_FIN_AN': 5000,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.8,
            'gnomAD_exomes_TOT_AN': 1,
            'gnomAD_exomes_NFE': 0.001,
            'gnomAD_exomes_NFE_AN': 5000,
            'gnomAD_exomes_FIN': 0.002,
            'gnomAD_exomes_FIN_AN': 5000,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.8,
            'gnomAD_genomes_TOT_AN': 1,
            'gnomAD_genomes_NFE': 0.001,
            'gnomAD_genomes_NFE_AN': 5000,
            'gnomAD_genomes_FIN': 0.002,
            'gnomAD_genomes_FIN_AN': 5000,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': 'u'
        }
        assert ExomeReport._filter_line(line)

    def test_qual_unmatch(self):
        # Test qualitative filter
        # (frequency is below thresholds)
        line = {
            'Inheritance': 'AD',
            'AF_OUSWES': 0.01,
            '1000g': 0.001,
            'ExAC_TOT': 0.8,
            'ExAC_TOT_AN': 1,
            'ExAC_NFE': 0.001,
            'ExAC_NFE_AN': 5000,
            'ExAC_FIN': 0.002,
            'ExAC_FIN_AN': 5000,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.8,
            'gnomAD_exomes_TOT_AN': 1,
            'gnomAD_exomes_NFE': 0.001,
            'gnomAD_exomes_NFE_AN': 5000,
            'gnomAD_exomes_FIN': 0.002,
            'gnomAD_exomes_FIN_AN': 5000,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.8,
            'gnomAD_genomes_TOT_AN': 1,
            'gnomAD_genomes_NFE': 0.001,
            'gnomAD_genomes_NFE_AN': 5000,
            'gnomAD_genomes_FIN': 0.002,
            'gnomAD_genomes_FIN_AN': 5000,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': ''
        }
        assert not ExomeReport._filter_line(line)

    def test_qual_num(self):
        # Test qualitative filter
        # (frequency is below thresholds)
        line = {
            'Inheritance': 'AD',
            'AF_OUSWES': 0.01,
            '1000g': 0.001,
            'ExAC_TOT': 0.8,
            'ExAC_TOT_AN': 1,
            'ExAC_NFE': 0.001,
            'ExAC_NFE_AN': 5000,
            'ExAC_FIN': 0.002,
            'ExAC_FIN_AN': 5000,
            'ExAC_AFR': 0.8,
            'ExAC_AFR_AN': 1,
            'ExAC_AMR': 0.8,
            'ExAC_AMR_AN': 1,
            'ExAC_EAS': 0.8,
            'ExAC_EAS_AN': 1,
            'ExAC_SAS': 0.8,
            'ExAC_SAS_AN': 1,
            'ExAC_OTH': 0.8,
            'ExAC_OTH_AN': 1,
            'gnomAD_exomes_TOT': 0.8,
            'gnomAD_exomes_TOT_AN': 1,
            'gnomAD_exomes_NFE': 0.001,
            'gnomAD_exomes_NFE_AN': 5000,
            'gnomAD_exomes_FIN': 0.002,
            'gnomAD_exomes_FIN_AN': 5000,
            'gnomAD_exomes_AFR': 0.8,
            'gnomAD_exomes_AFR_AN': 1,
            'gnomAD_exomes_AMR': 0.8,
            'gnomAD_exomes_AMR_AN': 1,
            'gnomAD_exomes_EAS': 0.8,
            'gnomAD_exomes_EAS_AN': 1,
            'gnomAD_exomes_SAS': 0.8,
            'gnomAD_exomes_SAS_AN': 1,
            'gnomAD_exomes_OTH': 0.8,
            'gnomAD_exomes_OTH_AN': 1,
            'gnomAD_genomes_TOT': 0.8,
            'gnomAD_genomes_TOT_AN': 1,
            'gnomAD_genomes_NFE': 0.001,
            'gnomAD_genomes_NFE_AN': 5000,
            'gnomAD_genomes_FIN': 0.002,
            'gnomAD_genomes_FIN_AN': 5000,
            'gnomAD_genomes_AFR': 0.8,
            'gnomAD_genomes_AFR_AN': 1,
            'gnomAD_genomes_AMR': 0.8,
            'gnomAD_genomes_AMR_AN': 1,
            'gnomAD_genomes_EAS': 0.8,
            'gnomAD_genomes_EAS_AN': 1,
            'gnomAD_genomes_SAS': 0.8,
            'gnomAD_genomes_SAS_AN': 1,
            'gnomAD_genomes_OTH': 0.8,
            'gnomAD_genomes_OTH_AN': 1,
            'Klasse': 6
        }
        assert not ExomeReport._filter_line(line)


class TestExACFrequency(object):

    def test_conversion(self):

        line = {
            'INFO': {
                'ALL': {
                    'EXAC__AC_Adj': [1000, 500],
                    'EXAC__AC_AFR': [1000, 500],
                    'EXAC__AC_AMR': [1000, 500],
                    'EXAC__AC_EAS': [1000, 500],
                    'EXAC__AC_FIN': [1000, 500],
                    'EXAC__AC_NFE': [0, 0],  # Test 0 freq
                    'EXAC__AC_OTH': [1, 2],
                    'EXAC__AC_SAS_MISSING': [8000, 1000],
                    'EXAC__AN_Adj': 1000,
                    'EXAC__AN_AFR': 2000,
                    'EXAC__AN_AMR': 4000,
                    'EXAC__AN_EAS': 5000,
                    'EXAC__AN_FIN': 100000,
                    'EXAC__AN_NFE': 1,
                    'EXAC__AN_OTH': 0,  # Test divide on 0
                    'EXAC__AN_SAS_MISSING': 1
                }
            }
        }

        f = Frequencies([])
        freqs, totals = f._get_exac_freqs_totals(line, 0)
        assert not 'SAS_MISSING' in freqs
        assert freqs['Adj'] == 1
        assert freqs['AFR'] == 0.5
        assert freqs['AMR'] == 0.25
        assert freqs['EAS'] == 0.2
        assert freqs['FIN'] == 0.01
        assert freqs['NFE'] == 0
        assert 'OTH' not in freqs

        assert totals['Adj'] == 1000
        assert totals['AFR'] == 2000
        assert totals['AMR'] == 4000
        assert totals['EAS'] == 5000
        assert totals['FIN'] == 100000
        assert totals['NFE'] == 1
        assert totals['OTH'] == 0

        freqs, totals = f._get_exac_freqs_totals(line, 1)
        assert not 'SAS_MISSING' in freqs
        assert freqs['Adj'] == 0.5
        assert freqs['AFR'] == 0.25
        assert freqs['AMR'] == 0.125
        assert freqs['EAS'] == 0.1
        assert freqs['FIN'] == 0.005
        assert freqs['NFE'] == 0
        assert 'OTH' not in freqs
