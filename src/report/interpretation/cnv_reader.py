import csv

class VcfFields(object):
    def process(self, row):
        row['chr'] = row.pop('#chr')
        return row


class CnvReader(object):

    def __init__(self, cnv_path,):
        self.cnv_path = cnv_path

    def get_data(self):
        processors = [
            VcfFields()
        ]

        try:
            with open(self.cnv_path, 'r') as cnv_file:
                cnv_reader = csv.DictReader(cnv_file, delimiter='\t')
                for row in cnv_reader:
                    data = dict()
                    for p in processors:
                        data.update(p.process(row))
                    yield data
        except IOError as err:
            raise(err)
