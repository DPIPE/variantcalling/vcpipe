#!/bin/env python

"""
Creates Excel/tsv reports from a vcf file. Valid genes are filtered using
a transcripts file from a genepanel.

It uses VcfReader for the data parsing/converint,
picking out the fields to include in the report.
"""

import argparse
import logging
from collections import OrderedDict

from report.interpretation.vcfreader import VcfReader
from report.interpretation.cnv_reader import CnvReader
from report.interpretation.metadata import CHECKLIST, CHECKLIST_COMMENT, PHENOTYPE_CHECKLIST
from report.interpretation.report_common import HGVS_INTRON_RE, HGVS_UPSTREAM_ATG_RE, HGVS_5PRIME_INTRON_RE, HGVS_3PRIME_INTRON_RE
from report.interpretation.report_common import is_intronic, check_class_unclassified, is_upstream_ATG, is_5_prime_intronic, is_3_prime_intronic, has_unclassified_vep_consequence

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('ExomeReport')


class ExomeReport(object):

    FILTER_CRITERIA = [
        # The subcriteria will be ANDed together,
        #  e.g. inheritance == 'AD' AND ExAC_TOT > 0.005

        # AD criteria
        [('Inheritance', lambda x: x == 'AD'), ('AF_OUSWES', lambda x: float(x) >= 0.05)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_NFE', lambda x: float(x) >= 0.005), ('gnomAD_exomes_NFE_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_FIN', lambda x: float(x) >= 0.005), ('gnomAD_exomes_FIN_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_AFR', lambda x: float(x) >= 0.005), ('gnomAD_exomes_AFR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_AMR', lambda x: float(x) >= 0.005), ('gnomAD_exomes_AMR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_EAS', lambda x: float(x) >= 0.005), ('gnomAD_exomes_EAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_SAS', lambda x: float(x) >= 0.005), ('gnomAD_exomes_SAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_exomes_OTH', lambda x: float(x) >= 0.005), ('gnomAD_exomes_OTH_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_NFE', lambda x: float(x) >= 0.005), ('gnomAD_genomes_NFE_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_FIN', lambda x: float(x) >= 0.005), ('gnomAD_genomes_FIN_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_AFR', lambda x: float(x) >= 0.005), ('gnomAD_genomes_AFR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_AMR', lambda x: float(x) >= 0.005), ('gnomAD_genomes_AMR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_EAS', lambda x: float(x) >= 0.005), ('gnomAD_genomes_EAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_SAS', lambda x: float(x) >= 0.005), ('gnomAD_genomes_SAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x == 'AD'), ('gnomAD_genomes_OTH', lambda x: float(x) >= 0.005), ('gnomAD_genomes_OTH_AN', lambda x: int(x) >= 5000)],

        # non-AD criteria
        [('Inheritance', lambda x: x != 'AD'), ('AF_OUSWES', lambda x: float(x) >= 0.05)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_NFE', lambda x: float(x) >= 0.01), ('gnomAD_exomes_NFE_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_FIN', lambda x: float(x) >= 0.01), ('gnomAD_exomes_FIN_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_AFR', lambda x: float(x) >= 0.01), ('gnomAD_exomes_AFR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_AMR', lambda x: float(x) >= 0.01), ('gnomAD_exomes_AMR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_EAS', lambda x: float(x) >= 0.01), ('gnomAD_exomes_EAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_SAS', lambda x: float(x) >= 0.01), ('gnomAD_exomes_SAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_exomes_OTH', lambda x: float(x) >= 0.01), ('gnomAD_exomes_OTH_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_NFE', lambda x: float(x) >= 0.01), ('gnomAD_genomes_NFE_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_FIN', lambda x: float(x) >= 0.01), ('gnomAD_genomes_FIN_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_AFR', lambda x: float(x) >= 0.01), ('gnomAD_genomes_AFR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_AMR', lambda x: float(x) >= 0.01), ('gnomAD_genomes_AMR_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_EAS', lambda x: float(x) >= 0.01), ('gnomAD_genomes_EAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_SAS', lambda x: float(x) >= 0.01), ('gnomAD_genomes_SAS_AN', lambda x: int(x) >= 5000)],
        [('Inheritance', lambda x: x != 'AD'), ('gnomAD_genomes_OTH', lambda x: float(x) >= 0.01), ('gnomAD_genomes_OTH_AN', lambda x: int(x) >= 5000)],
    ]

    FILTER_QUALITATIVES = [
        ('Klasse', 'u')
    ]

    # The follwing regex match for NM_ trasncripts only (useful when HGVSc has several non-RefSeq, pipe-separated transcripts)

    # Fields to include and their order
    GET_ROWS_FIELDS = [
        'amg_gene_panel',
        'extra_phenotypes',
        'CHR',
        'POS',
        'REF',
        'ALT',
        'Gene',
        'Inheritance',
        'Phenotype',
        'HGVSc',
        'HGVSp',
        'Consequence',
        'IGV',
        'Klasse',
        'Resultatvurdering kommentar',
        'Kontroll kommentar',
        'Genotype',
        'GNOMAD_URL',
        'HGMD_URL',
        'CLINVAR_URL',
        'OMIM_URL',
        'AlamutColumn',
        'AF_OUSWES',
        'AC_OUSWES',
        'Hom_OUSWES',
        'VCF_AD_AlleleDepth',
        'VCF_FILTER',
        'VCF_QUAL',
        'VCF_DP_depth',
        'indications_OUSWES',
        'AN_OUSWES',
        'Het_OUSWES',
        'filter_OUSWES',
        'repeatMasker',
        'gnomAD_exomes_TOT',
        'gnomAD_exomes_NFE',
        'gnomAD_exomes_FIN',
        'gnomAD_exomes_AFR',
        'gnomAD_exomes_AMR',
        'gnomAD_exomes_EAS',
        'gnomAD_exomes_SAS',
        'gnomAD_exomes_OTH',
        'gnomAD_exomes_TOT_AN',
        'gnomAD_exomes_NFE_AN',
        'gnomAD_exomes_FIN_AN',
        'gnomAD_exomes_AFR_AN',
        'gnomAD_exomes_AMR_AN',
        'gnomAD_exomes_EAS_AN',
        'gnomAD_exomes_SAS_AN',
        'gnomAD_exomes_OTH_AN',
        'gnomAD_genomes_TOT',
        'gnomAD_genomes_NFE',
        'gnomAD_genomes_FIN',
        'gnomAD_genomes_AFR',
        'gnomAD_genomes_AMR',
        'gnomAD_genomes_EAS',
        'gnomAD_genomes_SAS',
        'gnomAD_genomes_OTH',
        'gnomAD_genomes_TOT_AN',
        'gnomAD_genomes_NFE_AN',
        'gnomAD_genomes_FIN_AN',
        'gnomAD_genomes_AFR_AN',
        'gnomAD_genomes_AMR_AN',
        'gnomAD_genomes_EAS_AN',
        'gnomAD_genomes_SAS_AN',
        'gnomAD_genomes_OTH_AN',
        '1000g',
        # following are columns that will be removed later
        'pseudogenes',
        'ACMG kriterier',
        'Variantvurdering',
        'ExAC_URL',
        'sanger_verify',
        'dbSNP',
        'ExAC_TOT',
        'ExAC_NFE',
        'ExAC_FIN',
        'ExAC_AFR',
        'ExAC_AMR',
        'ExAC_EAS',
        'ExAC_SAS',
        'ExAC_OTH',
        'ExAC_TOT_AN',
        'ExAC_NFE_AN',
        'ExAC_FIN_AN',
        'ExAC_AFR_AN',
        'ExAC_AMR_AN',
        'ExAC_EAS_AN',
        'ExAC_SAS_AN',
        'ExAC_OTH_AN',
    ]

    def __init__(self, vcf_path, transcripts_path, phenotypes_path, cnv_path):
        self.vcf_path = vcf_path
        self.transcripts_path = transcripts_path
        self.phenotypes_path = phenotypes_path
        self.excel_writer = None
        self.tsv_fd = None
        self.cnv_path = cnv_path




    def get_cnv_rows(self):
        try:
            reader = CnvReader(self.cnv_path)
        except IOError as err:
            raise(err)

        # Fields to include and their order
        fields = [
            'chr',
            'start',
            'end',
            'cnv_state',
            'default_score',
            'len',
            'inDB_count',
            'inDBScore_MinMaxMedian',
            'gene_name',
            'gene_type',
            'gene_id',
            'exon_count',
            'UTR',
            'transcript',
            'phastConElement_count',
            'phastConElement_minMax',
            'haplo_insufIdx_count',
            'haplo_insufIdx_score',
            'Gene_intolarance_score',
            'sanger_cnv',
            'dgv_cnv',
            'dgv_varType',
            'dgv_varSubType',
            'dgv_pubmedId',
            'DGV_Stringency2_count',
            'DGV_Stringency2_PopFreq',
            'DGV_Stringency12_count',
            'DGV_Stringency12_popFreq',
            '1000g_del',
            '1000g_ins',
            'omim_morbidMap',
            'ddd_mutConsequence',
            'ddd_diseaseName',
            'ddd_pubmedId',
            'clinVar_disease',
            'hgvs_varName'
        ]

        for entry in reader.get_data():
            report_row = list()
            for field in fields:
                report_row.append((field, entry.get(field, 'N/A')))
            yield OrderedDict(report_row)

    def get_rows(self):

        reader = VcfReader(self.vcf_path, self.transcripts_path, self.phenotypes_path)

        empty_fields = [
            'IGV',
            'ACMG kriterier',
            'Klasse',
            'Resultatvurdering kommentar',
            'Kontroll kommentar',
            'Variantvurdering'
        ]

        rename_fields = {
        }

        for entry in reader.get_data():
            report_row = list()
            for field in self.GET_ROWS_FIELDS:
                # Rename field if in map
                fieldname = rename_fields.get(field, field)

                # mark Klasse as 'u' if any rule is satisfied
                if field == 'Klasse':
                    if check_class_unclassified(entry):
                        report_row.append((field, 'u'))
                    else:
                        report_row.append((field, ''))
                elif field in empty_fields:
                    report_row.append((field, ''))
                else:
                    report_row.append((field, entry.get(field, 'N/A')))
            yield OrderedDict(report_row)

    @staticmethod
    def _filter_line(line):

        for subcriteria in ExomeReport.FILTER_CRITERIA:
            subcriteria_results = list()
            for name, critera_func in subcriteria:
                if name not in line:
                    raise RuntimeError("Key {} used in filtering criteria is not part of report.".format(name))
                try:
                    subcriteria_results.append(critera_func(line[name]))
                except ValueError:  # For criteria using float()
                    subcriteria_results.append(False)
            if subcriteria_results and all(subcriteria_results):  # AND subcriteria results
                return True

        for name, crit in ExomeReport.FILTER_QUALITATIVES:
            val = line[name]
            if val == 'N/A' or not isinstance(val, str):
                continue
            if val == crit:
                return True

        return False

    def get_grouping_columns(self):

        ExAC_clm = self.GET_ROWS_FIELDS.index('ExAC_TOT')
        gnomAD_exomes_clm = self.GET_ROWS_FIELDS.index('gnomAD_exomes_TOT')
        gnomAD_genomes_clm = self.GET_ROWS_FIELDS.index('gnomAD_genomes_TOT')
        gnomAD_1000g_clm = self.GET_ROWS_FIELDS.index('1000g')

        ExAC_AN_clm = self.GET_ROWS_FIELDS.index('ExAC_TOT_AN')
        gnomAD_exomes_AN_clm = self.GET_ROWS_FIELDS.index('gnomAD_exomes_TOT_AN')
        gnomAD_genomes_AN_clm = self.GET_ROWS_FIELDS.index('gnomAD_genomes_TOT_AN')

        return([(ExAC_clm + 1, gnomAD_exomes_clm - 1, 1),
                (gnomAD_exomes_clm + 1, gnomAD_genomes_clm - 1, 1),
                (gnomAD_genomes_clm + 1, gnomAD_1000g_clm - 1, 1),
                (ExAC_AN_clm, gnomAD_exomes_clm - 1, 2),
                (gnomAD_exomes_AN_clm, gnomAD_genomes_clm - 1, 2),
                (gnomAD_genomes_AN_clm, gnomAD_1000g_clm - 1, 2)])

    def write(self, excel=None, tsv=None, igv=None, header=None, cnv=None, coverage_worksheet=False):

        if not any(a for a in [excel, tsv, igv]):
            raise RuntimeError("You must at least provide one output type.")

        lines = sorted(self.get_rows(), key=lambda x: (x['Inheritance'],
                                                       x['Gene'],
                                                       x['POS']))

        filtered_lines = [line for line in lines if not ExomeReport._filter_line(line)]

        if excel:
            # Lazy import to avoid unneccessary dependencies
            from report.interpretation.excelwriter import ExcelWriter, Worksheet, SectionedWorksheet
            from report.interpretation.custom_excel import VariantHelpWorksheet, VariantRowWriter, CnvRowWriter, ChecklistWriter, VersionWorksheet, CoverageTemplateWorksheet

            self.excel_writer = ExcelWriter(excel)

            col_grouping = self.get_grouping_columns()

            all_ws = Worksheet('All variants', freeze_panes=(1, 6), grouping_col=col_grouping)
            all_ws.set_writer(VariantRowWriter(lines))
            self.excel_writer.add_worksheet(all_ws)

            filtered_ws = SectionedWorksheet('Filtered variants', active=True, freeze_panes=(1, 6), grouping_col=col_grouping)

            filtered_ws.add_section(header, VariantRowWriter(filtered_lines, color_interpretation=True))
            filtered_ws.add_section(None, ChecklistWriter(offset=6, table=CHECKLIST, comment=CHECKLIST_COMMENT))
            filtered_ws.add_section(None, ChecklistWriter(offset=6, table=PHENOTYPE_CHECKLIST, comment=[]))
            self.excel_writer.add_worksheet(filtered_ws)

            if self.cnv_path:
                try:
                    cnv_ws = SectionedWorksheet('CNV', freeze_panes=(1, 6))
                    cnv_lines = sorted(self.get_cnv_rows(),
                        key=lambda x: (x['cnv_state'],
                                       x['default_score']))
                    cnv_ws.add_section(header, CnvRowWriter(cnv_lines, color_interpretation=True))
                    self.excel_writer.add_worksheet(cnv_ws)
                except IOError as err:
                    pass

            self.excel_writer.add_worksheet(VariantHelpWorksheet("Help"))
            self.excel_writer.add_worksheet(VersionWorksheet("Version"))

            if coverage_worksheet:
                self.excel_writer.add_worksheet(CoverageTemplateWorksheet("Coverage"))

            self.excel_writer.save()

        if tsv:
            self.tsv_fd = open(tsv, 'w')
            self.tsv_fd.write('\t'.join(list(lines[0].keys())))
            self.tsv_fd.write('\n')
            for line in lines:
                if self.tsv_fd:
                    self.tsv_fd.write('\t'.join([str(f) for f in list(line.values())]))
                    self.tsv_fd.write('\n')
            self.tsv_fd.close()

        if igv:
            igv_data = list()
            for line in filtered_lines:
                igv_data.append(
                    [
                        line['CHR'],
                        line['POS']-1,
                        line['POS'],
                        line['Gene'],
                    ]
                )

            with open(igv, 'w') as igv_fd:
                for line in igv_data:
                    igv_fd.write('\t'.join(str(l) for l in line) + '\n')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Creates report tsv file from VEP annotation for use in HTS-interpretation.')
    parser.add_argument('-i', required=True, help='VCF file', dest='input')
    parser.add_argument('--cnv', required=False, help='Input CNV file (*.annotated.filtered.tsv)', dest='cnv')
    parser.add_argument('--transcripts', required=True, help='Transcript file from genepanel', dest='transcripts')
    parser.add_argument('--phenotypes', help='Phenotype file from genepanel', dest='phenotypes')
    parser.add_argument('--excel', required=False, help='Output to excel file name', dest='excel')
    parser.add_argument('--tsv', required=False, help='Output to tsv file name', dest='tsv')
    parser.add_argument('--igv', required=False, help='Output to igv file name', dest='igv')
    parser.add_argument('--header', required=False, help='Header to display in excel report (must be single word)', dest='header')
    parser.add_argument('--coverage-worksheet', action='store_true', required=False, help='Include coverage worksheet in excel report', dest='coverage_worksheet')

    args = parser.parse_args()

    ssr = ExomeReport(
        args.input,
        args.transcripts,
        args.phenotypes,
        args.cnv
    )

    ssr.write(excel=args.excel, tsv=args.tsv, igv=args.igv, header=args.header, coverage_worksheet=args.coverage_worksheet)
