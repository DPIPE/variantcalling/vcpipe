#!/bin/env python

"""
Creates Excel/tsv reports from a vcf file.
As opposed to the Exome report, the vcf is not filtered
in this script, as the files are already prefiltered.

It uses VcfReader for the data parsing/converint,
picking out the fields to include in the report.
"""

import argparse
import logging
import re
import itertools
from collections import OrderedDict
from collections import defaultdict

from report.interpretation.vcfreader import VcfReader
from report.interpretation.cnv_reader import CnvReader
from report.interpretation.metadata import CHECKLIST, CHECKLIST_COMMENT, PHENOTYPE_CHECKLIST
from report.interpretation.report_common import HGVS_INTRON_RE, HGVS_UPSTREAM_ATG_RE, HGVS_5PRIME_INTRON_RE, HGVS_3PRIME_INTRON_RE
from report.interpretation.report_common import check_class_unclassified


logging.basicConfig(level=logging.INFO)
log = logging.getLogger('TrioReport')


class TrioReport(object):

    FILTER_QUALITATIVES = [
        ('Klasse', 'u')
    ]

    def __init__(self, vcf_path, transcripts_path, phenotypes_path, pedigree, cnv_path):
        """
        pedigree example:
            {
                "father": "NA12341",
                "mother": "NA14232",
                "proband": "NA12434"
            }
        """
        self.vcf_path = vcf_path
        self.transcripts_path = transcripts_path
        self.phenotypes_path = phenotypes_path
        self.excel_writer = None
        self.pedigree = pedigree
        self.cnv_path = cnv_path


    def get_cnv_rows(self):
        try:
            reader = CnvReader(self.cnv_path)
        except IOError as err:
            raise(err)

        # Fields to include and their order
        fields = [
            'chr',
            'start',
            'end',
            'cnv_state',
            'default_score',
            'len',
            'inDB_count',
            'inDBScore_MinMaxMedian',
            'gene_name',
            'gene_type',
            'gene_id',
            'exon_count',
            'UTR',
            'transcript',
            'phastConElement_count',
            'phastConElement_minMax',
            'haplo_insufIdx_count',
            'haplo_insufIdx_score',
            'Gene_intolarance_score',
            'sanger_cnv',
            'dgv_cnv',
            'dgv_varType',
            'dgv_varSubType',
            'dgv_pubmedId',
            'DGV_Stringency2_count',
            'DGV_Stringency2_PopFreq',
            'DGV_Stringency12_count',
            'DGV_Stringency12_popFreq',
            '1000g_del',
            '1000g_ins',
            'omim_morbidMap',
            'ddd_mutConsequence',
            'ddd_diseaseName',
            'ddd_pubmedId',
            'clinVar_disease',
            'hgvs_varName'
        ]

        for entry in reader.get_data():
            report_row = list()
            for field in fields:
                report_row.append((field, entry.get(field, 'N/A')))
            yield OrderedDict(report_row)

    @staticmethod
    def _filter_line(line):

        # Return True if the the variant is under the rules in FILTER_QUALITATIVES, e.g. the variant is a 'u' variant
        for name, crit in TrioReport.FILTER_QUALITATIVES:
            val = line[name]
            if val == 'N/A' or not isinstance(val, str):
                continue
            if val == crit:
                return True

        return False

    def get_rows(self):

        empty_fields = [
            'IGV',
            'ACMG kriterier',
            'Klasse',
            'Resultatvurdering kommentar',
            'Kontroll kommentar',
            'Variantvurdering'
        ]

        rename_fields = {
            'TRIO_TYPE': 'Type',
            'TRIO_DENOVO_P': 'Denovo (p)',
            'TRIO_ALT_CHILD_PCT': 'ALT Proband [%]',
            'TRIO_ALT_FATHER_PCT': 'ALT Father [%]',
            'TRIO_ALT_MOTHER_PCT': 'ALT Mother [%]',
        }

        # Fields to include and their order
        common_fields_start = [
            'amg_gene_panel',
            'extra_phenotypes',
            'CHR',
            'POS',
            'REF',
            'ALT',
            'Gene',
            'Inheritance',
            'Phenotype',
            'HGVSc',
            'HGVSp',
            'Consequence',
            'IGV',
            'Klasse',
            'Resultatvurdering kommentar',
            'Kontroll kommentar',
            'TRIO_TYPE',
        ]

        denovo_fields = [
            'TRIO_DENOVO_P',
            'TRIO_ALT_CHILD_PCT',
            'TRIO_ALT_MOTHER_PCT',
            'TRIO_ALT_FATHER_PCT',
        ]

        common_fields_end = [
            'GT_PROBAND',
            'GT_MOTHER',
            'GT_FATHER',
            'AF_OUSWES',
            'AN_OUSWES',
            'indications_OUSWES',
            'VCF_FILTER',
            'VCF_QUAL',
            'AlamutColumn',
            'GNOMAD_URL',
            'HGMD_URL',
            'CLINVAR_URL',
            'OMIM_URL',
            'AC_OUSWES',
            'Hom_OUSWES',
            'Het_OUSWES',
            'filter_OUSWES',
            'repeatMasker',
            'AD_PROBAND',
            'AD_MOTHER',
            'AD_FATHER',
            'DP_PROBAND',
            'DP_MOTHER',
            'DP_FATHER',
            'gnomAD_exomes_TOT',
            'gnomAD_exomes_NFE',
            'gnomAD_exomes_FIN',
            'gnomAD_exomes_AFR',
            'gnomAD_exomes_AMR',
            'gnomAD_exomes_EAS',
            'gnomAD_exomes_SAS',
            'gnomAD_exomes_OTH',
            'gnomAD_genomes_TOT',
            'gnomAD_genomes_NFE',
            'gnomAD_genomes_FIN',
            'gnomAD_genomes_AFR',
            'gnomAD_genomes_AMR',
            'gnomAD_genomes_EAS',
            'gnomAD_genomes_SAS',
            'gnomAD_genomes_OTH',
            '1000g',
            # following are columns that will be removed later
            'pseudogenes',
            'ACMG kriterier',
            'Variantvurdering',
            'ExAC_URL',
            'TRIO_Sanger_verify',
            'dbSNP',
            'ExAC_TOT',
            'ExAC_NFE',
            'ExAC_FIN',
            'ExAC_AFR',
            'ExAC_AMR',
            'ExAC_EAS',
            'ExAC_SAS',
            'ExAC_OTH'
        ]


        denovo_fields = common_fields_start + denovo_fields + common_fields_end
        recessive_fields = common_fields_start + common_fields_end
        special_genes_fields = common_fields_start + common_fields_end

        report_fields = [denovo_fields, recessive_fields, special_genes_fields]
        # [denovo_rows, recessive_rows]
        report_rows = [list(), list(), list()]
        report_rows_all = [list(), list()]


        report_check = [
            lambda x: x['Type'] and 'denovo' in x['Type'],
            lambda x: x['Type'] and 'recessive' in x['Type'],
            lambda x: x['Type'] and 'special' in x['Type'],  #any([sg in x['Gene'] for sg in self.special_genes])
        ]

        report_sort = [
            lambda x: (x['Inheritance'],
                       x['ALT Proband [%]'],
                       x['Gene']),  # sort for denovo variants
            lambda x: (x['Inheritance'],
                       x['Gene'],
                       x['Consequence'],
                       x['ExAC_TOT']),  # sort for recessive variants
            lambda x: (x['Inheritance'],
                       x['Consequence'],
                       x['gnomAD_genomes_TOT']),  # sort for special variants
            lambda x: (x['Inheritance'],
                       x['ALT Proband [%]'],
                       x['Gene']),  # sort for denovo variants - for all variants
            lambda x: (x['Inheritance'],
                       x['Gene'],
                       x['Consequence'],
                       x['ExAC_TOT'])  # sort for recessive variants - for all variants
        ]

        # Yes, reading twice is slower, but much easier to code...
        for fields, rows, check_func in zip(report_fields, report_rows, report_check):
            reader = VcfReader(self.vcf_path, self.transcripts_path, self.phenotypes_path)

            for entry in reader.get_data(include_trio=True, trio_pedigree=self.pedigree):
                report_row = list()
                for field in fields:
                    # Rename field if in map
                    fieldname = rename_fields.get(field, field)

                    # mark Klasse as 'u' if any rule is satisfied
                    if field == 'Klasse':
                        if check_class_unclassified(entry):
                            col = (fieldname, 'u') # report_row.append((field, 'u'))
                        else:
                            col = (fieldname, '') # report_row.append((field, ''))
                    elif field in empty_fields:
                        col = (fieldname, '') # report_row.append((field, ''))
                    else:
                        col = (fieldname, entry.get(field, 'N/A')) # report_row.append((field, entry.get(field, 'N/A')))
                    report_row.append(col)
                row_data = OrderedDict(report_row)

                # Mark 'klasse' as '?' if Klasse is not assiged as 'u' and ALT Proband [%] < 30 and Denovo (p) is 0
                if ('ALT Proband [%]' in row_data and row_data['ALT Proband [%]'] is not None and row_data['Klasse'] is not None and row_data['Klasse'] != 'u') and \
                   ('Denovo (p)' in row_data and row_data['Denovo (p)'] is not None):
                    alt_proband = row_data['ALT Proband [%]']
                    denovo_p = row_data['Denovo (p)']
                    # Try to convert to float, if not use as is
                    try:
                        denovo_p = float(denovo_p)
                    except ValueError:
                        pass
                    if (isinstance(alt_proband, (float, int)) and alt_proband < 30) and \
                       ((isinstance(denovo_p, (float, int)) and denovo_p < 0.000001) or denovo_p == '-'):
                        row_data['Klasse'] = '?'

                if check_func(row_data):
                    rows.append(row_data)

        report_rows_all[0] = list(report_rows[0])
        report_rows_all[1] = list(report_rows[1])

        # Remove 'u' from recessive and denovo
        def filter_denovo_u(r):
            if not TrioReport._filter_line(r):
                return True
            try:
                denovo_p = float(r['Denovo (p)'])
            except ValueError:
                denovo_p = r['Denovo (p)']

            if ((isinstance(denovo_p, (float, int)) and denovo_p >= 0.9) or denovo_p == '-') or r['HGMD_URL'] != 'N/A':
                # if the variant is an 'u' variant, but denovo_p >= 0.9 or it is annotated in HGMD, show variants
                return True
            else:
                return False

        report_rows[0] = list(filter(filter_denovo_u, report_rows[0]))
        report_rows[1] = [r for r in report_rows[1] if not TrioReport._filter_line(r)]

        # remove single compound heterzygosity from recessive_rows
        new_recessive = list()
        temp_recessive = defaultdict(dict)

        for each_recessive in report_rows[1]:
            if 'recessive' in each_recessive['Type']:
                gts = re.split(r'/|\|', each_recessive['GT_PROBAND'])

                # if the variant is homozygosity
                if gts[0] == gts[1]:
                    new_recessive.append(each_recessive)

                # if the variant is not homozygosity
                else:
                    # one variant could be mapped to several genes, get a list wihout any overlapping gene names. e.g. POLG,FANCI,FANCI to [POLG,FANCI]
                    gene_key = list(set(each_recessive['Gene'].split(',')))
                    pos_key = '-'.join([each_recessive['CHR'], str(each_recessive['POS'])])

                    # The structure in temp_recessive is gene_key, father/mother, pos_key
                    father_gts = re.split(r'/|\|', each_recessive['GT_FATHER'])
                    mother_gts = re.split(r'/|\|', each_recessive['GT_MOTHER'])

                    for each_p_gts in gts:
                        if each_p_gts != '0' and each_p_gts != '.':
                            for each_gene_key in gene_key:
                                if each_p_gts in father_gts:
                                    if 'father' not in list(temp_recessive[each_gene_key].keys()):
                                        temp_recessive[each_gene_key]['father'] = defaultdict(list)
                                    if each_recessive not in temp_recessive[each_gene_key]['father'][pos_key]:
                                        temp_recessive[each_gene_key]['father'][pos_key].append(each_recessive)

                                elif each_p_gts in mother_gts:
                                    if 'mother' not in list(temp_recessive[each_gene_key].keys()):
                                        temp_recessive[each_gene_key]['mother'] = defaultdict(list)
                                    if each_recessive not in temp_recessive[each_gene_key]['mother'][pos_key]:
                                        temp_recessive[each_gene_key]['mother'][pos_key].append(each_recessive)

        for each_gene in list(temp_recessive.keys()):
            if len(list(temp_recessive[each_gene].keys())) == 2:
                for parent in ['father', 'mother']:
                    for each_pos in list(temp_recessive[each_gene][parent].keys()):
                        for each_temp_recessive in temp_recessive[each_gene][parent][each_pos]:
                            if each_temp_recessive not in new_recessive:
                                new_recessive.append(each_temp_recessive)

        report_rows[1] = new_recessive
        report_rows.extend(report_rows_all)

        # report-rows: denovo_rows, recessive_rows, special_genes_rows, denovo_rows_all, recessive_rows_all
        for rows, sort_key in zip(report_rows, report_sort):
            rows.sort(key=sort_key)
        return report_rows

    def write(self, excel=None, igv=None, header=None, cnv=None):
        if not any(a for a in [excel, igv]):
            raise RuntimeError("You must at least provide one output type.")

        denovo_rows, recessive_rows, special_genes_rows, denovo_rows_all, recessive_rows_all = self.get_rows()

        if excel:
            # Lazy import to avoid unneccessary dependencies
            from report.interpretation.excelwriter import ExcelWriter, SectionedWorksheet
            from report.interpretation.custom_excel import VariantHelpWorksheet, VariantRowWriter, CnvRowWriter, ChecklistWriter, VersionWorksheet

            self.excel_writer = ExcelWriter(excel)

            denovo_all_ws = SectionedWorksheet('Denovo variants all', freeze_panes=(1, 6))
            denovo_all_ws.add_section(header, VariantRowWriter(denovo_rows_all, color_interpretation=True))

            recessive_all_ws = SectionedWorksheet('Recessive variants all', freeze_panes=(1, 6))
            recessive_all_ws.add_section(header, VariantRowWriter(recessive_rows_all, color_interpretation=True))

            denovo_ws = SectionedWorksheet('Denovo variants', active=True, freeze_panes=(1, 6))
            denovo_ws.add_section(header, VariantRowWriter(denovo_rows, color_interpretation=True))
            denovo_ws.add_section(None, ChecklistWriter(offset=6, table=CHECKLIST, comment=CHECKLIST_COMMENT))
            denovo_ws.add_section(None, ChecklistWriter(offset=6, table=PHENOTYPE_CHECKLIST, comment=[]))

            recessive_ws = SectionedWorksheet('Recessive variants', freeze_panes=(1, 6))
            recessive_ws.add_section(header, VariantRowWriter(recessive_rows, color_interpretation=True))

            special_ws = SectionedWorksheet('Special genes', freeze_panes=(1, 6))
            special_ws.add_section(header, VariantRowWriter(special_genes_rows, color_interpretation=True))

            self.excel_writer.add_worksheet(denovo_all_ws)
            self.excel_writer.add_worksheet(recessive_all_ws)
            self.excel_writer.add_worksheet(denovo_ws)
            self.excel_writer.add_worksheet(recessive_ws)
            self.excel_writer.add_worksheet(special_ws)

            if self.cnv_path:
                try:
                    cnv_ws = SectionedWorksheet('CNV', freeze_panes=(1, 6))
                    cnv_lines = sorted(self.get_cnv_rows(),
                                       key=lambda x: (x['cnv_state'], x['default_score']))
                    cnv_ws.add_section(header, CnvRowWriter(cnv_lines, color_interpretation=True))
                    self.excel_writer.add_worksheet(cnv_ws)
                except IOError as err:
                    pass

            self.excel_writer.add_worksheet(VariantHelpWorksheet("Help"))
            self.excel_writer.add_worksheet(VersionWorksheet("Version"))

            self.excel_writer.save()

        if igv:
            igv_data = list()
            for row in itertools.chain(denovo_rows, recessive_rows):
                row_data = [
                    row['CHR'],
                    row['POS']-1,
                    row['POS'],
                    row['Gene'],
                ]
                if row_data not in igv_data:
                    igv_data.append(row_data)

            igv_data.sort(key=lambda x: x[3])
            with open(igv, 'w') as igv_fd:
                for line in igv_data:
                    igv_fd.write('\t'.join(str(l) for l in line) + '\n')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Creates report tsv file from VEP annotation for use in HTS-interpretation.')
    parser.add_argument('-i', required=True, help='VCF file', dest='input')
    parser.add_argument('--transcripts', required=True, help='Transcript file from genepanel', dest='transcripts')
    parser.add_argument('--phenotypes', help='Phenotype file from genepanel', dest='phenotypes')
    parser.add_argument('--cnv', required=False, help='Input CNV file (*.annotated.filtered.tsv)', dest='cnv')
    parser.add_argument('--excel', required=False, help='Output to excel file name', dest='excel')
    parser.add_argument('--igv', required=False, help='Output to igv file name', dest='igv')
    parser.add_argument('--header', nargs='+', required=False, help='Header to display in excel report', dest='header')
    parser.add_argument('--father', required=True, help='Sample ID of the father in the VCF', dest='father')
    parser.add_argument('--mother', required=True, help='Sample ID of the mother in the VCF', dest='mother')
    parser.add_argument('--proband', required=True, help='Sample ID of the proband in the VCF', dest='proband')

    args = parser.parse_args()

    tr = TrioReport(
        args.input,
        args.transcripts,
        args.phenotypes,
        {
            "father": args.father,
            "mother": args.mother,
            "proband": args.proband
        },
        args.cnv
    )

    header = ''
    if args.header:
        header = ' '.join(args.header)
    tr.write(excel=args.excel, igv=args.igv, header=header)
