import random
import os
import argparse

HEADER = [
    'CHR',
    'START',
    'STOP',
    'GENE',
    'REGION_COV',
    'AVG_AUTOSOMAL_COV',
    'AVG_TOTAL_COV',
    'AVG_GENE_COV',
    'NORMALIZED_AUTOSOMAL',
    'NORMALIZED_TOTAL',
    'NORMALIZED_GENE'
]

def generate_test_data(regions, num_files, output):

    region_list = []
    with open(regions) as f:
        for line in f:
            parts = line.strip().split('\t')
            region_list.append({
                'chr': parts[0],
                'start': parts[1],
                'stop': parts[2],
                'gene': parts[3]
            })

    assert region_list

    os.makedirs(output)

    for idx in range(1, num_files):
        print("Generating TestSample{}".format(idx))
        auto_total_cov = dict()
        gene_cov = dict()
        with open(os.path.join(output, 'TestSample{}.normalized.coverage.txt'.format(idx)), 'w') as f:
            f.write('\t'.join(HEADER))
            for r in region_list:
                f.write('\n')
                if r['gene'] not in auto_total_cov:
                    auto_total_cov[r['gene']] = 700.0 + 700.0 * random.uniform(0.01, 0.2)
                if r['gene'] not in gene_cov:
                    gene_cov[r['gene']] = 700.0 + 700.0 * random.uniform(0.01, 0.2)
                data = [
                    r['chr'],
                    r['start'],
                    r['stop'],
                    r['gene'],
                    600.0 + 600.0 * random.uniform(0.01, 0.2),  # REGION_COV
                    auto_total_cov[r['gene']],  # AVG_AUTOSOMAL_COV
                    auto_total_cov[r['gene']],  # AVG_TOTAL_COV,
                    gene_cov[r['gene']], # AVG_GENE_COV
                    random.uniform(0.9, 1.1),  # NORMALIZED_AUTOSOMAL
                    random.uniform(0.9, 1.1),  # NORMALIZED_TOTAL
                    random.uniform(0.9, 1.1),  # NORMALIZED_GENE
                ]
                f.write('\t'.join([str(d) for d in data]))



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Creates fake background data for CoNVaDING from a region bed file.')
    parser.add_argument('-i', required=True, help='.bed file', dest='regions')
    parser.add_argument('-n', help='Number of samples desired', type=int, dest='num_samples', default=30)
    parser.add_argument('-o', required=True, help='Path to output directory', dest='output')

    args = parser.parse_args()
    generate_test_data(args.regions, args.num_samples, args.output)
