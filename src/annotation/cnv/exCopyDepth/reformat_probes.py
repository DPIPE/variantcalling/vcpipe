import sys
import re

with open(sys.argv[1]) as f:
    print('chr\tstart\tstop\tname')
    for line in f:
        if line.startswith('chr'):
            continue
        chr, start, stop, name = line.split('\t')
        name = set([l.split('|')[1] for l in re.split(',|;', name) if l.split('|')[0]=='ref'])
        l1 = {l for l in name if l.startswith('NR_')}
        l2 = {l for l in name if l.startswith('NM_')}
        l3 = name - l1 - l2
        name = list(l3) + list(l2) + list(l1)
        name = name[0].strip() if name else ''
        print('\t'.join([chr, start, stop, name]))

