#!/bin/bash -ue

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# extract chrM reads from the bam file
gatk PrintReads \
    -R ${BUNDLE_REFERENCE} \
    -L MT \
    --read-filter MateOnSameContigOrNoMappedMateReadFilter \
    --read-filter MateUnmappedAndUnmappedReadFilter \
    -I ${BAM} \
    --read-index ${BAI} \
    -O chrMT.bam

# Remove XQ tag in the Dragen bam file
samtools view -h chrMT.bam |sed 's/\tXQ\:i\:[^0-9]*//' > chrMT.reformat.sam
mv chrMT.reformat.sam chrMT.sam

# Picard RevertSam
picard RevertSam \
    INPUT=chrMT.sam \
    OUTPUT_BY_READGROUP=false \
    OUTPUT=revertSam.bam \
    VALIDATION_STRINGENCY=LENIENT \
    ATTRIBUTE_TO_CLEAR=FT ATTRIBUTE_TO_CLEAR=CO \
    SORT_ORDER=queryname \
    RESTORE_ORIGINAL_QUALITIES=false

# Align to MT
picard SamToFastq \
    INPUT=revertSam.bam \
    FASTQ=/dev/stdout \
    INTERLEAVE=true \
    NON_PF=true | bwa-mem2 mem -K 100000000 -p -v 3 -t 2 -Y ${BUNDLE_REFERENCE_MT} /dev/stdin - > temp.bam

BWA_VERSION=$(bwa-mem2 version)

picard MergeBamAlignment \
    VALIDATION_STRINGENCY=SILENT \
    EXPECTED_ORIENTATIONS=FR \
    ATTRIBUTES_TO_RETAIN=X0 ATTRIBUTES_TO_REMOVE=NM ATTRIBUTES_TO_REMOVE=MD \
    ALIGNED_BAM=temp.bam \
    UNMAPPED_BAM=revertSam.bam \
    OUTPUT=mba.bam \
    REFERENCE_SEQUENCE=${BUNDLE_REFERENCE_MT} \
    PAIRED_RUN=true \
    SORT_ORDER="unsorted" \
    IS_BISULFITE_SEQUENCE=false \
    ALIGNED_READS_ONLY=false \
    CLIP_ADAPTERS=false \
    MAX_RECORDS_IN_RAM=2000000 \
    ADD_MATE_CIGAR=true \
    MAX_INSERTIONS_OR_DELETIONS=-1 \
    PRIMARY_ALIGNMENT_STRATEGY=MostDistant \
    PROGRAM_GROUP_VERSION=${BWA_VERSION} \
    PROGRAM_RECORD_ID="bwamem2" \
    PROGRAM_GROUP_COMMAND_LINE="bwamem2" \
    PROGRAM_GROUP_NAME="bwamem2" \
    UNMAPPED_READ_STRATEGY=COPY_TO_TAG \
    ALIGNER_PROPER_PAIR_FLAGS=true \
    UNMAP_CONTAMINANT_READS=true ADD_PG_TAG_TO_READS=false

picard MarkDuplicates \
    INPUT=mba.bam \
    OUTPUT=md.bam \
    METRICS_FILE=${ANALYSIS}.mt.metrics \
    VALIDATION_STRINGENCY=SILENT \
    READ_NAME_REGEX="(?:.*:)?([0-9]+)[^:]*:([0-9]+)[^:]*:([0-9]+)[^:]*:[^:]+\$" \
    OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500 \
    ASSUME_SORT_ORDER="queryname" \
    CLEAR_DT='false' \
    ADD_PG_TAG_TO_READS=false

picard SortSam \
    INPUT=md.bam \
    OUTPUT=${ANALYSIS}.alignedMT.bam \
    SORT_ORDER="coordinate" \
    CREATE_INDEX=true \
    MAX_RECORDS_IN_RAM=300000

# Align to MT shifted
picard SamToFastq \
    INPUT=revertSam.bam \
    FASTQ=/dev/stdout INTERLEAVE=true NON_PF=true | bwa-mem2 mem -K 100000000 -p -v 3 -t 2 -Y ${BUNDLE_REFERENCE_MT_SHIFTED} /dev/stdin - > temp.bam

picard MergeBamAlignment \
    VALIDATION_STRINGENCY=SILENT \
    EXPECTED_ORIENTATIONS=FR \
    ATTRIBUTES_TO_RETAIN=X0 ATTRIBUTES_TO_REMOVE=NM ATTRIBUTES_TO_REMOVE=MD \
    ALIGNED_BAM=temp.bam \
    UNMAPPED_BAM=revertSam.bam \
    OUTPUT=mba.bam \
    REFERENCE_SEQUENCE=${BUNDLE_REFERENCE_MT_SHIFTED} \
    PAIRED_RUN=true \
    SORT_ORDER="unsorted" \
    IS_BISULFITE_SEQUENCE=false \
    ALIGNED_READS_ONLY=false \
    CLIP_ADAPTERS=false \
    MAX_RECORDS_IN_RAM=2000000 \
    ADD_MATE_CIGAR=true \
    MAX_INSERTIONS_OR_DELETIONS=-1 \
    PRIMARY_ALIGNMENT_STRATEGY=MostDistant \
    PROGRAM_GROUP_VERSION=${BWA_VERSION} \
    PROGRAM_RECORD_ID="bwamem2" \
    PROGRAM_GROUP_COMMAND_LINE="bwamem2" \
    PROGRAM_GROUP_NAME="bwamem2" \
    UNMAPPED_READ_STRATEGY=COPY_TO_TAG \
    ALIGNER_PROPER_PAIR_FLAGS=true \
    UNMAP_CONTAMINANT_READS=true \
    ADD_PG_TAG_TO_READS=false

picard MarkDuplicates \
    INPUT=mba.bam \
    OUTPUT=md.bam \
    METRICS_FILE=${ANALYSIS}.mtshifted.metrics \
    VALIDATION_STRINGENCY=SILENT \
    READ_NAME_REGEX="(?:.*:)?([0-9]+)[^:]*:([0-9]+)[^:]*:([0-9]+)[^:]*:[^:]+\$" \
    OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500 \
    ASSUME_SORT_ORDER="queryname" \
    CLEAR_DT='false' \
    ADD_PG_TAG_TO_READS=false

picard SortSam \
    INPUT=md.bam \
    OUTPUT=${ANALYSIS}.alignedMTshifted.bam \
    SORT_ORDER="coordinate" \
    CREATE_INDEX=true \
    MAX_RECORDS_IN_RAM=300000

# CollectWgsMetrics on bam on MT reference
picard CollectWgsMetrics \
    INPUT=${ANALYSIS}.alignedMT.bam \
    VALIDATION_STRINGENCY=SILENT \
    REFERENCE_SEQUENCE=${BUNDLE_REFERENCE_MT} \
    OUTPUT=${ANALYSIS}.CollectWgsMetrics.metrics \
    USE_FAST_ALGORITHM=true \
    READ_LENGTH=151 \
    COVERAGE_CAP=100000 \
    INCLUDE_BQ_HISTOGRAM=true \
    THEORETICAL_SENSITIVITY_OUTPUT=theoretical_sensitivity.mt.txt

# variant calling on MT
gatk Mutect2 \
    -R ${BUNDLE_REFERENCE_MT} \
    -I ${ANALYSIS}.alignedMT.bam \
    -O mt.vcf \
    --bam-output ${ANALYSIS}.bamout.mt.bam \
    -L chrM:576-16024 \
    --annotation StrandBiasBySample \
    --mitochondria-mode \
    --max-reads-per-alignment-start 75 \
    --max-mnp-distance 0

# variant calling on MT shifted
gatk Mutect2 \
    -R ${BUNDLE_REFERENCE_MT_SHIFTED} \
    -I ${ANALYSIS}.alignedMTshifted.bam \
    -O mtShifted.vcf \
    --bam-output ${ANALYSIS}.bamout.mtShifted.bam \
    -L chrM:8025-9144 \
    --annotation StrandBiasBySample \
    --mitochondria-mode \
    --max-reads-per-alignment-start 75 \
    --max-mnp-distance 0

# Liftover and CombineVcfs
picard LiftoverVcf \
    I=mtShifted.vcf \
    O=mtShifted.shifted_back.vcf \
    R=${BUNDLE_REFERENCE_MT} \
    CHAIN=${BUNDLE_MT_SHIFT_BACK_CHAIN} \
    REJECT=mtShifted.rejected.vcf

# the raw vcf needs sample ID in
picard MergeVcfs \
    I=mtShifted.shifted_back.vcf \
    I=mt.vcf \
    O=${ANALYSIS}.chrMT.raw.vcf

# MergeStats (note: the tool name in gatk is MergeMutectStats)
gatk MergeMutectStats \
    --stats mtShifted.vcf.stats \
    --stats mt.vcf.stats \
    -O raw.combined.vcf.stats 

# Filter
gatk FilterMutectCalls \
    -V ${ANALYSIS}.chrMT.raw.vcf \
    -R ${BUNDLE_REFERENCE_MT} \
    -O all.initialFiltered.vcf \
    --stats raw.combined.vcf.stats \
    --max-alt-allele-count 4 \
    --mitochondria-mode \
    --min-allele-fraction 0 \
    --contamination-estimate 0 

gatk VariantFiltration \
    -V all.initialFiltered.vcf \
    -O all.initialFiltered.variantFiltration.vcf \
    --apply-allele-specific-filters \
    --mask ${BLACKLIST} \
    --mask-name "blacklisted_site"

gatk LeftAlignAndTrimVariants \
    -R ${BUNDLE_REFERENCE_MT} \
    -V all.initialFiltered.variantFiltration.vcf \
    -O split.vcf \
    --split-multi-allelics \
    --dont-trim-alleles \
    --keep-original-ac

gatk SelectVariants \
    -V split.vcf \
    -O splitAndPassOnly.vcf \
    --exclude-filtered

AUTOSOME_COV=$(cat ${QC_REPORT}| python3 -c "import sys, json; print(json.load(sys.stdin)['MosdepthMetrics'][1]['metrics']['MEDIAN_COVERAGE'])")

echo ${AUTOSOME_COV}

gatk NuMTFilterTool \
    -R ${BUNDLE_REFERENCE_MT} \
    -V all.initialFiltered.variantFiltration.vcf \
    -O all.initialFiltered.variantFiltration.numtfiltertool.vcf \
    --autosomal-coverage ${AUTOSOME_COV}

gatk MTLowHeteroplasmyFilterTool \
    -R ${BUNDLE_REFERENCE_MT} \
    -V all.initialFiltered.variantFiltration.numtfiltertool.vcf \
    -O ${ANALYSIS}.chrMT.filter.vcf.gz \
    --create-output-variant-index true \
    --max-allowed-low-hets 3

# check contamination
haplocheck --raw --out=${ANALYSIS}.haplocheck.out ${ANALYSIS}.chrMT.filter.vcf.gz

# predict haplogroup
haplogrep classify --in ${ANALYSIS}.chrMT.filter.vcf.gz --format vcf --extend-report --out ${ANALYSIS}.haplogrep.out

python3 ${SCRIPT_DIR}/convertHaplogrep2VCF.py --haplogrep-result ${ANALYSIS}.haplogrep.out --output haplogroup.vcf --reference ${BUNDLE_REFERENCE_MT}

gatk IndexFeatureFile \
    --input haplogroup.vcf

gatk VariantAnnotator \
    -R ${BUNDLE_REFERENCE_MT} \
    -V ${ANALYSIS}.chrMT.filter.vcf.gz \
    -O ${ANALYSIS}.chrMT.final.vcf.gz \
    --resource:haplogroup  haplogroup.vcf \
    --expression haplogroup.HAPLOGROUP \
    --expression haplogroup.HAPLOGROUP_MARKER \
    --resource-allele-concordance

# Parsing coverage metrics data
python3 ${SCRIPT_DIR}/generateMTqcjson.py --mt_cov_input ${ANALYSIS}.CollectWgsMetrics.metrics --mt_qc_output ${ANALYSIS}.mt.qc_result.json
