""" Add chrMT MEDIAN_COVERAGE into a json file

The script parsing out MEDIAN_COVERAGE from chrMT Picard CollectWgsMetrics output into a json file. All the metric information will be under data["CollectWgsMetrics"][1]["MT"], e.g the MEDIAN_COVERAGE will be under data["CollectWgsMetrics"][1]["MT"]["MEDIAN_COVERAGE"].

"""

import argparse
import json
import os

parser = argparse.ArgumentParser(description="Parse out chrMT coverage and write into a json file")
parser.add_argument(
    "--mt_qc_output",
    help="Output file for mtDNA coverage qc_result json",
    dest="mt_qc_json",
    required=True,
)
parser.add_argument(
    "--mt_cov_input",
    help="mtDNA CollectWgsMetrics results",
    dest="metrics_file",
    required=True,
)
args = parser.parse_args()

with open(args.metrics_file) as fd:
    lines = fd.readlines()

idx_metrix = None
idx_histogram = None
for i, l in enumerate(lines):
    if l.startswith("## METRICS CLASS"):
        idx_metrix = i
    elif l.startswith("## HISTOGRAM"):
        idx_histogram = i
        break

if idx_metrix is None:
    raise RuntimeError("Couldn't find data tag for {}".format(self.name))

tsv_data = "".join(lines[idx_metrix + 1 : idx_histogram - 1])
metrics_data = dict(list(zip(tsv_data.split("\n")[0].split("\t"), tsv_data.split("\n")[1].split("\t"))))

data = dict()
data["CollectWgsMetrics"] = list()
if float(metrics_data["MEDIAN_COVERAGE"]) > 3000:
    data["CollectWgsMetrics"].append("True")
else:
    data["CollectWgsMetrics"].append("False")

data["CollectWgsMetrics"].append(dict())
data["CollectWgsMetrics"][1]["MT"] = metrics_data
data["CollectWgsMetrics"].append({})

# print information into the json file
output_json = open(args.mt_qc_json, "w")
output_json.write(json.dumps(data, indent=4))
