""" Haplogrep result converter

This script will convert the information (73G 263G 709A...) in Found_Polys column from Haplogrep results into a vcf file. The result vcf file can be used to annotate other vcf files, e.g. variant calling file from mitochondrial variant calling pipeline.

The "Found_Polys" column contains SNP markders which are used to define the sample's haplogroup. The markser is in the format e.g. '73G', which means that the sample has 'G' at the position chrM:73. When converting these markers into VCF format, each marker will be one VCF entry. The haplogroup information will be written in the INFO tag "HAPLOGROUP" for each marker line. And the original notation in "Found_Polys" will be written in the INFO tag "HAPLOGROUP_MARKER".

Since there is no reference base information for each marker, a reference genome is required to parse this information out.
"""

import os
import argparse
import subprocess
import re

parser = argparse.ArgumentParser(description="Convert Haplogrep results Found_Polys to a vcf file")
parser.add_argument(
    "--haplogrep-result",
    help="The haplogrep results generated with --extend-report option",
    dest="input",
    required=True,
)
parser.add_argument("--output", help="The output VCF file", dest="output", required=True)
parser.add_argument("--reference", help="The reference genome", dest="reference", required=True)
args = parser.parse_args()

# Get reference genome
with open(args.reference) as r:
    lines = r.read().splitlines()
lines.pop(0)
chrm = "".join(lines)

with open(args.input) as i:
    lines = i.read().splitlines()
lines.pop(0)

haplogroup = lines[0].split("\t")[1].replace('"', "")

with open(args.output, "w") as output_vcf:
    # print vcf headers
    output_vcf.write("##fileformat=VCFv4.2\n")
    output_vcf.write(
        '##INFO=<ID=HAPLOGROUP,Number=1,Type=String,Description="Haplogroup prediction from Haplogrep">\n'
    )
    output_vcf.write(
        '##INFO=<ID=HAPLOGROUP_MARKER,Number=1,Type=String,Description="Haplogroup markers used to define the haplogroup by Haplogrep">\n'
    )
    output_vcf.write("\t".join(["#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO\n"]))

    for mark in lines[0].split("\t")[6].replace('"', "").split(" "):
        # 16192T! means: ! at the end of a labeled position denotes a reversion to the ancestral or original state
        # in this case, ignor this site
        if mark.endswith("!"):
            continue

        mark_info = re.split("(\d+)", mark)
        ref = chrm[int(mark_info[1]) - 1 : int(mark_info[1])]

        output_vcf.write(
            "\t".join(
                [
                    "chrM",
                    mark_info[1],
                    ".",
                    ref,
                    mark_info[2],
                    ".",
                    ".",
                    f"HAPLOGROUP_MARKER={mark};HAPLOGROUP={haplogroup}\n",
                ]
            )
        )

output_vcf.close()
