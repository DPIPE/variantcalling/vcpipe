#!/usr/bin/env python

import os
import json
import logging
import glob
import re
from settings import settings

log = logging.getLogger(__name__)


def dependency(analysis_config_path):
    with open(analysis_config_path) as fd:
        analysis_config = json.load(fd)

    sample = analysis_config['samples'][0]
    sample_project_name = '-'.join(sample.split('-')[:2])
    base_sample_path = settings['base_sample_path']
    ready_path = os.path.join(
        base_sample_path,
        sample,
        'READY'
    )

    if not os.path.isfile(ready_path):
        log.error("Dependencies not met for sample '{}', file not found: '{}'".format(sample, ready_path))
        return False

    return True


if __name__ == '__main__':

    logging.basicConfig(format='basepipe-dependency: %(levelname)s: %(message)s', level=logging.INFO)

    import argparse
    parser = argparse.ArgumentParser(description='Dependency check for annopipe')
    parser.add_argument('--config', required=True, help='Path to .analysis config file (e.g. `Diag-Target12-123456899-KREFT17-v01.analysis`)', dest='analysis_config_path')
   
    args = parser.parse_args()

    dependency(**vars(args))
