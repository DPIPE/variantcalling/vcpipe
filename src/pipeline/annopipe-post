#!/usr/bin/env python3

import os
import json
import logging
import glob
import shutil
import subprocess
from settings import settings
from pipeline.util.check_qc_result import check_qc_result

log = logging.getLogger(__name__)


def postcmd(*, export_priority, platform, analysis_name, result_path, **kwargs):

    ella_path = settings['ella_incoming']
    dot_analysis_file = os.path.join(settings['base_analysis_path'], analysis_name, analysis_name + '.analysis')
    rename_dot_analysis = analysis_name + '.annopipe'

    if platform == 'NSC':
        # replace {priority_subdir} with priority level for NSC pipeline
        PRIORITY_MAP = {'3': 'urgent', '2': 'high', '1': 'normal'}
        ella_path = ella_path.replace(
            '{priority_subdir}', PRIORITY_MAP[str(export_priority)])

    log.info("Copying files to ELLA incoming folder.")

    # copy data, exclude READY file(s)
    subprocess.check_call(['rsync', '-av', '--omit-dir-times', '--no-g', '--no-perms', '--exclude', 'READY', result_path, ella_path])
    # copy .analysis file, can be removed when using Steps
    subprocess.check_call(['cp', dot_analysis_file, os.path.join(ella_path, analysis_name, rename_dot_analysis)])
    # copy possible READY file(s)
    subprocess.check_call(['rsync', '-av', '--omit-dir-times', '--no-g', '--no-perms', '--ignore-existing', result_path, ella_path])


if __name__ == '__main__':

    logging.basicConfig(format='postcmd: %(levelname)s: %(message)s', level=logging.INFO)

    import argparse
    parser = argparse.ArgumentParser(description='Post analysis processing for pipeline.')
    parser.add_argument('--export_priority', required=False, help='Export priority to decide which one to transfer first', dest='export_priority')
    parser.add_argument('--platform', required=False, help='The platform running the pipeline', dest='platform')
    parser.add_argument('--analysis', required=True, help='Name of analysis (e.g. Diag-excap45-123456899)', dest='analysis_name')
    parser.add_argument('--result', required=True, help='Path to pipeline result directory', dest='result_path')

    args = parser.parse_args()

    postcmd(**vars(args))
