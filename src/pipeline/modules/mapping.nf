params.mapping_storage_path = ''

params.analysis_name = 'unnamed_analysis'
params.capture_kit = 'wgs'

params.bam_header = ''

include { progress } from './util'

process bwa_mem {
    input:
    tuple path(read1), path(read2)
    val index_basename

    output:
    tuple path('sorted.bam'), path('sorted.bam.bai'), emit: bam_and_bai

    script:
    progress(10, task.process)
    """
    bwa-mem2 mem \
        -R '${params.bam_header}' \
        -v 2 \
        -M \
        -t ${task.cpus} \
        -Y ${index_basename} \
        ${read1} ${read2} | \
    samtools view -@ ${task.cpus} -Sb - | \
    samtools sort -@ ${task.cpus} -o sorted.bam -T sorted -

    samtools index -@ ${task.cpus} sorted.bam
    """
}

process mark_duplicates {
    // in case 'output.name' differs from params.analysis_name we don't publish bam/bai:
    publishDir params.mapping_storage_path, overwrite: true, pattern: "${params.analysis_name}.{bam,bai}"
    // always publish the metrics file (using the analysis name prefix):
    publishDir params.mapping_storage_path, overwrite: true, pattern: "${output_name}.markduplicates.metrics", saveAs: { "${params.analysis_name}.markduplicates.metrics" }

    input:
    tuple path(bam_file), path(bai_file)
    val output_name

    output:
    tuple path("${output_name}.bam"), path("${output_name}.bai"), emit: bam_and_bai
    path("${output_name}.markduplicates.metrics"), emit: metrics

    script:
    progress(10, task.process)
    """
    picard MarkDuplicates \
        -INPUT $bam_file \
        -OUTPUT "${output_name}.bam" \
        -METRICS_FILE "${output_name}.markduplicates.metrics" \
        -CREATE_INDEX true \
        -CREATE_MD5_FILE true \
        -VALIDATION_STRINGENCY STRICT
    """
}

process base_recalibrator {
    publishDir params.mapping_storage_path, overwrite: true

    input:
    path bam_file
    val reference
    path intervals_slop50
    val known_sites

    output:
    path("${params.analysis_name}.recal_data.table"), emit: recalibration_table

    script:
    progress(10, task.process)
    def known_sites_arg = ""
    for (String known_site : known_sites) {
        known_sites_arg += " --known-sites " + known_site
    }
    """
    # Picard-style formatted intervals files require a .interval_list extension (TODO: change file extension in refdata)
    cp ${intervals_slop50} calling_region.interval_list

    gatk BaseRecalibrator \
        -I ${bam_file} \
        -R ${reference} \
        -L calling_region.interval_list \
        ${known_sites_arg} \
        -O ${params.analysis_name}.recal_data.table
    """
}

process apply_bqsr {
    publishDir params.mapping_storage_path, overwrite: true, enabled: true

    input:
    path bam_file
    val reference
    path recalibration_table

    output:
    tuple path("${params.analysis_name}.bam"), path("${params.analysis_name}.bai"), emit: bam_and_bai

    script:
    progress(10, task.process)
    """
    ls -la ${recalibration_table}
    gatk ApplyBQSR \
        -I ${bam_file} \
        -R ${reference} \
        --bqsr-recal-file ${recalibration_table} \
        --static-quantized-quals 10 \
        --static-quantized-quals 20 \
        --static-quantized-quals 30 \
	    --create-output-bam-md5 true \
        -O ${params.analysis_name}.bam
    """
}


workflow mapping {
    take:
    reads
    index_basename
    reference
    intervals_slop50
    known_sites

    main:
    bwa_mem(
        reads,
        index_basename,
    )
    if ( ['wgs', 'CuCaV1', 'CuCaV2', 'CuCaV3', 'TSCarV1'].contains(params.capture_kit) ) {
        mark_duplicates(
            bwa_mem.out.bam_and_bai,
            params.analysis_name,
        )
        bam_and_bai = mark_duplicates.out.bam_and_bai
    } else {
        // trick to prevent publishing bam/bai files from the mark_duplicates process:
        mark_duplicates(
            bwa_mem.out.bam_and_bai,
            'to_recalibration',
        )
        bam_file = mark_duplicates.out.bam_and_bai.map{it[0]}
        base_recalibrator(
            bam_file,
            reference,
            intervals_slop50,
            known_sites,
        )
        apply_bqsr(
            bam_file,
            reference,
            base_recalibrator.out.recalibration_table,
        )
        bam_and_bai = apply_bqsr.out.bam_and_bai
    }

    emit:
    bam_and_bai
}
