params.variantcalling_storage_path = ''
params.qc_storage_path = ''

params.analysis_name = 'unnamed_analysis'
params.analysis_base = 'unnamed_analysis'
params.sample_name = 'unnamed_sample'

params.test_settings = 'integration'

include { progress } from '../util'

process haplotype_caller_fingerprinting {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    tuple path(bam_file), path(bai_file)
    val dbsnp
    val reference
    path taqman

    output:
    path("${params.analysis_name}.fingerprint.g.vcf"), emit: fingerprinting_vcf

    script:
    progress(10, task.process)
    if ( params.containsKey('test_settings') && params.test_settings == 'integration' ) {
        filter_cmd = 'grep -E $"^1\t183086757.*"'
    } else {
        filter_cmd = 'cat'
    }
    """
    ${filter_cmd} ${taqman} | awk '{print \$1":"\$2"-"\$2}' > region.intervals

    gatk HaplotypeCaller \
        -R ${reference} \
        -I ${bam_file} \
        --dbsnp ${dbsnp} \
        --emit-ref-confidence BP_RESOLUTION \
        -L region.intervals \
	--contamination-fraction-to-filter 0.05 \
        -O "${params.analysis_name}.fingerprint.g.vcf"
    """
}

process fingerprinting {
    publishDir params.variantcalling_storage_path, overwrite: true, pattern: "${params.analysis_name}.fingerprinting_output.txt", enabled: true
    publishDir params.qc_storage_path, overwrite: true, pattern: "*.qc_result.json", enabled: true

    stageInMode 'copy'

    input:
    path fingerprinting_vcf
    path qc_result
    path taqman

    output:
    path("${params.analysis_name}.fingerprinting_output.txt")
    path(qc_result)
    val('fingerprinting_done'), emit: done

    script:
    progress(10, task.process)
    if ( params.containsKey('test_settings') && params.test_settings == 'integration' ) {
        snp_reqs = '1,0,0'
    } else {
        snp_reqs = '23,8,1 16,4,1'
    }
    """
    qc_fingerprinting \
        --vcf ${fingerprinting_vcf} \
        --taq ${taqman} \
        --snp_reqs ${snp_reqs} \
        --output "${params.analysis_name}.fingerprinting_output.txt" \
        --json fingerprinting_output.json

    python3 <<< "
import json
with open('${qc_result}', 'r') as f:
    qc_result = json.load(f)
with open('fingerprinting_output.json', 'r') as f:
    fingerprinting_output = json.load(f)
qc_result.update(fingerprinting_output)
with open('${qc_result}', 'w') as f:
    json.dump(qc_result, f, sort_keys=True, indent=4, separators=(',', ': '))
"
    """
}
