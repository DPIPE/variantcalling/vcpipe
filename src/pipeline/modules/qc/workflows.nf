params.specialized_pipeline = ''
params.capture_kit = 'wgs'

/*
COVERAGE:
*/
include {
    mosdepth ;
    bigwig ;
} from './coverage'

workflow qc_coverage {
    take:
    bam_and_bai
    reference

    main:
    mosdepth(bam_and_bai, reference)
    bigwig(mosdepth.out.regions)

    emit:
    coverage = mosdepth.out.coverage
    done = bigwig.out.done
}


/*
QC REPORT:
*/
include {
    as_metrics ;
    is_metrics ;
    hs_metrics ;
    qy_metrics ;
    qc_prepare_vcf ;
    qc_analysis ;
    qc_report ;
} from './report'

workflow quality_control {
    take:
    bam_and_bai
    reference
    final_vcf
    coverage
    dragen_mapping_metrics
    noslop_list
    noslop_bed
    contamination_test

    main:
    as_metrics_out = Channel.fromPath("NO_FILE_AS")
    is_metrics_out = Channel.fromPath("NO_FILE_IS")
    hs_metrics_out = Channel.fromPath("NO_FILE_HS")
    qy_metrics_out = Channel.fromPath("NO_FILE_QY")

    if ( params.specialized_pipeline != 'Dragen' ) {
        as_metrics(
            bam_and_bai,
            reference,
        )
        as_metrics_out = as_metrics.out.as_metrics
        is_metrics(
            bam_and_bai,
            reference,
        )
        is_metrics_out = is_metrics.out.is_metrics
        qy_metrics(
            bam_and_bai
        )
        qy_metrics_out = qy_metrics.out.qy_metrics
    }

    if ( params.capture_kit != 'wgs' ) {
           hs_metrics(
              bam_and_bai,
              noslop_list,
              reference,
           )
           hs_metrics_out = hs_metrics.out.hs_metrics
           vcf_file = final_vcf
    }

    // before: the logic was inside basepipe.nf's basepipe_qc_prepare_vcf:
    if ( params.capture_kit != 'wgs' ) {
        qc_prepare_vcf(
            final_vcf,
            noslop_bed,
        )
        vcf_file = qc_prepare_vcf.out.qc_prepared_vcf
    } else {
       vcf_file = final_vcf
    }

    qc_analysis(
        bam_and_bai,
        vcf_file,
        coverage,
        as_metrics_out,
        is_metrics_out,
        hs_metrics_out,
        qy_metrics_out,
        dragen_mapping_metrics,
        reference,
        noslop_list.ifEmpty(''),
        contamination_test.ifEmpty(''),
    )

    qc_report(
        qc_analysis.out.qc_result,
        file('fake'),
        'single',
    )

    emit:
    qc_result = qc_analysis.out.qc_result
    done = qc_report.out.done
}


/*
QC REPORT TRIO:
*/
include { gender_check } from './pedigree'
include { qc_analysis_trio } from './report'

workflow quality_control_trio {
    take:
    final_vcf
    ped_file
    proband_qc_json
    mother_qc_json
    father_qc_json
    proband_sample_name
    mother_sample_name
    father_sample_name

    main:
    gender_check(
        final_vcf,
        ped_file,
    )
    qc_analysis_trio(
        gender_check.out.vcfped_json,
        proband_qc_json,
        mother_qc_json,
        father_qc_json,
        proband_sample_name,
        mother_sample_name,
        father_sample_name,
    )

    qc_report(
        qc_analysis_trio.out.qc_result,
        ped_file,
        'trio',
    )

    emit:
    qc_result = qc_analysis_trio.out.qc_result
    done = qc_report.out.done
}


/*
FINGERPRINTING:
*/
include {
    haplotype_caller_fingerprinting ;
    fingerprinting ;
} from './fingerprinting'

workflow qc_fingerprinting {
    take:
    bam_and_bai
    qc_result
    reference
    dbsnp
    taqman

    main:
    haplotype_caller_fingerprinting(
        bam_and_bai,
        dbsnp,
        reference,
        taqman,
    )
    fingerprinting(
        haplotype_caller_fingerprinting.out.fingerprinting_vcf,
        qc_result,
        taqman,
    )

    emit:
    done = fingerprinting.out.done
}


/*
REGRESSION:
*/
include {
    happy_target_regions ;
    happy ;
} from './regression'

workflow regression {
    take:
    final_vcf
    calling_region
    noslop_bed
    hc_region
    truth_vcf
    happy_reference

    main:
    if ( params.capture_kit == 'wgs' || params.specialized_pipeline == 'TUMOR' ) {
        target_region = hc_region
    } else {
        happy_target_regions(
            calling_region,
            noslop_bed,
            hc_region,
        )
        target_region = happy_target_regions.out.target_region
    }
    happy(
        target_region,
        final_vcf,
        truth_vcf,
        happy_reference,
    )

    emit:
    done = happy.out.done
}