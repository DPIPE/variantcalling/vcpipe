params.qc_storage_path = ''

params.analysis_name = 'unnamed_analysis'

include { progress } from '../util'

process happy_target_regions {
    input:
    path calling_region
    path noslop_bed
    path hc_region

    output:
    path('target_region.bed'), emit: target_region

    script:
    progress(10, task.process)
    """
    bedtools multiinter -i ${hc_region} \
        ${noslop_bed} \
        <(grep -v ^@ ${calling_region}) \
        | perl -lane 'print join("\t",@F[0..2]) if (\$F[3] > 2)' \
    > target_region.bed
    """
}

process happy {
    publishDir "${params.qc_storage_path}", overwrite: true, enabled: true

    input:
    path target_region
    path final_vcf
    path truth_vcf
    val happy_reference

    output:
    path("high_conf_intersect.bed") optional true // Optional for WGS samples
    path("*.runinfo.json")
    path("*.summary.csv")
    path("*.vcf.gz")
    val('happy_done'), emit: done

    script:
    progress(10, task.process)
    """
    hap.py \
        ${truth_vcf} \
        ${final_vcf} \
        --target-regions ${target_region} \
        --reference "${happy_reference}/human_g1k_v37_decoy_modified.fasta" \
        --threads=${task.cpus} \
        -o ${params.analysis_name} \
        --write-vcf
    """
}
