params.cnv_storage_path = ''
params.qc_storage_path = ''

params.analysis_name = 'unnamed_sample'

include { progress } from '../util'

process mosdepth {
    publishDir params.cnv_storage_path, overwrite: true, pattern: "*.{txt}", enabled: true

    input:
    tuple path(bam_file), path(bai_file)
    val reference

    output:
    tuple path("${params.analysis_name}.mosdepth.global.dist.txt"), path("${params.analysis_name}.mosdepth.summary.txt"), emit: coverage
    path("${params.analysis_name}.regions.bed.gz"), emit: regions

    script:
    progress(10, task.process)
    """
    mosdepth \
        -t ${task.cpus} \
        --fast-mode \
        -b 50 \
        -f ${reference} \
        -F 0 \
        -Q 20 \
        ${params.analysis_name} \
        ${bam_file}
    """
}

process bigwig {
    publishDir params.cnv_storage_path, overwrite: true, enabled: true

    input:
    path regions

    output:
    path("${params.analysis_name}.bigWig")
    val('bigwig_done'), emit: done

    script:
    progress(10, task.process)
    """
    zgrep -E "^[1-9XY]" ${regions} | \
        awk '{printf "%s\\t%d\\t%d\\t%2.3f\\n" , \$1,\$2,\$3,\$4}' \
    > "${params.analysis_name}.bedGraph"

    sort \
        -T ./ \
        -k1,1 -k2,2n \
        "${params.analysis_name}.bedGraph" \
    > "${params.analysis_name}.sorted.bedGraph"

    bedGraphToBigWig \
        "${params.analysis_name}.sorted.bedGraph" \
        /filters/human.genome \
        "${params.analysis_name}.bigWig"
    """
}
