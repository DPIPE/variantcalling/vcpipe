params.qc_storage_path = ''
params.analysis_name = 'unnamed_analysis'
params.sample_id = 'unnamed_sample'
params.specialized_pipeline = ''
params.capture_kit = 'wgs'

params.qc_key = params.specialized_pipeline == 'Dragen' ? 'qc_dragen' : 'qc'
params.is_dragen = params.specialized_pipeline == 'Dragen' ? true : false

include { progress } from '../util'

process as_metrics {
    publishDir params.qc_storage_path, overwrite: true, enabled: true

    input:
    tuple path(bam_file), path(bai_file)
    val reference

    output:
    path("CollectAlignmentSummaryMetrics.txt"), emit: as_metrics
    path("*.{txt,pdf,log}")

    script:
    progress(10, task.process)
    """
    picard CollectAlignmentSummaryMetrics \
        R=${reference} \
        I=${bam_file} \
        O=CollectAlignmentSummaryMetrics.txt \
        MAX_INSERT_SIZE=600
    """
}

process is_metrics {
    publishDir params.qc_storage_path, overwrite: true, enabled: true

    input:
    tuple path(bam_file), path(bai_file)
    val reference

    output:
    path("CollectInsertSizeMetrics.txt"), emit: is_metrics
    path("*.{txt,pdf,log}")

    script:
    progress(10, task.process)
    """
    picard CollectInsertSizeMetrics \
        R=${reference} \
        I=${bam_file} \
        O=CollectInsertSizeMetrics.txt \
        HISTOGRAM_FILE=CollectInsertSizeMetrics-histogram.pdf
    """
}

process hs_metrics {
    publishDir params.qc_storage_path, overwrite: true, enabled: true

    input:
    tuple path(bam_file), path(bai_file)
    val noslop_list
    val reference

    output:
    path("CollectHsMetrics.txt"), emit: hs_metrics
    path("*.{txt,pdf,log}")

    script:
    progress(10, task.process)
    """
    picard CollectHsMetrics \
        R=${reference} \
        I=${bam_file} \
        O=CollectHsMetrics.txt \
        BAIT_INTERVALS=${noslop_list} \
        TARGET_INTERVALS=${noslop_list} \
        PER_TARGET_COVERAGE=CollectHsMetrics-per-target-coverage.txt
    """
}

process qy_metrics {
    publishDir params.qc_storage_path, overwrite: true, enabled: true

    input:
    tuple path(bam_file), path(bai_file)

    output:
    path("CollectQualityYieldMetrics.txt", emit: qy_metrics)

    script:
    progress(10, task.process)
    """
    picard CollectQualityYieldMetrics \
        I=${bam_file} \
        O=CollectQualityYieldMetrics.txt
    """
}

process qc_prepare_vcf {
    publishDir params.qc_storage_path, overwrite: true, enabled: true

    input:
    path final_vcf
    val noslop_bed

    output:
    path("variants.for_qc.vcf"), emit: qc_prepared_vcf

    script:
    """
    bedtools intersect \
        -header \
        -wa \
        -a ${final_vcf} \
        -b ${noslop_bed} \
        -u \
    > variants.for_qc.vcf
    """
}

process qc_analysis {
    publishDir params.qc_storage_path, overwrite: true, enabled: true

    input:
    tuple path(bam_file), path(bai_file)
    path vcf_file
    tuple path(coverage_file), path(coverage_summary)
    path alignment_metrics_file // might be dummy empty
    path insert_size_metrics_file // might be dummy empty
    path hs_metrics_file // might be dummy empty
    path qy_metrics_file // might be dummy empty
    path dragen_mapping_metrics // might be dummy empty
    val reference
    val noslop_list
    val contamination_test

    output:
    path("${params.analysis_name}.qc_result.json"), emit: qc_result

    script:
    progress(10, task.process)

    options = "--dragen-mapping-metrics ${dragen_mapping_metrics}"
    options += " --hs-metrics ${hs_metrics_file}"
    options += " --alignment-summary-metrics ${alignment_metrics_file}"
    options += " --insert-size-metrics ${insert_size_metrics_file}"
    options += " --qy-metrics ${qy_metrics_file}"
    if ( params.specialized_pipeline == 'TUMOR' ) {
        options += " --vcf variants.noslop.reformat.vcf "
        options += " --capturekit ${params.capture_kit}_${params.specialized_pipeline} "
    } else {
        options += " --vcf ${vcf_file} "
        options += " --capturekit ${params.capture_kit} "
    }
    if ( params.capture_kit == 'wgs' ) {
        options += " --mosdepth-coverage ${coverage_file} "
        options += " --mosdepth-summary ${coverage_summary} "
    } else {
        options += " --regions ${noslop_list} "
    }
    if ( ['CuCaV1', 'CuCaV2', 'CuCaV3', 'TSCarV1'].contains(params.capture_kit) ) {
        options += " --contamination_sitefile ${contamination_test} "
    }
    """
    if [[ ${params.specialized_pipeline} == 'TUMOR' ]]
    then
      convertMutect --vcf ${vcf_file} > variants.noslop.reformat.vcf
    fi

     qc_analysis \
        --output ${params.qc_storage_path} \
        --refgenome ${reference} \
        --pipeline basepipe \
        --bam ${bam_file} \
        --qc-key ${params.qc_key} \
        ${options} \
    > ${params.analysis_name}.qc_result.json
    """
}

process qc_analysis_trio {
    publishDir params.qc_storage_path, overwrite: true, enabled: true

    input:
    path(vcfped_json)
    path(proband_qc_json)
    path(mother_qc_json)
    path(father_qc_json)
    val(proband_sample_id)
    val(mother_sample_id)
    val(father_sample_id)

    output:
    path("${params.analysis_name}.qc_result.json"), emit: qc_result

    script:
    progress(10, task.process)
    """
    #!/usr/bin/env python3
    import json
    from collections import OrderedDict

    with open('${vcfped_json}', 'r') as fin:
        gender_test = json.load(fin, object_pairs_hook=OrderedDict)
    with open('${proband_qc_json}', 'r') as fin:
        proband = json.load(fin, object_pairs_hook=OrderedDict)
    with open('${mother_qc_json}', 'r') as fin:
        mother = json.load(fin, object_pairs_hook=OrderedDict)
    with open('${father_qc_json}', 'r') as fin:
        father = json.load(fin, object_pairs_hook=OrderedDict)

    qc = OrderedDict({
        'samples': {
            '${proband_sample_id}': proband,
            '${mother_sample_id}': mother,
            '${father_sample_id}': father
        },
        'filtus-gender-test': gender_test
    })

    with open('${params.analysis_name}.qc_result.json', 'w') as fout:
        json.dump(
            qc,
            fout,
            sort_keys=False,
            indent=4,
            separators=(',', ': '),
            ensure_ascii=False,
        )
    """
}

process qc_report {
    publishDir params.qc_storage_path, overwrite: true, enabled: true

    input:
    path qc_result
    path ped_file
    val type

    output:
    path("${params.analysis_name}.qc_report.md")
    path("${params.analysis_name}.qc_warnings.md") optional true
    val('report_done'), emit: done

	script:
	progress(10, task.process)
	if ( type == 'single' ) {
	    cmd_append = "--sample-id ${params.sample_id}"
	} else if ( type == 'trio' ) {
	    cmd_append = "--ped-file ${ped_file} --sample-id ${params.analysis_name}"
	}
	"""
	qc_report_markdown \
		--report-file "${params.analysis_name}.qc_report.md" \
		--warnings-file "${params.analysis_name}.qc_warnings.md" \
		--qc-result ${qc_result} \
		--type ${type} \
		--is-dragen ${params.is_dragen} \
        ${cmd_append}
	"""
}
