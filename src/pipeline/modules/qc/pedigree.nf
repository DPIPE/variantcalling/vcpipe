params.qc_storage_path = ''

params.analysis_name = 'unnamed_analysis'

include { progress } from '../util'

process gender_check {
    publishDir "${params.qc_storage_path}", overwrite: true, enabled: true

    input:
    path final_vcf
    path ped_file

    output:
    path("${params.analysis_name}.vcfped_test.json"), emit: vcfped_json
    path("*.vcfped.{gender,log,pair,trio}")

    script:
    progress(10, task.process)
    vcfped_cmd = "vcfped ${final_vcf} -ped=${ped_file} -s 50000 -d 2500 -e 300000 -male 10 --all -o ${params.analysis_name}"
    """
    ${vcfped_cmd}

    grep -A1 '====COMPARISON WITH PEDIGREE FILE====' "${params.analysis_name}.log" | \
        tail -1 | \
        grep '^OK\$' 2>&1 \
    >/dev/null || ${vcfped_cmd}

    qc_vcfped_test \
        --vcfped-log-file "${params.analysis_name}.log" \
        --vcfped-gender-file "${params.analysis_name}.gender" \
        --vcfped-pair-file "${params.analysis_name}.pair" \
        --vcfped-trio-file "${params.analysis_name}.trio" \
        --no-exception \
        --output "${params.analysis_name}.vcfped_test.json"

    mv "${params.analysis_name}.gender" "${params.analysis_name}.vcfped.gender"
    mv "${params.analysis_name}.log" "${params.analysis_name}.vcfped.log"
    mv "${params.analysis_name}.pair" "${params.analysis_name}.vcfped.pair"
    mv "${params.analysis_name}.trio" "${params.analysis_name}.vcfped.trio"
    """
}
