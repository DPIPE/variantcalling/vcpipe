process trio_pedigree_file {
    publishDir "${params.output}/data", overwrite: true, pattern: "${analysis_name}.ped", enabled: true

    input:
    val analysis_name
    val proband_sample_name
    val proband_gender
    val mother_sample_name
    val father_sample_name

    output:
    path("${analysis_name}.ped"), emit: pedigree

    script:
    """
    #!/usr/bin/env python3
    ped_gender = {"male":"1", "female":"2"}
    with open("${analysis_name}.ped", "w") as pedfile:
        pedfile.write(f"${analysis_name} ${proband_sample_name} ${father_sample_name} ${mother_sample_name} {ped_gender['${proband_gender}']} 2\\n")
        pedfile.write(f"${analysis_name} ${father_sample_name} 0 0 {ped_gender['${'male'}']} 1\\n")
        pedfile.write(f"${analysis_name} ${mother_sample_name} 0 0 {ped_gender['${'female'}']} 1\\n")
    """
}