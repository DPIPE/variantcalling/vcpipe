params.dummy_storage_path = 'output-dummy-nf'

include { progress } from './util'

process dummy {
    echo true
    publishDir "${workDir}/${params.dummy_storage_path}", overwrite: true, enabled: true

    output:
    path("dummy-out.txt")

    script:
    progress(20, task.process)
    """
    echo "this is process ${task.process}"
    touch dummy-out.txt
    """
}
