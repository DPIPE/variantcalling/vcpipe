params.variantcalling_storage_path = ''

params.analysis_name = 'unnamed_analysis'
params.capture_kit = 'wgs'

include { progress } from '../util'

process mutect2 {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true
    tag "$calling_region"

    input:
    tuple path(bam_file), path(bai_file)
    val calling_region
    val germline_resource
    val panel_of_normal
    val reference

    output:
    tuple path("${params.analysis_name}${output_suffix}.raw.vcf.gz"), path("${params.analysis_name}${output_suffix}.raw.vcf.gz.tbi"), emit: raw_vcf_and_tbi
    path("${params.analysis_name}${output_suffix}.raw.vcf.gz.stats"), emit: raw_vcf_table
    path('f1r2.tar.gz'), emit: f1r2
    path("${params.analysis_name}.debug.bam")

    script:
    progress(10, task.process)
    if ( params.capture_kit == 'wgs' ) {
        output_suffix = ".chr{calling_region}"
        intervals = calling_region
    } else {
        output_suffix = ''
        intervals = 'calling_region.interval_list'
    }
    """
    # Picard-style formatted intervals files require a .interval_list extension (TODO: change file extension in refdata)
    if [[ ${params.capture_kit} != 'wgs' ]]
    then
      cp ${calling_region} ${intervals}
    fi

    gatk Mutect2 \
        -R ${reference} \
        --germline-resource ${germline_resource} \
        --panel-of-normals ${panel_of_normal} \
        --genotype-germline-sites true \
        --genotype-pon-sites true \
        -I ${bam_file} \
        -tumor ${params.analysis_name} \
        -L ${intervals} \
        --bamout "${params.analysis_name}.debug.bam" \
        --f1r2-tar-gz f1r2.tar.gz \
        -O "${params.analysis_name}${output_suffix}.raw.vcf.gz"
    """
}

process collect_sequencing_artifact_metrics {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    tuple path(bam_file), path(bai_file)
    val reference

    output:
    path("${params.analysis_name}.CollectSequencingArtifactMetrics.*.table")

    script:
    progress(10, task.process)
    """
    picard CollectSequencingArtifactMetrics \
        I=${bam_file} \
        R=${reference} \
        O="${params.analysis_name}.CollectSequencingArtifactMetrics" \
        FILE_EXTENSION=".table" \
        VALIDATION_STRINGENCY=LENIENT
    """
}

process get_pileup_summaries {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    tuple path(bam_file), path(bai_file)
    val variants_for_contamination
    val reference

    output:
    path("${params.analysis_name}.GetPileupSummaries.table"), emit: pileup_summaries

    script:
    progress(10, task.process)
    """
    gatk GetPileupSummaries \
        -R ${reference} \
        -I ${bam_file} \
        -V ${variants_for_contamination} \
        -L ${variants_for_contamination} \
        -O "${params.analysis_name}.GetPileupSummaries.table"
    """
}

process calculate_contamination {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    path pileup_summaries

    output:
    path("${params.analysis_name}.CalculateContamination.table"), emit: contamination_table
    path("${params.analysis_name}.segments.table"), emit: segments_table

    script:
    progress(10, task.process)
    """
    gatk CalculateContamination \
        -I "${pileup_summaries}" \
        -O "${params.analysis_name}.CalculateContamination.table" \
        --tumor-segmentation "${params.analysis_name}.segments.table"
    """
}

process learn_read_orientation_model {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    path f1r2

    output:
    path('artifact-prior.tar.gz'), emit: artifact_prior

    script:
    progress(10, task.process)
    """
    gatk LearnReadOrientationModel \
        -I ${f1r2} \
        -O artifact-prior.tar.gz
    """
}

process filter_mutect_calls {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    tuple path(raw_vcf), path(raw_vcf_index)
    path raw_vcf_table
    path segments_table
    path artifact_prior
    val calling_region
    val reference

    output:
    path("${params.analysis_name}${output_suffix}.final.vcf"), emit: final_vcf

    script:
    progress(10, task.process)
    if ( params.capture_kit == 'wgs' ) {
        output_suffix = ".chr{calling_region}"
        intervals = calling_region
    } else {
        output_suffix = ''
        intervals = 'calling_region.interval_list'
    }
    """
    gatk FilterMutectCalls \
        -R ${reference} \
        -V ${raw_vcf} \
        -O "${params.analysis_name}${output_suffix}.final.vcf" \
        --tumor-segmentation ${segments_table} \
        --orientation-bias-artifact-priors ${artifact_prior}
    """
}
