params.mt_variantcalling_storage_path = ''

params.analysis_name = 'unnamed_analysis'
params.refdata_home = ''
params.vcpipe_dir = ''

include { progress } from '../util'

process mt_variantcalling {
    publishDir params.mt_variantcalling_storage_path, overwrite: true, enabled: true
    containerOptions "--bind ${params.refdata_home},${params.vcpipe_dir}/src/annotation/mt-dna:/mtdna-pipeline/"

    input:
    tuple path(bam_file), path(bai_file)
    path qc_result
    val reference
    val reference_mt
    val reference_mt_shifted
    val mt_shift_back_chain
    val blacklist
    val blacklist_shifted

    output:
    path("${params.analysis_name}.alignedMT.bam")
    path("${params.analysis_name}.alignedMTshifted.bam")
    path("${params.analysis_name}.mt.metrics")
    path("${params.analysis_name}.mtshifted.metrics")
    path("${params.analysis_name}.CollectWgsMetrics.metrics")
    path("${params.analysis_name}.chrMT.raw.vcf")
    path("${params.analysis_name}.chrMT.final.vcf.gz")
    path("${params.analysis_name}.chrMT.final.vcf.gz.tbi")
    path("${params.analysis_name}.chrMT.filter.vcf.gz")
    path("${params.analysis_name}.chrMT.filter.vcf.gz.tbi")
    path("${params.analysis_name}.bamout.mt.bam")
    path("${params.analysis_name}.bamout.mtShifted.bam")
    path("${params.analysis_name}.haplogrep.out")
    path("${params.analysis_name}.haplocheck.*")
    path("${params.analysis_name}.mt.qc_result.json")
    val('mt_variantcalling_done'), emit: done

    stub:
    """
    echo "Stubbing"
    touch ${params.analysis_name}.alignedMT.bam
    touch ${params.analysis_name}.alignedMTshifted.bam
    touch ${params.analysis_name}.mt.metrics
    touch ${params.analysis_name}.mtshifted.metrics
    touch ${params.analysis_name}.CollectWgsMetrics.metrics
    touch ${params.analysis_name}.chrMT.raw.vcf
    touch ${params.analysis_name}.chrMT.final.vcf.gz
    touch ${params.analysis_name}.chrMT.final.vcf.gz.tbi
    touch ${params.analysis_name}.chrMT.filter.vcf.gz
    touch ${params.analysis_name}.chrMT.filter.vcf.gz.tbi
    touch ${params.analysis_name}.bamout.mt.bam
    touch ${params.analysis_name}.bamout.mtShifted.bam
    touch ${params.analysis_name}.haplogrep.out
    touch ${params.analysis_name}.haplocheck.out
    touch ${params.analysis_name}.mt.qc_result.json
    """

    script:
    progress(10, task.process)
    """
    # The following environment variables can also be defined using --env in the containerOptions directive of the process
	export ANALYSIS=${params.analysis_name}
	export BAM=${bam_file}
	export BAI=${bai_file}
	export QC_REPORT=${qc_result}
	export BUNDLE_REFERENCE=${reference}
	export BUNDLE_REFERENCE_MT=${reference_mt}
	export BUNDLE_REFERENCE_MT_SHIFTED=${reference_mt_shifted}
	export BUNDLE_MT_SHIFT_BACK_CHAIN=${mt_shift_back_chain}
	export BLACKLIST=${blacklist}
    # The BLACKLIST_SHIFTED isn't used in mtDNA-vc-pipeline.bash 
	export BLACKLIST_SHIFTED=${blacklist_shifted}

	/bin/bash /mtdna-pipeline/mtDNA-vc-pipeline.bash
    """
}

process mt_variantcalling_trio {
    publishDir params.mt_variantcalling_storage_path, overwrite: true, enabled: true

    input:
    path mtvcf_tuple
    path mtvcf_tbi_tuple

    output:
    path("${params.analysis_name}.all.filter.chrMT.vcf")
    val('mt_variantcalling_trio_done'), emit: done

    stub:
    """
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen-trio/*.all.filter.chrMT.vcf ${params.analysis_name}.all.filter.chrMT.vcf
    """

    script:
    progress(10, task.process)
    def fix_filter_status_command = ''
    def mtvcf_list = ''
    for ( String mtvcf : mtvcf_tuple ) {
        filename = mtvcf.split('/').last()
        new_filename = filename.replace('.gz', '')
        // Fixed "AS_FilterStatus uses an incorrect number of fields" problem by changing Number=A to Number=.
        // See detail: https://github.com/broadinstitute/gatk/issues/6857
        fix_filter_status_command += "bgzip -d ${filename}\nsed -i \'s/AS_FilterStatus,Number=A/AS_FilterStatus,Number=\\./\' ${new_filename}\n"
        fix_filter_status_command += "bgzip -c ${new_filename} > ${filename}\ntabix -p vcf ${filename}\n"
        mtvcf_list += " ${filename}"
    }

    """
    ${fix_filter_status_command}

    bcftools merge -o "${params.analysis_name}.all.filter.chrMT.vcf" ${mtvcf_list}
    """
}
