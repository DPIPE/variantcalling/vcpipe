params.capture_kit = 'wgs'


/*
GERMLINE VARIANT CALLING:

A process can be invoked multiple times with different parameters only when
it is included from a module under different aliases.
*/
include {
    haplotype_caller ;
    combine_gvcfs ;
    genotype_gvcfs ;
    select_variants as select_variants_snp ;
    select_variants as select_variants_indel ;
    select_variants as select_variants_mixed ;
    variant_recalibrator as variant_recalibrator_snp ;
    variant_recalibrator as variant_recalibrator_indel ;
    apply_vqsr as apply_vqsr_indel ;
    apply_vqsr as apply_vqsr_snp ;
    variant_filtration as variant_filtration_snp ;
    variant_filtration as variant_filtration_indel ;
    merge_vcfs ;
    depth_of_coverage ;
    msh2_variants ;
    sort_vcf ;
    md5_sum ;
} from './germline'

workflow vc_germline {
    take:
    bam_and_bai
    reference
    calling_regions
    dbsnp
    hapmap
    thousandgenomes_omni
    thousandgenomes_phase1
    mills

    main:
    haplotype_caller(
        bam_and_bai,
        calling_regions,
        dbsnp,
        reference,
    )
    combine_gvcfs(
        haplotype_caller.out.vcf.collect(),
        haplotype_caller.out.vcf_tbi.collect(),
        reference,
    )
    genotype_gvcfs(
        combine_gvcfs.out.combined_gvcf_and_tbi,
        dbsnp,
        reference,
    )
    select_variants_snp(
        genotype_gvcfs.out.raw_vcf_and_tbi,
        reference,
        'SNP',
    )
    select_variants_indel(
        genotype_gvcfs.out.raw_vcf_and_tbi,
        reference,
        'INDEL',
    )
    select_variants_mixed(
        genotype_gvcfs.out.raw_vcf_and_tbi,
        reference,
        'MIXED',
    )

    if ( params.capture_kit == 'wgs' ) {
        variant_recalibrator_snp(
            select_variants_snp.out.selected_vcf,
            dbsnp,
            hapmap,
            thousandgenomes_omni,
            thousandgenomes_phase1,
            mills,
            reference,
            'SNP',
        )
        variant_recalibrator_indel(
            select_variants_indel.out.selected_vcf,
            dbsnp,
            hapmap,
            thousandgenomes_omni,
            thousandgenomes_phase1,
            mills,
            reference,
            'INDEL',
        )
        filtered_snp = apply_vqsr_snp(
            select_variants_snp.out.selected_vcf,
            variant_recalibrator_snp.out.tranches,
            variant_recalibrator_snp.out.recal,
            reference,
            'SNP',
        )
        filtered_indel = apply_vqsr_indel(
            select_variants_indel.out.selected_vcf,
            variant_recalibrator_indel.out.tranches,
            variant_recalibrator_indel.out.recal,
            reference,
            'INDEL',
        )
    } else {
        filtered_snp = variant_filtration_snp(
            select_variants_snp.out.selected_vcf,
            reference,
            'SNP',
        )
        filtered_indel = variant_filtration_indel(
            select_variants_indel.out.selected_vcf,
            reference,
            'INDEL',
        )
    }

    merge_vcfs(
        filtered_snp,
        filtered_indel,
        select_variants_mixed.out.selected_vcf,
        reference,
    )

    depth_of_coverage(
        bam_and_bai,
        calling_regions,
        reference,
    )
    msh2_variants(
        merge_vcfs.out.merged_vcf,
        depth_of_coverage.out.depth,
    )
    sort_vcf(
        msh2_variants.out.msh2_vcf,
    )
    final_vcf = sort_vcf.out.final_vcf

    md5_sum(
        final_vcf,
    )

    emit:
    final_vcf
}


/*
TRIO VARIANT CALLING:
*/
include {
    genotype_gvcfs_ped ;
} from './germline'

workflow vc_trio {
    take:
    gvcf
    gvcf_tbi
    reference
    dbsnp
    hapmap
    thousandgenomes_omni
    thousandgenomes_phase1
    mills

    main:
    combine_gvcfs(
        gvcf,
        gvcf_tbi,
        reference,
    )
    genotype_gvcfs(
        combine_gvcfs.out.combined_gvcf_and_tbi,
        dbsnp,
        reference,
    )
    select_variants_snp(
        genotype_gvcfs.out.raw_vcf_and_tbi,
        reference,
        'SNP',
    )
    select_variants_indel(
        genotype_gvcfs.out.raw_vcf_and_tbi,
        reference,
        'INDEL',
    )
    select_variants_mixed(
        genotype_gvcfs.out.raw_vcf_and_tbi,
        reference,
        'MIXED',
    )

    if ( params.capture_kit == 'wgs' ) {
        variant_recalibrator_snp(
            select_variants_snp.out.selected_vcf,
            dbsnp,
            hapmap,
            thousandgenomes_omni,
            thousandgenomes_phase1,
            mills,
            reference,
            'SNP',
            )
        variant_recalibrator_indel(
            select_variants_indel.out.selected_vcf,
            dbsnp,
            hapmap,
            thousandgenomes_omni,
            thousandgenomes_phase1,
            mills,
            reference,
            'INDEL',
            )
        filtered_snp = apply_vqsr_snp(
            select_variants_snp.out.selected_vcf,
            variant_recalibrator_snp.out.tranches,
            variant_recalibrator_snp.out.recal,
            reference,
            'SNP',
            )
        filtered_indel = apply_vqsr_indel(
            select_variants_indel.out.selected_vcf,
            variant_recalibrator_indel.out.tranches,
            variant_recalibrator_indel.out.recal,
            reference,
            'INDEL',
            )
    } else {
        filtered_snp = variant_filtration_snp(
            select_variants_snp.out.selected_vcf,
            reference,
            'SNP',
            )
        filtered_indel = variant_filtration_indel(
            select_variants_indel.out.selected_vcf,
            reference,
            'INDEL',
            )
    }

    merge_vcfs(
        filtered_snp,
        filtered_indel,
        select_variants_mixed.out.selected_vcf,
        reference,
    )

    md5_sum(
        merge_vcfs.out.merged_vcf,
    )

    emit:
    final_vcf = merge_vcfs.out.merged_vcf
}


/*
SOMATIC VARIANT CALLING:
*/
include {
    mutect2 ;
    collect_sequencing_artifact_metrics ;
    get_pileup_summaries ;
    calculate_contamination ;
    learn_read_orientation_model ;
    filter_mutect_calls ;
} from './somatic'

workflow vc_somatic {
    take:
    bam_and_bai
    reference
    calling_regions
    germline_resource
    panel_of_normal
    variants_for_contamination

    main:
    mutect2(
        bam_and_bai,
        calling_regions,
        germline_resource,
        panel_of_normal,
        reference,
    )
    collect_sequencing_artifact_metrics(
        bam_and_bai,
        reference,
    )
    get_pileup_summaries(
        bam_and_bai,
        variants_for_contamination,
        reference,
    )
    calculate_contamination(
        get_pileup_summaries.out.pileup_summaries,
    )
    learn_read_orientation_model(
        mutect2.out.f1r2,
    )
    filter_mutect_calls(
        mutect2.out.raw_vcf_and_tbi,
        mutect2.out.raw_vcf_table,
        calculate_contamination.out.segments_table,
        learn_read_orientation_model.out.artifact_prior,
        calling_regions,
        reference,
    )

    depth_of_coverage(
        bam_and_bai,
        calling_regions,
        reference,
    )

    msh2_variants(
        filter_mutect_calls.out.final_vcf,
        depth_of_coverage.out.depth,
    )

    sort_vcf(
        msh2_variants.out.msh2_vcf,
    )

    final_vcf = sort_vcf.out.final_vcf

    md5_sum(
        final_vcf,
    )

    emit:
    final_vcf

}


/*
SV & CNV VARIANT CALLING:
*/
include {
    excopydepth ;
    convading ;
    convading_to_vcf ;
} from './sv_cnv'

workflow vc_sv_cnv {
    take:
    bam_and_bai
    index_basename
    reference
    excopydepth_background
    convading_regions
    convading_background

    main:
    if ( params.capture_kit == 'wgs' ) {
        // No CNV calling for 'wgs' outside of Dragen
        done = Channel.of('done')
    } else if ( ['agilent_sureselect_v05', 'agilent_sureselect_v07', 'twist_humancoreexome_v01'].contains(params.capture_kit) ) {
        excopydepth(
            bam_and_bai,
            excopydepth_background,
        )
        done = excopydepth.out.done
    } else if ( ['CuCaV2', 'CuCaV3'].contains(params.capture_kit) ) {
        convading(
            bam_and_bai,
            convading_regions,
            convading_background,
        )
        convading_to_vcf(
            convading.out.convading_lists,
        )
        done = convading_to_vcf.out.done
    } else {
     done = Channel.of('done')
    }

    emit:
    done
}
