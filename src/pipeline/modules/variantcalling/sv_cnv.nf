params.cnv_storage_path = ''

params.analysis_name = 'unnamed_analysis'

params.sensitive_db = ''
params.vcpipe_dir = ''
params.test_settings = 'integration'

include { progress } from '../util'


process svdb_merge_dragen {
    publishDir params.cnv_storage_path, overwrite: true, enabled: true

    input:
    path(manta_vcf)
    path(canvas_vcf)

    output:
    path("manta_${params.analysis_name}_std.vcf")
    path("canvas_${params.analysis_name}_std.vcf")
    path("merged_${params.analysis_name}.vcf")
    val('svdb_done'), emit: done

    script:
    progress(10, task.process)
    """
    sv_standardizer --caller manta --sample ${params.analysis_name} ${manta_vcf} > "manta_${params.analysis_name}_std.vcf"
    sv_standardizer --caller canvas --sample ${params.analysis_name} ${canvas_vcf} > "canvas_${params.analysis_name}_std.vcf"

    svdb \
        --merge \
        --same_order \
        --pass_only \
        --overlap 0.7 \
        --bnd_distance 2500 \
        --notag \
        --vcf \
            "manta_${params.analysis_name}_std.vcf" \
            "canvas_${params.analysis_name}_std.vcf" \
    > "merged_${params.analysis_name}_pre.vcf"

    sv_standardizer --caller merged "merged_${params.analysis_name}_pre.vcf" > "merged_${params.analysis_name}.vcf"
    """
}


process excopydepth {
    publishDir params.cnv_storage_path, overwrite: true, enabled: true
    containerOptions "--bind ${params.sensitive_db},${params.vcpipe_dir}/src/annotation/cnv/exCopyDepth:/exCopyDepth/"

    input:
    tuple path(bam_file), path(bai_file)
    val background

    output:
    path("${params.analysis_name}.cnv.raw.bed")
    path("${params.analysis_name}.cnv.bed")
    val('excopydepth_done'), emit: done

    script:
    progress(10, task.process)
    """
    Rscript /exCopyDepth/cnv.R ${bam_file} \
        --background ${background} \
        --output "${params.analysis_name}.cnv.raw.bed"

    # Reformat for cnvScan
    awk 'BEGIN \
    {
        OFS = "\\t"
        cnv = ""
    }
    {
        if (\$1 != "start.p") {
            # Remove the chrX and chrY CNVs
            if (\$7 !~ /^X/ && \$7 !~ /^Y/){
                if (\$3 == "deletion") {cnv = 1}
                else if (\$3 == "duplication") {cnv = 3}
                split(\$8, a, ":")
                split(a[2], b, "-")
                print substr(a[1], 4), b[1], b[2], cnv, \$9
            }
        }
    }' "${params.analysis_name}.cnv.raw.bed" > "${params.analysis_name}.cnv.bed"
    """

    stub:
    """
    touch "${params.analysis_name}.cnv.raw.bed"
    touch "${params.analysis_name}.cnv.bed"
    """
}


process convading {
    publishDir params.cnv_storage_path, overwrite: true, saveAs: {
        if ( it.startsWith('matchscore/') ) {
            it - 'matchscore/'
        } else {
            "${it}"
        }
    }, enabled: true
    if ( params.containsKey('test_settings') && params.test_settings == 'integration' ) errorStrategy 'ignore'

    input:
    tuple path(bam_file), path(bai_file)
    path convading_regions
    val convading_background

    output:
    path("matchscore/*coverage.txt")
    path("*best.score.log")
    path("*best.score.totallist.txt")
    tuple path("*best.score.longlist.txt"), path("*best.score.shortlist.txt"), emit: convading_lists

    script:
    progress(10, task.process)
    """
    perl /CoNVaDING/CoNVaDING.pl \
        -mode StartWithBam \
        -rmdup \
        -bed ${convading_regions} \
        -inputDir \$PWD \
        -controlsDir ${convading_background} \
        -outputDir matchscore

    perl /CoNVaDING/CoNVaDING.pl \
        -mode StartWithMatchScore \
        -inputDir matchscore \
        -controlsDir ${convading_background} \
        -outputDir bestscore

    perl /CoNVaDING/CoNVaDING.pl \
        -mode StartWithBestScore \
        -inputDir bestscore \
        -outputDir \$PWD \
        -ratioCutOffHigh 1.2 \
        -ratioCutOffLow 0.65 \
        -sampleRatioScore 0.099 \
        -zScoreCutOffLow=-3 \
        -zScoreCutOffHigh=3

    # Work around bug in CoNVaDING where HOM_DEL/HOM_DUPs are only present in shortlist
    # (they will still be counted in longlist, but as DEL/DUP)
    set +e
    grep 'HOM_DEL\\|HOM_DUP' "${params.analysis_name}.aligned.only.best.score.shortlist.txt" >> "${params.analysis_name}.aligned.only.best.score.longlist.txt"
    set -e

    # Sort everything except header line (first line)
    cat "${params.analysis_name}.aligned.only.best.score.longlist.txt" | awk 'NR<2{print \$0;next}{print \$0 | "sort -k1V -k2n -k3n"}' > "${params.analysis_name}.aligned.only.best.score.longlist.withmissing.sorted.txt"
    mv "${params.analysis_name}.aligned.only.best.score.longlist.withmissing.sorted.txt" "${params.analysis_name}.aligned.only.best.score.longlist.txt"
    """
}

process convading_to_vcf {
    publishDir params.cnv_storage_path, overwrite: true, enabled: true

    input:
    tuple path(longlist), path(shortlist)

    output:
    path("${params.analysis_name}.cnv.vcf")
    val('convading_done'), emit: done

    script:
    progress(10, task.process)
    """
    convading_to_vcf \
        --longlist ${longlist} \
        --shortlist ${shortlist} \
        -o "${params.analysis_name}.cnv.vcf" \
        --sample-name ${params.analysis_name}
    """
}
