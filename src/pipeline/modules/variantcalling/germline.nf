params.variantcalling_storage_path = ''

params.analysis_name = 'unnamed_analysis'
params.vcf_suffix = ''
params.capture_kit = 'wgs'

include { progress } from '../util'

process haplotype_caller {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true
    tag "$calling_region"

    input:
    tuple path(bam_file), path(bai_file)
    val calling_region
    val dbsnp
    val reference

    output:
    path("*.g.vcf.gz"), emit: vcf
    path("*.g.vcf.gz.tbi"), emit: vcf_tbi

    script:
    progress(10, task.process)
    if ( params.capture_kit == 'wgs' ) {
        output_suffix = ".chr{calling_region}"
        intervals = calling_region
    } else {
        output_suffix = ''
        intervals = 'calling_region.interval_list'
    }
    """
    # Picard-style formatted intervals files require a .interval_list extension (TODO: change file extension in refdata)
    if [[ ${params.capture_kit} != 'wgs' ]]
    then
      cp ${calling_region} ${intervals}
    fi

    gatk HaplotypeCaller \
        -R ${reference} \
        -I ${bam_file} \
        --max-alternate-alleles 3 \
        --read-filter OverclippedReadFilter \
        --dbsnp ${dbsnp} \
        --emit-ref-confidence GVCF \
        -L ${intervals} \
        -O "${params.analysis_name}${output_suffix}.g.vcf.gz"
    """
}

process combine_gvcfs {
    input:
    path vcf_files
    path vcf_tbi
    val reference

    output:
    tuple path("${params.analysis_name}${params.vcf_suffix}.combined.g.vcf.gz"), path("${params.analysis_name}${params.vcf_suffix}.combined.g.vcf.gz.tbi"), emit: combined_gvcf_and_tbi

    script:
    progress(10, task.process)
    def vcf_files_arg = ""
    for (String vcf : vcf_files) {
        vcf_files_arg += " --variant ${vcf}"
    }
    """
    gatk CombineGVCFs \
        -R ${reference} \
        ${vcf_files_arg} \
        -O "${params.analysis_name}${params.vcf_suffix}.combined.g.vcf.gz"
    """
}

process genotype_gvcfs {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    tuple path(gvcf), path(gvcf_index)
    val dbsnp
    val reference

    output:
    tuple path("${params.analysis_name}${params.vcf_suffix}.raw.vcf.gz"), path("${params.analysis_name}${params.vcf_suffix}.raw.vcf.gz.tbi"), emit: raw_vcf_and_tbi

    script:
    progress(10, task.process)
    """
    gatk GenotypeGVCFs \
        -R ${reference} \
        --variant ${gvcf} \
        --dbsnp ${dbsnp} \
        -O "${params.analysis_name}${params.vcf_suffix}.raw.vcf.gz"
    """
}

process genotype_gvcfs_ped {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    tuple path(gvcf), path(gvcf_index)
    path pedfile
    val dbsnp
    val reference

    output:
    tuple path("${params.analysis_name}${params.vcf_suffix}.raw.vcf.gz"), path("${params.analysis_name}${params.vcf_suffix}.raw.vcf.gz.tbi"), emit: raw_vcf_and_tbi

    script:
    progress(10, task.process)
    """
    gatk GenotypeGVCFs \
        -R ${reference} \
        --variant ${gvcf} \
        --dbsnp ${dbsnp} \
        -ped ${pedfile} \
        -O "${params.analysis_name}${params.vcf_suffix}.raw.vcf.gz"
    """
}

process select_variants {
    tag "$mode"

    input:
    tuple path(raw_vcf), path(raw_vcf_index)
    val reference
    val mode

    output:
    path("variants.${mode}.vcf"), emit: selected_vcf

    script:
    progress(10, task.process)
    """
    gatk SelectVariants \
        -R ${reference} \
        --variant ${raw_vcf} \
        --select-type-to-include ${mode} \
        -O "variants.${mode}.vcf"
    """
}

process variant_recalibrator {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true
    tag "$mode"

    input:
    path selected_vcf
    path dbsnp
    path hapmap
    path thousandgenomes_omni
    path thousandgenomes_phase1
    path mills
    val reference
    val mode

    output:
    path("${params.analysis_name}.snp.vqsr.output.recal"), emit: recal
    path("${params.analysis_name}.snp.vqsr.output.tranches"), emit: tranches
    path("${params.analysis_name}.snp.vqsr.output.plots.R")

    script:
    progress(10, task.process)
    dbsnp_ressource = "--resource:dbsnp,known=true,training=false,truth=false,prior=2.0 ${dbsnp}"
    if ( mode == "SNP" ) {
        hapmap_ressource = "--resource:hapmap,known=false,training=true,truth=true,prior=15.0 ${hapmap}"
        thousandgenomes_omni_ressource = "--resource:omni,known=false,training=true,truth=false,prior=12.0 ${thousandgenomes_omni}"
        thousandgenomes_phase1_ressource = "--resource:1000G,known=false,training=true,truth=false,prior=10.0 ${thousandgenomes_phase1}"
        mills_ressource = ""
    } else if ( mode == "INDEL" ) {
        hapmap_ressource = ""
        thousandgenomes_omni_ressource = ""
        thousandgenomes_phase1_ressource = ""
        mills_ressource = "--resource:mills,known=false,training=true,truth=true,prior=12.0 ${mills}"
    }
    """
    gatk VariantRecalibrator \
        -R ${reference} \
        --variant ${selected_vcf} \
        ${hapmap_ressource} \
        ${thousandgenomes_omni_ressource} \
        ${thousandgenomes_phase1_ressource} \
        ${mills_ressource} \
        ${dbsnp_ressource} \
        -an QD \
        -an MQRankSum \
        -an ReadPosRankSum \
        -an FS \
        -an SOR \
        -an DP \
        -tranche 100.0 -tranche 99.5 -tranche 99.0 -tranche 90.0 \
        -O "${params.analysis_name}.${mode}.vqsr.output.recal" \
        --tranches-file "${params.analysis_name}.${mode}.vqsr.output.tranches" \
        --rscript-file "${params.analysis_name}.${mode}.vqsr.output.plots.R" \
        --mode ${mode} \
        --max-gaussians 4
    """
}

process apply_vqsr {
    tag "$mode"

    input:
    path selected_vcf
    path tranches
    path recal
    val reference
    val mode

    output:
    path("variants.${mode}.filtered.vcf"), emit: filtered_vcf

    script:
    progress(10, task.process)
    """
    gatk ApplyVQSR \
        -R ${reference} \
        --variant ${selected_vcf} \
        -O "variants.${mode}.filtered.vcf" \
        --truth-sensitivity-filter-level 99.0 \
        --tranches-file ${tranches} \
        --recal-file ${recal} \
        --mode ${mode}
    """
}

process variant_filtration {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true
    tag "$mode"

    input:
    path selected_vcf
    val reference
    val mode

    output:
    path("variants.${mode}.filtered.vcf"), emit: filtered_vcf

    script:
    progress(10, task.process)
    qd_filter = "--filter-expression \"QD < 2.0\" --filter-name \"QD_failed\""
    if ( mode == "SNP" ) {
        fs_filter = "--filter-expression \"FS > 60.0\" --filter-name \"FS_failed\""
        mq_filter = "--filter-expression \"MQ < 40.0\" --filter-name \"MQ_failed\""
        mqranksum_filter = "--filter-expression \"MQRankSum < -12.5\" --filter-name \"MQRankSum_failed\""
        readposranksum_filter = "--filter-expression \"ReadPosRankSum < -8.0\" --filter-name \"ReadPosRankSum_failed\""
    } else if ( mode == "INDEL" ) {
        fs_filter = "--filter-expression \"FS > 200.0\" --filter-name \"FS_failed\""
        mq_filter = ""
        mqranksum_filter = ""
        readposranksum_filter = "--filter-expression \"ReadPosRankSum < -20.0\" --filter-name \"ReadPosRankSum_failed\""
    }
    """
    gatk VariantFiltration \
        -R ${reference} \
        --variant ${selected_vcf} \
        ${qd_filter}  \
        ${fs_filter} \
        ${mq_filter} \
        ${mqranksum_filter} \
        -O "variants.${mode}.filtered.vcf"
    """
}

process merge_vcfs {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    path snps_vcf
    path indels_vcf
    path mixed_vcf
    val reference

    output:
    path("${params.analysis_name}${params.vcf_suffix}.final.vcf"), emit: merged_vcf

    script:
    progress(10, task.process)
    """
    picard MergeVcfs \
        R=${reference} \
        I=${snps_vcf} \
        I=${indels_vcf} \
        I=${mixed_vcf} \
        O="${params.analysis_name}${params.vcf_suffix}.final.vcf"
    """
}

process depth_of_coverage {
    input:
    tuple path(bam_file), path(bai_file)
    val calling_region
    val reference

    output:
    path("${params.analysis_name}.DepthOfCoverage.txt"), emit: depth

    script:
    progress(10, task.process)
    """
    # Picard-style formatted intervals files require a .interval_list extension (TODO: change file extension in refdata)
    cp ${calling_region} calling_region.interval_list

    gatk DepthOfCoverage \
        -R ${reference} \
        -I ${bam_file} \
        -O "${params.analysis_name}.DepthOfCoverage.txt" \
        -L calling_region.interval_list \
        --print-base-counts true
    """
}

process msh2_variants {
    input:
    path merged_vcf
    path depth

    output:
    path "${params.analysis_name}.msh2.vcf", emit: msh2_vcf

    script:
    progress(10, task.process)
    """
    cp ${merged_vcf} "${params.analysis_name}.msh2.vcf"

    msh2 \
        ${depth} \
        "${params.analysis_name}.msh2.vcf" \
    """
}

process sort_vcf {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    path vcf

    output:
    path("${params.analysis_name}.final.vcf"), emit: final_vcf

    script:
    progress(10, task.process)
    """
    picard SortVcf \
        I=${vcf} \
        O="${params.analysis_name}.final.vcf"
    """
}

process md5_sum {
    input:
    path final_vcf

    script:
    progress(10, task.process)
    """
    md5sum ${final_vcf} > "${params.analysis_name}${params.vcf_suffix}.final.vcf.md5"
    """
}
