params.capture_kit = ''
params.analysis_name = ''
params.output = ''
params.platform = ''

include { progress } from './util'

process postcmd{
    input:
    val pipeline
    val export_priority
    path qc_result
    val workflow_complete

    script:
    progress(90, task.process)
    if ( pipeline == 'triopipe' ) {
        cmd_append = "--ignore_qc 0 \
            --pipeline_type 'triopipe'"
    } else if ( pipeline == 'basepipe') {
        cmd_append = '--pipeline_type "basepipe"'
    }
    """
    ${workflow.projectDir}/pipeline-post \
        --export_priority ${export_priority} \
        --platform ${params.platform} \
        --qc_result ${qc_result} \
        --result ${params.output} \
        --analysis ${params.analysis_name} \
        --capture_kit ${params.capture_kit} \
        ${cmd_append}
    """
}