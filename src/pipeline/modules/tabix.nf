process tabix {
    input:
    path input_file
    val options

    output:
    path("*.tbi"), optional:true, emit: tbi // Default if --csi not passed in options
    path("*.csi"), optional:true, emit: csi

    script:
    """
    #!/bin/bash
    
    options="\$@"
    echo "INPUT FILE: ${input_file}"
    echo "OPTIONS: ${options}"
    
    force=false
    extension=tbi
    
    for option in ${options}; do
      case \${option} in
        -f|--force) force=true ;;
        --csi) extension=csi ;;
      esac
    done
    
    outputfile="${input_file}.\${extension}"
    if [[ -f \${outputfile} && force != true ]]; then
      echo "File \${outputfile} already exists."
      exit 0
    fi
    
    tabix ${options} ${input_file}
    """
}