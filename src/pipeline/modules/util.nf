// progress
params.output = '.'
file(params.output).mkdirs()
logFile = new File(params.output.toString() + '/PROGRESS')

def progress(pct, pro) {
    logFile.write(pct + '\t' + pro)
}
// progress end
