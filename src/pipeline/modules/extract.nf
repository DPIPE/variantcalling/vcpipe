include { progress } from './util'


File _customTempFile(String prefix, String suffix) {
    def f = new File(prefix + UUID.randomUUID().toString() + suffix)
    return f.exists() ? _customTempFile(prefix, suffix) : f
}


process genounzip {
    input:
    tuple val(reads_nr), path(reads_in)
    path(reference)

    output:
    tuple val(reads_nr), path(reads_out)

    script:
    progress(10, task.process)
    reads_suffix = reads_in.name.split('[.]').last()
    switch(reads_suffix) {
        case ['fastq', 'genozip']:
            reads_prefix = reads_in.baseName.replaceAll('.fastq$', '');
            reads_out = _customTempFile(reads_prefix + '_', '.fastq');
            break;
        default:
            reads_prefix = reads_in.baseName.replaceAll('.' + reads_suffix + '$', '');
            reads_out = _customTempFile(reads_prefix + '_', '.' + reads_suffix);
            break;
    }
    """
    is_genozipped=true
    echo "Testing input reads file with genozip.."
    ln -s '${reads_in}' reads.fastq.genozip
    genols reads.fastq.genozip > /dev/null 2>&1 || is_genozipped=false
    if \${is_genozipped} ; then
        echo "Extracting input reads file with genozip.."
        genounzip -e '${reference}' -o '${reads_out}' reads.fastq.genozip
    else
        echo "No genozip compression detected: Passing input reads file as is.."
        ln -s reads.fastq.genozip '${reads_out}'
    fi
    """
}


process oraunzip {
    input:
    tuple val(reads_nr), path(reads_in)
    path(reference_dir)

    output:
    tuple val(reads_nr), path(reads_out)

    script:
    progress(10, task.process)
    reads_suffix = reads_in.name.split('[.]').last()
    reads_out = _customTempFile(reads_in.baseName + '_', '.' + reads_suffix)
    if (params.specialized_pipeline == 'Dragen')
        """
        ln -s '${reads_in}' '${reads_out}'
        """
    else
        """
        is_orazipped=true
        echo "Testing input reads file with ORA utility.."
        orad --check '${reads_in}' > /dev/null 2>&1 || is_orazipped=false
        if \${is_orazipped} ; then
            echo "Extracting input reads file with Illumina's ORA utility.."
            orad --ora-reference '${reference_dir}' -c '${reads_in}' > '${reads_out}'
        else
            echo "No ORA compression detected: Passing input reads file as is.."
            ln -s '${reads_in}' '${reads_out}'
        fi
        """
}


workflow extract {
    take:
    reads_archive
    reference

    main:
    reference_base = reference.split('.fasta$')[0]
    reference_genozip = reference_base + ".ref.genozip"
    reference_dir = reference.split('/[^/]+$')[0]
    if (reference_dir == "")
        reference_dir = workflow.launchDir

    reads_tmp = genounzip(reads_archive, reference_genozip)
    reads = oraunzip(reads_tmp, reference_dir)

    emit:
    reads | map {
        it -> [
            'nr': it[0],
            'name': it[1]
        ]
    }
}

