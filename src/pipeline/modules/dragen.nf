params.mapping_storage_path = ''
params.variantcalling_storage_path = ''
params.qc_storage_path = ''
params.cnv_storage_path = ''

params.analysis_name = 'unnamed_analysis'

params.dragen_reference_dir = ''
params.bam_header = ''

include { progress } from './util'

process dragen {
    publishDir params.mapping_storage_path, overwrite: true, pattern: "${params.analysis_name}.{bam,bam.md5sum}", enabled: true
    publishDir params.mapping_storage_path, overwrite: true, pattern: "${params.analysis_name}.bam.bai", saveAs: { "${params.analysis_name}.bai" }, enabled: true
    publishDir params.variantcalling_storage_path, overwrite: true, pattern: "${params.analysis_name}.{g.vcf.gz,roh.bed,roh_metrics.csv,vc_hethom_ratio_metrics.csv,vc_metrics.csv}*", enabled: true
    publishDir params.qc_storage_path, overwrite: true, pattern: "*{_coverage_metrics.csv,_overall_mean_cov.csv,_contig_mean_cov.csv,insert-stats.tab,mapping_metrics.csv,gc_metrics.csv,fragment_length_hist.csv,fastqc_metrics.csv,trimmer_metrics.csv,_fine_hist.csv,_hist.csv}", enabled: true
    publishDir params.cnv_storage_path, overwrite: true, pattern: "${params.analysis_name}.{cnv.vcf,cnv.gff3,cnv_sv.vcf.gz,cnv_sv.vcf.gz.md5sum,cnv_sv.vcf.gz.tbi,target.counts.diploid.bw,sv.vcf,ploidy.vcf,cnv_metrics.csv,ploidy_estimation_metrics.csv,sv_metrics.csv}", enabled: true

    input:
    tuple path(read1), path(read2)
    val dbsnp
    val downsampling_depth

    output:
    tuple path("*.bam"), path("*.bai"), emit: bam_and_bai
    path("*.bam.md5sum")
    path("*_coverage_metrics.csv")
    path("*_overall_mean_cov.csv")
    path("*_contig_mean_cov.csv")
    path("*.insert-stats.tab")
    path("*.mapping_metrics.csv"), emit: mapping_metrics
    path("*.final.vcf"), emit: final_vcf
    path("*.g.vcf.gz")
    path("*.g.vcf.gz.tbi")
    path('*.ploidy.vcf')
    path('*.gc_metrics.csv')
    path("*.cnv.vcf"), emit: canvas_vcf
    path("*.cnv_sv.vcf.gz")
    path("*.cnv_sv.vcf.gz.md5sum")
    path("*.cnv_sv.vcf.gz.tbi")
    path("*.cnv.gff3")
    path("*.target.counts.diploid.bw")
    path("*.sv.vcf"), emit: manta_vcf
    path("*.roh.bed")
    path("*.cnv_metrics.csv")
    path("*.fastqc_metrics.csv")
    path("*.fragment_length_hist.csv")
    path("*.ploidy_estimation_metrics.csv")
    path("*.roh_metrics.csv")
    path("*.sv_metrics.csv")
    path("*.trimmer_metrics.csv")
    path("*.vc_hethom_ratio_metrics.csv")
    path("*.vc_metrics.csv")
    path("*_fine_hist.csv")
    path("*_hist.csv")

    script:
    progress(10, task.process)
    if ( read1.getExtension() == "ora" ) {
        cmd_append = '--ora-reference ' + "${params.dragen_ora_reference_dir}" + ' '
    } else if ( read1.getExtension() == "gz" || read1.getExtension() == "fastq" ) {
        cmd_append = ''
    }

    if ( downsampling_depth == 'NA' ) {
        cmd_append = cmd_append + ''
    } else {
        cmd_append = cmd_append + '--enable-down-sampler true --down-sampler-coverage ' + downsampling_depth
    }
    """
    #dragen_reset

    dragen -l -r ${params.dragen_reference_dir}

    echo "Dragen gVCF variant calling on ${params.analysis_name}"

    dragen \
        -f \
        -r ${params.dragen_reference_dir} \
	    -1 ${read1} -2 ${read2} \
        ${cmd_append} \
        --output-file-prefix ${params.analysis_name} \
        --output-format BAM \
        --output-directory \$(pwd) \
        --enable-map-align-output true \
        --enable-bam-indexing true \
        --enable-duplicate-marking true \
        --enable-variant-caller true \
        --vc-enable-joint-detection=true \
        --vc-emit-ref-confidence GVCF \
        --sample-sex auto \
        ${params.bam_header} \
        --dbsnp ${dbsnp} \
	    --gc-metrics-enable=true \
        --enable-cnv true \
        --cnv-enable-self-normalization true \
        --cnv-wgs-interval-width 1000 \
        --cnv-counts-method start \
        --cnv-segmentation-mode slm \
        --cnv-merge-distance 0 \
        --cnv-merge-threshold 0.2 \
        --cnv-enable-tracks true \
        --cnv-filter-bin-support-ratio 0.2 \
        --cnv-filter-copy-ratio 0.2 \
        --cnv-filter-length 5000 \
        --cnv-min-qual 3 \
        --cnv-max-qual 200 \
        --cnv-filter-qual 10 \
        --enable-sv true \
        --verbose

    echo "Dragen joint variant calling on ${params.analysis_name}"

    dragen \
        -f \
        -r ${params.dragen_reference_dir} \
        --enable-joint-genotyping true \
        --output-file-prefix ${params.analysis_name} \
        --output-directory \$(pwd) \
        --variant \$(pwd)/"${params.analysis_name}.hard-filtered.gvcf.gz"

    # Fix the header of the single VCF file before renaming it
    gunzip "${params.analysis_name}.hard-filtered.vcf.gz"
    grep '^##' "${params.analysis_name}.hard-filtered.vcf" > header.txt
    echo '##INFO=<ID=DB,Number=.,Type=Flag,Description="Variants which match dbSNP on CHROM, POS, REF and at least one ALT">' >> header.txt
    grep -v '^##' "${params.analysis_name}.hard-filtered.vcf" > body.txt
    cat header.txt body.txt > "${params.analysis_name}.final.vcf"

    # Rename the gVCF files
    mv "${params.analysis_name}.hard-filtered.gvcf.gz" "${params.analysis_name}.g.vcf.gz"
    mv "${params.analysis_name}.hard-filtered.gvcf.gz.tbi" "${params.analysis_name}.g.vcf.gz.tbi"
    gunzip ${params.analysis_name}.cnv.vcf.gz
    gunzip ${params.analysis_name}.sv.vcf.gz
    gunzip ${params.analysis_name}.ploidy.vcf.gz
    """

    stub:
    """
    echo "Dragen gVCF variant calling on ${params.analysis_name}"
    echo "Dragen joint variant calling on ${params.analysis_name}"
    echo "Fixing VCF file's header"
    echo "Renaming gVCF files"
    echo "Decompressing structural VCF files"
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.bam ${params.analysis_name}.bam
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.bai ${params.analysis_name}.bam.bai
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.bam.md5sum ${params.analysis_name}.bam.md5sum
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*_coverage_metrics.csv ${params.analysis_name}_coverage_metrics.csv
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*_overall_mean_cov.csv ${params.analysis_name}_overall_mean_cov.csv
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*_contig_mean_cov.csv ${params.analysis_name}_contig_mean_cov.csv
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.insert-stats.tab ${params.analysis_name}.insert-stats.tab
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.mapping_metrics.csv ${params.analysis_name}.mapping_metrics.csv
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM-DR.final.vcf ${params.analysis_name}.final.vcf
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.g.vcf.gz ${params.analysis_name}.g.vcf.gz
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.g.vcf.gz.tbi ${params.analysis_name}.g.vcf.gz.tbi
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.ploidy.vcf ${params.analysis_name}.ploidy.vcf
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.gc_metrics.csv ${params.analysis_name}.gc_metrics.csv
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.cnv.vcf ${params.analysis_name}.cnv.vcf
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.cnv.gff3 ${params.analysis_name}.cnv.gff3
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.target.counts.diploid.bw ${params.analysis_name}.target.counts.diploid.bw
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen/*PM*.sv.vcf ${params.analysis_name}.sv.vcf
    touch ${params.analysis_name}.roh.bed
    touch ${params.analysis_name}.cnv_metrics.csv
    touch ${params.analysis_name}.fastqc_metrics.csv
    touch ${params.analysis_name}.fragment_length_hist.csv
    touch ${params.analysis_name}.ploidy_estimation_metrics.csv
    touch ${params.analysis_name}.roh_metrics.csv
    touch ${params.analysis_name}.sv_metrics.csv
    touch ${params.analysis_name}.trimmer_metrics.csv
    touch ${params.analysis_name}.vc_hethom_ratio_metrics.csv
    touch ${params.analysis_name}.vc_metrics.csv
    touch ${params.analysis_name}_fine_hist.csv
    touch ${params.analysis_name}_hist.csv
    touch ${params.analysis_name}.cnv_sv.vcf.gz
    touch ${params.analysis_name}.cnv_sv.vcf.gz.tbi
    touch ${params.analysis_name}.cnv_sv.vcf.gz.md5sum
    """

}

process dragen_trio {
    publishDir params.variantcalling_storage_path, overwrite: true, enabled: true

    input:
    path(pedfile_path)
    path(gvcf_files)
    path(gvcf_tbi)

    output:
    path("${params.analysis_name}.all.final.vcf"), emit: final_vcf

    script:
    progress(10, task.process)
    def gvcf_files_arg = ''
    for (String gvcf : gvcf_files) {
        gvcf_files_arg += " --variant ${gvcf}"
    }
    """
    #dragen_reset

    dragen -l -r ${params.dragen_reference_dir}

    dragen \
        -f \
        -r ${params.dragen_reference_dir} \
        --enable-joint-genotyping true \
        --output-directory \$PWD \
        --output-file-prefix ${params.analysis_name} \
        --pedigree-file ${pedfile_path} \
        ${gvcf_files_arg}

    # Fix the header of the final VCF file before renaming it
    gunzip "${params.analysis_name}.hard-filtered.vcf.gz"
    grep '^##' "${params.analysis_name}.hard-filtered.vcf" > header.txt
    echo '##INFO=<ID=DB,Number=.,Type=Flag,Description="Variants which match dbSNP on CHROM, POS, REF and at least one ALT">' >> header.txt
    grep -v '^##' "${params.analysis_name}.hard-filtered.vcf" > body.txt
    cat header.txt body.txt > "${params.analysis_name}.all.final.vcf"

    ls *.vcf.gz | grep -v '.g.vcf.gz' | xargs gunzip
    """

    stub:
    """
    echo "Dragen joint variant calling on ${params.analysis_name}"
    echo "Fixing VCF file's header"
    echo "Renaming gVCF files"
    echo "Decompressing VCF files"
    cp -v --no-preserve=mode ${params.stub_data_dir}/dragen-trio/*.all.final.vcf ${params.analysis_name}.all.final.vcf
    """
}
