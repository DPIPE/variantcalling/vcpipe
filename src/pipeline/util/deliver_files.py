import glob
import grp
import logging
import os
import shutil

from settings import settings

log = logging.getLogger(__name__)

PRIORITY_MAP = {"3": "urgent", "2": "high", "1": "normal"}

def deliver_files(
    export_priority,
    platform,
    copy_rules,
    result_path,
    analysis_name,
    extra_files=[],
    touch_ready=False,
    flatten=True,
    hardlink=False
):
    # export_priority is the priority level for NSC pipeline
    # platform is the platform running the pipeline
    # copy_rules is defined: settings.get_settings(capturekit)[pipeline_type]['copy'], pipeline_type can be basepipe/triopipe/annopipe
    # result_path is the abs path to the result directory, just till the dir containing the data folder
    # analysis_name is the name of the analysis
    # extra_files is a list of extra files to be copied to the dest folder
    # touch_ready is a boolean, if True, a file named READY is created in the dest folder
    # flatten is a boolean, if True, all files are copied under the destination folder without hierarchy?
    # hardlink is a boolean, if True, hardlink the files; otherwise, copy them

    for interpretation in copy_rules:
        if platform not in interpretation["platform"]:
            continue
        files_patterns = interpretation["files"]

        try:
            destination_config = interpretation["destination"]
        except Exception as e:
            print("Error: destination not defined in the copy rules: ", e)
            raise

        interpretation_path = settings[destination_config]

        if platform == "NSC":
            # replace {priority_subdir} with priority level for NSC pipeline
            interpretation_path = interpretation_path.replace(
                "{priority_subdir}", PRIORITY_MAP[str(export_priority)]
            )

        # Add project folder to interpretation path: never used anymore, can be removed
        # The project_name can be removed, then
        # Leave the code here for now, in case it is used in the future
        # if interpretation.get('include_project_folder', True):
        #    interpretation_path = os.path.join(interpretation_path, project_name)

        interpretation_path = os.path.join(interpretation_path, analysis_name)

        # Add subfolder to interpretation path: no json file has this field, can be removed
        # Leave the code here for now, in case it is used in the future
        # if interpretation.get('subfolder') is not None:
        #     interpretation_path = os.path.join(interpretation_path, interpretation['subfolder'])

        copy_batch(
            result_path,
            interpretation_path,
            files_patterns,
            flatten=flatten,
            hardlink=hardlink,
        )

        # for copying extra files to destinations: e.g. .analysis file, sample_config file

        # TODO: should the extra files be hardlinked as well?
        for extra in extra_files:
            if hardlink:
                real_src = os.path.realpath(extra)
                dst_path = os.path.join(interpretation_path, os.path.basename(real_src))
                assert os.path.isfile(real_src), "Can not hardlink a folder {}".format(
                    real_src
                )
                assert not os.path.exists(
                    dst_path
                ), "File already exists: {}, remove the old files first.".format(
                    dst_path
                )
                log.info("Hardlinking {} to {}".format(real_src, dst_path))
                os.link(real_src, dst_path)
            else:
                log.info("Copying {} to {}".format(extra, interpretation_path))
                shutil.copy(extra, interpretation_path)
        if touch_ready:
            ready_path = os.path.join(interpretation_path, "READY")
            with open(ready_path, "a"):
                os.utime(ready_path, None)


def _get_modes():
    modes = {
        "read_only": {
            "perm": int(settings["read_only_perm"], 8)
            if "read_only_perm" in settings
            else None,
            "group": settings.get("read_only_group"),
        },
        "writable": {
            "perm": int(settings["writable_perm"], 8)
            if "writable_perm" in settings
            else None,
            "group": settings.get("writable_group"),
        },
    }
    return modes


def set_perm(path, mode):
    modes = _get_modes()

    if modes[mode]["group"] is not None:
        # Set group if applicable
        os.chown(path, -1, grp.getgrnam(modes[mode]["group"]).gr_gid)
    if modes[mode]["perm"] is not None:
        # Change file permission
        # if file is owned by another person, chmod will fail. Ignore this error only if the
        # permission is already correctly set
        try:
            os.chmod(path, modes[mode]["perm"])
        except OSError as err:
            if (
                err.strerror == "Operation not permitted"
                and os.stat(path).st_mode & 0o777 == modes[mode]["perm"]
            ):
                pass
            else:
                raise


def copy_batch(
    source, dest, patterns, replace_patterns=None, flatten=True, hardlink=False
):
    """ "
    Copies or hardlinks files from 'source' to 'dest' according to patterns.
    Patterns example (paths are relative to 'source'
        {
            "read_only": [
                "data/mapping/*.refined.bam",
                "data/mapping/*.refined.bai",
                "data/annotation/*.full.annotated.vcf",
                "data/annotation/*.region.annotated.vcf",
                "data/qc/*-HTS-vedlegg.pdf",
                "data/annotation/*.igv.txt",
                "data/variantcalling/*.HaplotypeCaller.bam",
                "data/variantcalling/*.HaplotypeCaller.bai"
            ],
            "writable": [
                "data/annotation/*.interpretation.xlsx"
            ]
        }
    Copy files from results folder to dest folder by pattern and set permissions accordingly.
    Possible permission groups are ['read_only', 'writable']
    Permission group settings are fetched from settings file.

    replace_patterns describe parts of the file name that should be replaced.
    The replace_pattern is checked against every match on patterns
    Example:
        [
            ["markduplicates.ba", ".ba"],
            ["region.annotated.vcf", ".vcf"]
        ]

    flatten: all files are copied under the destination folder
        set to True: without hierarchy
        set to False: according to the source hierarchy
    """

    try:
        if not os.path.exists(dest):
            os.umask(0o02)
            os.makedirs(dest)

        for mode in patterns:
            for pattern in patterns[mode]:
                for match in glob.glob(os.path.join(source, pattern)):
                    if flatten:
                        relpath = os.path.basename(match)
                    else:
                        relpath = os.path.relpath(match, source)
                    cp_dest = os.path.join(dest, relpath)

                    # Check if any replace_patterns matches
                    # and modify destination name accordingly
                    if replace_patterns:
                        for p in replace_patterns:
                            if p[0] in relpath:
                                cp_dest = os.path.join(
                                    dest, relpath.replace(p[0], p[1], 1)
                                )


                    # Recreate tree
                    subfolder = os.path.dirname(cp_dest)
                    if not os.path.exists(subfolder):
                        os.umask(0o02)
                        os.makedirs(subfolder)

                    if hardlink:
                        real_src = os.path.realpath(match)
                        assert os.path.isfile(
                            real_src
                        ), "Can not hardlink a folder {}".format(real_src)
                        assert not os.path.exists(
                            cp_dest
                        ), "File already exists: {}, remove the old files first.".format(
                            cp_dest
                        )
                        log.info("Hardlinking {} to {}".format(real_src, cp_dest))
                        os.link(real_src, cp_dest)
                    else:
                        log.info("Copying {} to {}".format(match, cp_dest))
                        shutil.copy(match, cp_dest)

    except Exception as e:
        log.exception(
            "Failed while copying/hardlinking result files.\n{}".format(
                getattr(e, "message", e)
            )
        )
        raise
