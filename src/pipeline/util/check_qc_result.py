import logging

log = logging.getLogger(__name__)


def check_qc_result(qc):
    """
    Returns True if all the parameters have passed Quality Control
    :param qc: Dictionnary of the QC results
    """
    if 'samples' in qc:
        return _check_qc_result_multi_samples(qc)
    else:
        return _check_qc_result_single_sample(qc)


def _check_qc_result_multi_samples(qc):
    qc_passed = True
    for sample_name, qc_data in qc['samples'].items():
        log.info("Checking QC for {}.".format(sample_name))
        non_passed = list()
        for name, data in qc_data.items():
            # data = [passed (bool), {data}, {criteria}]
            log.info("-- {}: {}".format(name, 'PASSED' if data[0] else 'FAILED'))
            if not data[0]:
                non_passed.append(name)
        if non_passed:
            log.warn('QC FAILED for sample {}.\nThe following QC checks failed: {}\n'.format(sample_name, ', '.join(non_passed)))
            qc_passed = False
        else:
            log.info('QC PASSED for sample {}\n'.format(sample_name))

    log.info("Checking QC for trio/gender test.")
    if not qc['filtus-gender-test'][0]:
        log.error("QC FAILED for trio/gender test: {}".format(qc['filtus-gender-test'][1]['1-triotype']))
        qc_passed = False
    else:
        log.info("QC PASSED for trio/gender test.")

    return qc_passed


def _check_qc_result_single_sample(qc):
    non_passed = list()
    for name, data in qc.items():
        # data = [passed (bool), {data}]
        log.info("-- {}: {}".format(name, 'PASSED' if data[0] else 'FAILED'))
        if not data[0]:
            non_passed.append(name)
    if non_passed:
        log.warn('QC FAILED!\nThe following QC checks failed: {}'.format(', '.join(non_passed)))
        return False
    else:
        log.info('QC PASSED')
        return True
