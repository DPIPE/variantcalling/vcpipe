/*
 Use this script as a starting point for experiments.

 create a workflow and run it using ops/run-nxf-test. In run-nxf-test create a function that
 will do 'nextflow run -entry <your workflow>'
*/


nextflow.enable.dsl=2

// empty files are place in a dedicated folder along side the main nextflow scripts:
EMPTY_FILE1 = "${baseDir}/emptyfiles/empty1.txt"
EMPTY_FILE2 = "${baseDir}/emptyfiles/empty2.txt"
EMPTY_FILE3 = "${baseDir}/emptyfiles/empty3.txt"
EMPTY_FILE4 = "${baseDir}/emptyfiles/empty4.txt"
EMPTY_FILE5 = "${baseDir}/emptyfiles/empty5.txt"
EMPTY_FILE6 = "${baseDir}/emptyfiles/empty6.txt"
EMPTY_FILE7 = "${baseDir}/emptyfiles/empty7.txt"
EMPTY_FILE8 = "${baseDir}/emptyfiles/empty8.txt"
EMPTY=Channel.fromPath(EMPTY_FILE1)
EMPTY2=Channel.fromPath(EMPTY_FILE2)
EMPTY3=Channel.fromPath(EMPTY_FILE3)
EMPTY4=Channel.fromPath(EMPTY_FILE4)
EMPTY5=Channel.fromPath(EMPTY_FILE5)
EMPTY6=Channel.fromPath(EMPTY_FILE6)
EMPTY7=Channel.fromPath(EMPTY_FILE7)
EMPTY8=Channel.fromPath(EMPTY_FILE8)
EMPTY_VALUE = ''


include {
    quality_control ;
} from './modules/qc/workflows'

include {
    qc_analysis ;
} from './modules/qc/report'

include {
   dragen_trio ;
} from './modules/dragen'

process show_workflow_info {
   echo true

   script:
   """
   echo Running ${workflow.scriptName}
   echo Config files: ${workflow.configFiles}
   echo Profiles: ${workflow.profile}
   echo Using params: ${params}
   echo Launched from ${launchDir}
   echo Working dir: ${workDir}
   """
}
// test workflow/processes by running nextflow like: nextflow run <script> -entry test
// and set various params to check workflow logic:  --specialized_pipeline=NonDragen --capture_kit=wgs


process empty {
   echo true
   input:
     tuple val(name), path(mightBeEmpty)
     tuple path(one), path(two)

   script:
      """
       echo "running even if mightBeEmpty is empty"
       echo ${name}
       echo baseDir=${baseDir}
       cat ${baseDir}/${mightBeEmpty}
       wc -l ${baseDir}/${mightBeEmpty}
       ls ${baseDir}/${one}
       ls ${baseDir}/${two}
      """
 }


workflow unittest_empty {
    possible_empty_channel = Channel.value('just a value').combine(EMPTY)
    //EMPTY.combine(Channel.value('just a value'))

//  EMPTY2.combine(EMPTY3).set { a_tuple }
  empty(possible_empty_channel, EMPTY2.combine(EMPTY3))
  //empty(EMPTY)
  //empty(EMPTY, Channel.of([EMPTY, EMPTY]))
  //empty(Channel.empty())
}

workflow unittest_quality_control {
  show_workflow_info()
  quality_control(
    // bam_and_bai
    EMPTY.combine(EMPTY2),
    // reference:
    EMPTY_VALUE,
    // final_vcf:
    Channel.fromPath(EMPTY_FILE3),
    // coverage tuple:
    EMPTY4.combine(EMPTY5),
    // noslop_list:
    Channel.value(2),
    // noslop_bed:
    Channel.value(3),
    //EMPTY5,
//    Channel.fromPath('/Users/severin/tmp/illumina_trusightone_v02.probes.bed'),
    // contamination_test:
    Channel.value(4)
  )
}

workflow unittest_qc_analysis {
  qc_analysis(
    // bam_and_bai
    EMPTY.combine(EMPTY2),
    // final_vcf:
    Channel.fromPath(EMPTY3),
    // coverage tuple:
    EMPTY4.combine(EMPTY5),
    // alignment_metrics_file:
    EMPTY6,
    // insert_size_metrics_file:
    EMPTY7,
    // hs_metrics_file:
    EMPTY8,
    // reference
    Channel.value(5),
    // noslop_list:
    Channel.value(2),
    // contamination_test
    Channel.value(3),
  )
}

workflow unittest_trio {
  show_workflow_info()
  dragen_trio(
    // pedfile_path
    EMPTY,
    // gvcf_files
    EMPTY2,
    // gvcf_tbi
    EMPTY3
  )
}

// this is used rather than the one in dummy.nf itself:
params.dummy_storage_path = 'output-test.nf'
// however any --dummy_storage_path given on command lines wins

include { dummy } from './modules/dummy'
// this gives a warning and don't change the value:
params.dummy_storage_path = 'output-test-after-include.nf'

params.output = workflow.launchDir.toAbsolutePath()

workflow unittest_param {
  // this value is ignored, and no warning given:
  params.dummy_storage_path = 'output-in-workflow'
  show_workflow_info()
  dummy()
}