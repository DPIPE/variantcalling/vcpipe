#!/usr/bin/env nextflow

import groovy.json.JsonSlurperClassic
import Util


///////////////////////////////////
///////// PIPELINE SETUP ///////////
////////////////////////////////////

// Initialize parameters: options for all =params.,
// options for some analyses =params.get(, null)
analysis_name = params.analysis_name
type = params.type
specialized_pipeline = params.specialized_pipeline
date_analysis_requested = params.date_analysis_requested
priority = params.priority
capture_kit = params.capture_kit
gene_panel = params.gene_panel
sample_id = params.sample_id
bam = params.bam
cnv_result = params.cnv_result
bigwig = params.bigwig
qc_report = params.qc_report
final_vcf = params.final_vcf
mtdna_vcf = params.mtdna_vcf
mtdna_haplo = params.mtdna_haplo
sequencer_id = params.sequencer_id

qc_warning = params.get("qc_warning", null)

//NOTE: can be removed if we don't use executor
qc_result = params.qc_result

// parameters for mtDNA analysis
mtdna_vcf = params.get("mtdna_vcf", "")
mtdna_haplo = params.get("mtdna_haplo", "")

// parameters only for trios
gender_proband = params.get("gender_proband", null)
sample_id_mother = params.get("sample_id_mother", null)
sample_id_father = params.get("sample_id_father", null)
bam_mother = params.get("bam_mother", null)
bam_father = params.get("bam_father", null)
cnv_result_mother = params.get("cnv_result_mother", null)
cnv_result_father = params.get("cnv_result_father", null)
mtdna_haplo_mother = params.get("mtdna_haplo_mother", null)
bigwig_mother = params.get("bigwig_mother", null)
bigwig_father = params.get("bigwig_father", null)
capture_kit_mother = params.get("capture_kit_mother", null)
capture_kit_father = params.get("capture_kit_father", null)
sequencer_id_mother = params.get("sequencer_id_mother", null)
sequencer_id_father = params.get("sequencer_id_father", null)

// parameters only for CuCa
convading_log = params.get("convading_log", null)
convading_totallist = params.get("convading_totallist", null)
convading_longlist = params.get("convading_longlist", null)
convading_shortlist = params.get("convading_shortlist", null)
convading_coverage = params.get("convading_coverage", null)

// Import Gene panels
genepanels_json = new File("${params.genepanels_home}/panels.json")
genepanels = new JsonSlurperClassic().parse(genepanels_json)
genepanel = Util.getLatestGenePanelVersion(gene_panel, genepanels)
genepanels = Util.prependPath(genepanels, params.genepanels_home)

// Set the output path to the value of the output parameter, or the current directory if not set
output_path = params.get("output", workflow.launchDir)
output_path = output_path + '/'
file(output_path).mkdirs()
output_path = new File(output_path.toString()).getAbsolutePath()

ella_storage_path = "${output_path}/data/ella/"
file(ella_storage_path).mkdirs()
qc_storage_path = "${output_path}/data/qc/"
file(qc_storage_path).mkdirs()

// !!! Can be removed when running this with Steps !!!
// Copy the .analysis file under ${output_path}/data/ella to backup the .analysis file
analysis_config = "${params.base_analyses_work_path}/${analysis_name}/${analysis_name}.analysis"
new_analysis_config = "${analysis_name}.annopipe"
"cp ${analysis_config} ${output_path}/data/ella/${analysis_name}/${new_analysis_config}".execute()

// Import sensitive-db
sensitive_db_json = new File([params.sensitive_db, 'sensitive-db.json'].join(File.separator))
sensitive_db = new JsonSlurperClassic().parse(sensitive_db_json)
excap_db = sensitive_db['cnv']['exCopyDepth']['excap']['database']
wes_cnv_inhouse_db = [params.sensitive_db, excap_db].join(File.separator)
wgs_cnv_inhouse_db_dir = [params.sensitive_db, 'wgs-sv'].join(File.separator)

// Import capturekit dependent configuration (such as QC)
if (specialized_pipeline == "TUMOR"){
   capturekitSpecification = capture_kit + '_' + specialized_pipeline
} else {
   capturekitSpecification = capture_kit
}

analysis_config_json_path = "${params.vcpipe_dir}/config/analysis/${capturekitSpecification}.json"
analysis_config_json_abspath = new File(analysis_config_json_path).getAbsolutePath()
analysis_config_json = new File(analysis_config_json_abspath)
analysis_config = new JsonSlurperClassic().parse(analysis_config_json)

(gp_name, gp_version) = gene_panel.tokenize('_')

transcripts = "${genepanels[gene_panel]['transcripts']}"
phenotypes = "${genepanels[gene_panel]['phenotypes']}"
report_config = "${genepanels[gene_panel]['report_file']}"
exon_regions = "${genepanels[gene_panel]['regions']}"


cores = params.cores.default

input_regions = Channel.fromPath(transcripts)

anno_image = params.anno_path + "/anno.sif"

// Header + progress function
def HEADER = { header, name, pct ->
"""
echo ""
echo "#######################################"
echo " Starting ${header}"
echo "${name}"
echo \$(date)
echo "#######################################"
echo ""
echo "${pct}\t${header}" >| "./PROGRESS"
"""
}

///////////////////////////////////////
//////////////  Pre-checks  ///////////
///////////////////////////////////////

Util.checkGenePanel(
    genepanel, genepanels, keys=["regions", "report_file", "phenotypes", "transcripts"]
)


//////////////////////////////
// ANNO
//////////////////////////////

process anno {
    echo true
    if (!Util.env("NXF_CI")) errorStrategy 'retry'
    if (!Util.env("NXF_CI")) maxRetries 0
    stageInMode 'copy'

    input:
    env SINGULARITYENV_ANALYSIS_NAME from analysis_name
    env SINGULARITYENV_SAMPLE_ID from sample_id
    env SINGULARITYENV_SEQUENCER_ID from sequencer_id
    env SINGULARITYENV_GP_NAME from gp_name
    env SINGULARITYENV_GP_VERSION from gp_version
    env SINGULARITYENV_CAPTUREKIT from capture_kit
    env SINGULARITYENV_TYPE from type
    env SINGULARITYENV_TRANSCRIPTS from transcripts
    env SINGULARITYENV_PHENOTYPES from phenotypes
    env SINGULARITYENV_REPORT_CONFIG from report_config
    env SINGULARITYENV_EXON_REGIONS from exon_regions
    env SINGULARITYENV_BAM from bam
    env SINGULARITYENV_PRIORITY from priority
    env SINGULARITYENV_VCF from final_vcf
    env SINGULARITYENV_MTDNA_VCF from mtdna_vcf
    env SINGULARITYENV_MTDNA_HAPLO from mtdna_haplo
    env SINGULARITYENV_TARGETS_OUT__ELLA from ella_storage_path
    env SINGULARITYENV_WES_CNV_INHOUSE_DB from wes_cnv_inhouse_db
    env SINGULARITYENV_WGS_CNV_INHOUSE_DB_DIR from wgs_cnv_inhouse_db_dir
    env SINGULARITYENV_SPECIALIZED_PIPELINE from specialized_pipeline
    file input_regions

    output:
    val "start_post" into start_postcmd_annopipe


    beforeScript "source ${params.anno_path}/sourceme.annopipe"
    afterScript HEADER('anno finished', '', 95)

    """
    SINGULARITYENV_ANNOTATION_CNV=\$(
        jq -r "
            .config
            .\${SINGULARITYENV_TYPE}
            .capture_kit.\${SINGULARITYENV_CAPTUREKIT}
            .\${SINGULARITYENV_GP_NAME}
            .cnv
        " ${params.anno_path}/analysis_config.annopipe.json
    )
    export SINGULARITYENV_ANNOTATION_CNV
    if [ ${type} == trio ]; then
        export SINGULARITYENV_MOTHER_SAMPLE_ID=${sample_id_mother}
        export SINGULARITYENV_FATHER_SAMPLE_ID=${sample_id_father}
        export SINGULARITYENV_MOTHER_BAM=${bam_mother}
        export SINGULARITYENV_FATHER_BAM=${bam_father}
        export SINGULARITYENV_GENDER=${gender_proband}
        export SINGULARITYENV_MOTHER_SEQUENCER_ID=${sequencer_id_mother}
        export SINGULARITYENV_FATHER_SEQUENCER_ID=${sequencer_id_father}
        export SINGULARITYENV_MOTHER_CAPTURE_KIT=${capture_kit_mother}
        export SINGULARITYENV_FATHER_CAPTURE_KIT=${capture_kit_father}
        
        if [ ${bigwig_mother} != null ]; then
            export SINGULARITYENV_MOTHER_BIGWIG=${bigwig_mother}
        fi
        if [ ${bigwig_father} != null ]; then
            export SINGULARITYENV_FATHER_BIGWIG=${bigwig_father}
        fi
        if [ ${cnv_result_mother} != null ]; then
            export SINGULARITYENV_MOTHER_CNV_VCF=${cnv_result_mother}
        fi
        if [ ${cnv_result_father} != null ]; then
            export SINGULARITYENV_FATHER_CNV_VCF=${cnv_result_father}
        fi
        if [ ${mtdna_haplo_mother} != null ]; then
            export SINGULARITYENV_MOTHER_MTDNA_HAPLO=${mtdna_haplo_mother}
        fi
    fi

    # Bigwig Track
    if [ ${bigwig} != null ]; then
        export SINGULARITYENV_BIGWIG=${bigwig}
    fi

    # CNV files non-WGS
    if [ ${cnv_result} != null ]; then
        export SINGULARITYENV_CNV_VCF=${cnv_result}
    fi

    # If target sequencing samples, CoNVaDING files
    if [[ ${capture_kit} == CuCa* ]]; then
        export SINGULARITYENV_CONVADING_LOG=${convading_log}
        export SINGULARITYENV_CONVADING_TOTALLIST=${convading_totallist}
        export SINGULARITYENV_CONVADING_LONGLIST=${convading_longlist}
        export SINGULARITYENV_CONVADING_SHORTLIST=${convading_shortlist}
    fi
    
    if [ ${date_analysis_requested} != null ]; then
        export SINGULARITYENV_ANALYSIS_REQUESTED_DATE=${date_analysis_requested}
    fi
    
    if [ ${qc_report} != null ]; then
       export SINGULARITYENV_PIPELINE_QC_REPORT=${qc_report}
    fi

    if [ ${qc_warning} != null ]; then
        export SINGULARITYENV_PIPELINE_QC_WARNINGS=${qc_warning}
    fi

    ${HEADER('anno running', '', 20)}

    cp ${qc_result} ${qc_storage_path}

    singularity exec \
        --no-home \
        -e \
        ${anno_image} \
        annotate_with_target \
            --workdir \$(pwd) \
            --target ella \
            --vcf \${SINGULARITYENV_VCF} \
            --regions \$(readlink -f $input_regions)
    """
}

process anno_postcmd {
    echo true
    errorStrategy 'retry'
    maxRetries 1

    input:
    val(start_copy) from start_postcmd_annopipe

    afterScript HEADER('anno postcmd done', '', 100)

    """
    "${workflow.projectDir}/annopipe-post" \
        --export_priority ${priority} \
        --platform ${params.platform} \
        --analysis ${analysis_name} \
        --result ${ella_storage_path}
    """
}
