#!/usr/bin/env nextflow

/*
* DSL2 VCPIPE
*/

import groovy.json.JsonSlurperClassic // The old JsonSlurperClassic is thread safe compared to JsonSlurper
import Util


// Pipeline parameters
nextflow.enable.dsl=2

params.vcpipe_dir = workflow.projectDir.getParent().getParent()

params.output = workflow.launchDir

// !!! Can be removed when running this with Steps !!!
// Copy the .analysis file under ${params.output}/data/
analysis_config = "${params.base_analyses_work_path}/${params.analysis_name}/${params.analysis_name}.analysis"
"cp ${analysis_config} ${params.output}/".execute()


params.variantcalling_storage_path = "${params.output}/data/variantcalling/"
file(params.variantcalling_storage_path).mkdirs()
params.mt_variantcalling_storage_path = "${params.output}/data/mtdna"
file(params.mt_variantcalling_storage_path).mkdirs()
params.qc_storage_path = "${params.output}/data/qc/"
file(params.qc_storage_path).mkdirs()

progress_path = "${params.output}/PROGRESS"


// Analysis parameters
params.specialized_pipeline = null // Set to 'Dragen' if using Dragen

// Default value, the input from command line will overwrite this
params.priority = '1'
export_priority = params.priority

params.skipPostcmd = false
// Sample parameters
proband_qc_json = channel.fromPath(params.qc_json_proband)
mother_qc_json = channel.fromPath(params.qc_json_mother)
father_qc_json = channel.fromPath(params.qc_json_father)

capture_kit_path = new File("${params.vcpipe_dir}/config/analysis/${params.capture_kit}.json").getAbsolutePath()


// Mitochondrial variant calling parameters
if ( params.capture_kit == 'wgs' ) {
   mt_vcf_proband_index = params.mt_vcf_proband + '.tbi'
   mt_vcf_mother_index = params.mt_vcf_mother + '.tbi'
}


// Refdata parameters
refdata_json = new File(params.refdata_declaration)
refdata = new JsonSlurperClassic().parse(refdata_json)
refdata = Util.prependPath(refdata, params.refdata_home)


// Sensitive DB parameters
sensitive_db_json = new File("${params.sensitive_db}/sensitive-db.json")
sensitive_db = new JsonSlurperClassic().parse(sensitive_db_json)

// Input data
gvcf_input = Channel.fromPath( [
    new File(params.gvcf_proband).getAbsolutePath(),
    new File(params.gvcf_mother).getAbsolutePath(),
    new File(params.gvcf_father).getAbsolutePath(),
] )

Util.checkFile(refdata.reference.fasta)
Util.checkFile(refdata.knownSites.dbsnp)
if (params.capture_kit == 'wgs') {
    calling_regions_path = Channel.empty()
    noslop_bed = Channel.empty()
    mtvcf_input = Channel.fromPath( [
        params.mt_vcf_proband,
        params.mt_vcf_mother,
    ] ).collect()
    mtvcf_tbi_input = Channel.fromPath( [
        mt_vcf_proband_index,
        mt_vcf_mother_index,
    ] ).collect()
} else {
    calling_regions_path = refdata.captureKit[params.capture_kit]['calling_region']
    Util.checkFile(calling_regions_path)
    noslop_bed = Channel.fromPath( refdata.captureKit[params.capture_kit]['noslop_bed'] )
    mtvcf_input = Channel.empty()
    mtvcf_tbi_input = Channel.empty()
}


// Pipeline starting log info
log.info """
=========================================
               TRIOPIPE
=========================================
Analysis       : ${params.analysis_name}
Capture kit    : ${params.capture_kit}
Output path    : ${params.output}
Cores          : ${params.cores}
"""
log.info """
-----------------------------------------
Input:
"""
log.info "--capture-kit ${params.capture_kit}"
log.info "--proband-sample-id ${params.sample_id_proband}"
log.info "--proband-gender ${params.gender_proband}"
log.info "--mother-sample-id ${params.sample_id_mother}"
log.info "--father-sample-id ${params.sample_id_father}"
log.info "--gvcf-proband ${params.gvcf_proband}"
log.info "--gvcf-mother ${params.gvcf_mother}"
log.info "--gvcf-father ${params.gvcf_father}"
log.info "--proband-qc-json ${params.qc_json_proband}"
log.info "--mother-qc-json ${params.qc_json_mother}"
log.info "--father-qc-json ${params.qc_json_father}"
log.info """
=========================================
"""


/*
WORKFLOW:
*/
include { tabix } from './modules/tabix'
include { trio_pedigree_file } from './modules/pedigree'
include { dragen_trio } from './modules/dragen'
include { vc_trio } from './modules/variantcalling/workflows'
include {
    quality_control_trio ;
} from './modules/qc/workflows'
include {
    mt_variantcalling_trio ;
} from './modules/variantcalling/mitochondrial'
include { postcmd } from './modules/postcmd'

workflow {
    main:

    tabix(
        gvcf_input,
        "-p vcf"
    )

    trio_pedigree_file(
        params.analysis_name,
        params.sample_id_proband,
        params.gender_proband,
        params.sample_id_mother,
        params.sample_id_father,
    )

    // VARIANT CALLING
    if ( params.specialized_pipeline == 'Dragen' ) {
        dragen_trio(
            trio_pedigree_file.out.pedigree,
            gvcf_input.collect(),
            tabix.out.tbi.collect(),
        )
        final_vcf = dragen_trio.out.final_vcf
    } else {
        vc_trio (
            gvcf_input.collect(),
            tabix.out.tbi.collect(),
            refdata.reference.fasta,
            refdata.knownSites.dbsnp,
            "${refdata.reference.general}/hapmap_3.3.b37.vcf",
            "${refdata.knownSites['1000G']}/1000G_omni2.5.b37.vcf",
            "${refdata.knownSites['1000G']}/1000G_phase1.snps.high_confidence.b37.vcf",
            refdata.knownSites.Mills,
        )
        final_vcf = vc_trio.out.final_vcf
    }

    // QUALITY CONTROL
    quality_control_trio(
        final_vcf,
        trio_pedigree_file.out.pedigree,
        proband_qc_json,
        mother_qc_json,
        father_qc_json,
        params.sample_id_proband,
        params.sample_id_mother,
        params.sample_id_father,
    )
    analysis_qc_result = quality_control_trio.out.qc_result

    // MITOCHONDRIAL VARIANT-CALLING
    if ( params.capture_kit == 'wgs' ) {
        mt_variantcalling_trio(
            mtvcf_input,
            mtvcf_tbi_input,
        )
        mt_variantcalling_trio_done = mt_variantcalling_trio.out.done
    } else {
        mt_variantcalling_trio_done = Channel.of('mt_variantcalling_skipped')
    }

    // POST COMMAND
    if (!params.skipPostcmd) {
        postcmd(
            'triopipe',
            export_priority,
            analysis_qc_result,
            quality_control_trio.out.done.mix(
                mt_variantcalling_trio_done
            ).collect()
        )
    }

}
