#!/usr/bin/env nextflow

/*
* DSL2 VCPIPE
*/

import groovy.json.JsonSlurperClassic // The old JsonSlurperClassic is thread safe compared to JsonSlurper
import Util

// Pipeline parameters
nextflow.enable.dsl=2

params.output = workflow.launchDir
params.skipPostcmd = false

// !!! Can be removed when running this with Steps !!!
// Copy the .analysis file under ${params.output}/data/
analysis_config = "${params.base_analyses_work_path}/${params.analysis_name}/${params.analysis_name}.analysis"
"cp ${analysis_config} ${params.output}/".execute()


// Parameters used for modules
params.mapping_storage_path = "${params.output}/data/mapping/"
params.variantcalling_storage_path = "${params.output}/data/variantcalling/"
params.mt_variantcalling_storage_path = "${params.output}/data/mtdna"
params.qc_storage_path = "${params.output}/data/qc/"
params.cnv_storage_path = "${params.output}/data/cnv/"

String short_sample_id_str = (params.sample_id).split('-')[2]

// Capture kit parameters
params.capture_kit = 'wgs'
IS_TUMOR = params.specialized_pipeline == 'TUMOR'
if ( IS_TUMOR ) {
    capturekitSpecification = "${params.capture_kit}_${params.specialized_pipeline}"
} else {
    capturekitSpecification = params.capture_kit
}
params.capture_kit_path = new File("${params.vcpipe_dir}/config/analysis/${capturekitSpecification}.json").getAbsolutePath()

// Default value, the input from command line will overwrite this
params.priority = '1'
export_priority = params.priority

if (params.containsKey("taqman")) {
    fingerprinting = 'true'
    taqman = Channel.fromPath(new File(params.taqman).getAbsolutePath())
} else {
    fingerprinting = 'false'
}

if ( params.specialized_pipeline == 'Dragen' ) {
    params.bam_header = [
        "--RGID ${params.sample_id} ",
        "--RGDT ${new Date().format('yyyy-MM-dd')} ",
        "--RGSM ${params.sample_id} ",
        '--RGPL ILLUMINA ',
    ].join('')
} else {
    params.bam_header = [
        '@RG\\t',
        "ID:${params.sample_id}",
        "\\tDT:${new Date().format('yyyy-MM-dd')}",
        "\\tSM:${params.sample_id}",
        '\\tPL:ILLUMINA',
    ].join('')
}

// Refdata parameters
refdata_json = new File(params.refdata_declaration)
refdata = new JsonSlurperClassic().parse(refdata_json)
refdata = Util.prependPath(refdata, params.refdata_home)


// Sensitive DB parameters
sensitive_db_json = new File("${params.sensitive_db}/sensitive-db.json")
sensitive_db = new JsonSlurperClassic().parse(sensitive_db_json)


// Regression data
regression_test = 'false'
if (["NA12878","tNA12878"].any({x -> short_sample_id_str.startsWith(x)})) {
    regression_test = 'true'
    hc_region = channel.fromPath( refdata.regression.na12878.HIGH_CONF_CALL_ORI_HCREGION )
    truth_vcf = channel.fromPath( refdata.regression.na12878.HIGH_CONF_CALL_ORI )
} else if (["NA24385","tNA24385","HG002C2","tHG002C2"].any({x -> short_sample_id_str.startsWith(x)})){
    regression_test = 'true'
    hc_region = channel.fromPath( refdata.regression.na24385.HIGH_CONF_CALL_ORI_HCREGION )
    truth_vcf = channel.fromPath( refdata.regression.na24385.HIGH_CONF_CALL_ORI )
} else if (["NA24149","tNA24149","HG003B2","tHG003B2"].any({x -> short_sample_id_str.startsWith(x)})){
    regression_test = 'true'
    hc_region = channel.fromPath( refdata.regression.na24149.HIGH_CONF_CALL_ORI_HCREGION )
    truth_vcf = channel.fromPath( refdata.regression.na24149.HIGH_CONF_CALL_ORI )
} else if (["NA24143","tNA24143","HG004B3","tHG004B3"].any({x -> short_sample_id_str.startsWith(x)})){
    regression_test = 'true'
    hc_region = channel.fromPath( refdata.regression.na24143.HIGH_CONF_CALL_ORI_HCREGION )
    truth_vcf = channel.fromPath( refdata.regression.na24143.HIGH_CONF_CALL_ORI )
}

// Input data
reads1 = new File(params.reads1).getAbsolutePath()
reads2 = new File(params.reads2).getAbsolutePath()

if ( params.capture_kit != 'wgs' ) {
    Util.checkCaptureKit(params.capture_kit, refdata, keys=["noslop_list", "noslop_bed"])
}


// Pipeline starting log info
log.info """
=========================================
               BASEPIPE
=========================================
Sample         : ${params.sample_id}
Analysis       : ${params.analysis_name}
Priority       : ${params.priority}

Read 1
           Path: ${reads1}

Read 2
           Path: ${reads2}

Capture kit    : ${params.capture_kit}
Output path    : ${params.output}
Cores          : ${params.cores}
Taqman check   : ${fingerprinting}
Skip postcmd   : ${params.skipPostcmd}
=========================================
"""

// Pipeline input channels
index_basename = refdata.reference.index['pms2cl_masked_bwa-mem2']
if (params.capture_kit == 'wgs') {
    calling_regions_path = Channel.empty()
    calling_regions = Channel.fromList( [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13',
        '14', '15', '16', '17', '18', '19', '20', '21', '22', 'X', 'Y', 'MT',
    ] )
    noslop_list = Channel.empty()
    noslop_bed = Channel.empty()
    calling_regions_msh2_path = Channel.fromPath( refdata.captureKit['msh2']['calling_region'] )
} else {
    calling_regions_path = refdata.captureKit[params.capture_kit]['calling_region']
    Util.checkFile(calling_regions_path)
    calling_regions = Channel.fromList( [ calling_regions_path ] )
    noslop_list = Channel.fromPath( refdata.captureKit[params.capture_kit]['noslop_list'] )
    noslop_bed = Channel.fromPath( refdata.captureKit[params.capture_kit]['noslop_bed'] )
    calling_regions_msh2_path = Channel.fromPath( refdata.captureKit['msh2']['calling_region'] )
}

Util.checkFile(refdata.reference.fasta)
Util.checkFile(refdata.knownSites.dbsnp)
if ( ['wgs', 'CuCaV1', 'CuCaV2', 'CuCaV3', 'TSCarV1'].contains(params.capture_kit) ) {
    intervals_slop50 = Channel.empty()
    known_sites = Channel.empty()
} else {
    Util.checkFile(refdata.knownSites.Mills)
    Util.checkFile(refdata.knownSites['1000G'])
    intervals_slop50 = Channel.fromPath( refdata.captureKit[params.capture_kit]['slop50_list'] )
    known_sites = Channel.of( [refdata.knownSites.dbsnp, refdata.knownSites.Mills, "${refdata.knownSites['1000G']}/1000G_phase1.indels.b37.vcf"] )
}

if ( ['CuCaV1', 'CuCaV2', 'CuCaV3', 'TSCarV1'].contains(params.capture_kit) ) {
    contamination_test = Channel.fromPath( refdata.captureKit[params.capture_kit]['contamination_test'] )
} else {
    contamination_test = Channel.empty()
}

if ( ['agilent_sureselect_v05', 'agilent_sureselect_v07', 'twist_humancoreexome_v01'].contains(params.capture_kit) ) {
    excopydepth_background = Channel.fromPath( "${params.sensitive_db}/${sensitive_db['cnv']['exCopyDepth']['excap']['background']}" )
} else {
    excopydepth_background = Channel.empty()
}

if ( ['CuCaV2', 'CuCaV3'].contains(params.capture_kit) ) {
    convading_regions = Channel.fromPath( refdata.captureKit[params.capture_kit].cnv_regions_bed )
    convading_background = Channel.fromPath( "${params.sensitive_db}/${sensitive_db['cnv']['convading'][params.capture_kit]['background']}" )
} else {
    convading_regions = Channel.empty()
    convading_background = Channel.empty()
}


/*
WORKFLOW:
*/
include { mapping } from './modules/mapping'
include { extract } from './modules/extract'
include { dragen } from './modules/dragen'
include {
    vc_germline ;
    vc_somatic ;
    vc_sv_cnv ;
} from './modules/variantcalling/workflows'
include {
    depth_of_coverage ;
    msh2_variants ;
    sort_vcf ;
} from './modules/variantcalling/germline'
include {
    svdb_merge_dragen ;
} from './modules/variantcalling/sv_cnv'
include {
    qc_coverage ;
    qc_fingerprinting ;
    quality_control ;
    regression ;
} from './modules/qc/workflows'
include {
    mt_variantcalling ;
} from './modules/variantcalling/mitochondrial'
include { postcmd } from './modules/postcmd'

workflow {
    main:

    reads_archives = Channel.of(['R1', reads1], ['R2', reads2])

    reads_maps = extract(
        reads_archives,
        refdata.reference.fasta
    ) | branch {
        R1: it['nr'] == 'R1'
        R2: it['nr'] == 'R2'
    }

    reads1_ch = reads_maps.R1 | map { it -> it['name'] }
    reads2_ch = reads_maps.R2 | map { it -> it['name'] }

    reads = reads1_ch.combine(reads2_ch)

    // DRAGEN VARIANT CALLING
    if ( params.specialized_pipeline == 'Dragen' ) {
        dragen(
            reads,
            refdata.knownSites.dbsnp,
            params.get("downsampling_depth", "NA")
        )

        bam_and_bai = dragen.out.bam_and_bai
        dragen_mapping_metrics = dragen.out.mapping_metrics

        depth_of_coverage(
            bam_and_bai,
            calling_regions_msh2_path,
            refdata.reference.fasta,
        )
        msh2_variants(
            dragen.out.final_vcf,
            depth_of_coverage.out.depth,
        )
        sort_vcf(
            msh2_variants.out.msh2_vcf,
        )

        final_vcf = sort_vcf.out.final_vcf

        svdb_merge_dragen(
            dragen.out.manta_vcf,
            dragen.out.canvas_vcf,
        )
        vc_sv_cnv_done = svdb_merge_dragen.out.done

    // NON-DRAGEN VARIANT CALLING
    } else {
        bam_and_bai = mapping(
            reads,
            index_basename,
            refdata.reference.fasta,
            intervals_slop50,
            known_sites,
        )
        if ( IS_TUMOR ) {
            final_vcf = vc_somatic(
                bam_and_bai,
                refdata.reference.fasta,
                calling_regions,
                refdata.mutect2.germline_resource,
                "${params.sensitive_db}/${sensitive_db['tumor'][params.capture_kit]['panel_of_normal']}",
                refdata.mutect2.variants_for_contamination,
            )
        } else {
            final_vcf = vc_germline(
                bam_and_bai,
                refdata.reference.fasta,
                calling_regions,
                refdata.knownSites.dbsnp,
                "${refdata.reference.general}/hapmap_3.3.b37.vcf",
                "${refdata.knownSites['1000G']}/1000G_omni2.5.b37.vcf",
                "${refdata.knownSites['1000G']}/1000G_phase1.snps.high_confidence.b37.vcf",
                "${refdata.knownSites.Mills}",
            )
        }
        vc_sv_cnv(
            bam_and_bai,
            index_basename,
            refdata.reference.fasta,
            excopydepth_background,
            convading_regions,
            convading_background,
        )
        vc_sv_cnv_done = vc_sv_cnv.out.done

    }

    // QUALITY CONTROL
    qc_coverage(
        bam_and_bai,
        refdata.reference.fasta,
    )

    qc_coverage_done = qc_coverage.out.done

    quality_control(
        bam_and_bai,
        refdata.reference.fasta,
        final_vcf,
        qc_coverage.out.coverage,
        params.specialized_pipeline == 'Dragen' ? dragen.out.mapping_metrics : Channel.fromPath("NO_FILE"),
        noslop_list,
        noslop_bed,
        contamination_test,
    )
    qc_report_done = quality_control.out.done
    analysis_qc_result = quality_control.out.qc_result

    // MITOCHONDRIAL VARIANT CALLING
    if ( params.capture_kit == 'wgs' ) {
        mt_variantcalling(
            bam_and_bai,
            quality_control.out.qc_result,
            refdata.reference.fasta,
            refdata.reference.mtdna.mt_fasta,
            refdata.reference.mtdna.mt_shifted_fasta,
            refdata.reference.mtdna.mt_shift_back_chain,
            refdata.reference.mtdna.mt_blacklist,
            refdata.reference.mtdna.mt_shifted_blacklist,
        )
        mt_variantcalling_done = mt_variantcalling.out.done
    } else {
        mt_variantcalling_done = Channel.of('mt_variantcalling_skipped')
    }

    // FINGERPRINTING
    if ( fingerprinting == 'true' ) {
        qc_fingerprinting(
            bam_and_bai,
            quality_control.out.qc_result,
            refdata.reference.fasta,
            refdata.knownSites.dbsnp,
            taqman,
        )
        fingerprinting_done = qc_fingerprinting.out.done
    } else {
        fingerprinting_done = Channel.of('qc_fingerprinting_skipped')
    }

    // REGRESSION TESTING
    if ( regression_test == 'true' ) {
        regression(
            final_vcf,
            calling_regions_path,
            noslop_bed,
            hc_region,
            truth_vcf,
            refdata.reference.happy,
        )
        regression_done = regression.out.done
    } else {
        regression_done = Channel.of('regression_skipped')
    }

    // POST COMMAND
    if (!params.skipPostcmd) {
        postcmd(
            'basepipe',
            export_priority,
            analysis_qc_result,
            vc_sv_cnv_done.mix(
                mt_variantcalling_done,
                fingerprinting_done,
                regression_done,
                qc_coverage_done,
                qc_report_done
            ).collect(),
        )
    }

}
