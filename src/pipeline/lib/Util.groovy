import java.security.MessageDigest
import groovy.json.JsonSlurperClassic
import static groovy.io.FileType.FILES


class Util {

    /**
     * merge - Merge maps with recursive nested maps
     * https://stackoverflow.com/questions/27475111/merge-maps-with-recursive-nested-maps-in-groovy
     * Usage: merged_map = merge(map1, map2)
     */
    static Map merge(Map... maps) {
        Map result
        if (maps.length == 0) {
            result = [:]
        } else if (maps.length == 1) {
            result = maps[0]
        } else {
            result = [:]
            maps.each { map ->
                map.each { k, v ->
                    result[k] = result[k] instanceof Map ? merge(result[k], v) : v
                }
            }
        }
        result
    }

    /**
     * findFiles - Find files recursively
     *    - returns a list of files found
     * @params pattern file matches this pattern
     * @params alt_pattern file matches this pattern if pattern matches nothing
     * @params nullable return empty list instead of throwing exception
     * @params folder search under this folder
     */
    static def findFiles(String folder, pattern, alt_pattern = null, nullable = false) {
        def files = []
        def dir = new File(folder)
        dir.traverse(type: FILES, nameFilter: pattern) { files.add(it) };
        if (files.empty) {
            if (alt_pattern) {
                dir.traverse(type: FILES, nameFilter: alt_pattern) { files.add(it) };
                if (files.empty && !nullable) {
                    def msg = "Cannot find any file with pattern '" + pattern + "' or pattern '" + alt_pattern + "' in '" + folder + "' folder.\n"
                    throw new Exception(msg)
                }
            } else {
                if (!nullable) {
                    def msg = "Cannot find any file with pattern '" + pattern + "' in '" + folder + "' folder.\n"
                    throw new Exception(msg)
                }
            }
        }
        return files
    }

    /**
     * findFile - Find a file recursively
     *    - returns the first file found
     * @params pattern file matches this pattern
     * @params alt_pattern file matches this pattern if pattern matches nothing
     * @params nullable return empty list instead of throwing exception
     * @params folder search under this folder
     */
    static def findFile(String folder, pattern, alt_pattern = null, nullable = false) {
        def files = findFiles(folder, pattern, alt_pattern, nullable)
        if (!files.empty) {
            return files[0]
        } else {
            return null
        }
    }

    /**
     * env - Checks if variable exists
     */
    static def env(String name) {
        System.getenv().containsKey(name) ? System.getenv(name) : null
    }

    static def checkFile(def path, def size = null, def md5 = null) {
        def File file = new File(path)
        if (!file.exists()) {
            throw new Exception("File " + path + " doesn't exist.")
        }
        if (size != null && size > 0 && file.length() != size) {
            throw new Exception("Size of file " + path + " doesn't match. File is " + file.length() + ", but should be " + size)
        }
    }

    /**
     * checkPedigree - Checks if
     *   - the pedigree is well-formed
     * Throws exceptions otherwise.
     *
     * @param analysis_config JSON object (HashMap) to the *.analysis
     */
    static def checkPedigree(def analysis_config) {
        System.out.println("Checking pedigree for '" + analysis_config['name'] + "' analysis.")
        try {
            assert analysis_config['params']['pedigree']['father']['sample'] in analysis_config['samples']
            assert analysis_config['params']['pedigree']['father']['gender'] == 'male'
            assert analysis_config['params']['pedigree']['mother']['sample'] in analysis_config['samples']
            assert analysis_config['params']['pedigree']['mother']['gender'] == 'female'
            assert analysis_config['params']['pedigree']['proband']['sample'] in analysis_config['samples']
            assert analysis_config['params']['pedigree']['proband']['gender'] in ['male', 'female']
        } catch (AssertionError ex) {
            def msg = "ERROR. Failed while checking pedigree for '" + analysis_config['name'] + "' analysis.\n"
            throw new Exception(msg)
        }
    }


    /**
     * checkGenePanel - Checks if
     *   - the clinical genepanel exists in the panels
     *   - all 'keys' exist (key, value, files) exists for that genepanel
     * Throws exceptions otherwise.
     *
     * @param genepanel string name of the gene panel
     * @param panels JSON object (HashMap) to the panels.json
     * @param keys list of keys to check in the genepanel dict
     */
    static def checkGenePanel(def genepanel,
                              def panels,
                              def keys = ["report_file", "regions", "transcripts", "phenotypes"]) {

        System.out.println("Checking clinical gene panel '" + genepanel + "' in panels.")

        if (!(genepanel in panels)) {
            def msg = "ERROR. The gene panel '" + genepanel + " does not appear to exist in panels."
            throw new Exception(msg)
        }

        for (String key in keys) {
            if (!(key in panels[genepanel])) {
                def msg = "ERROR: " + key + " not in panels[" + genepanel + "]"
                throw new Exception(msg)
            }

            try {
                checkFile(panels[genepanel][key])
            } catch (Exception ex) {
                def msg = "ERROR. Failed while checking panels for panels['" + genepanel + "']['" + key + "'].\n"
                msg += "File " + panels[genepanel][key] + " does not exist!"
                throw new Exception(msg)
            }
        }
    }


    /**
     * checkCaptureKits - Checks if
     *   - calls checkCaptureKit for each sample in analysis
     * Throws exceptions otherwise.
     *
     * @param analysis_config JSON object (HashMap) to the *.analysis
     * @param panels JSON object (HashMap) to the panels.json
     * @param full_config JSON object (HashMap) to the settings*.json
     * @parans keys list of keys to check in the genepanel dict
     */
    static def checkCaptureKits(def analysis_config,
                                def panels,
                                def full_config,
                                def keys = ["gene_list", "noslop_bed", "slop50_list"]) {

        for (String sample : analysis_config['samples']) {

            def sample_json = new File(full_config['base_sample_path'] + '/' + sample + '/' + sample + '.sample').getAbsoluteFile()
            def sample_config = new JsonSlurperClassic().parse(sample_json)

            checkCaptureKit(sample_config, panels, keys)
        }
    }

    /**
     * checkCaptureKit - Checks if
     *   - the capture kit of that sample exists in capture kit overview
     * Throws exceptions otherwise.
     *
     * @param sample_config JSON object (HashMap) to the *.sample
     * @param kits JSON object (HashMap) to the capture kit overview file
     * @parans keys list of keys to check in the genepanel dict
     */
    static def checkCaptureKit(def capturekit,
                               def kits,
                               def keys=["gene_list",  "noslop_bed", "slop50_list"]) {

        System.out.print("Checking for capture kit for '" + capturekit + "':")
        System.out.println(capturekit)
        if (!(capturekit in kits['captureKit'])) {
            def msg = "ERROR. The capture kit '" + capturekit + "' does not appear to exist in kit overview."
            throw new Exception(msg)
        }

        for (String key in keys) {
            if (!(key in kits['captureKit'][capturekit])) {
                def msg = "ERROR: " + key + " not in kits['captureKit']['" + capturekit + "']"
                throw new Exception(msg)
            }

            try {
                checkFile(kits['captureKit'][capturekit][key])
            } catch (Exception ex) {
                def msg = "ERROR. Failed while checking kits for kits['captureKit']['" + capturekit + "']['" + key + "'].\n"
                msg += "File " + kits['captureKit'][capturekit][key] + " does not exist!"
                throw new Exception(msg)
            }
        }
        System.out.println("Capture kit check performed")
    }

    /**
     * whois - returns the familly affiliation ("proband", "mother" or "father")
     * of that sample_name in the analysis
     * Returns "proband" if the analysis is not a trio.
     *
     * @param sample_name string with the name of the sample
     * @param analysis_config JSON object (HashMap) to the *.analysis
     */
    static def whois(def sample_name, def analysis_config) {
        if ('pedigree' in analysis_config.params) {
            for (e in analysis_config.params.pedigree) {
                if (e.value.sample == sample_name) return e.key
            }
            def msg = "${sample_name} not part of pedigree"
            throw new Exception(msg)

        }
        return 'proband'
    }

    /**
     * igvOrder - returns the igv track order of the family affilation, starting from proband (1),
     * mother (2), father(3) of that sample_name in the analysis
     *
     * @param sample_name string with the name of the sample
     * @param analysis_config JSON object (HashMap) to the *.analysis
     * @param start_order Integer to start from
     */
    static def igvOrder(def sample_name, def analysis_config, def start_order) {
        def result
        def val = Util.whois(sample_name, analysis_config)
        switch (val) {
            case 'proband':
                result = start_order + 1
                break
            case 'mother':
                result = start_order + 2
                break
            case 'father':
                result = start_order + 3
                break
            default:
                result = start_order + 4
                break
        }
        return result
    }

    /**
     * getLatestGenePanelVersion - appends latest version to gene panel
     * Returns itself if found as is (that is, the version is already supplied)
     * Returns itself if not found in panels
     *
     * @param gene_panel the string name of the gene panel
     * @param panels JSON object (HashMap) to the panels.json
     */
    static def getLatestGenePanelVersion(def gene_panel,
                                         def panels) {

        if (gene_panel in panels) {
            return gene_panel
        }

        def latest_version = null
        panels.each { k, v ->
            def (gp_name, gp_version) = k.split('_')
            if (gp_name == gene_panel) { // should only be one entry
                if (gp_version) {
                    latest_version = gp_version
                }
            }
        }
        if (latest_version) {
            System.out.println("Latest version found for gene panel " +
                    gene_panel + " : " + latest_version)
            return gene_panel + '_' + latest_version
        }
        return gene_panel
    }

    /**
     * prependPathWithLookup - prepend every String value in a Map with value from a lookup map,
     * works with nested keys thanks to recursion
     *
     * @param d the Map (dictionary) where values contain relative paths
     * @param lookup the Map (dictionary) that contains the base path(s) used for construction abs paths
     */
    static def prependPathWithLookup(def d,
                                     def lookup) {
        Map result
        if (d.length == 0) {
            result = [:]
        } else if (d.length == 1) {
            result = d[0]
        } else {
            result = [:]
            d.each { k, v ->
                def new_v
                if (v instanceof Map) {
                    new_v = prependPathWithLookup(v, lookup)
                } else if (v instanceof String) {
                    def (prefix, rest) = v.split('/', 2)
                    new_v = (prefix in lookup) ? lookup[prefix] + '/' + rest : v
                }
                result[k] = new_v
            }
        }
        return result
    }

    /**
     * prependPath - prepend every String value in a Map with basepath
     * works with nested keys thanks to recursion
     *
     * @param d the Map (dictionary) where values contain relative paths
     * @param basepath the path that will be used to construct absolute paths
     */
    static def prependPath(def d,
                           def basepath) {
        Map result
        if (d.length == 0) {
            result = [:]
        } else if (d.length == 1) {
            result = d[0]
        } else {
            result = [:]
            d.each { k, v ->
                def new_v
                if (v instanceof Map) {
                    new_v = prependPath(v, basepath)
                } else if (v instanceof String) {
                    new_v = basepath + '/' + v
                }
                result[k] = new_v
            }
        }
        return result
    }

}