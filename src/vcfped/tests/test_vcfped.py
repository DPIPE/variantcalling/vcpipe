import os
from vcfped.vcfped import vcfped


def test_vcfped_compiles():
    vcf_file = os.path.dirname(os.path.abspath(__file__)) + '/../testfiles/trioHG002_22X.vcf'
    _ = vcfped(vcf_file)
    assert True, "vcfped run successfully"
