import os
import sys

# This file is given a "strange" name so it won't be run as a pytest test
# TODO: check -v parameter, if variables are among *available* variables.
# TODO: include pairwise thresholds in output

trueG = ["Male", "Male", "Female"]
trueP = ["Parent-child", "Parent-child"]
trueT = ["Regular trio"]

this_dir = os.path.dirname(os.path.abspath(__file__))
basic_command = "python3 {dir}/../vcfped.py {dir}/../testfiles/trioHG002_22X.vcf -o test ".format(dir=this_dir)


def setup():
    if not os.path.exists("TEMP"): 
        os.mkdir("TEMP")
    os.chdir("TEMP")


def cleanup():
    if not os.getcwd().endswith("TEMP"): return
    os.chdir("..")
    for f in os.listdir("TEMP"):
        os.remove("TEMP/" + f)
    os.rmdir("TEMP")


def test_gender():
    with open("test.gender", 'r') as inf:
        g = [line.strip().split('\t')[7] for line in inf][1:]
    if g != trueG: 
        raise RuntimeError("Gender test failed!\nExpected: %s\nGot: %s" %(trueG, g))


def test_pair():
    with open("test.pair", 'r') as inf:
        p = [line.strip().split('\t')[9] for line in inf][1:]
    if p != trueP: 
        raise RuntimeError("Pairwise test failed!\nExpected: %s\nGot: %s" %(trueP, p))


def test_trio():
    with open("test.trio", 'r') as inf:
        t = [line.strip().split('\t')[10] for line in inf][1:]
    if t != trueT: 
        raise RuntimeError("Trio test failed!\nExpected: %s\nGot: %s" %(trueT, t))


def test(parameters):
    print(parameters)
    _ = os.system(basic_command + parameters)
    if not '-ped' in parameters: 
        test_gender(); test_pair(); test_trio()
    
setup()
try:
    #tt = vcfped.vcfped("../testfiles/trioHG002_22X.vcf", pedfile="../testfiles/trio.ped")
    #if not tt: sys.exit()
    #test("")
    #test("-ped ../testfiles/trio.ped")

    # gives error message as the ped file has four lines, not three lines:
   with open("testped.ped", 'w') as p:
       p.write("1\tHG002\tHG003\tHG004\t1\n1\tHG003\t0\t0\t1\n1\tHG004\t0\t0\t2\n1\tHG004\t0\t0\t2")
   test("-ped testped.ped --all")

  # These fails, March 2022, vcpipe 5
#    test("-v AD -male 0 -t1 95".format(dir=this_dir))
#    test("-ped {dir}/../testfiles/trio.ped -v AD -male 0 -t1 95 --all".format(dir=this_dir))
   test("-v AD -male 0 -t1 95 --all".format(dir=this_dir))
   #test("-v QUAL")
   #test("-v DP GQ")
   #test("-e 1000")
except Exception as e:
    print(e)
    sys.exit(1)
        

print("All test passed")

cleanup()