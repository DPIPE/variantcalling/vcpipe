import os
import abc
from collections import namedtuple

from qc.fastqc import FastQC


class BaseSampleQCProcessor(object):

    def __init__(self, sample_path):
        self.sample_path = sample_path

    def getFastQCPaths(self):

        return [
            '/work/genomic/repo/sample/real_agilentV1_chr5/real_agilentV1_chr5.100k.R1.fastq_fastqc/real_agilentV1_chr5.100k.R1_fastqc',
            '/work/genomic/repo/sample/real_agilentV1_chr5/real_agilentV1_chr5.100k.R2.fastq_fastqc/real_agilentV1_chr5.100k.R2_fastqc'
        ]

    @abc.abstractmethod
    def check(self):
        pass


class SampleFastQcChecks(BaseSampleQCProcessor):

    name = 'SampleFastQcChecks'

    def _checkSequenceQuality(self, data):
        """
        Checks that a certain percentage of sequences lies within a given quality threshold.
        """

        QUALITY_THRESHOLD = 30
        PERCENTAGE_ABOVE = 80

        seq_scores = data['Per sequence quality scores']['data']

        q_seqs = 0
        total_seqs = 0
        for c, q in zip(seq_scores['Count'], seq_scores['Quality']):
            if q > QUALITY_THRESHOLD:
                q_seqs += c
            total_seqs += c

        percentage = q_seqs / total_seqs

        print(percentage)
        passed = percentage >= PERCENTAGE_ABOVE

        return passed

    def check(self):
        result = namedtuple('SampleQCResult', ['passed', 'data'])

        checks = {
            'SequenceQuality': self._checkSequenceQuality
        }

        data = list()
        passed = True
        for path in self.getFastQCPaths():

            fq = FastQC(os.path.join(path, 'fastqc_data.txt'))
            fq_data = fq.parse()

            for name, check_func in checks.items():
                check_result = check_func(fq_data)

            data.append(fq_data)
            if not check_result:
                passed = False

        return result(passed, data)


class SampleQC(object):

    def __init__(self, sample_path):
        self.sample_path = sample_path
        self.processors = [
            SampleFastQcChecks
        ]

    def check(self):
        data = dict()
        for processor in self.processors:
            p = processor(self.sample_path)
            passed, ds = p.check()
            data[p.name] = {
                'passed': passed,
                'data': ds
            }

        passed = all(v['passed'] for v in list(data.values()))
        return (passed, data)




if __name__ == "__main__":

    sq = SampleQC('test')
    passed, data = sq.check()
    # import pprint; print '\033[91m================\nsampleqc.py (104):\n================\033[0m'; pprint.pprint(data); print '\033[91m================\033[0m'
