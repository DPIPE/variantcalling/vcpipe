import sys
import subprocess
import os
import abc
import json
import argparse
from collections import defaultdict, OrderedDict

from settings import settings
from util import CsvParser, CoverageData
from qc import vcfquality


class BaseAnalysisQCProcessor(object):

    def __init__(self, config, analysis_config):
        self.config = config
        self.analysis_config = analysis_config

    def getQcSettings(self):
        return self.analysis_config

    @abc.abstractmethod
    def check(self):
        pass


# value 0.8256 is within [0.83,1.00]
def withinRange(value, threshold_interval):
    return round(float(value), 2) >= threshold_interval[0] and value < threshold_interval[1]


class AnalysisCollectAlignmentSummaryMetrics(BaseAnalysisQCProcessor):

    name = 'CollectAlignmentSummaryMetrics'

    def _parse(self, output):
        with open(output) as fd:
            lines = fd.readlines()

            idx_start = None
            idx_stop = None
            for i, l in enumerate(lines):
                if l.startswith('## METRICS CLASS'):
                    idx_start = i
                if l.startswith('## HISTOGRAM'):
                    idx_stop = i
                    break
            if idx_start is None or idx_stop is None:
                raise RuntimeError("Couldn't find data tag for {}".format(self.name))

            tsv_data = ''.join(lines[idx_start + 1:idx_stop])
            parser = CsvParser(dialect='excel-tab')
            data = parser.parse(tsv_data)
            return data

    def check(self):

        output = self.config['alignment_summary_metrics']

        if output is None or not os.path.exists(output):
            raise RuntimeError("{} analysis file was not created: {}".format(self.name, output))

        data = self._parse(output)

        # Just for data collection, return True as we're not performing any checks
        return True, data, {}


class AnalysisCollectInsertSizeMetrics(BaseAnalysisQCProcessor):

    name = 'CollectInsertSizeMetrics'

    def _parse(self, output):
        with open(output) as fd:
            lines = fd.readlines()

            idx = None
            idx2 = None
            for i, l in enumerate(lines):
                if l.startswith('## METRICS CLASS'):
                    idx = i
                if l.startswith('## HISTOGRAM'):
                    idx2 = i

            if idx is None or idx2 is None:
                raise RuntimeError("Couldn't find data tag for {}".format(self.name))

            tsv_data1 = ''.join(lines[idx + 1:idx2])
            tsv_data2 = ''.join(lines[idx2 + 1:])
            parser = CsvParser(dialect='excel-tab')
            data = {
                'metrics': parser.parse(tsv_data1),
                'histogram': parser.parse(tsv_data2)
            }
            return data

    def check(self):

        output = self.config['insert_size_metrics']

        if output is None or not os.path.exists(output):
            raise RuntimeError("{} analysis file was not created".format(self.name))

        data = self._parse(output)

        # Just for data collection, return True as we're not performing any checks
        return True, data, {}


class AnalysisMosdepthMetrics(BaseAnalysisQCProcessor):

    name = 'MosdepthMetrics'

    def check(self):

        IGNORE_CHR = ['GL', 'hs', 'NC']  # ignore unlocalized scaffolds and decoy genome
        CHR_ORDER = {'X': 23, 'Y': 24, 'MT': 25, 'metrics': 0}  # metrics: whole genome including GL hs NC

        data = defaultdict(OrderedDict)

        # add 10X 20X etc.
        # collect (fold, percent) pairs, add median coverage

        median_pairs = defaultdict(list)

        with open(self.config['mosdepth_coverage']) as cov:

            for line in cov:
                chrom, fold, percent = line.split()
                # skip region
                if '_region' in chrom:
                    continue
                if chrom == 'total':
                    # to be consistent with CollectWgsMetrics for WebUI
                    chrom = 'metrics'
                if (
                        float(percent) != 0 and
                        int(fold) <= 100 and
                        not any(chrom.startswith(p) for p in IGNORE_CHR)
                ):
                    data[chrom]['PCT_'+fold+'X'] = float(percent)

                if not any(chrom.startswith(p) for p in IGNORE_CHR):
                    median_pairs[chrom].append([int(fold), abs(float(percent)-0.5)])  # fold closest to 0.5 is median

            # add median coverage
            for chrom in median_pairs:
                chrom_median = sorted(median_pairs[chrom], key=lambda x: x[1])[0][0]
                data[chrom]['MEDIAN_COVERAGE'] = chrom_median

        # add mean coverage
        with open(self.config['mosdepth_summary']) as cov:
            header = cov.readline()
            for line in cov:
                chrom, _, tot_bases, mean_cov, min_cov, max_cov = line.split()
                if '_region' in chrom:
                    continue
                if chrom == 'total':
                    # to be consistent with CollectWgsMetrics for WebUI
                    chrom = 'metrics'
                if not any(chrom.startswith(p) for p in IGNORE_CHR):
                    data[chrom]['MEAN_COVERAGE'] = float(mean_cov)

        # sort by chromosome
        data = OrderedDict(sorted(iter(data.items()), key= lambda t: int(t[0]) if t[0].isdigit() else CHR_ORDER[t[0]]))

        passed = True

        qc_settings = self.getQcSettings()['MosdepthMetrics']

        for key, interval in qc_settings.items():
            if not withinRange(data['metrics'][key], interval):
                passed = False

        return passed, data, qc_settings


class AnalysisCollectHsMetrics(BaseAnalysisQCProcessor):

    name = 'CollectHsMetrics'

    def _parse(self, output):
        with open(output) as fd:
            lines = fd.readlines()

            idx_metrix = None
            idx_histogram = None
            for i, l in enumerate(lines):
                if l.startswith('## METRICS CLASS'):
                    idx_metrix = i
                elif l.startswith('## HISTOGRAM'):
                    idx_histogram = i
                    break
            if idx_metrix is None:
                raise RuntimeError("Couldn't find data tag for {}".format(self.name))

            tsv_data = ''.join(lines[idx_metrix+1:idx_histogram-1])
            parser = CsvParser(dialect='excel-tab')
            data = parser.parse(tsv_data)
            return data

    def check(self):

        output = self.config['hs_metrics']

        if output is None or not os.path.exists(output):
            raise RuntimeError("{} analysis file was not created: {}".format(self.name, output))

        data = self._parse(output)

        passed = True

        qc_settings = self.getQcSettings()['CollectHsMetrics']

        for key, interval in qc_settings.items():
            if not withinRange(data[key], interval):
                passed = False

        return passed, data, qc_settings


class AnalysisCollectQualityYieldMetrics(BaseAnalysisQCProcessor):

    name = 'CollectQualityYieldMetrics'

    def _parse(self, output):
        with open(output) as fd:
            lines = fd.readlines()

            idx_metrix = None
            idx_histogram = None
            for i, l in enumerate(lines):
                if l.startswith('## METRICS CLASS'):
                    idx_metrix = i
            if idx_metrix is None:
                raise RuntimeError("Couldn't find data tag for {}".format(self.name))

            tsv_data = ''.join(lines[idx_metrix+1:idx_metrix+3])
            parser = CsvParser(dialect='excel-tab')
            data = parser.parse(tsv_data)
            for k in ["Q20_BASES", "Q30_BASES"]:
                data["PCT_"+k] = data[k] / data["TOTAL_BASES"]
                data["PCT_PF_"+k] = data["PF_"+k] / data["PF_BASES"]
            return data

    def check(self):

        output = self.config['qy_metrics']

        if output is None or not os.path.exists(output):
            raise RuntimeError("{} analysis file was not created: {}".format(self.name, output))

        data = self._parse(output)

        passed = True

        criteria = self.getQcSettings()['CollectQualityYieldMetrics']

        for key, interval in criteria.items():
            if not withinRange(data[key], interval):
                passed = False

        return passed, data, criteria


class AnalysisVcfQuality(BaseAnalysisQCProcessor):

    name = 'VcfQuality'

    def check(self):

        vcf_file = self.config['vcf']

        stats = vcfquality.VcfQuality().getStats(vcf_file)

        passed = True

        criteria = self.getQcSettings()['VcfQuality']
        titvcriteria = criteria['tiTvRatio']
        if not withinRange(stats['tiTvRatio'], titvcriteria):
            passed = False

        skewedRatio = criteria['skewedRatio']
        if not withinRange(stats['skewedRatio'], skewedRatio):
            passed = False

        return passed, stats, criteria


class AnalysisSmallVcfQuality(BaseAnalysisQCProcessor):

    name = 'SmallVcfQuality'

    def check(self):

        vcf_file = self.config['vcf']

        stats = vcfquality.SmallVcfQuality().getStats(vcf_file, self.config['site_file'])

        passed = True

        small_vcf_quality = self.getQcSettings()['SmallVcfQuality']
        if 'tiTvRatio' in small_vcf_quality:
            titvcriteria = small_vcf_quality['tiTvRatio']
            if not withinRange(stats['tiTvRatio'], titvcriteria):
                passed = False

        if 'skewedRatio' in small_vcf_quality:
            skewedRatio = small_vcf_quality['skewedRatio']
            if not withinRange(stats['skewedRatio'], skewedRatio):
                passed = False

        if 'contamination' in small_vcf_quality:
            contamination_criteria = small_vcf_quality['contamination']
            if not withinRange(stats['contamination'], contamination_criteria):
                passed = False

        return passed, stats, small_vcf_quality


class LowCoverageRegions(BaseAnalysisQCProcessor):
    """
    Checks low coverage regions.
    """
    name = "LowCoverageRegions"

    def _get_failed(self, rows, depth_threshold):
        # Organize by position, as there can be duplicate rows due to multiple
        # transcripts for a single gene. These would be repeated twice,
        # with only the transcript differing
        depth = dict()  # {(pos, start, end): depth}
        for row in rows:
            key = (row['chromosome'], row['start'], row['end'])
            transcript_data = {
                'gene': row['gene'],
                'transcript': row['transcript'],
                'exon': row['exon']
            }
            if key not in depth:
                depth[key] = {
                    'depth': int(row['reads']),
                    'transcripts': [
                        transcript_data
                    ]
                }
            else:
                depth[key]['transcripts'].append(transcript_data)

        # Check threshold
        failed = list()
        for key, value in depth.items():
            if value['depth'] <= depth_threshold:
                failed.append({
                    'chr': key[0],
                    'start': key[1],
                    'stop': key[2],
                    'transcripts': value['transcripts'],
                    'depth': value['depth']
                })

        return failed

    def check(self):

        if not self.config['low_coverage'] or not os.path.exists(self.config['low_coverage']):
            raise RuntimeError("Missing input --low_coverage or file does not exist.")

        if not self.config['low_coverage_transcripts'] or not os.path.exists(self.config['low_coverage_transcripts']):
            raise RuntimeError("Missing input --low_coverage_transcripts or file does not exist.")
        criteria = self.getQcSettings()['LowCoverageRegions']
        depth_threshold = criteria['depthThreshold']
        failed_region_count = criteria['failedRegionCount']
        cdata = CoverageData(self.config['low_coverage_transcripts'])
        lc_rows = cdata.loadLowCoverage(self.config['low_coverage'])
        failed = self._get_failed(lc_rows, depth_threshold)

        # Pass only if number of failed regions is less or equal to threshold
        return len(failed) <= failed_region_count, {'failed': failed}, criteria


class AnalysisDragenMappingMetrics(BaseAnalysisQCProcessor):
    """
    Parses and checks the mapping_metrics.csv file output by Dragen (only lines starting with "MAPPING/ALIGNING SUMMARY")
    """
    name = "DragenMappingMetrics"

    def _parse(self, output):
        data = {}
        with open(output) as fd:
            for line in fd:
                if not line.startswith("MAPPING/ALIGNING SUMMARY"):
                    continue
                line_data = line.rstrip("\n").split(",")[2:]
                if len(line_data) == 3:
                    key, val, val_pct = line_data
                elif len(line_data) == 2:
                    key, val = line_data
                    val_pct = None
                else:
                    raise RuntimeError("Unexpected number of columns in data: {line}")

                if val == "NA":
                    val = None
                else:
                    try:
                        val = int(val)
                    except:
                        val = float(val)
                data[key] = val
                if val_pct is not None:
                    data["Percent "+key] = float(val_pct)
        return data

    def check(self):
        output = self.config["dragen_mapping_metrics"]

        if output is None or not os.path.exists(output):
            raise RuntimeError("{} analysis file was not created: {}".format(self.name, output))

        data = self._parse(output)

        passed = True

        qc_settings = self.getQcSettings()['DragenMappingMetrics']

        for key, interval in qc_settings.items():
            if not withinRange(data[key], interval):
                passed = False

        return passed, data, qc_settings


class AnalysisQC(object):

    def __init__(self,
                 output,
                 capturekit,
                 pipeline,
                 bam,
                 vcf,
                 alignment_summary_metrics,
                 insert_size_metrics,
                 hs_metrics,
                 qy_metrics,
                 dragen_mapping_metrics,
                 regions,
                 ref_genome_path,
                 site_file,
                 low_coverage,
                 low_coverage_transcripts,
                 mosdepth_coverage,
                 mosdepth_summary,
                 qc_key):
        self.capturekit = capturekit
        self.pipeline = pipeline
        self.output = output
        self._qc_settings = None
        self._processors = []

        # Define general config that includes all items, even those not relevant to certain capturekits, for convenience
        self.config = {
            'bam': bam,
            'ref_genome_path': ref_genome_path,
            'output_dir': output,
            'regions': regions,
            'vcf': vcf,
            'alignment_summary_metrics': alignment_summary_metrics,
            'insert_size_metrics': insert_size_metrics,
            'hs_metrics': hs_metrics,
            'qy_metrics': qy_metrics,
            'dragen_mapping_metrics': dragen_mapping_metrics,
            'low_coverage': low_coverage,
            'low_coverage_transcripts': low_coverage_transcripts,
            'site_file': site_file,
            'mosdepth_coverage': mosdepth_coverage,
            'mosdepth_summary': mosdepth_summary,
            'qc_key': qc_key
            }

    @property
    def qc_settings(self):
        if not self._qc_settings:
            config_settings = settings.get_settings(self.capturekit)
            qc_key = self.config['qc_key']
            self._qc_settings = config_settings[self.pipeline][qc_key]

        return self._qc_settings

    @property
    def processors(self):
        """
        Create list of tuples (processor, config) for this capturekit
        """
        if not self._processors:
            # Create dict of available processors inherited from BaseAnalysisQCProcessor
            avail_processors = {p.name: p for p in BaseAnalysisQCProcessor.__subclasses__()}
            for key in list(self.qc_settings.keys()):
                self._processors +=[(avail_processors[key], self.config)]

        return self._processors

    def _createOutputPath(self):
        try:
            os.makedirs(self.output)
        except OSError:
            pass

    def check(self):
        self._createOutputPath()
        data = dict()
        for processor, config in self.processors:
            p = processor(
                config,
                self.qc_settings 
            )
            data[p.name] = p.check()
            sys.stderr.write('{}: {}\n'.format(processor.name, 'PASS' if data[p.name][0] else 'FAIL'))
        return data

    def writeCheck(self):
        data = self.check()
        print((json.dumps(data, indent=4)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run quality control on analysis result. Resulting JSON data is output to stdout")
    parser.add_argument("--output", help="Path to output directory for logs and program output.", dest='path', required=True)
    parser.add_argument("--capturekit", help="Capture Kit. Eg.: 'agilent_sureselect_v05' or 'wgs' for WGS.",
                        dest='capturekit', required=True)
    parser.add_argument("--pipeline", help="Pipeline name. Eg. 'basepipe' (default) or 'annopipe'.",
                        dest='pipeline', default='basepipe', required=False)
    parser.add_argument("--bam", help="Path to bam file.", dest='bam', required=False)
    parser.add_argument("--vcf", help="Path to vcf file.", dest='vcf', required=True)
    parser.add_argument("--alignment-summary-metrics", help="Path to Picard's alignment summary metrics file.", dest='alignment_summary_metrics', required=False)
    parser.add_argument("--insert-size-metrics", help="Path to Picard's insert size metrics file.", dest='insert_size_metrics', required=False)
    parser.add_argument("--hs-metrics", help="Path to Picard's hs metrics file.", dest='hs_metrics', required=False)
    parser.add_argument("--qy-metrics", help="Path to Picard's qy metrics file.", dest='qy_metrics', required=False)
    parser.add_argument("--dragen-mapping-metrics", help="Path to Dragen's mapping metrics file", dest="dragen_mapping_metrics", required=False)
    parser.add_argument("--low_coverage", help="Path to lowCoverage file. Required for type 'target''", dest='low_coverage')
    parser.add_argument("--low_coverage_transcripts", help="Path to transcripts used for lowCoverage file. Required for type 'target''", dest='low_coverage_transcripts')
    parser.add_argument("--contamination_sitefile", dest="sitefile", help="SNP sites for contamination check", default=None)
    parser.add_argument("--refgenome", dest="ref_genome_path", required=True,
                        help="Path to reference genome")
    parser.add_argument("--regions", dest="regions", required=False,
                        help="Full path to .bed file that specifies the regions to calculate QC on.")
    parser.add_argument("--mosdepth-coverage", dest="mosdepth_coverage", required=False,
                        help="Coverage percentage calculated by mosdepth for Dragen")
    parser.add_argument("--mosdepth-summary", dest="mosdepth_summary", required=False,
                        help="Coverage summary calculated by mosdepth for Dragen")
    parser.add_argument("--qc-key", dest="qc_key", required=False, default='qc',
                        help="the key for qc dict in analysis config")
    args = parser.parse_args()

    #if args.capturekit in ['CuCaV1', 'TSCarV1'] and args.sitefile is None:
    #    raise RuntimeError("Missing required option --contamination_sitefile for capturekit 'CuCaV1' or 'TSCarV1'")

    aqc = AnalysisQC(
        output=args.path,
        capturekit=args.capturekit,
        pipeline=args.pipeline,
        bam=args.bam,
        vcf=args.vcf,
        alignment_summary_metrics=args.alignment_summary_metrics,
        insert_size_metrics=args.insert_size_metrics,
        hs_metrics=args.hs_metrics,
        qy_metrics=args.qy_metrics,
        dragen_mapping_metrics=args.dragen_mapping_metrics,
        regions=args.regions,
        ref_genome_path=args.ref_genome_path,
        site_file=args.sitefile,
        low_coverage=args.low_coverage,
        low_coverage_transcripts=args.low_coverage_transcripts,
        mosdepth_coverage=args.mosdepth_coverage,
        mosdepth_summary=args.mosdepth_summary,
        qc_key = args.qc_key
    )
    aqc.writeCheck()
