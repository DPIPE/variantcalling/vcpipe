from util import CsvParser


class FastQC(object):

    def __init__(self, path):
        self.path = path

    def _process_module(self, module_text):

        # TODO: Rework to check whole column if any values cannot be converted to number.
        #       We don't want to mix types.
        module_text = module_text.replace('#', '')
        parser = CsvParser(dialect='excel-tab')
        data = parser.parse(module_text)
        return data

    def parse(self):

        data = dict()
        with open(self.path) as fd:
            in_module = False
            module_text = ''
            module_name = ''
            module_status = ''
            for idx, line in enumerate(fd.readlines()):
                # >>END_MODULE signals end of module
                if line.startswith('>>END_MODULE'):
                    if not in_module:
                        raise RuntimeError("Got END_MODULE, but I'm not currently inside a module. Wrong output format?")
                    in_module = False
                    module_data = self._process_module(module_text)
                    data[module_name] = {
                        'status': module_status
                    }
                    if module_data:
                        data[module_name]['data'] = module_data
                    module_text = ''

                # >> Signals start of new module
                elif line.startswith('>>'):
                    in_module = True
                    module_name, module_status = line.replace('>>', '').replace('\n', '').split('\t')

                # Add data to module
                else:
                    # Hack: Get version information
                    if line.startswith('##FastQC'):
                        data['FastQC'] = line.split('\t')[1].replace('\n', '')
                        continue
                    if in_module:
                        # Hack: Work around difference in output format
                        if line.startswith('#Total Deduplicated Percentage'):
                            data[module_name] = {
                                'Total Deduplicated Percentage': line.split('\t')[1]
                            }
                            continue
                        module_text += line

        return data


if __name__ == '__main__':

    fq = FastQC('/work/genomic/repo/sample/real_agilentV1_chr5/real_agilentV1_chr5.100k.R1.fastq_fastqc/real_agilentV1_chr5.100k.R1_fastqc/fastqc_data.txt')
    data = fq.parse()

    seq_scores = data['Per sequence quality scores']['data']

    q_seqs = 0
    total_seqs = 0
    for c, q in zip(seq_scores['Count'], seq_scores['Quality']):
        if q < 30:
            q_seqs += c
        total_seqs += c

    print(q_seqs)
    print(total_seqs)

    print(q_seqs / total_seqs)

    for k, v in data.items():
        if 'status' in v:
            print(k, v['status'])


    import json
    print(json.dumps(data, indent=4))
