import vcfiterator
from vcfiterator.processors import NativeInfoProcessor, CsvAlleleParser, VEPInfoProcessor

class SkewedRatio(object):
    """
    Calculates the ratio of read depth per allele for heterozygote variants.
    Then keeps count of number of variants with ratio is above 3x.
    Finally, give result as ratio of (num of skewed variants) / (total num of heterozygote)

    Should usually be somewhere around 0.005 for exome.
    """

    def __init__(self):
        self.heterozygote = 0
        self.skewed_snp = 0

    def processVariant(self, variant):
        if not variant['FILTER'] == 'PASS':
            return

        if len(variant['SAMPLES']) > 1:
            raise RuntimeError("Only one sample is supported for statistics.")
        sample = list(variant['SAMPLES'].values())[0]

        # A very few variants doesn't have AD for some reason
        # They are so few, so for this kind of QC we can skip them
        if 'AD' not in sample:
            return

        if '/' in str(sample['GT']):
            gt = sample['GT'].split('/')
        elif '|' in str(sample['GT']):
            gt = sample['GT'].split('|')
        else:
            return
        if len(gt) > 2:
            raise RuntimeError("len(gt) > 2. Should not be possible.")

        # remove ./. cases, shouldn't be many
        if '.' in gt:
            return

        gt0, gt1 = int(gt[0]), int(gt[1])
        # If genotypes not equal -> heterozygote
        if gt0 != gt1:
            self.heterozygote += 1
            n1 = sample['AD'][gt0]
            n2 = sample['AD'][gt1]
            if n2 > 0:
                r = float(n1) / n2
                if r > 3 or r < 0.25:
                    self.skewed_snp += 1
            else:
                self.skewed_snp += 1

    def getResult(self):
        ratio = 0
        if self.heterozygote > 0:
            ratio = float(self.skewed_snp) / self.heterozygote
        return {
            'skewedRatio': ratio
        }

class SmallSampleContaminationTest(object):
    """
    check whether there are bad variant (variants with skewed allele ratio) in some specific snp sites. Only applied on small capture kit
    should be none
    """
    def __init__(self):
        self.skewed_het_snp = 0
        self.skewed_hom_snp = 0

    def processVariant(self, variant, sites):
        #if not variant['FILTER'] == 'PASS':
        #    return

        if len(variant['SAMPLES']) > 1:
            raise RuntimeError("Only one sample is supported for statistics.")
        sample = list(variant['SAMPLES'].values())[0]

        # A very few variants doesn't have AD for some reason
        # They are so few, so for this kind of QC we can skip them
        if 'AD' not in sample:
            return

        if '/' in sample['GT']:
            gt = sample['GT'].split('/')
        elif '|' in sample['GT']:
            gt = sample['GT'].split('|')
        else:
            return
        if len(gt) > 2:
            raise RuntimeError("len(gt) > 2. Should not be possible.")

        gt0, gt1 = int(gt[0]), int(gt[1])
        # If genotypes not equal -> heterozygote
        if gt0 != gt1:
            n1 = sample['AD'][gt0]
            n2 = sample['AD'][gt1]
            if n2 > 0:
                r = float(n1) / n2
                if (r > 3 or r < 0.25) and ((variant['CHROM'],variant['POS']) in sites):
#                    print variant['CHROM'], variant['POS'], sample['GT'], n1, n2, r
                    self.skewed_het_snp += 1
            else:
                if (variant['CHROM'],variant['POS']) in sites:
                    self.skewed_het_snp += 1
        elif gt0 == gt1:
            n1 = sample['AD'][0]
            n2 = sample['AD'][gt1]
            if n2 > 0:
                r = float(n1) / n2
                if r > 0.1 and ((variant['CHROM'],variant['POS']) in sites):
#                    print variant['CHROM'], variant['POS'], sample['GT'], n1, n2, r
                    self.skewed_hom_snp += 1
            else:
                if (variant['CHROM'],variant['POS']) in sites:
                    self.skewed_hom_snp += 1

    def getResult(self):
        if self.skewed_het_snp > 0 or self.skewed_hom_snp > 0:
            total = self.skewed_het_snp + self.skewed_hom_snp
            return {
                'contamination': total
            }
        else:
            return {
                'contamination': 0
            }


class TiTvRatio(object):
    """
    Calculates the ratio of transversion vs transitions.
    """

    TRANSITIONS = {
        'ti': [
            ['A', 'G'],
            ['C', 'T']
        ],
        'tv': [
            ['A', 'T'],
            ['A', 'C'],
            ['G', 'T'],
            ['G', 'C']
        ]
    }

    def __init__(self):
        self.count = {
            'ti': 0,
            'tv': 0
        }

    def processVariant(self, variant):
        ref = variant['REF']
        if len(ref) != 1:
            return
        for transversion_type, transversion in TiTvRatio.TRANSITIONS.items():
            for alt in variant['ALT']:
                if any(ref in t and alt in t for t in transversion):
                    self.count[transversion_type] += 1

    def getResult(self):
        return {
            'tiTvRatio': float(self.count['ti']) / self.count['tv']
        }


class NonsenseCount(object):

    def __init__(self):
        self.nonsense = 0

    def processVariant(self, variant):
        nonsense_found = False
        for allele, info in variant['INFO'].items():
            if 'CSQ' not in info:
                continue
            # We only care per position, not which of the alleles it is in
            if any(['stop_gained' in i.get('Consequence') for i in info['CSQ']]):
                nonsense_found = True
            if nonsense_found:
                break

        if nonsense_found:
            self.nonsense += 1

    def getResult(self):
        return {
            'nonsenseCount': self.nonsense
        }


class VariantCount(object):

    def __init__(self):
        self.count = 0

    def processVariant(self, variant):
        self.count += 1

    def getResult(self):
        return {
            'variants': self.count
        }


class VcfQuality(object):

    def __init__(self):
        self.processors = [
            SkewedRatio(),
            TiTvRatio(),
            VariantCount()
        ]

    def getStats(self, path):

        v = vcfiterator.VcfIterator(path)
        v.addInfoProcessor(VEPInfoProcessor)

        for variant in v.iter():

            for processor in self.processors:
                processor.processVariant(variant)

        stats = dict()
        for processor in self.processors:
            stats.update(processor.getResult())

        return stats

class SmallVcfQuality(object):

    def __init__(self):
        self.processors = [
            SkewedRatio(),
            TiTvRatio(),
            VariantCount()
        ]

    def getStats(self, path, site_file):
        
        v = vcfiterator.VcfIterator(path)
        v.addInfoProcessor(VEPInfoProcessor)

        sites = []
        with open(site_file) as s:
            for line in s:
                line.strip()
                line.split("\t")
                sites.append((line.split("\t")[0],int(line.split("\t")[1])))

        s = SmallSampleContaminationTest()
        for variant in v.iter():
            
            for processor in self.processors:
                processor.processVariant(variant)
            s.processVariant(variant, sites)

        stats = dict()
        stats.update(s.getResult())

        for processor in self.processors:
            stats.update(processor.getResult())

        return stats

if __name__ == '__main__':
    #print VcfQuality().getStats("../util/testVCF/all.filter.vcf")
    print(SmallVcfQuality().getStats("all.filter.vcf", "sitesForCancer.txt"))
