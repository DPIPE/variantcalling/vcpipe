from .. import vcfquality
import pytest


class TestSkewedRatio:

    def test_filter_fail(self):
        sr = vcfquality.SkewedRatio()
        variant = {
            'FILTER': 'FAIL',
        }
        assert sr.processVariant(variant) is None

    def test_multiple_samples(self):
        """
        Should fail for multiple samples.
        """
        variant = {
            'FILTER': 'PASS',
            'SAMPLES': {
                'SAMPLE1': {},
                'SAMPLE2': {}
            }
        }
        sr = vcfquality.SkewedRatio()
        with pytest.raises(RuntimeError):
            sr.processVariant(variant)

    def test_no_ad_field(self):
        """
        Variants without AD fields should be ignored.
        """
        variant = {
            'FILTER': 'PASS',
            'SAMPLES': {
                'SAMPLE1': {},
            }
        }
        sr = vcfquality.SkewedRatio()
        sr.processVariant(variant)
        assert sr.skewed_snp == 0
        assert sr.heterozygote == 0

    def test_homozygote(self):
        variant = {
            'FILTER': 'PASS',
            'SAMPLES': {
                'SAMPLE1': {
                    'AD': [12, 14],
                    'GT': '0|0'
                },
            }
        }
        sr = vcfquality.SkewedRatio()
        sr.processVariant(variant)
        assert sr.heterozygote == 0

    def test_heterozygote(self):
        variant = {
            'FILTER': 'PASS',
            'SAMPLES': {
                'SAMPLE1': {
                    'AD': [12, 14],
                    'GT': '0|1'
                },
            }
        }
        sr = vcfquality.SkewedRatio()
        sr.processVariant(variant)
        assert sr.heterozygote == 1

    def test_skewed(self):
        variant = {
            'FILTER': 'PASS',
            'SAMPLES': {
                'SAMPLE1': {
                    'AD': [10, 50],  # >3x diff
                    'GT': '0|1'
                },
            }
        }
        sr = vcfquality.SkewedRatio()
        sr.processVariant(variant)
        assert sr.heterozygote == 1
        assert sr.skewed_snp == 1

        variant = {
            'FILTER': 'PASS',
            'SAMPLES': {
                'SAMPLE1': {
                    'AD': [50, 10],  # >3x diff
                    'GT': '0|1'
                },
            }
        }

        sr.processVariant(variant)
        assert sr.heterozygote == 2
        assert sr.skewed_snp == 2

    def test_non_skewed(self):
        variant = {
            'FILTER': 'PASS',
            'SAMPLES': {
                'SAMPLE1': {
                    'AD': [10, 29],  # <3x diff
                    'GT': '0|1'
                },
            }
        }
        sr = vcfquality.SkewedRatio()
        sr.processVariant(variant)
        assert sr.heterozygote == 1
        assert sr.skewed_snp == 0

        variant = {
            'FILTER': 'PASS',
            'SAMPLES': {
                'SAMPLE1': {
                    'AD': [29, 10],  # <3x diff
                    'GT': '0|1'
                },
            }
        }

        sr.processVariant(variant)
        assert sr.heterozygote == 2
        assert sr.skewed_snp == 0


class TestTiTvRatio:

    def test_transversions(self):

        tr = vcfquality.TiTvRatio()

        v = {'REF': 'A', 'ALT': ['C']}
        tr.processVariant(v)
        assert tr.count['tv'] == 1

        v = {'REF': 'C', 'ALT': ['A']}
        tr.processVariant(v)
        assert tr.count['tv'] == 2

        v = {'REF': 'A', 'ALT': ['T']}
        tr.processVariant(v)
        assert tr.count['tv'] == 3

        v = {'REF': 'T', 'ALT': ['A']}
        tr.processVariant(v)
        assert tr.count['tv'] == 4

        v = {'REF': 'G', 'ALT': ['T', 'C']}
        tr.processVariant(v)
        assert tr.count['tv'] == 6

    def test_transitions(self):

        tr = vcfquality.TiTvRatio()

        v = {'REF': 'A', 'ALT': ['G']}
        tr.processVariant(v)
        assert tr.count['ti'] == 1

        v = {'REF': 'G', 'ALT': ['A']}
        tr.processVariant(v)
        assert tr.count['ti'] == 2

        v = {'REF': 'C', 'ALT': ['T']}
        tr.processVariant(v)
        assert tr.count['ti'] == 3

        v = {'REF': 'T', 'ALT': ['C']}
        tr.processVariant(v)
        assert tr.count['ti'] == 4


class TestNonsenseCount:

    def test_nonsense_count(self):
        # Test with nonsense
        v = {
            'INFO': {
                'A': {
                    'CSQ': [
                        {
                            'Consequence': ['stop_gained', 'missense_variant'],
                        },
                        {
                            'Consequence': ['something_else']
                        }
                    ]
                }
            }
        }

        nc = vcfquality.NonsenseCount()
        nc.processVariant(v)
        assert nc.nonsense == 1

        # Test without nonsense
        v = {
            'INFO': {
                'A': {
                    'CSQ': [
                        {
                            'Consequence': ['something_else']
                        }
                    ]
                }
            }
        }

        nc.processVariant(v)
        assert nc.nonsense == 1

        # Test with multiple alleles
        # We only count per position, so increment will be only by one
        v = {
            'INFO': {
                'A': {
                    'CSQ': [
                        {
                            'Consequence': ['stop_gained', 'something_else']
                        }
                    ]
                },
                'T': {
                    'CSQ': [
                        {
                            'Consequence': ['stop_gained']
                        }
                    ]
                }
            }
        }

        nc.processVariant(v)
        assert nc.nonsense == 2
