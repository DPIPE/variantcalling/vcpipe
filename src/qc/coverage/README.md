# Contents
This directory contains scripts for coverage calculations and coverage QC.
The main script calculates which regions have low coverage (under a given threshold setting) and what percentage of a gene/transcript/exon have sufficient coverage (i.e. do _not_ have low coverage).

An important input file in this regard is _coverageRegions.bed_, which contains the regions on which to calculate the coverage. These regions are typically coding exons +/- a few basepairs, and are named like GENESYMBOL\_\_REFSEQNAME\_\_exonX where X is the number. These files are stored in amg/clinicalGenePanels/genepanelname.
Importantly, the relationship between genesymbols, transcripts (i.e. refseqnames) and exons as given by this file is used for aggregating data on exon level up to transcript or gene level.
For example, coverage of exons is summed for all exons belonging to a transcript in order to report the coverage on transcript level.

# Most important files

* **coverage\_qc.py** : Runs GATK DepthOfCoverage tool and aggregates results. Can be run on single or multiple BAM files.
