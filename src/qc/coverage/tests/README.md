covPerBP.bed : A file for testing the script for finding low coverage regions.
coverageRegions.bed : A file specifying the regions for coverage calculations.
features.bed : A file with features (HGMD mutations) for feature_coverage.py
lowcov.bed : A file with low coverage regions.
