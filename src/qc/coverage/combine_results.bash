#!/usr/bin/env bash

# Script to aggregate coverage reports for multiple samples. Also calculates average coverage and adds column headers

# Usage: <script>  folder
# The 'folder' contains SAMPLE_ID_coverageReport_exon and SAMPLE_ID_coverage files

# overview of file input/output:
filename_all_coverage="coverage_all_transcript.tsv" # -> coverage_all_transcript.average.tsv
transcript_coverage_pattern="*_coverageReport_transcript" # -> coverage_under_100p_transcript.tsv and coverage_all_transcript.tsv
exon_coverage_pattern="*_coverageReport_exon" # -> coverage_under_100p_exon.tsv and coverage_all_exon.tsv

# checks if there are any files with a given pattern (like a_folder/*_sample.csv)
function filesExistsWithPattern() {
    if ls $1 1> /dev/null 2>&1; then
        return 0
    else
        return 1
    fi
}

# checks if there any sample files in the folder
sample_files=($(ls $1/$transcript_coverage_pattern))
n=${#sample_files[@]}

if [[ ! $n -gt 0 ]]; then
    echo "Couldn't find the number of samples based on file names"
    exit 1
fi


if [ -d "$1" ]; then
    if filesExistsWithPattern $1/$exon_coverage_pattern ; then
        # output files have 7 columns:
        exon_100p_filename="coverage_under_100p_exon.tsv"
        cat $1/$exon_coverage_pattern | grep -v '#' | grep -v '100.0%' | sort -k2,4 | sort -ns -k5,5 > $1/$exon_100p_filename.tmp
        printf "#%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "sampleName" "gene" "transcript" "exon" "fraction covered" "nucleotides covered" "nucleotides in total" | cat - $1/$exon_100p_filename.tmp > $1/$exon_100p_filename
        rm $1/$exon_100p_filename.tmp

        exon_all_filename="coverage_all_exon.tsv"
        cat $1/$exon_coverage_pattern | grep -v '#' | sort -k2,4 > $1/$exon_all_filename.tmp
        printf "#%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "sampleName" "gene" "transcript"  "exon" "fraction covered" "nucleotides covered" "nucleotides in total" | cat - $1/$exon_all_filename.tmp > $1/$exon_all_filename
        rm $1/$exon_all_filename.tmp
    else
        echo "No files named $exon_coverage_pattern in $1"
    fi

    if filesExistsWithPattern $1/$transcript_coverage_pattern ; then
        # output files have 6 columns:
        transcript_100p_filename="coverage_under_100p_transcript.tsv"
        cat $1/$transcript_coverage_pattern | grep -v '#' | grep -v '100.0%' | sort -k2,3 | sort -ns -k4,4 > $1/$transcript_100p_filename.tmp
        printf "#%s\t%s\t%s\t%s\t%s\t%s\n" "sampleName" "gene" "transcript" "fraction covered" "nucleotides covered" "nucleotides in total" | cat - $1/$transcript_100p_filename.tmp > $1/$transcript_100p_filename
        rm $1/$transcript_100p_filename.tmp

        cat $1/$transcript_coverage_pattern | grep -v '#' | sort -k2,3 > $1/$filename_all_coverage.tmp
        printf "#%s\t%s\t%s\t%s\t%s\t%s\n" "sampleName" "gene" "transcript"  "fraction covered" "nucleotides covered" "nucleotides in total" | cat - $1/$filename_all_coverage.tmp > $1/$filename_all_coverage
        rm $1/$filename_all_coverage.tmp

    else
        echo "No files named $transcript_coverage_pattern in $1"
    fi


    average_file_name="coverage_all_transcript.average.tsv"
    if filesExistsWithPattern $1/$filename_all_coverage ; then
        echo "Calculates average coverage using $n samples"
#         +2 means exclude first line:
        tail -n +2 $1/$filename_all_coverage | awk '{sub("%","",$4);print $0}'|awk '{s+=$4}NR%"'"$n"'"==0{print $2"\t"$3"\t"s/"'"$n"'";s=0}'|sort -n -k3,3 > $1/$average_file_name.tmp
        printf '#%s\t%s\t%s\n' "gene symbol" "refseq" "x10" | cat - $1/$average_file_name.tmp > $1/$average_file_name
        rm $1/$average_file_name.tmp
    else
        echo "No files named $1/$filename_all_coverage"
    fi


    exit 0
else
  echo "The folder $1 doesn't exists"
  exit 1
fi


