#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

"""
Main script for coverage quality control.
Requires bedtools2.20.1 or higher.

Calculates the percentage of coverageRegions (exons and aggregated to trancripts)
that are equal to or above a minimum coverage threshold,
and also the regions that are below the threshold.
Takes a single BAM file as input,
or a file that contains one path at each line.

Uses GATK DepthOfCoverage to get read-filtered coveragePerBasepair.
Converts this to a BED file that is split into two where
one file has those above the threshold and another the positions that
have coverage lower than the threshold value.
Counts how many matches from the above-threshold file
that there are to each coverageRegion.
Finally, merges the continuous regions that are below the coverage threshold
and outputs lowCoverageRegions.bed.

"""

import os
import sys
import glob
import shutil
import argparse
import tempfile
import subprocess
from collections import OrderedDict

from pybedtools import BedTool


def run_coverage_check_on_bam(bamPath, coverageRegions, outputDir, sampleName,
                              refGenomePath, extraReadFilters, extraGATKFlags,
                              minMQ, minimumCoverage, outputCovPerBP=False, excludedregions=None):
    """Runs the coverage check on a single BAM file (i.e. single sample)"""
    gatkOutputFile, gatkOutputPath = tempfile.mkstemp(".gatkCoveragePerBP", sampleName, text=True)
    run_gatk_depthOfCoverage(refGenomePath, bamPath,
                             coverageRegions, gatkOutputPath,
                             extraReadFilters, extraGATKFlags, minMQ)

    # Convert GATK output to BED format
    covPerBPFile, covPerBPPath = tempfile.mkstemp(".bed", sampleName + "_covPerBP", text=True)
    with open(gatkOutputPath) as f:
        with open(covPerBPPath, 'w') as g:
            convert_gatk_file_to_bed(f, g, sampleName)

    # Split perBPCoverage into above/below threshold
    aboveThresholdFile, aboveThresholdPath = tempfile.mkstemp(".bed", sampleName + "_above", text=True)
    belowThresholdFile, belowThresholdPath = tempfile.mkstemp(".bed", sampleName + "_below", text=True)
    with open(covPerBPPath) as f:
        with open(aboveThresholdPath, 'w') as g:
            with open(belowThresholdPath, 'w') as h:
                split_bed_on_score(f, g, h, minimumCoverage)

    covRegionsBED = BedTool(coverageRegions)
    aboveBED = BedTool(aboveThresholdPath)
    covRegionsCountAboveBED = covRegionsBED.intersect(aboveBED, c=True) # Keeps all covRegions
    make_aggregated_report(covRegionsCountAboveBED, outputDir, 3, sampleName) # 3 = exon
    make_aggregated_report(covRegionsCountAboveBED, outputDir, 2, sampleName) # 2 = transcript

    belowBED = BedTool(belowThresholdPath)
    make_low_coverage_regions_report(belowBED, covRegionsBED, outputDir, sampleName)

    if outputCovPerBP:
        shutil.move(covPerBPPath, os.path.join(outputDir, "{}_coveragePerBP.bed".format(sampleName)))
    else:
        os.remove(covPerBPPath)
    for tmpFile in (gatkOutputPath, aboveThresholdPath, belowThresholdPath):
        os.remove(tmpFile)


def run_gatk_depthOfCoverage(refGenomePath, bamPath, coverageRegionsPath,
                             output, extraReadFilters="BadMate", extraGATKFlags=None, minMQ=4):
    """Runs GATK -T DepthOfCoverage

    extraReadFilters must be specified as a comma-separated list.
    """
    cmd = "gatk -l WARN -T DepthOfCoverage -R {0} -o {1} -I {2} -L:intervals,bed {3} --minMappingQuality {4} {5}".format(
        refGenomePath, output, bamPath, coverageRegionsPath, minMQ,
        "--omitIntervalStatistics --omitLocusTable --omitPerSampleStats")
    if extraReadFilters is not None:
        for extraFilter in extraReadFilters.split(','):
            cmd +=" --read_filter {}".format(extraFilter)
    if extraGATKFlags is not None:
        for extraGATKFlag in extraGATKFlags.split(','):
            cmd += " --{}".format(extraGATKFlag.strip('--'))
    returnCode = subprocess.call(cmd.split(), stdout=sys.stderr)
    return returnCode

def convert_gatk_file_to_bed(inFile, outFile, sampleName="N"):
    """Converts GATK DepthOfCoverage main output file to BED format

    DepthOfCoverage outputs 1-based positions, must convert to 0-based.
    """
    for line in inFile:
        if line.startswith("Locus") or line.strip().isspace(): continue
        parts = line.strip().split('\t')
        chromosome, foo, start = parts[0].partition(':')
        start = int(start) - 1
        stop = start + 1
        depth = parts[3]
        outFile.write('\t'.join((chromosome, str(start), str(stop),
                                 sampleName, depth, '+')) + '\n')

def split_bed_on_score(inFile, aboveFile, belowFile, minPass):
    """Writes BED records with score >= minPass to aboveFile, else belowFile"""
    for line in inFile:
        if line.strip().isspace(): continue
        if int(line.split('\t')[4]) >= minPass:
            aboveFile.write(line)
        else:
            belowFile.write(line)

def make_aggregated_report(covRegionsCountAboveBED, outputdir, aggregation=3, sampleName=""):
    """Writes a coverageReport at an aggregated level

    Assumes all regions at lowest level of aggregation  are given in BED object
    and outputs in same order.
    Aggregation level is 1 (gene), 2 (transcript) or 3 (exon).
    """
    aggregationLevel = ("gene", "transcript", "exon")[aggregation - 1]
    outputFilePath = os.path.join(outputdir, "{}_coverageReport_{}".format(sampleName, aggregationLevel))
    with open(outputFilePath, 'w') as g:
        g.write("#sampleName\t" + '\t'.join(("gene", "transcript", "exon")[:aggregation]))
        g.write("\tfraction covered\tnucleotides covered\tnucleotides in total\n")
        lengths = OrderedDict()
        counts = OrderedDict()

        for i in covRegionsCountAboveBED:
            name = i.name.split("__")[:aggregation] # Gene__transcript__exon
            name = "__".join(name)
            lengths[name] = lengths[name] + i.length if name in lengths else i.length
            counts[name] = counts[name] + i.count if name in counts else i.count

        namesOutputted = set()
        for name in counts:
            if name in namesOutputted: continue # Aggregate already output
            try:
                fractionAbove = counts[name] / (lengths[name] * 1.0)
            except ZeroDivisionError as e:
                fractionAbove = 0.0
            namesOutputted.add(name)
            g.write("{}\t{}\t{:.1%}\t{}\t{}\n".format(sampleName, '\t'.join(name.split("__")),
                                                      fractionAbove, counts[name], lengths[name]))

def make_low_coverage_regions_report(belowBED, covRegionsBED, outputDir, sampleName=""):
    """Writes a BED6+3 with any continuous regions below threshold"""
    if belowBED.count() > 0:
        belowMergedBED = belowBED.merge(c=5, o="min")
        # Get the names of the regions, then remove the extra columns.
        belowMergedWithNames = belowMergedBED.intersect(covRegionsBED, loj=True, wa=True, wb=True)
    else:
        belowMergedWithNames = ()  # Empty tuple, to create an empty output file
    outputFilePath = os.path.join(outputDir, "{}_lowCoverage.bed".format(sampleName))
    with open(outputFilePath, 'w') as output:
        for i in belowMergedWithNames:
            gene, transcript, exon = i[7].split("__")
            output.write("{pos}\t{name}\t{score}\t+\t{sampleName}\t{gte}\n".format(pos='\t'.join(i[0:3]),
                                                                                   name=i[7], score=i[3], sampleName=sampleName,
                                                                                   gte='\t'.join((gene, transcript, exon))))




def main(argv=None):
    argv = argv or sys.argv[1:]
    parser = argparse.ArgumentParser(description="""Outputs for each region the percentage
    that is above the minimum coverage threshold,
    and which continuous regions that are below the threshold. When processing multiple bam files using --bampaths
    the real sample names are replaced with 'sample-N' where is the index of the file in bampaths""")
    argGroup = parser.add_mutually_exclusive_group(required=True)
    argGroup.add_argument("--bam", dest="bamPath", help="Path to BAM file")
    argGroup.add_argument("--bampaths", dest="bamPaths", default=None,
                          help="Path to file with BAM paths")
    parser.add_argument("--regions", dest="coverageRegions", required=True,
                        help="Full path to coverageRegions.bed, the file that "
                             "specifies the regions to calculate coverage on")
    parser.add_argument("--refgenome", dest="refGenomePath", required=True,
                        help="Path to reference genome")
    parser.add_argument("--outputdir", dest="outputDir", required=False,
                        default='.', help="Path to output directory")
    parser.add_argument("--samplename", dest="sampleName", required=False,
                        default="nosamplename", help="Name of sample")
    parser.add_argument("--minimumcoverage", dest="minimumCoverage", required=False, type=int,
                        default=10, help="Minimum acceptable coverage (no of reads)")
    parser.add_argument("--extrareadfilters", dest="extraReadFilters", required=False,
                        default="BadMate", help="Additional read filters to be"
                                                " applied (comma-separated)")
    parser.add_argument("--extragatkflags", dest="extraGATKFlags", required=False,
                        default=None, help="Additional flags to GATK"
                                           " e.g. allow_potentially_misencoded_quality_scores (comma-separated)")
    parser.add_argument("--minmq", dest="minMQ", required=False, default=20, type=int,
                        help="Minimum mapping quality of reads to be counted")
    parser.add_argument("--perbp", dest="outputCovPerBP", required=False, action="store_true",
                        default=False, help="Output coveragePerBP.bed (large file)")
    parser.add_argument('--excludedregions', required=False, help="A bed file with regions to exclude from coverage check")
    args = parser.parse_args(argv)

    _, filtered_bed_file = tempfile.mkstemp(".bed", "filtered_regions_file", text=True)

    if args.excludedregions:
        # assume a single bed file:
        exclusions = BedTool(args.excludedregions)
        target = BedTool(args.coverageRegions)
        _ = target.subtract(exclusions, output=filtered_bed_file)
    else:
        shutil.copy(args.coverageRegions, filtered_bed_file)

    if args.bamPaths is None:  # Run on single BAM file
        run_coverage_check_on_bam(args.bamPath, filtered_bed_file, args.outputDir,
                                  args.sampleName, args.refGenomePath,
                                  args.extraReadFilters, args.extraGATKFlags,
                                  args.minMQ, args.minimumCoverage,
                                  args.outputCovPerBP,
                                  args.excludedregions)

    else:  # Run on multiple BAM files, their paths in the bamPaths file
        with open(args.bamPaths) as bamPathsFile:
            idx = 0
            for bamPath in bamPathsFile:
                if bamPath.strip().startswith("#") or len(bamPath.strip()) < 1:
                    continue
                idx += 1
                bamPath = glob.glob(bamPath.strip()).pop()
                sampleName = "sample-" + str(idx)
                run_coverage_check_on_bam(bamPath, filtered_bed_file,
                                          args.outputDir, sampleName,
                                          args.refGenomePath, args.extraReadFilters,
                                          args.extraGATKFlags, args.minMQ,
                                          args.minimumCoverage, args.outputCovPerBP,
                                          args.excludedregions)
    os.remove(filtered_bed_file)

    return 0

if __name__ == "__main__":
    sys.exit(main())
