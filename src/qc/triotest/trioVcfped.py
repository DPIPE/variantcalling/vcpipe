import argparse
import json
import logging
import csv
from collections import defaultdict, OrderedDict


class trioVcfped(object):

    def __init__(self, vcfped_log_file, vcfped_gender_file, vcfped_pair_file, vcfped_trio_file,
                 output, exception=True):
        self.vcfped_log_file = vcfped_log_file
        self.vcfped_gender_file = vcfped_gender_file
        self.vcfped_pair_file = vcfped_pair_file
        self.vcfped_trio_file = vcfped_trio_file
        self.output = output
        self.exception = exception

        self.samples = self._get_sample_names()
        self.genders = self._get_genders()
        self.pairs = self._get_pair()
        self.trio = self._get_trio()
        self.conclusion = self._get_conclusion()
        self.thresholds = self._get_thresholds()

    def _get_sample_names(self):
        """
        parse sample names from vcfped_log_file
        """

        samples = OrderedDict()

        reached = 0

        with open(self.vcfped_log_file) as log:
            for line in log:

                if line.startswith("Sample names:"):
                    reached = 1
                    continue

                if reached != 0 and reached <= 3:
                    parts = line.split(':')
                    samples[parts[0].strip()] = parts[1].strip()
                    reached += 1

        if reached == 0:
            raise RuntimeError(
                "vcfped log file {} contains no samples".format(self.vcfped_log_file)
            )

        return samples

    def _get_conclusion(self):
        """
        parse conclusion from vcfped_log_file
        """

        conclusion = defaultdict(str)

        reached = 0

        with open(self.vcfped_log_file) as log:
            for line in log:

                if line.startswith("====COMPARISON WITH PEDIGREE FILE===="):
                    reached = 1
                    continue

                if reached == 1:
                    conclusion['result'] = line.strip()
                    reached += 1
                elif reached > 1:
                    conclusion['reason'] = conclusion['reason'] + line.strip() + " "

        if reached == 0:
            raise RuntimeError(
                "vcfped log file {} contains no conclusion".format(self.vcfped_log_file)
            )

        return conclusion

    def _get_thresholds(self):
        """
        parse thresholds from vcfped_log_file
        """

        thresholds = OrderedDict()

        reached = 0

        with open(self.vcfped_log_file) as log:
            for line in log:

                if line.startswith("====FILE INFO===="):
                    break

                if line.startswith("Options in effect:"):
                    reached = 1
                    continue

                if reached == 1 and ':' in line:
                        parts = line.split(':')
                        key = ':'.join(parts[0:-1])
                        value = parts[-1].strip()
                        thresholds[key] = value

        if reached == 0:
            raise RuntimeError(
                "vcfped log file {} contains no Options in effect".format(self.vcfped_log_file)
            )

        return thresholds

    def _get_genders(self):
        """
        parse vcfped.gener output
        """

        genders = OrderedDict()

        with open(self.vcfped_gender_file) as vgf:
            rd = csv.DictReader(vgf, dialect="excel-tab")
            for row in rd:
                genders[self.samples[row['Sample']]] = {
                    "gender": row["Gender"],
                    "heterozygosity": row["Xhet"]
                }

        return genders

    def _get_trio(self):
        """
        parse vcfped.trio output
        """

        with open(self.vcfped_trio_file) as vtf:
            rd = csv.DictReader(vtf, dialect="excel-tab")
            best = next(rd)

        trio = OrderedDict([
            ('conclusion', best['Verdict']),
            ('triple', best['Triple']),
            ('proband', best['Pivot']),
            ('percentile', best['Perc']),
            ('Autos', best['Autos']),
            ('AA+BB=AB Ratio', best['Test1'] + '% (' + best['=AB'] + '/' + best['AA+BB'] + ')'),
            ('BB+BB=BB Ratio', best['Test2'] + '% (' + best['=BB'] + '/' + best['BB+BB'] + ')')
        ])

        return trio

    def _get_pair(self):
        """
        parse vcfped.pari output
        """

        pair = OrderedDict()

        with open(self.vcfped_pair_file) as vpf:
            rd = csv.DictReader(vpf, dialect="excel-tab")
            for row in rd:
                pair[row['Pair']] = {
                    'relation': row['Verdict'],
                    'parent-offspring score': row['POscore'],
                    'monozygotic twins Score': row['MZscore']
                }

        return pair

    def process(self):

        resultDict = OrderedDict()

        passed = self.conclusion['result'] == "OK"

        if not passed:
            resultDict['reason'] = self.conclusion['reason']

        resultDict["1-triotype"] = self.trio['conclusion']

        resultDict["samples"] = self.samples
        resultDict["trio-check"] = self.trio
        resultDict["pair-check"] = self.pairs
        resultDict["gender-check"] = self.genders
        resultDict["thresholds"] = self.thresholds

        resultArray = [passed, resultDict]

        self.dump(resultArray)

        if self.exception and not passed:
            raise RuntimeError("vcfped check failed. " + self.conclusion['reason'])

    def dump(self, resultArray):
        with open(self.output, 'w') as jsonOutput:
            json.dump(resultArray, jsonOutput, indent=4, sort_keys=False)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='summarize vcfped outputs')
    parser.add_argument('--vcfped-log-file', required=True, dest='vcfped_log_file',
                        help='vcfped .log file')
    parser.add_argument('--vcfped-gender-file', required=True, dest='vcfped_gender_file',
                        help='vcfped .gender file')
    parser.add_argument('--vcfped-pair-file', required=True, dest='vcfped_pair_file',
                        help='vcfped .pair file')
    parser.add_argument('--vcfped-trio-file', required=True, dest='vcfped_trio_file',
                        help='vcfped .trio file')
    parser.add_argument('--output', required=True, dest='output',
                        help='Output json file name')
    parser.add_argument('--no-exception', required=False, dest='exception',
                        help='do NOT raise exception when test fails', action='store_false')
    parser.add_argument('--exception', required=False, dest='exception',
                        help='raise exception when test fails', action='store_true')
    parser.set_defaults(exception=True)

    args = parser.parse_args()

    qc = trioVcfped(
        vcfped_log_file=args.vcfped_log_file,
        vcfped_gender_file=args.vcfped_gender_file,
        vcfped_pair_file=args.vcfped_pair_file,
        vcfped_trio_file=args.vcfped_trio_file,
        output=args.output,
        exception=args.exception
    )
    qc.process()
