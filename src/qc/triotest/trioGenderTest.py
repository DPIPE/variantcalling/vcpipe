import argparse
import json
import logging
from triotest.triotest import triotype
from triotest.triotest import cal_gender


class trioGenderTest(object):

    def __init__(self, vcf, motherID, fatherID, probandID, probandGender,
                 output, allow_undetermined=True, exception=True):
        self.vcf = vcf
        self.samples = self.get_sample_names(self.vcf)
        self.motherID = motherID
        self.fatherID = fatherID
        self.probandID = probandID
        self.probandGender = probandGender
        self.output = output
        self.allow_undetermined = allow_undetermined
        self.exception = exception

    def get_sample_names(self, filename):
        '''
        Returns the sample names ( = all headers after the "FORMAT" column) of
        a vcf-like file
        '''
        with open(filename, 'r') as infil:
            headerline = next(line for line in infil if line.startswith("#CHROM") or line.startswith("Chr") or "vcf_chrom" in line.lower())
        cleanHeaderline = headerline.rstrip('\r\n')
        h = cleanHeaderline.split("\t")
        return h[(h.index('FORMAT')+1):]

    def summarize(self, evidenceDict):
        '''
        Creates two dictionaries in the format we use in the analysis config.
        '''
        # if heterozygosities (0,10]: male; (10,20]: undetermined; (20, 100]: female
        fProbandGender = cal_gender(evidenceDict["heterozygosities"][self.samples[evidenceDict["probandIndex"]]])

        parent_samples = []
        for x in [0, 1, 2]:
            if x != evidenceDict["probandIndex"]:
                parent_samples.append([self.samples[x], cal_gender(evidenceDict["heterozygosities"][self.samples[x]])])

        if evidenceDict["heterozygosities"][parent_samples[0][0]] <= evidenceDict["heterozygosities"][parent_samples[1][0]]:
            fFatherID = parent_samples[0][0]
            fFatherGender = parent_samples[0][1]
            fMotherID = parent_samples[1][0]
            fMotherGender = parent_samples[1][1]
        else:
            fMotherID = parent_samples[0][0]
            fMotherGender = parent_samples[0][1]
            fFatherID = parent_samples[1][0]
            fFatherGender = parent_samples[1][1]

        computedResult = {
            "proband": {
                "sample": self.samples[evidenceDict["probandIndex"]],
                "gender": fProbandGender
            },
            "father": {
                "sample": fFatherID,
                "gender": fFatherGender
            },
            "mother": {
                "sample": fMotherID,
                "gender": fMotherGender
            }
        }

        expectedResult = {
            "proband": {
                "sample": self.probandID,
                "gender": self.probandGender
            },
            "father": {
                "sample": self.fatherID,
                "gender": "male"
            },
            "mother": {
                "sample": self.motherID,
                "gender": "female"
            }
        }

        return computedResult, expectedResult

    def process(self):
        results = triotype(self.vcf, PASS=True, DPmin=50, GQmin=50,
                           ADrange="30,60", QUALmin=0, QUALcol=5,
                           threshMale=10, thresh1=95, thresh2=95, thresh3=60,
                           verbose=True)

        testDict = {}
        for key in results[6].keys():

            sampleKey = []

            for i in key:
                if not sampleKey:
                    sampleKey = self.samples[i]
                else:
                    sampleKey = sampleKey + "_" + self.samples[i]

            testDict[sampleKey] = {
                results[6][key][0][0]: {
                    "ratio": results[6][key][0][1],
                    "totalCount": results[6][key][0][2],
                    "perInAutosomalChr": results[6][key][0][3]
                },
                results[6][key][1][0]: {
                    "ratio": results[6][key][1][1],
                    "totalCount": results[6][key][1][2],
                    "perInAutosomalChr": results[6][key][1][3]
                },
                results[6][key][2][0]: {
                    "ratio": results[6][key][2][1],
                    "totalCount": results[6][key][2][2],
                    "perInAutosomalChr": results[6][key][2][3]
                }
            }

        resultDict = {
            "1-triotype": results[0],
            "probandIndex": results[1],
            "totalNoVariants": results[2],
            "totalPFautosomalVariants": results[3],
            "totalPFxVariants": results[4],
            "heterozygosities": {
                self.samples[0]: results[5][0],
                self.samples[1]: results[5][1],
                self.samples[2]: results[5][2]
            },
            "tests": testDict
        }

        (computedResult, expectedResult) = self.summarize(resultDict)
        resultDict["2-trioTestResult"] = computedResult
        resultDict["3-inputResult"] = expectedResult

        is_trio_regular = self.is_trio_regular(resultDict)
        is_trio_gender_matching = self.is_trio_gender_matching(resultDict, allow_undetermined=False)

        resultArray = [is_trio_regular and is_trio_gender_matching, resultDict]

        self.dump(resultArray)

        if not is_trio_regular:
            msg = "Three samples are not a regular trio, sample swapped?"
            logging.error(msg)
            if self.exception:
                raise RuntimeError(msg)

        if self.allow_undetermined:
            # Re-run the test with more leniency, for RuntimeError purposes only
            is_trio_gender_matching = self.is_trio_gender_matching(resultDict, allow_undetermined=True)
        if not is_trio_gender_matching:
            msg = "Three samples are a regular trio, but the input pedigree is wrong"
            logging.error(msg)
            if self.exception:
                raise RuntimeError(msg)

    def is_trio_regular(self, resultDict):
        '''
        Returns True if Trio is a regular trio
        '''
        if resultDict['1-triotype'] == 'regular trio':
            return True
        else:
            return False

    def is_trio_gender_matching(self, resultDict, allow_undetermined):
        '''
        Returns True if gender for all three members are matching.
        If allow_undetermined is True, then 'undertermined' also matches
        '''
        res = True
        for (computed_key, computed_value), (expected_key, expected_value) in \
            zip(iter(resultDict['2-trioTestResult'].items()),
                iter(resultDict['3-inputResult'].items())):
            assert computed_key == expected_key
            assert computed_key in ['proband', 'father', 'mother']
            try:
                assert computed_value['sample'] == expected_value['sample']
            except AssertionError:
                msg = "The {} was expected to be {}, but Filtus says it is {}".format(
                    computed_key,
                    expected_value['sample'],
                    computed_value['sample']
                )
                logging.error(msg)
                res = False
            if allow_undetermined:
                res &= computed_value['gender'] in [expected_value['gender'] or "undetermined"]
            else:
                res &= computed_value['gender'] == expected_value['gender']
        return res

    def dump(self, resultArray):
        with open(self.output, 'w') as jsonOutput:
                json.dump(resultArray, jsonOutput, indent=4, sort_keys=True)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='QC test for trio type and gender of each individual in the vcf file')
    parser.add_argument('-i', required=True, dest='input',
                        help='a VCF file with exactly 3 samples')
    parser.add_argument('--motherID', required=True, dest='mid',
                        help='the sample ID of mother, should be the same ID in the vcf file')
    parser.add_argument('--fatherID', required=True, dest='fid',
                        help='the sample ID of father, should be the same ID in the vcf file')
    parser.add_argument('--probandID', required=True, dest='pid',
                        help='the sample ID of proband, should be the same ID in the vcf file')
    parser.add_argument('--probandGender', required=True, dest='pgender',
                        help='the gender of proband, should be female or male')
    parser.add_argument('--output', required=True, dest='output',
                        help='Output json file name')
    parser.add_argument('--allow-undetermined', required=False,
                        dest='allow_undetermined', action='store_true',
                        help='do NOT raise exception when gender test fails because of undertermined sex')
    parser.add_argument('--not-allow-undetermined', required=False,
                        dest='allow_undetermined', action='store_false',
                        help='raise exception exception when gender test fails because of undertermined sex')
    parser.add_argument('--no-exception', required=False, dest='exception',
                        help='do NOT raise exception when test fails', action='store_false')
    parser.add_argument('--exception', required=False, dest='exception',
                        help='raise exception when test fails', action='store_true')
    parser.set_defaults(exception=True)
    parser.set_defaults(allow_undetermined=True)

    args = parser.parse_args()

    qc = trioGenderTest(vcf=args.input,
                        motherID=args.mid,
                        fatherID=args.fid,
                        probandID=args.pid,
                        probandGender=args.pgender,
                        output=args.output,
                        allow_undetermined=args.allow_undetermined,
                        exception=args.exception)
    qc.process()
