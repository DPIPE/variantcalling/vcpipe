import os

import pytest
import io
from compare_genotype import CompareGenotype, UndeterminedError, UnmatchedError


class CloseSupportIO(io.StringIO):

    def close(self):
        pass


@pytest.fixture
def cg():
    return CompareGenotype('', '')





@pytest.fixture
def dbsnp():
    return 'rs12345'


@pytest.fixture
def taq_file():
    return CloseSupportIO("""1\t89271574\trs786906\tT\tT
1\t183086757\trs2296292\tA\tC
2\t30381505\trs1137288\tC\tT""")


# A data structure mutated by tests to gather output from various functions.
# Assertions are made on this at end of the tests
def result_container():
    return {
        'hts_undetermined': 0,
        'taq_undetermined': 0,
        'matched': 0,
        'unmatched': 0,
        'genotypes': list()
    }



def test_check_positions_return_rs(cg, taq_file):
    cg._get_taq_fh = lambda: taq_file

    dbsnp, gt = cg._check_positions_return_rs(('1', '89271574'))
    assert dbsnp == 'rs786906'
    assert gt == ['T', 'T']

    dbsnp, gt = cg._check_positions_return_rs(('2', '30381505'))
    assert dbsnp == 'rs1137288'
    assert gt == ['C', 'T']

    with pytest.raises(RuntimeError):
        dbsnp, gt = cg._check_positions_return_rs(('1', '89271571'))


def test_format_genotype(cg):

    assert cg._format_genotype(['Undetermined', 'A']) == 'Undetermined'
    assert cg._format_genotype(['A', 'Undetermined']) == 'Undetermined'
    assert cg._format_genotype(['A', 'A']) == 'A/A'


def test_check_genotype_hts_undetermined(cg, dbsnp):

    result = result_container()
    cg._check_genotype(
        dbsnp,
        ['Undetermined', 'A'],
        ['A', 'T'],
        result
    )

    assert result['hts_undetermined'] == 1
    assert result['taq_undetermined'] == 0
    assert result['matched'] == 0
    assert result['unmatched'] == 0


def test_check_genotype_taq_undetermined(cg, dbsnp):

    result = result_container()
    cg._check_genotype(
        dbsnp,
        ['C', 'A'],
        ['Undetermined', 'T'],
        result
    )

    assert result['hts_undetermined'] == 0
    assert result['taq_undetermined'] == 1
    assert result['matched'] == 0
    assert result['unmatched'] == 0


def test_check_genotype_matched(cg, dbsnp):

    result = result_container()
    cg._check_genotype(
        dbsnp,
        ['C', 'A'],
        ['A', 'C'],
        result
    )

    assert result['hts_undetermined'] == 0
    assert result['taq_undetermined'] == 0
    assert result['matched'] == 1
    assert result['unmatched'] == 0


def test_check_genotype_unmatched(cg, dbsnp):

    result = result_container()
    cg._check_genotype(
        dbsnp,
        ['C', 'A'],
        ['A', 'T'],
        result
    )

    assert result['hts_undetermined'] == 0
    assert result['taq_undetermined'] == 0
    assert result['matched'] == 0
    assert result['unmatched'] == 1

def test_snp_required():
    # number_snp_tested
    cg = CompareGenotype('', '', ['2,0,0'])
    r = result_container()
    cg._check_genotype('rs1', ['C', 'A'], ['C', 'A'], r)
    cg.result = r
    with pytest.raises(KeyError):
        cg.check()

    # mismatch
    cg = CompareGenotype('', '', ['2,0,1'])
    r = result_container()
    cg._check_genotype('rs1', ['C', 'A'], ['A', 'T'], r)
    cg._check_genotype('rs2', ['C', 'A'], ['A', 'G'], r)
    cg.result = r
    with pytest.raises(UnmatchedError):
        check_rules = cg.check()
        cg.raise_exception(check_rules)

    # undetermined HTS
    cg = CompareGenotype('', '', ['2,1,0'])
    r = result_container()
    cg._check_genotype('rs1', ['Undetermined', 'Undetermined'], ['C', 'C'], r)
    cg._check_genotype('rs2', ['Undetermined', 'Undetermined'], ['G', 'A'], r)
    cg.result = r
    with pytest.raises(UndeterminedError):
        check_rules = cg.check()
        cg.raise_exception(check_rules)

    # undetermined TAQ
    cg = CompareGenotype('', '', ['2,1,0'])
    r = result_container()
    cg._check_genotype('rs1', ['C', 'C'], ['Undetermined', 'Undetermined'], r)
    cg._check_genotype('rs2', ['G', 'A'], ['Undetermined', 'Undetermined'], r)
    cg.result = r
    with pytest.raises(UndeterminedError):
        check_rules = cg.check()
        cg.raise_exception(check_rules)

    # undetermined HTS+TAQ ?
    cg = CompareGenotype('', '', ['2,1,0'])
    r = result_container()
    cg._check_genotype('rs1', ['Undetermined', 'Undetermined'], ['C', 'C'], r)
    cg._check_genotype('rs2', ['G', 'A'], ['Undetermined', 'Undetermined'], r)
    cg.result = r
    with pytest.raises(UndeterminedError):
        check_rules = cg.check()
        cg.raise_exception(check_rules)

def test_vcf():
    vcf = os.path.dirname(os.path.abspath(__file__)) + '/testdata/snp.raw.snpFingerPrintingTest.vcf'
    taq = os.path.dirname(os.path.abspath(__file__)) + '/testdata/NA12878.taqman'
    cg = CompareGenotype(vcf, taq, ['2,0,0'])
    records = cg._get_vcf_records()
    assert len(records) > 1

    cg.compare()