from cyvcf2 import VCF
import argparse
import json

####
# This script compare the results for the snp contained in the SNP-ID test (16 by default)
# The script take as argument a file created by gettaqman script, and the hts vcf file produced by the pipeline.
####

class UnmatchedError(Exception):
    pass


class UndeterminedError(Exception):
    pass


class CompareGenotype(object):

    def __init__(self, vcf_path, taq_path, snp_reqs=['16,4,1']):
        self.vcf_path = vcf_path
        self.taq_path = taq_path
        self.snp_reqs = dict()
        for reqs in snp_reqs:
            number_snp_tested, undetermined, mismatch = tuple(int(s) for s in reqs.split(','))
            self.snp_reqs[number_snp_tested] = {'undetermined': undetermined, 'mismatch': mismatch}

        self.result = None

    def _get_vcf_records(self):
        '''
        Get the vcf file and return an handle (vcf_reader) as well as an object (vcf_records) representing the variants
        '''
        vcf_records = []
        vcf_reader = VCF(self.vcf_path)
        for record in vcf_reader:
            vcf_records.append(record)
        return vcf_records, vcf_reader

    def _get_taq_fh(self):
        return open(self.taq_path, 'r')

    def _check_positions_return_rs(self, hts_pos):
        '''get  the hts postion and a taq file result file,
           return the rs number and the genotype for the taqm man file.

           ToDo: How to return a mismatch?
        '''

        try:
            fh = self._get_taq_fh()
            for line in fh:
                parts = line.strip().split("\t")
                if str(hts_pos[0]) == parts[0] and str(hts_pos[1]) == parts[1]:
                    return (parts[2], sorted(parts[3:5]))

            raise RuntimeError("Found no matching snp in taq file for HTS {}".format(str(hts_pos)))
        finally:
            fh.close()

    def _format_genotype(self, gt):
        """
        Return 'Undetermined' if any allele is undetermined, else return format 'C/T'
        """
        if 'Undetermined' in gt:
            return 'Undetermined'
        else:
            return '/'.join(gt)

    def _check_genotype(self, dbsnp, hts_genotype, taq_genotype, result):
        gt = {
            'taq': self._format_genotype(taq_genotype),
            'hts': self._format_genotype(hts_genotype),
            'dbSNP': dbsnp
        }

        result['genotypes'].append(gt)

        if 'Undetermined' in hts_genotype:
            result['hts_undetermined'] += 1
            return

        if sorted(hts_genotype) == sorted(taq_genotype):
            gt['match'] = True
            result['matched'] += 1
        elif 'Undetermined' in taq_genotype:
            result['taq_undetermined'] += 1
        else:
            result['unmatched'] += 1

    def compare(self):
        vcf_records, vcf_reader = self._get_vcf_records()
#        sample_name = vcf_reader.samples[0]

        result = {
            'hts_undetermined': 0,
            'taq_undetermined': 0,
            'matched': 0,
            'unmatched': 0,
            'genotypes': list()
        }

        for record in vcf_records:
            dbsnp, taq_genotype = self._check_positions_return_rs((record.CHROM, record.POS))
            hts_genotype = record.gt_bases[0]

            if hts_genotype is None:
                hts_genotype = ['Undetermined', 'Undetermined']
            else:
                hts_genotype = hts_genotype.split('/')
                #If the genotype return islonger than one nucleotide per strand the check will fail, 
                #so need to get only the first character of each strand.
                if len(hts_genotype[0]) > 1 and len(hts_genotype[1]) > 1:
                  print("For this snp (", record.CHROM, record.POS,") went from ",hts_genotype[0],"/",hts_genotype[1], "to ", hts_genotype[0][0],"/",hts_genotype[1][0])
                  hts_genotype = hts_genotype[0][0],hts_genotype[1][0]

            self._check_genotype(dbsnp, hts_genotype, taq_genotype, result)

        return result

    def check(self):
        if self.result is None:
            self.result = self.compare()

        number_snp_tested = len(self.result['genotypes'])
        try:
            self.mismatch = self.snp_reqs[number_snp_tested]['mismatch']
            self.undetermined = self.snp_reqs[number_snp_tested]['undetermined']
        except KeyError:
            print(("The taqman file has {} SNPs but no valid --snp_reqs option for this number of SNPs has been provided.".format(number_snp_tested)))
            print(("DEBUG: `snp_reqs = {}`".format(self.snp_reqs)))
            raise

        check_rules = [
                # (rule, message, exception)
            (self.result['unmatched'] <= self.mismatch,
             "Error: Too many unmatched snps. Unmatched: {}, threshold: {}.\n".format(self.result['unmatched'],
                                                                            self.mismatch),
             UnmatchedError),
            (self.result['hts_undetermined'] + self.result['taq_undetermined'] <= self.undetermined - self.result['unmatched'],
             "Error: Too many undetermined SNPs: Undetermined TAQ: {}, Undetermined HTS: {}, Threshold: {}.\n".format(self.result['taq_undetermined'],
                                                                                                            self.result['hts_undetermined'],
                                                                                                            self.undetermined),
             UndeterminedError)
        ]
        return check_rules

    def write_report(self, report_path, check_rules):

        if self.result is None:
            self.result = self.compare()

        with open(report_path, 'w') as fd_out:

            fd_out.write('HTS file: {}\n'.format(self.vcf_path))
            fd_out.write('Taq file: {}\n'.format(self.taq_path))
            fd_out.write('\n')

            for gt in self.result['genotypes']:
                fd_out.write('\t'.join(
                    [
                        gt['dbSNP'],
                        gt['taq'],
                        gt['hts'],
                        'Matched' if gt.get('match') else ''
                    ])
                )
                fd_out.write('\n')

            fd_out.write('\n' * 2)
            fd_out.write('Matched: {}\n'.format(self.result['matched']))
            fd_out.write('Unmatched: {}\n'.format(self.result['unmatched']))
            fd_out.write('HTS undetermined: {}\n'.format(self.result['hts_undetermined']))
            fd_out.write('Taq undetermined: {}\n'.format(self.result['taq_undetermined']))
            for result, msg, _ in check_rules:
                if not result:
                    fd_out.write(msg)
            fd_out.write('Success: {}\n'.format(all([r[0] for r in check_rules])))
    
    def write_json_report(self, json_report_path, check_rules):
        if self.result is None:
            self.result = self.compare()   
        
        result_dict = {
            'HTS file': self.vcf_path,
            'Taq file': self.taq_path,
            'genotypes': dict(),
            'Matched': self.result['matched'],
            'Unmatched': self.result['unmatched'],
            'HTS undetermined': self.result['hts_undetermined'],
            'Taq undetermined': self.result['taq_undetermined']   
        }
        for gt in self.result['genotypes']:
            result_dict['genotypes'][gt['dbSNP']] = {
                'taq': gt['taq'],
                'hts': gt['hts'],
                'Matched': gt.get('match')
            }

        json_report = {'FingerPrinting': [
            all([r[0] for r in check_rules]),
            result_dict
        ]}
        with open(json_report_path, 'w') as f:
            json.dump(json_report, f, sort_keys=True, indent=4, separators=(',', ': '))


    def raise_exception(self, check_rules):
        for result, msg, exception in check_rules:
            if not result:
                raise exception(msg)


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Compare genotype of VCF file called in HTS with TAQMAN file produced by SNP-ID.")
    parser.add_argument('--vcf', help='Input VCF file called by GATK (or other caller)', required=True, type=str)
    parser.add_argument('--taq', help='Input Taqman GT calls', required=True, type=str)
    parser.add_argument('-o', '--output', help='Output report file', required=True, type=str)
    parser.add_argument('--json', help='Output json file', required=False, type=str)
    parser.add_argument('--snp_reqs', nargs='+', help='Requirements for testing SNPs \
where number_snp_tested, undetermined, mismatch separated by commas, \
and different requirement groups are separated by spaces \
Eg: "23,8,1 16,4,1" or "16,4,1"', required=True, type=str)

    args = parser.parse_args()

    cg = CompareGenotype(args.vcf, args.taq, args.snp_reqs)
    check_rules = cg.check()
    cg.write_report(args.output, check_rules)
    if args.json:
        cg.write_json_report(args.json, check_rules)
    cg.raise_exception(check_rules)
