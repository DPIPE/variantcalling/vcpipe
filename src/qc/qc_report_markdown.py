#!/usr/bin/env python

import json
import argparse

RENAME_QC = {
    'PCT_TARGET_BASES_10X': 'Coverage >10x',
    'PCT_10X': 'Coverage >10x',
    'PCT_TARGET_BASES_20X': 'Coverage >20x',
    'PCT_20X': 'Coverage >20x',
    'MEAN_TARGET_COVERAGE': 'Mean coverage',
    'MEAN_COVERAGE': 'Mean coverage',
    'MEDIAN_COVERAGE': 'Median coverage',
    'contamination': 'Variants skewed allele depth',
    'Q30': 'Base quality >30',
    'PCT_PF_READS_ALIGNED[2]': 'Aligned non-filtered',
    'tiTvRatio': 'Transitions/transversions',
    'skewedRatio': 'Ratio skewed allele depth',
    'variants': 'Total variants',
}


def report_basepipe_qc(sample_name, basepipe_qc_result):
    report.write("\n## " + sample_name + "\n\n")
    table_smp_qc_header = """\
|   QC            | PASS/FAIL        |  Value          | Acceptable range |
|:----------------|:----------------:|:----------------|:-----------------|
"""
    report.write(table_smp_qc_header)
    for qc_group in basepipe_qc_result:

        failed_elements = []
        table_smp_qc_row = "| {qc_element} | {qc_pass} | {qc_value} | {qc_range} |\n"
        if len(basepipe_qc_result[qc_group]) <= 2:
            # Legacy QC result without criteria/thresholds available
            qc_pass = basepipe_qc_result[qc_group][0]
            report.write(table_smp_qc_row.format(
                qc_element=qc_group,
                qc_pass=qc_pass,
                qc_value="N/A",
                qc_range="N/A"
            ))
        else:
            for qc_element in basepipe_qc_result[qc_group][2]:
                # QC value
                if qc_element == "PCT_PF_READS_ALIGNED[2]":
                    qc_value = basepipe_qc_result[qc_group][1]["PCT_PF_READS_ALIGNED"][2]
                elif qc_group in ["MosdepthMetrics"]:
                    qc_value = basepipe_qc_result[qc_group][1]["metrics"][qc_element]
                else:
                    qc_value = basepipe_qc_result[qc_group][1][qc_element]

                # QC range
                qc_range = basepipe_qc_result[qc_group][2][qc_element]

                # QC PASS/FAIL
                if isinstance(qc_range, list):
                    qc_pass = "PASS" if round(qc_value,2) >= qc_range[0] and qc_value < qc_range[1] else "FAIL"
                else:
                    qc_pass = "PASS" if qc_value > qc_range else "FAIL"

                # accumulate failed qc element
                if qc_pass == "FAIL":
                    failed_elements.append(qc_element)

                report.write(table_smp_qc_row.format(
                    qc_element=RENAME_QC.get(qc_element, qc_element),
                    qc_pass=qc_pass,
                    qc_value=qc_value,
                    qc_range=qc_range))

        # qc_result.json reflects the real check as in qcanalysis.py
        # changes in qcaanlysis.py e.g. use '>=' instead of '>', may cause inconsistency
        if not basepipe_qc_result[qc_group][0] and (failed_elements != 0):
            qc_warnings.append('{} FAILED pipeline QC: {}'.format(
                sample_name, ", ".join([
                    '"' + RENAME_QC.get(e, e) + '"' for e in failed_elements
                ])))


def report_trio_qc(trio_name, trio_qc_result, ped_file):
    trio = trio_qc_result["filtus-gender-test"][1]["1-triotype"]
    pedigree_check = "PASS" if trio == "Regular trio" else "FAIL"

    if pedigree_check == "FAIL":
        qc_warnings.append("Pedigree test FAILED for {}".format(trio_name))

    report.write("## Pedigree and gender test\n\n")

    table_ped_gen = """\
|   Sample        |   QC            | PASS/FAIL        |  Value          | Acceptable range |
|:----------------|:----------------|:----------------:|:---------------:|:----------------:|
| {trio_name}     | Pedigree test   | {pedigree_check} | {trio}          | Regular trio     |
"""
    report.write(
        table_ped_gen.format(trio_name=trio_name,
                             pedigree_check=pedigree_check,
                             trio=trio))

    def _write_gender_test_row(member, gender_check_pass, real_gender, inferred_gender):
        gender_row = ("| {sample} | Gender test | {gender_check_pass} |" +
                      " {inferred_gender} | {real_gender} |\n")
        report.write(
            gender_row.format(sample=member,
                              gender_check_pass=gender_check_pass,
                              real_gender=real_gender,
                              inferred_gender=inferred_gender))

    # new gender test with vcfped
    if "gender-check" in trio_qc_result["filtus-gender-test"][1]:
        gender_checks = trio_qc_result["filtus-gender-test"][1]["gender-check"]
        real_genders = {}
        with open(ped_file, 'r') as f_ped:
            for ped in f_ped:
                member = ped.split()[1]
                gender = {"1":"Male", "2":"Female"}[ped.split()[-2]]
                real_genders[member] = gender
        assert set(gender_checks.keys()) == set(real_genders.keys())

        for member in gender_checks:
            inferred_gender = gender_checks[member]["gender"].capitalize()
            real_gender = real_genders[member]
            gender_check_pass = "PASS" if inferred_gender == real_gender else "FAIL"
            if gender_check_pass == "FAIL":
                qc_warnings.append("{} FAILED gender test".format(member))

            _write_gender_test_row(member, gender_check_pass, real_gender, inferred_gender)

    # old gender test with filtus
    elif "2-trioTestResult" in trio_qc_result["filtus-gender-test"][1]:
        gender_checks = trio_qc_result["filtus-gender-test"][1]["2-trioTestResult"]
        input_genders = trio_qc_result["filtus-gender-test"][1]["3-inputResult"]
        percent_het = trio_qc_result["filtus-gender-test"][1]["heterozygosities"]
        for ped in gender_checks:
            inferred_gender = gender_checks[ped]["gender"].capitalize()
            member = gender_checks[ped]["sample"]
            heterozygosity = float(percent_het[member])
            real_gender = input_genders[ped]["gender"].capitalize()
            gender_check_pass = "PASS" if inferred_gender == real_gender else "FAIL"
            if gender_check_pass == "FAIL":
                qc_warnings.append("{} FAILED gender test".format(member))

            _write_gender_test_row(member, gender_check_pass, real_gender, inferred_gender)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description=
        "parse pipeline qc_result.json and convert to markdown to append ella report.txt"
    )

    parser.add_argument("--report-file",
                        action="store",
                        dest="report_file",
                        required=True,
                        help="Path to report.txt file.")

    parser.add_argument("--warnings-file",
                        action="store",
                        dest="warnings_file",
                        required=True,
                        help="Path to warnings.txt file.")


    parser.add_argument("--qc-result",
                        action="store",
                        dest="qc_result_json",
                        required=True,
                        help="Path to qc_result.json file.")

    parser.add_argument("--type",
                        action="store",
                        dest="analysis_type",
                        required=True,
                        help="type of analysis, [single, trio]")

    parser.add_argument("--ped-file",
                        action="store",
                        dest="ped_file",
                        required=False,
                        help="pedigree file for trio analysis")

    parser.add_argument("--sample-id",
                        action="store",
                        dest="sample_id",
                        required=True,
                        help="trio .analysis file")

    parser.add_argument("--is-dragen",
                        type=lambda s: s.lower() in ['true', 'y', 'yes', '1'],
                        dest="is_dragen",
                        help="DRAGEN pipeline")

    args = parser.parse_args()

    report_file = args.report_file
    warnings_file = args.warnings_file
    pipeline_qc_result_json = args.qc_result_json
    ped_file = args.ped_file
    analysis_type = args.analysis_type
    sample_id = args.sample_id
    is_dragen = args.is_dragen

    report = open(report_file,"a")
    report.write("# Pipeline QC\n\n")

    # accumulate qc warnings
    qc_warnings = []

    with open(pipeline_qc_result_json) as qc:
        qc_result = json.load(qc)

    qc_key = 'qc_dragen' if is_dragen else 'qc'

    if analysis_type == "trio":  # trio
        trio_name = sample_id
        report_trio_qc(trio_name, qc_result, ped_file)

        for member in qc_result["samples"]:
            report_basepipe_qc(member, qc_result["samples"][member])
    else:
        sample_name = sample_id
        report_basepipe_qc(sample_name, qc_result)

    report.close()

    # write qc_warnings.md if any qc failed
    if qc_warnings:
        with open(warnings_file,'w') as wf:
            for w in qc_warnings:
                wf.write("\n\n" + w + "\n\n")
