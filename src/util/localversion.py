import os
import json
import io
from settings import settings

LOCAL_PATH = os.path.dirname(os.path.realpath(__file__))


class LocalVersion(object):

    def __init__(self):
        pass

    def getVersion(self):
        paths = {
            'vcpipe': os.path.join(LOCAL_PATH, '../../version'),
            # TODO: These now live in the nextflow config file - how to get them here?
            'genepanels': os.path.join(settings['genepanels_home'], 'version'),
            'sensitive-db': os.path.join(settings['sensitive_db'], 'version')
        }

        version = dict()
        for name, path in paths.items():
            o = self.readVersionFile(path)
            o = self.checkForProduction(o)
            o = self.checkForDevelopment(o)
            version[name] = o

        return version

    def checkForProduction(self, obj):
        prod_tag = '-rel'
        if prod_tag in obj.get('tag', ''):
            obj['production'] = True
        else:
            obj['production'] = False
        return obj

    def checkForDevelopment(self, obj):
        """
        If version fields are same as original, it means it was never run through
        git archive. In that case, replace the values with 'development'.
        """
        dev_tag = '$Format:'
        for k, v in dict({k: v for k, v in obj.items() if k in ['tag', 'revision']}).items():
            if dev_tag in v:
                obj[k] = 'development'
                obj['production'] = False
        return obj

    def readVersionFile(self, path):
        with open(path) as fd:
            version = json.load(fd)
            return version

    def formatVersion(self):
        version = self.getVersion()
        version_str = '###########################\nVERSION INFORMATION\n###########################\n'
        for k, v in version.items():

            _version = [
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~',
                '{}'.format(k),
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~',
                'Revision: {}'.format(v['revision']),
                'Tags: {}'.format(v['tag']),
                '\n'
            ]
            version_str += '\n'.join(_version)

        return version_str

    def refdataVersion(self):
        content = '~~~~~~~~~~~~~~~~~~~~~~~~~~~\nReference data\n~~~~~~~~~~~~~~~~~~~~~~~~~~~\n'
        version_file = os.path.join(settings['refdata_home'], 'refdata-current-versions.txt')
        with io.open(version_file, mode='r', encoding='utf8') as fd:
            content += fd.read()

        return content

    def printVersion(self):
        print(self.formatVersion())
        print(self.refdataVersion())


if __name__ == '__main__':
    try:
        v = LocalVersion()
        v.printVersion()
    except Exception:
        print(("*" * 80 + "\n")*20)
        import traceback
        #print("Error while parsing version information. Cannot proceed.")
        print(traceback.format_exc())
        print("Error while parsing version information. Proceeding, but should be fixed!")
        print(("*" * 80 + "\n")*20)
