"""
Tools for downgrading/upgrading the database schema
"""

import os
from alembic.config import Config
from alembic import command

from util import DB


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
ALEMBIC_CFG = os.path.join(SCRIPT_DIR, '../executor/model/migration/alembic.ini')
ALEMBIC_DIR = os.path.join(SCRIPT_DIR, '../executor/model/migration/alembic/')


def _get_alembic_config():
    alembic_cfg = Config(ALEMBIC_CFG)
    alembic_cfg.set_main_option("script_location", ALEMBIC_DIR)
    return alembic_cfg


def migration_upgrade(rev):
    alembic_cfg = _get_alembic_config()
    db = DB(os.environ['DB_URL'], pool_size=5, pool_max_overflow=60, pool_timeout=120)
    with db.engine.begin() as connection:
        alembic_cfg.attributes['connection'] = connection
        command.upgrade(alembic_cfg, rev)


def migration_downgrade(rev):
    alembic_cfg = _get_alembic_config()
    db = DB(os.environ['DB_URL'], pool_size=5, pool_max_overflow=60, pool_timeout=120)
    with db.engine.begin() as connection:
        alembic_cfg.attributes['connection'] = connection
        command.downgrade(alembic_cfg, rev)


def migration_current():
    alembic_cfg = _get_alembic_config()
    db = DB(os.environ['DB_URL'], pool_size=5, pool_max_overflow=60, pool_timeout=120)
    with db.engine.begin() as connection:
        alembic_cfg.attributes['connection'] = connection
        command.current(alembic_cfg)
