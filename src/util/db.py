

class DB(object):

    def __init__(self, host, pool_size=5, pool_max_overflow=10, pool_timeout=30):
        # Lazy load dependencies
        from sqlalchemy import create_engine
        from sqlalchemy.orm import scoped_session, sessionmaker

        self.host = host
        self.engine = create_engine(
            self.host,
            max_overflow=pool_max_overflow,
            pool_size=pool_size,
            pool_timeout=pool_timeout
        )
        self.sessionmaker = scoped_session(sessionmaker(bind=self.engine))
