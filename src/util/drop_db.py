"""
For dropping the database on TSD, since we don't have psql available.
"""

from executor.model import Sample, Analysis, Version
from executor.model import Base, db


print("Are you sure you want to drop database? Currently connected to:")
print("")
print(db.host)
print("")

q = input("Type DROP DATABASE to proceed:\n")
if q == 'DROP DATABASE':
    print("Dropping database")
    Base.metadata.drop_all(bind=db.engine)
    print("Done")
else:
    print("Aborted.")

