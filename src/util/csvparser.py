import csv
import io
from collections import OrderedDict


def tryConvertInt(value):
    try:
        v = int(value)
    except ValueError:
        try:
            v = float(value)
        except ValueError:
            v = value
    return v


class CsvParser(object):

    def __init__(self, dialect='excel-tab'):
        self.dialect = dialect

    def parse(self, text):
        fd = io.StringIO(text)
        reader = csv.reader(fd, dialect=self.dialect)

        lines = [r for r in reader]
        if not lines:
            return None
        data = dict()
        header = lines[0]
        rows = [line for line in lines[1:] if line]

        for h_idx, header in enumerate(header):
            data[header] = list()
            should_convert = all(
                isinstance(tryConvertInt(r[h_idx]), int) or
                isinstance(tryConvertInt(r[h_idx]), float)
                for r in rows if r
            )
            if should_convert:
                conv_func = tryConvertInt
            else:
                conv_func = lambda x: x

            for r in rows:
                if r:
                    data[header].append(conv_func(r[h_idx]))

            if len(data[header]) == 1:
                data[header] = data[header][0]
            elif not data[header]:
                data.pop(header)


            # if idx == 0:
            #     header = [r.replace('#', '') for r in row]
            #     data = OrderedDict((k, list()) for k in header)
            # else:
            #     for i, r in enumerate(row):
            #         data[header[i]].append(tryConvertInt(r))
        return dict(data)
