import logging
import os
import threading
import time


class KillFileWatcher(object):
    """
    Watches for existence of a given file.
    If the file exists, the process issues SIGKILL on its own PID.
    """

    def __init__(self, path, poll_interval=5):
        self.path = path
        self.poll_interval = poll_interval
        self.running = False

    def _watcher(self):
        while self.running:
            if os.path.exists(self.path):
                logging.info("Killfile at {} detected. Issuing SIGKILL on self ({})!".format(self.path, os.getpid()))
                os.kill(os.getpid(), 9)
            time.sleep(self.poll_interval)

    def start(self):
        if os.path.exists(self.path):
            raise RuntimeError("Killfile {} already exists. Cannot start watcher.".format(self.path))
        self.running = True
        t = threading.Thread(target=self._watcher)
        t.daemon = True
        t.start()

    def stop(self):
        self.running = False
