import logging
from report.interpretation.metadata import VEP_CONSEQUENCE

log = logging.getLogger(__name__)


class CSQAllele(object):
    """
    Class for filtering and handling CSQ (VEP) transcripts for a single allele.
    Some rules:
    1. Only CSQ transcripts matching a gene in the genepanel gene list will be used.
    2. Only refseq transcripts are used. If no refseq, then use Ensembl.

    """
    def __init__(self, csq_items, gp_transcripts):
        """
        csq_items = list of CSQ items
        gp_transcripts = [{TRANSCRIPT: {'inheritance': 'AR'}}]

        """
        self.gp_transcripts = gp_transcripts
        # 'NM_ABC.1' -> 'NM_ABC'
        self.gp_no_version_transcripts = {self._strip_version(t): v for t, v in self.gp_transcripts.items()}
        self.org_csq_items = csq_items
        self.csq_items = self._filter_csq_items(csq_items)

    def _strip_version(self, transcript_name):
        return transcript_name.split('.', 1)[0]

    def _compare_item_transcript(self, csq_item):
        """
        Check if the provided transcript is in the genepanel transcripts.
        COMPARE HAPPENS WITHOUT TRANSCRIPT VERSION NUMBER!
        """
        no_version = csq_item.get('Feature', '').split('.', 1)[0]
        return no_version in self.gp_no_version_transcripts

    def _filter_csq_items(self, csq_items):
        # Filter csq_items by gene since we should never look at csq_items outside provided genes
        genes = [t['gene'] for t in list(self.gp_transcripts.values())]
        items_gp_genes = [t for t in csq_items if t.get('SYMBOL') in genes]
        # Filter out non-refseq transcripts as we normally don't use them
        filtered = [t for t in items_gp_genes if t.get('Feature', '').startswith('NM')]
        if filtered:
            return filtered

        # If no csq_item remains, it is because VEP did not provide any
        # annotation for the RefSeq transcript (NM_).
        # -> Provide anno for the Ensembl (ENST) transcript instead
        gene_symbol = ''
        for csq_item in items_gp_genes:
            gene_symbol = csq_item['SYMBOL']
            transcripts = [t for symbol, t in self.gp_transcripts.items() if t.get('gene', '') == gene_symbol]
            for transcript in transcripts:
                ensembl_transcript = transcript.get('ensembl_transcript', '')
                if csq_item.get('Feature', 'N/A') == ensembl_transcript:
                    log.info("No RefSeq (NM_) transcript found in VEP annotation; " +
                                "using Ensembl transcript instead: " +
                                "{} --> {}.".format(gene_symbol,ensembl_transcript))
                    filtered += [csq_item]
        if filtered:
            return filtered
        else:
            log.warning("No RefSeq (NM_) nor Ensembl transcript found in VEP annotation: " +
                                    "{}.".format(gene_symbol))
            return []

    def _get_consequence_text(self, csq_items, include_transcripts=False):
        if include_transcripts:
            return ' | '.join('{} ({})'.format(','.join(t.get('Consequence', 'not_available')), t['Feature']) for t in csq_items)
        return ' | '.join(','.join(t.get('Consequence', 'not_available')) for t in csq_items)

    def get_worst_consequence(self):
        """
        Returns tuple of (sorted_items [list], worse_outside_gp [bool])
        where sorted_items are items in data sorted by worse consequences.

        If there are any items with worse consequences where the item's transcript
        is not in the genepanel, it will be the first item in the returned list.
        Also the 'worse_outside_gp' flag will be set to True.
        """
        if not self.csq_items:
            return [], False

        # Get minimum index for each item (since each have several consequences), then sort by that index
        # Prioritise items with transcripts that are in genepanel,
        # so if all transcript consequences are equal, the items with
        # genepanel transcripts come on top
        sorted_items = sorted(self.csq_items, key=lambda x: (min(list(VEP_CONSEQUENCE.keys()).index(c) for c in x.get('Consequence',  ['not_available'])), not self._compare_item_transcript(x)))

        # gp_items = items with transcripts that are in the genepanel
        gp_items = [t for t in sorted_items if self._compare_item_transcript(t)]

        # If top item has transcript in our genepanel list,
        # there are no worse consequences than in our genepanel items.
        # If so, use only the genepanel one(s).
        # If there are worse, include the top one in addition to the genepanel ones
        if gp_items and any(sorted_items[0] == c for c in gp_items):
            return gp_items, False
        else:
            log.debug("Found worse consequence in another transcript, including it as well...")
            return [sorted_items[0]] + gp_items, True

    def format_worst_consequence(self):
        """
        See get_worst_consequence(). Returns a formatted string with results
        like "stop_gained (NM_1234567.4) | missense_variant (NM_018418.4)"
        """
        items, worse_outside_gp = self.get_worst_consequence()
        return self._get_consequence_text(items, include_transcripts=worse_outside_gp)

    def get_filtered_items(self):
        """
        Filters down the available transcripts according to following rules:
        1. If there are no items at all, returns an empty dictionary. This can happen due to filtering in __init__()
        2a. Return the items with transcripts in the genepanel_transcripts, if any.
        2b. If the cDNA doesn't match between the refseq and ensembl, include ensembl as well.
            This happens when there's a mapping mismatch between reference genome and Refseq transcript.
            We're afraid of indels in the reference genomes, as they cause wrong cDNA position and protein
            for the Refseq transcript. Ensembl doesn't have this problem, as they use the reference genome
            as base. Hence we can include them as a control in the troublesome cases.
        3. If no items have transcripts in genepanel_transcripts, return all items.
        """

        if not self.csq_items:
            return [dict()]
        selected = [t for t in self.csq_items if self._compare_item_transcript(t)]
        if selected:
            filtered = list()
            for t in selected:
                filtered.append(t)
                # Find ensembl transcript matching our refseq
                ensembl_transcript_name = self.gp_no_version_transcripts.get(self._strip_version(t['Feature']), {}).get('ensembl_transcript')
                # Find HGVSc for ensembl transcript
                ensembl_transcript = next((t for t in self.org_csq_items if t['Feature'] == ensembl_transcript_name), {})
                if not ensembl_transcript:
                    log.info("Missing c.DNA for corresponding Ensembl transcript" +
                    "({})".format(ensembl_transcript.get('Feature')))
                elif not ensembl_transcript.get('HGVSc'):
                    log.info("Missing HGVSc for corresponding Ensembl transcript. Including it regardless." +
                            "({})".format(ensembl_transcript.get('Feature')))
                    filtered.append(ensembl_transcript)
                else:
                    refseq_cdna = t.get('HGVSc', ':N/A').split(':', 1)[1]
                    ensembl_cdna = ensembl_transcript['HGVSc'].split(':', 1)[1]
                    if refseq_cdna != ensembl_cdna:
                        log.debug("RefSeq <-> Ensembl c.DNA mismatch, including Ensembl transcript")
                        filtered.append(ensembl_transcript)
            return filtered
        return self.csq_items

    def get_field(self, func):
        """
        Runs the provided function on all filtered items,
        including the transcript name in output if more than one item.
        """
        filtered_items = self.get_filtered_items()
        if len(filtered_items) > 1:
            return ' | '.join('{} ({})'.format(func(t), t['Feature']) for t in filtered_items)
        return func(filtered_items[0])

    def get_gene(self):
        """
        Returns gene symbols
        """
        filtered_items = self.get_filtered_items()
        no_versions = [t.get('Feature', '').split('.', 1)[0] for t in filtered_items]

        display_genes = list()
        for transcript, no_version in zip(filtered_items, no_versions):
            display_genes.append(transcript.get('SYMBOL', 'N/A'))

        return ','.join(display_genes)

    def get_inheritance(self):
        """
        Returns inheritance codes
        """
        filtered_items = self.get_filtered_items()
        no_versions = [t.get('Feature', '').split('.', 1)[0] for t in filtered_items]

        display_inh = list()
        for transcript, no_version in zip(filtered_items, no_versions):
            inh_codes = self.gp_no_version_transcripts.get(no_version, {}).get('display_inheritance', 'N/A')
            # If inheritance is not available it may mean that the transcript is not a RefSeq transcript
            # Use the inheritance associated with the gene symbol
            if inh_codes == 'N/A':
                symbol = transcript.get('SYMBOL', 'N/A')
                inh_codes = ','.join([v.get('display_inheritance', 'N/A') for k, v in self.gp_transcripts.items() if v.get('gene', '') == symbol])
            display_inh.append(inh_codes)
        return ' | '.join(display_inh)

    def get_omim_numbers(self):
        """
        Returns omim numbers
        """
        filtered_items = self.get_filtered_items()
        no_versions = [t.get('Feature', '').split('.', 1)[0] for t in filtered_items]

        omim_numbers = list()
        for transcript, no_version in zip(filtered_items, no_versions):
            omim_numbers += self.gp_no_version_transcripts.get(no_version, {}).get('omim_number', [])
        return list(set(omim_numbers))

    def get_extra_phenotypes(self):
        """
        Returns extra_phenotypes (for example in Metabolsk panel)
        """
        filtered_items = self.get_filtered_items()
        no_versions = [t.get('Feature', '').split('.', 1)[0] for t in filtered_items]

        extra_phenotypes = list()
        for transcript, no_version in zip(filtered_items, no_versions):
            extra_phenotypes += self.gp_no_version_transcripts.get(no_version, {}).get('extra_phenotypes', [])
        return ' | '.join(ep for ep in list(set(extra_phenotypes)) if ep)

    def get_phenotype(self):
        """
        Returns phenotypes (the english description of the diseases associated with that gene)
        Eg, for ABCC8 returns: "Diabetes mellitus, permanent neonatal"
        """
        filtered_items = self.get_filtered_items()
        no_versions = [t.get('Feature', '').split('.', 1)[0] for t in filtered_items]

        phenotype = list()
        for transcript, no_version in zip(filtered_items, no_versions):
            phenotype += self.gp_no_version_transcripts.get(no_version, {}).get('phenotype', [])
        return ' | '.join(p for p in list(set(phenotype)) if p)
