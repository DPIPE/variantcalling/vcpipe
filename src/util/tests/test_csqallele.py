import pytest
from ..csqallele import CSQAllele


@pytest.fixture
def csqdata():
    from .vcftestdata import vcfdata_singleallele
    allele = vcfdata_singleallele['ALT'][0]
    csqdata = vcfdata_singleallele['INFO'][allele]['CSQ']
    return csqdata


@pytest.fixture
def csqallele(csqdata):
    from .vcftestdata import transcripts
    return CSQAllele(csqdata, transcripts)


@pytest.fixture
def vcfsingleallele():
    from .vcftestdata import vcfdata_singleallele
    return vcfdata_singleallele


@pytest.fixture
def genes_in_transcripts():
    from .vcftestdata import transcripts
    return [v['gene'] for v in list(transcripts.values())]


class TestCSQAllele(object):

    def test_compare_item_transcript(self, csqallele):
        in_transcript_list = {
            'Feature': 'NM_017651.4',
            'SYMBOL': 'AHI1'
        }
        assert csqallele._compare_item_transcript(in_transcript_list)

        not_in_transcript_list = {
            'Feature': 'NM_12345.1',
            'SYMBOL': 'AHI1'
        }
        assert not csqallele._compare_item_transcript(not_in_transcript_list)

    def test_filter_data(self, csqallele, csqdata, genes_in_transcripts):

        filtered = csqallele._filter_csq_items(csqdata)

        for f in filtered:
            assert f['Feature'].startswith('NM')
            assert f['SYMBOL'] in genes_in_transcripts

    def test_get_filtered_items(self):

        transcripts = {
            'NM_201253.2': {
                'transcript': 'NM_201253.2',
                'ensembl_transcript': 'ENST00000484075',
                'inheritance': ['AR'],
                'gene': 'SPATA7'
            }
        }

        fake_data_same_hgvsc = [
            {
                'Feature': 'NM_201253.2',
                'SYMBOL': 'SPATA7',
                'HGVSc': 'NM_201253.2:c.1234A>T'
            },
            {
                'Feature': 'ENST00000484075',
                'SYMBOL': 'SPATA7',
                'HGVSc': 'ENST00000484075:c.1234A>T',
            }
        ]
        csqallele = CSQAllele(fake_data_same_hgvsc, transcripts)

        # Should return RefSeq item only, as the HGVSc match
        assert csqallele.get_filtered_items() == [fake_data_same_hgvsc[0]]

        fake_data_different_hgvsc = [
            {
                'Feature': 'NM_201253.2',
                'SYMBOL': 'SPATA7',
                'HGVSc': 'NM_201253.2:c.1234A>T'
            },
            {
                'Feature': 'ENST00000484075',
                'SYMBOL': 'SPATA7',
                'HGVSc': 'ENST00000484075:c.9999A>T',
            }
        ]
        csqallele = CSQAllele(fake_data_different_hgvsc, transcripts)

        # Should return both items
        assert csqallele.get_filtered_items() == fake_data_different_hgvsc

    def test_get_worst_consquence(self, csqallele, csqdata):

        # With alternative transcript having no worse consequence
        fake_data = [
            {
                'Feature': 'NM_018418.4',
                'SYMBOL': 'SPATA7',
                'Consequence': ['missense_variant']
            },
            {
                'Feature': 'NM_1234567.4',
                'SYMBOL': 'SPATA7',
                'Consequence': ['synonymous_variant']
            }
        ]

        csqallele.csq_items = fake_data
        assert csqallele.format_worst_consequence() == 'missense_variant'

        # With alternative transcript having worse consequence
        fake_data = [
            {
                'Feature': 'NM_018418.4',
                'SYMBOL': 'SPATA7',
                'Consequence': ['missense_variant']
            },
            {
                'Feature': 'NM_1234567.4',
                'SYMBOL': 'SPATA7',
                'Consequence': ['stop_gained']
            }
        ]

        csqallele.csq_items = fake_data
        assert csqallele.format_worst_consequence() == 'stop_gained (NM_1234567.4) | missense_variant (NM_018418.4)'

    def test_get_gene(self, csqallele):

        # Single match
        fake_data = [
            {
                'Feature': 'NM_018418.4',
                'SYMBOL': 'SPATA7',
            },
            {
                'Feature': 'NM_DOESNTEXIST.4',
                'SYMBOL': 'GARBAGE',
            }
        ]

        csqallele.csq_items = fake_data
        assert csqallele.get_gene() == 'SPATA7'

        # Multiple matches
        fake_data = [
            {
                'Feature': 'NM_018418.4',
                'SYMBOL': 'SPATA7',
            },
            {
                'Feature': 'NM_018051.4',
                'SYMBOL': 'WDR60',
            }
        ]

        csqallele.csq_items = fake_data
        assert csqallele.get_gene() == 'SPATA7,WDR60'

    def test_get_inheritance(self, csqallele):

        # Single match
        fake_data = [
            {
                'Feature': 'NM_018418.4',
                'SYMBOL': 'SPATA7',
            },
            {
                'Feature': 'NM_DOESNTEXIST.4',
                'SYMBOL': 'GARBAGE',
            }
        ]

        csqallele.csq_items = fake_data
        assert csqallele.get_inheritance() == 'AR'

        # Multiple matches
        fake_data = [
            {
                'Feature': 'NM_018418.4',
                'SYMBOL': 'SPATA7',
            },
            {
                'Feature': 'NM_018051.4',
                'SYMBOL': 'WDR60',
            }
        ]

        csqallele.csq_items = fake_data
        assert csqallele.get_inheritance() == 'AR | AR'
