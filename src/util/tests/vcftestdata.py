vcfdata_singleallele = {'ALT': ['T'],
     'CHROM': '1',
     'FILTER': 'PASS',
     'ID': 'rs12042179',
     'INFO': {'ALL': {'AN': 2,
                      'BaseQRankSum': 4.306,
                      'CLINVAR__ASP': True,
                      'CLINVAR__CAF': '0.5968,0.4032',
                      'CLINVAR__CLNACC': 'RCV000082823.1|RCV000124604.1',
                      'CLINVAR__CLNALLE': 1,
                      'CLINVAR__CLNDBN': 'not_specified|not_provided',
                      'CLINVAR__CLNDSDB': '.|.',
                      'CLINVAR__CLNDSDBID': '.|.',
                      'CLINVAR__CLNHGVS': 'NC_000001.10:g.197297540A>T',
                      'CLINVAR__CLNORIGIN': '1',
                      'CLINVAR__CLNREVSTAT': 'single|single',
                      'CLINVAR__CLNSIG': '2|2',
                      'CLINVAR__CLNSRC': 'ClinVar|.|GeneDx',
                      'CLINVAR__CLNSRCID': 'NM_201253.2:c.71-12A>T|.|11323',
                      'CLINVAR__COMMON': 1,
                      'CLINVAR__G5': True,
                      'CLINVAR__GENEINFO': 'CRB1:23418',
                      'CLINVAR__GNO': True,
                      'CLINVAR__INT': True,
                      'CLINVAR__KGPhase1': True,
                      'CLINVAR__KGPhase3': True,
                      'CLINVAR__PM': True,
                      'CLINVAR__PMC': True,
                      'CLINVAR__RS': 12042179,
                      'CLINVAR__RSPOS': 197297540,
                      'CLINVAR__SAO': 0,
                      'CLINVAR__SSR': 0,
                      'CLINVAR__VC': 'SNV',
                      'CLINVAR__VLD': True,
                      'CLINVAR__VP': '0x05002808000515013e000100',
                      'CLINVAR__WGT': 1,
                      'CLINVAR__dbSNPBuildID': 120,
                      'ClippingRankSum': -0.459,
                      'DB': True,
                      'DP': 108,
                      'EXAC__AC': [53414],
                      'EXAC__AC_AFR': [854],
                      'EXAC__AC_AMR': [6856],
                      'EXAC__AC_Adj': [52645],
                      'EXAC__AC_EAS': [6779],
                      'EXAC__AC_FIN': [3451],
                      'EXAC__AC_Het': [27043],
                      'EXAC__AC_Hom': [12801],
                      'EXAC__AC_NFE': [27433],
                      'EXAC__AC_OTH': [382],
                      'EXAC__AC_SAS': [6890],
                      'EXAC__AF': [0.435],
                      'EXAC__AN': 122864,
                      'EXAC__AN_AFR': 9374,
                      'EXAC__AN_AMR': 11382,
                      'EXAC__AN_Adj': 118216,
                      'EXAC__AN_EAS': 8554,
                      'EXAC__AN_FIN': 6668,
                      'EXAC__AN_NFE': 65054,
                      'EXAC__AN_OTH': 896,
                      'EXAC__AN_SAS': 16288,
                      'EXAC__BaseQRankSum': 1.72,
                      'EXAC__ClippingRankSum': -0.299,
                      'EXAC__DB': True,
                      'EXAC__DP': 2026729,
                      'EXAC__DP_HIST': ['191|1641|1687|2982|18204|10447|7727|5572|3877|2549|1692|1143|838|661|469|316|248|183|142|863'],
                      'EXAC__FS': 0.0,
                      'EXAC__GQ_HIST': ['238|243|148|158|1612|417|528|456|371|537|695|544|15132|2316|826|1176|1118|593|1007|33317'],
                      'EXAC__GQ_MEAN': 231.82,
                      'EXAC__GQ_STDDEV': 274.92,
                      'EXAC__Het_AFR': [766],
                      'EXAC__Het_AMR': [2656],
                      'EXAC__Het_EAS': [1449],
                      'EXAC__Het_FIN': [1673],
                      'EXAC__Het_NFE': [16377],
                      'EXAC__Het_OTH': [202],
                      'EXAC__Het_SAS': [3920],
                      'EXAC__Hom_AFR': [44],
                      'EXAC__Hom_AMR': [2100],
                      'EXAC__Hom_EAS': [2665],
                      'EXAC__Hom_FIN': [889],
                      'EXAC__Hom_NFE': [5528],
                      'EXAC__Hom_OTH': [90],
                      'EXAC__Hom_SAS': [1485],
                      'EXAC__InbreedingCoeff': 0.0942,
                      'EXAC__MQ': 60.0,
                      'EXAC__MQ0': 0,
                      'EXAC__MQRankSum': 0.054,
                      'EXAC__NCC': 156,
                      'EXAC__POSITIVE_TRAIN_SITE': True,
                      'EXAC__QD': 19.4,
                      'EXAC__ReadPosRankSum': 0.287,
                      'EXAC__VQSLOD': 5.63,
                      'EXAC__culprit': 'MQ',
                      'FS': 3.686,
                      'MQ': 60.0,
                      'MQ0': 0,
                      'MQRankSum': 0.835,
                      'POSITIVE_TRAIN_SITE': True,
                      'QD': 10.23,
                      'ReadPosRankSum': 0.109,
                      'SOR': 0.258,
                      'VQSLOD': 16.87,
                      'culprit': 'QD',
                      'inDB__inDB_alleleFreq': 'A:0.6113,T:0.3887',
                      'inDB__inDB_filter': 'PASS',
                      'inDB__inDB_genotypeFreq': 'A/A:0.3632,A/T:0.4962,T/T:0.1407',
                      'inDB__inDB_indications': 'Albinism:1,AuditivNeuropati:2,Bindevev:48,Cardiomyopathy:4,Ciliopati:36,EEogPU:108,HSP:1,HSPogHA:15,Iktyose:11,Immunodeficiency:4,Joubert:1,Microcephali:1,Mitokon:3,Na:8,OnePatientIndication:4,PCD:2',
                      'inDB__inDB_noMutInd': 249,
                      'set': 'variant'},
              'T': {'AC': 1,
                    'AF': 0.5,
                    'CSQ': [{'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['upstream_gene_variant'],
                             'DISTANCE': 4791,
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'ENSP00000439579',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'ENST00000543483',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': 'ENSG00000134376',
                             'HGNC_ID': '2343',
                             'IMPACT': 'MODIFIER',
                             'PUBMED': [19339744],
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1',
                             'SYMBOL_SOURCE': 'HGNC'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CCDS': 'CCDS53454.1',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'ENSP00000356369',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'ENST00000367399',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': 'ENSG00000134376',
                             'HGNC_ID': '2343',
                             'HGVSc': 'ENST00000367399.2:c.71-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/9',
                             'PUBMED': [19339744],
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1',
                             'SYMBOL_SOURCE': 'HGNC'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'nonsense_mediated_decay',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant',
                                             'NMD_transcript_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'ENSP00000433932',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'ENST00000484075',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': 'ENSG00000134376',
                             'HGNC_ID': '2343',
                             'HGVSc': 'ENST00000484075.1:c.71-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/11',
                             'PUBMED': [19339744],
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1',
                             'SYMBOL_SOURCE': 'HGNC'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CANONICAL': 'YES',
                             'CCDS': 'CCDS1390.1',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'ENSP00000356370',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'ENST00000367400',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': 'ENSG00000134376',
                             'HGNC_ID': '2343',
                             'HGVSc': 'ENST00000367400.3:c.71-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/11',
                             'PUBMED': [19339744],
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1',
                             'SYMBOL_SOURCE': 'HGNC'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'NP_001180569.1',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'NM_001193640.1',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': '23418',
                             'HGVSc': 'NM_001193640.1:c.71-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/9',
                             'PUBMED': [19339744],
                             'REFSEQ_MATCH': 'rseq_mrna_nonmatch&rseq_cds_mismatch&rseq_ens_match_cds',
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'NP_001244895.1',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'NM_001257966.1',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': '23418',
                             'HGVSc': 'NM_001257966.1:c.71-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/9',
                             'PUBMED': [19339744],
                             'REFSEQ_MATCH': 'rseq_mrna_nonmatch&rseq_cds_mismatch&rseq_ens_match_cds',
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'retained_intron',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant',
                                             'non_coding_transcript_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'ENST00000475659',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': 'ENSG00000134376',
                             'HGNC_ID': '2343',
                             'HGVSc': 'ENST00000475659.1:n.208-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/4',
                             'PUBMED': [19339744],
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1',
                             'SYMBOL_SOURCE': 'HGNC'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'misc_RNA',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant',
                                             'non_coding_transcript_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'NR_047564.1',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': '23418',
                             'HGVSc': 'NR_047564.1:n.280-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/11',
                             'PUBMED': [19339744],
                             'REFSEQ_MATCH': 'rseq_mrna_nonmatch&rseq_nctran_mismatch&rseq_ens_no_match',
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CANONICAL': 'YES',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'NP_957705.1',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'NM_201253.2',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': '23418',
                             'HGVSc': 'NM_201253.2:c.71-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/11',
                             'PUBMED': [19339744],
                             'REFSEQ_MATCH': 'rseq_mrna_nonmatch&rseq_cds_mismatch&rseq_ens_match_cds',
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CCDS': 'CCDS58052.1',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'ENSP00000438091',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'ENST00000538660',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': 'ENSG00000134376',
                             'HGNC_ID': '2343',
                             'HGVSc': 'ENST00000538660.1:c.71-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/9',
                             'PUBMED': [19339744],
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1',
                             'SYMBOL_SOURCE': 'HGNC'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CCDS': 'CCDS58053.1',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'ENSP00000438786',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'ENST00000535699',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': 'ENSG00000134376',
                             'HGNC_ID': '2343',
                             'HGVSc': 'ENST00000535699.1:c.-137-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '3/14',
                             'PUBMED': [19339744],
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1',
                             'SYMBOL_SOURCE': 'HGNC'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'misc_RNA',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant',
                                             'non_coding_transcript_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'NR_047563.1',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': '23418',
                             'HGVSc': 'NR_047563.1:n.280-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '1/12',
                             'PUBMED': [19339744],
                             'REFSEQ_MATCH': 'rseq_mrna_nonmatch&rseq_nctran_mismatch&rseq_ens_no_match',
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1'},
                            {'AA_MAF': {'T': 0.0861},
                             'AFR_MAF': {'T': 0.0378},
                             'ALLELE_NUM': 1,
                             'AMR_MAF': {'T': 0.5461},
                             'Allele': 'T',
                             'BIOTYPE': 'protein_coding',
                             'CLIN_SIG': 'benign',
                             'Consequence': ['intron_variant'],
                             'EAS_MAF': 'T:0.7599',
                             'EA_MAF': {'T': 0.3889},
                             'ENSP': 'NP_001244894.1',
                             'EUR_MAF': {'T': 0.4235},
                             'Existing_variation': ['rs12042179',
                                                    'rs369741574'],
                             'Feature': 'NM_001257965.1',
                             'Feature_type': 'Transcript',
                             'GMAF': {'T': 0.4032},
                             'Gene': '23418',
                             'HGVSc': 'NM_001257965.1:c.-137-12A>T',
                             'IMPACT': 'MODIFIER',
                             'INTRON': '3/14',
                             'PUBMED': [19339744],
                             'REFSEQ_MATCH': 'rseq_mrna_nonmatch&rseq_cds_mismatch&rseq_ens_match_cds',
                             'SAS_MAF': 'T:0.407',
                             'STRAND': 1,
                             'SYMBOL': 'CRB1'}],
                    'MLEAC': 1,
                    'MLEAF': 0.5}},
     'POS': 197297540,
     'QUAL': 1104.77,
     'REF': 'A',
     'SAMPLES': {'test_sample': {'AD': [53, 50],
                                 'DP': 103,
                                 'GQ': 99,
                                 'GT': '0/1',
                                 'PL': [1133, 0, 1667]}}}


transcripts = {
    'NM_017651.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'AHI1'
    },
    'NM_014336.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'AIPL1'
    },
    'NM_015120.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'ALMS1'
    },
    'NM_173551.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'ANKS6'
    },
    'NM_182896.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'ARL13B'
    },
    'NM_177976.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'ARL6'
    },
    'NM_018076.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'ARMC4'
    },
    'NM_013236.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'ATXN10'
    },
    'NM_015681.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'B9D1'
    },
    'NM_030578.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'B9D2'
    },
    'NM_001195306.1': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBIP1'
    },
    'NM_024649.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBS1'
    },
    'NM_024685.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBS10'
    },
    'NM_152618.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBS12'
    },
    'NM_031885.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBS2'
    },
    'NM_033028.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBS4'
    },
    'NM_152384.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBS5'
    },
    'NM_176824.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBS7'
    },
    'NM_198428.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'BBS9'
    },
    'NM_021254.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'C21orf59'
    },
    'NM_023073.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'C5orf42'
    },
    'NM_145200.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CABP4'
    },
    'NM_001080522.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CC2D2A'
    },
    'NM_213607.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CCDC103'
    },
    'NM_144577.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CCDC114'
    },
    'NM_145045.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CCDC151'
    },
    'NM_024296.4': {
        'inheritance': ['Modifiserende faktor'],
        'display_inheritance': 'Modifiserende aktor',
        'gene': 'CCDC28B'
    },
    'NM_181426.1': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CCDC39'
    },
    'NM_017950.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CCDC40'
    },
    'NM_016122.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CCDC41'
    },
    'NM_033124.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CCDC65'
    },
    'NM_021147.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CCNO'
    },
    'NM_153223.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CEP120'
    },
    'NM_014956.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CEP164'
    },
    'NM_025114.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CEP290'
    },
    'NM_018718.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CEP41'
    },
    'NM_201253.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CRB1'
    },
    'NM_000554.4': {
        'inheritance': ['AR / AD'],
        'display_inheritance': 'AD,AR',
        'gene': 'CRX'
    },
    'NM_024790.6': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'CSPP1'
    },
    'NM_016356.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DCDC2'
    },
    'NM_178452.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DNAAF1'
    },
    'NM_018139.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DNAAF2'
    },
    'NM_001256714.1': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DNAAF3'
    },
    'NM_001277115.1': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DNAH11'
    },
    'NM_001369.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DNAH5'
    },
    'NM_012144.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DNAI1'
    },
    'NM_023036.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DNAI2'
    },
    'NM_031427.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DNAL1'
    },
    'NM_145038.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DRC1'
    },
    'NM_001080463.1': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DYNC2H1'
    },
    'NM_130810.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'DYX1C1'
    },
    'NM_153717.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'EVC'
    },
    'NM_147127.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'EVC2'
    },
    'NM_032575.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'GLIS2'
    },
    'NM_000180.3': {
        'inheritance': ['AR / AD'],
        'display_inheritance': 'AD,AR',
        'gene': 'GUCY2D'
    },
    'NM_017802.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'HEATR2'
    },
    'NM_052985.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'IFT122'
    },
    'NM_014714.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'IFT140'
    },
    'NM_015662.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'IFT172'
    },
    'NM_006860.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'IFT27'
    },
    'NM_052873.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'IFT43'
    },
    'NM_020800.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'IFT80'
    },
    'NM_000883.3': {
        'inheritance': ['AR / AD'],
        'display_inheritance': 'AD,AR',
        'gene': 'IMPDH1'
    },
    'NM_019892.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'INPP5E'
    },
    'NM_014425.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'INVS'
    },
    'NM_001023570.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'IQCB1'
    },
    'NM_002242.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'KCNJ13'
    },
    'NM_198525.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'KIF7'
    },
    'NM_181714.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'LCA5'
    },
    'NM_004744.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'LRAT'
    },
    'NM_012472.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'LRRC6'
    },
    'NM_020347.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'LZTFL1'
    },
    'NM_018848.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'MKKS'
    },
    'NM_017777.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'MKS1'
    },
    'NM_012224.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'NEK1'
    },
    'NM_178170.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'NEK8'
    },
    'NM_022787.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'NMNAT1'
    },
    'NM_000272.3': {
        'inheritance': ['AD / AR'],
        'display_inheritance': 'AD,AR',
        'gene': 'NPHP1'
    },
    'NM_153240.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'NPHP3'
    },
    'NM_015102.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'NPHP4'
    },
    'NM_003611.2': {
        'inheritance': ['XR'],
        'display_inheritance': 'XR',
        'gene': 'OFD1'
    },
    'NM_002601.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'PDE6D'
    },
    'NM_000322.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'PRPH2'
    },
    'NM_183059.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'RD3'
    },
    'NM_152443.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'RDH12'
    },
    'NM_000329.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'RPE65'
    },
    'NM_000328.2': {
        'inheritance': ['XR'],
        'display_inheritance': 'XR',
        'gene': 'RPGR'
    },
    'NM_020366.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'RPGRIP1'
    },
    'NM_015272.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'RPGRIP1L'
    },
    'NM_080860.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'RSPH1'
    },
    'NM_001010892.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'RSPH4A'
    },
    'NM_152732.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'RSPH9'
    },
    'NM_006642.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'SDCCAG8'
    },
    'NM_172218.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'SPAG1'
    },
    'NM_018418.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'SPATA7'
    },
    'NM_001082538.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TCTN1'
    },
    'NM_024809.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TCTN2'
    },
    'NM_015631.5': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TCTN3'
    },
    'NM_016464.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TMEM138'
    },
    'NM_001173990.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TMEM216'
    },
    'NM_001077416.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TMEM231'
    },
    'NM_001044385.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TMEM237'
    },
    'NM_153704.5': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TMEM67'
    },
    'NM_012210.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TRIM32'
    },
    'NM_024753.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TTC21B'
    },
    'NM_198309.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TTC8'
    },
    'NM_003322.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'TULP1'
    },
    'NM_015910.5': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'WDPCP'
    },
    'NM_025132.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'WDR19'
    },
    'NM_052844.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'WDR34'
    },
    'NM_001006657.1': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'WDR35'
    },
    'NM_018051.4': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'WDR60'
    },
    'NM_022098.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'XPNPEP3'
    },
    'NM_015896.2': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'ZMYND10'
    },
    'NM_015069.3': {
        'inheritance': ['AR'],
        'display_inheritance': 'AR',
        'gene': 'ZNF423'
    }
}
