import csv
from report.util.genepanelreader import GenepanelReader


class CoverageData(object):

    def __init__(self, transcripts_path, phenotypes_path=None):
        self.transcripts_path = transcripts_path
        self.phenotypes_path = phenotypes_path
        self.transcripts = self._loadTrancripts()

    @staticmethod
    def rewrite_inheritance(inh_codes):
        """
        Rewrites the inheritance codes per request from lab.
        XD or XR -> X-bundet
        """
        rewritten_codes = list()
        for code in inh_codes:
            for inh_code in ['XR/XD', 'XD/XR', 'XD', 'XR']:
                code = code.replace(inh_code, 'X-bundet')
            if code not in rewritten_codes:
                rewritten_codes.append(code)
        return rewritten_codes

    def _loadTrancripts(self):
        return GenepanelReader().get_transcripts(self.transcripts_path, self.phenotypes_path)

    def loadCoverage(self, path):
        data = list()
        with open(path) as fd:
            reader = csv.reader(fd, delimiter='\t')
            for idx, r in enumerate(reader):
                if idx == 0:
                    continue
                transcript = r[2]
                t = self.transcripts.get(transcript, {'gene': '({})'.format(transcript), 'transcript': transcript, 'chromosome': '?'})
                t.update({
                    'percent': r[3],
                    'bp': r[5],
                })
                data.append(t)

        return sorted(data, key=lambda x: (x['gene'], x['transcript']))

    def loadLowCoverage(self, path, hgvsg=False):
        data = list()
        with open(path) as fd:
            reader = csv.reader(fd, delimiter='\t')
            for r in reader:
                f = {
                    'chromosome': r[0],
                    'start': r[1],
                    'end': r[2],
                    'gene': r[7],
                    'transcript': r[8],
                    'exon': r[9],
                    'reads': r[4],
                    # HGVSg example: chr11:g.111959693G>T
                    'start_hgvsg': 'chr{0}:g.{1}N>N'.format(r[0], r[1]),
                    'end_hgvsg': 'chr{0}:g.{1}N>N'.format(r[0], r[2]),
                }
                data.append(f)
        return sorted(data, key=lambda x: (x['gene'], x['transcript'], x['chromosome'], x['start']))