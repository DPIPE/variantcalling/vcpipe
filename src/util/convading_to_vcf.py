import sys
import argparse

"""
CoNVaDING output file to VCF converter.

Expected input format:

CHR	START	STOP	GENE	NUMBER_OF_TARGETS	NUMBER_OF_TARGETS_PASS_SHAPIRO-WILK_TEST	ABBERATION
1	45796187	45796229	MUTYH	1	0	DUP
1	45798434	45798506	MUTYH	1	0	DUP
2	47630330	47630541	MSH2	1	0	DEL
...

Output:

#CHROM  POS     ID   REF  ALT    QUAL  FILTER  INFO                    FORMAT
1       45796188  sv1  .    <DUP>  .     .       SVTYPE=DUP;END=45796229   GT:CN   0/1:3
...

"""

HEADER = """##fileformat=VCFv4.1
##source=CoNVaDING
##ALT=<ID=DEL,Description="Deletion">
##ALT=<ID=DUP,Description="Duplication">
##INFO=<ID=END,Number=1,Type=Integer,Description="End position of the structural variant">
##INFO=<ID=IMPRECISE,Number=0,Type=Flag,Description="Imprecise structural variation">
##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Type of the SV.">
##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Difference in length between REF and ALT allele">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=CN,Number=1,Type=Integer,Description="Copy number genotype for imprecise events">
"""

VCF_HEADERS = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT']

CN_MAP = {
    'HOM_DEL': 0,
    'DEL': 1,
    'DUP': 3,
    'HOM_DUP': 4,
}

class VcfWriter(object):
    def __init__(self, path, sample_name):
        self.path = path
        self.written_count = 0
        self.sample_name = sample_name

    def _create_header(self):
        header = HEADER
        header += '#' + '\t'.join(VCF_HEADERS) + '\t' + self.sample_name + '\n'
        return header

    def write_data(self, longlist_data, shortlist_data=None):
        """
        If shortlist_data is provided, CNVs only present in longlist will get LONGLIST in FILTER column.
        """

        longlist = []
        shortlist = []
        if shortlist_data is None:
            shortlist_data = []
        for list_type, items in [(longlist, longlist_data), (shortlist, shortlist_data)]:
            for l in items:
                chrom = l['CHR']
                # Convading (bed) result is 0-index, VCF is 1-indexed.
                # Since we move one base to left due to adding REF, we don't add 1
                start = int(l['START'])
                # Bed END is 0-indexed, non-inclusive. Since VCF is END 1-indexed, inclusive, we can use same number
                end = int(l['STOP'])
                cnv_type = l['ABBERATION']
                assert chrom in [str(i) for i in list(range(1, 23)) + ['X', 'Y', 'MT']]
                assert start
                assert end
                assert cnv_type in ['DEL', 'DUP', 'HOM_DEL', 'HOM_DUP']
                list_type.append((chrom, start, end, cnv_type))

        if shortlist_data:
            assert len(shortlist_data) <= len(longlist_data)

        with open(self.path, 'w') as f:
            f.write(self._create_header())
            for chrom, start, end, cnv_type in longlist:
                cn_value = CN_MAP[cnv_type]

                homozygous = False
                if cnv_type == 'HOM_DUP':
                    cnv_type = 'DUP'
                    homozygous = True
                elif cnv_type == 'HOM_DEL':
                    cnv_type = 'DEL'
                    homozygous = True

                if cnv_type == 'DEL':
                    sv_len = start - end
                elif cnv_type == 'DUP':
                    sv_len = end - start

                gt_value = '1/1' if homozygous else '0/1'
                filter_status = 'LONGLIST' if (chrom, start, end, cnv_type) not in shortlist else '.'

                vcf_data = [
                    chrom,
                    str(start),
                    'CONVERTED_{}'.format(str(self.written_count + 1)),
                    'N',
                    '<{}>'.format(cnv_type),
                    '.',
                    filter_status,
                    'IMPRECISE;SVTYPE={cnv_type};END={end};SVLEN={sv_len}'.format(cnv_type=cnv_type, end=end, sv_len=sv_len),
                    'GT:CN',
                    '{}:{}'.format(gt_value, str(cn_value))
                ]
                f.write(
                    '\t'.join(vcf_data) + '\n'
                )
                self.written_count += 1


def read_convading(convading_result):
    result = []
    with open(convading_result) as f:
        header = None
        for idx, line in enumerate(f):
            if idx == 0:
                header = [t.strip() for t in line.split('\t')]
                continue

            assert header
            line_data = {k: v.strip() for k, v in zip(header, line.split('\t'))}
            result.append(line_data)
    return result


if __name__ == "__main__":


    parser = argparse.ArgumentParser(description='Converts CoNVaDING result files to VCF format.')
    parser.add_argument('--longlist', required=True, help='CoNVaDING .longlist.txt output file', dest='convading_longlist_result')
    parser.add_argument('--shortlistlist', required=False, help='CoNVaDING .shortlist.txt output file', dest='convading_shortlist_result')
    parser.add_argument('-o', required=True, help='Output vcf file', dest='vcf')
    parser.add_argument('--sample-name', required=True, help='Sample name to put in VCF file', dest='sample_name')


    args = parser.parse_args()

    w = VcfWriter(args.vcf, args.sample_name)
    longlist_data = read_convading(args.convading_longlist_result)
    shortlist_data = None
    if args.convading_shortlist_result:
        shortlist_data = read_convading(args.convading_shortlist_result)
    w.write_data(longlist_data, shortlist_data=shortlist_data)

    print("Converting CoNVaDING result to {}".format(args.vcf))
    print("Wrote {} lines".format(w.written_count))
