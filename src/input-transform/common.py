from pathlib import Path
import json

from settings import settings


def sample_dir(sample_id: str) -> Path:
    return Path(settings["base_sample_path"]) / sample_id


def get_sample_config(sample_id):
    with open(sample_file(sample_id), "rt") as f:
        return json.load(f)


def sample_file(sample_id: str) -> Path:
    return sample_dir(sample_id) / (sample_id + ".sample")
