import argparse
from pathlib import Path
import json

from common import sample_dir, get_sample_config


def sample_file(sample_id: str) -> Path:
    requested_path = sample_dir(sample_id) / (sample_id + ".sample")
    if requested_path.exists():
        return requested_path
    else:
        raise RuntimeError(f".sample path {requested_path} does not exist")


def get_reads_files(sample_id: str):
    reads_files = []
    for i, read_file in enumerate(get_sample_config(sample_id)["reads"]):
        read_path: Path = sample_dir(sample_id) / read_file["path"]

        if read_path.exists():
            # If it is fastq.gz files
            # Check size
            assert (
                read_path.stat().st_size == read_file["size"]
            ), f"Size of file {read_path} doesn't match. File is {read_path.stat().st_size}, but should be {read_file['size']}"

            reads_files.append(read_path)
        else:
            # if it is genozip file, expect file name: file.fastq.genozip
            # if it is ora file, expect file name: file.fastq.ora
            genozip_path = ''
            ora_path = ''
            if str(read_path).endswith(".fastq.gz"):
                genozip_path = str(read_path)[:-len("fastq.gz")] + "fastq.genozip"
                ora_path = str(read_path)[:-len("fastq.gz")] + "fastq.ora"

                if Path(genozip_path).exists():
                    reads_files.append(Path(genozip_path))
                elif Path(ora_path).exists():
                    reads_files.append(Path(ora_path))
                else:
                    raise RuntimeError(f"No fastq.gz, fastq.genozip or fastq.gz.ora files are available for {sample_id}")
            else:
                raise RuntimeError(f"The file name in the .sample file is not ending with fastq.gz for {sample_id}")
    assert (
        len(reads_files) == 2
    ), f"Expected two reads files, but found {len(read_files)}"
    return reads_files


def get_taqman_file(sample_id: str):
    requested_path = sample_dir(sample_id) / (sample_id + ".taqman")
    if requested_path.exists():
        return requested_path
    else:
        raise RuntimeError(f"Taqman path {requested_path} does not exist")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--analysis", required=True, type=Path)
    args = parser.parse_args()

    with open(args.analysis, "rt") as f:
        analysis = json.load(f)

    sample_id = analysis["samples"][0]

    assert (
        sample_dir(sample_id) / "READY"
    ).exists(), f"READY-file missing from {sample_dir(sample_id)}"

    args = []
    read_files = get_reads_files(sample_id)
    args.append(f"--reads1 {read_files[0]}")
    args.append(f"--reads2 {read_files[1]}")

    if analysis["params"]["taqman"] == "true":
        args.append(f"--taqman {get_taqman_file(sample_id)}")

    sample_config = get_sample_config(sample_id)
    args.append(f"--sample_id {sample_id}")
    args.append(f"--capture_kit {sample_config['capturekit']}")
    args.append(f"--specialized_pipeline {analysis['specialized_pipeline']}")
    args.append(f"--analysis_name {analysis['name']}")
    args.append(f"--priority {analysis['priority']}")

    if 'downsampling_depth' in analysis['params'].keys():
        args.append(f"--downsampling_depth {analysis['params']['downsampling_depth']}")

    print(" ".join(args))
