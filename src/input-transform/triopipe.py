import argparse
from pathlib import Path
import json

from settings import settings
from common import get_sample_config


def preprocessed_dir(base_analysis: str) -> Path:
    requested_path = Path(settings["base_single_preprocessed_path"]) / base_analysis
    if requested_path.exists():
        return requested_path
    else:
        raise RuntimeError(
            f"analyses-results/singles/{requested_path} path does not exist"
        )


def get_gvcf_file(base_analysis: str) -> Path:
    requested_path = (
        preprocessed_dir(base_analysis)
        / f"data/variantcalling/{base_analysis}.g.vcf.gz"
    )

    if requested_path.exists():
        return requested_path
    else:
        raise RuntimeError(f"Variant calling gvcf {requested_path} path does not exist")


def get_qc_result(base_analysis: str) -> Path:
    requested_path = (
        preprocessed_dir(base_analysis) / f"data/qc/{base_analysis}.qc_result.json"
    )

    if requested_path.exists():
        return requested_path
    else:
        raise RuntimeError(f"Qc_result file {requested_path} path does not exist")


def get_mtdna_result(base_analysis: str) -> Path:
    if base_analysis.endswith("-DR"):
        base_analysis_nodr = base_analysis.replace("-DR", "")
    else:
        base_analysis_nodr = base_analysis

    requested_path = (
        preprocessed_dir(base_analysis)
        / f"data/mtdna/{base_analysis}.chrMT.final.vcf.gz"
    )
    if requested_path.exists():
        return requested_path
    else:
        requested_path = (
            preprocessed_dir(base_analysis)
            / f"data/mtdna/{base_analysis_nodr}.chrMT.final.vcf.gz"
        )
        if requested_path.exists():
            return requested_path
        else:
            raise RuntimeError(
                f"Mitochondrial variant file {requested_path} path does not exist"
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--analysis", required=True, type=Path)
    args = parser.parse_args()

    with open(args.analysis, "rt") as f:
        analysis = json.load(f)

    samples = {
        k: analysis["params"]["pedigree"][k]["sample"]
        for k in analysis["params"]["pedigree"]
    }

    proband_sample_config = get_sample_config(samples["proband"])
    args = [
        f"--analysis_name {analysis['name']}",
        f"--capture_kit {proband_sample_config['capturekit']}",
        f"--gender_proband {analysis['params']['pedigree']['proband']['gender']}",
        f"--specialized_pipeline {analysis['specialized_pipeline']}",
    ]
    for family_member, sample in samples.items():
        if analysis["specialized_pipeline"] == "Dragen":
            base_analysis = sample + "-DR"
        else:
            base_analysis = sample
        args.append(f"--sample_id_{family_member} {sample}")
        args.append(f"--gvcf_{family_member} {get_gvcf_file(base_analysis)}")
        args.append(f"--qc_json_{family_member} {get_qc_result(base_analysis)}")

        if proband_sample_config["capturekit"] == "wgs" and (
            family_member == "proband" or family_member == "mother"
        ):
            args.append(f"--mt_vcf_{family_member} {get_mtdna_result(base_analysis)}")

    print(" ".join(args))
