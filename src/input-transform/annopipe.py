import argparse
from pathlib import Path
import json

from settings import settings
from common import get_sample_config
from typing import Optional


def get_sample_info(sample_id: str):
    return get_sample_config(sample_id)["capturekit"], get_sample_config(sample_id)[
        "sequencer_id"
    ]


def preprocessed_dir(analysis_name: str, analysis_type: str) -> Path:
    requested_path = Path(settings[f"base_{analysis_type}_preprocessed_path"]) / analysis_name

    if requested_path.exists():
        return requested_path

    raise RuntimeError(f"{requested_path} path does not exist")


def assemble_command_line_arguments(
    analysis_name: str,
    analysis_type: str,
    file_regex: list,
    opts: list,
) -> list:
    """
    This function takes an analysis name, file regular expressions and option names as input and
    returns a list of command line arguments.

    Parameters:
    - analysis_name (str):  Name of the analysis directory where the files are.
    - analysis_type (str):  Type of analysis, either "trio" or "single".
    - file_regex (list):    List of regular expressions matching files.
    - opts (list):          List of command line options, should be as long as file_regex.

    Returns:
    List of command line arguments defined in opts. Any `not_required_opts` with no files
    matching the corresponding file_regex will not be included.
    """

    not_required_opts = [
        "--bigwig",
        "--cnv_result",
        "--mtdna_vcf",
        "--mtdna_haplo",
        "--mtdna_haplo_father",
        "--mtdna_haplo_mother",
        "--qc_report",
        "--qc_warning"
    ]

    return_args = []
    for opt, regex in zip(opts, file_regex):
        result_dir = preprocessed_dir(analysis_name, analysis_type)

        matching_paths = list(result_dir.glob(regex))

        # multiple regex for --bam option only when bam file could not be found
        if opt in ("--bam", "--bam_mother", "--bam_father") and not matching_paths:
            matching_paths = list(result_dir.glob(regex.replace("bam", "cram")))

        if len(matching_paths) == 1 and matching_paths[0].exists():
            return_args.append(f"{opt} {str(matching_paths[0])}")

        elif opt not in not_required_opts:
            if not matching_paths:
                raise RuntimeError(f"No files match <{regex}> under '{analysis_name}'")
            elif len(matching_paths) > 1:
                raise RuntimeError(f"Multiple files match <{regex}> under '{analysis_name}'")
            else:
                raise RuntimeError(f"File '{matching_paths[0]}' not found")

    return return_args


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--analysis", required=True, type=Path)
    args = parser.parse_args()

    with open(args.analysis, "rt") as f:
        analysis = json.load(f)

    args = []

    # Common to both single and trio
    args.append(f"--analysis_name {analysis['name']}")
    args.append(f"--specialized_pipeline {analysis['specialized_pipeline']}")
    if "date_analysis_requested" in analysis.keys():
        args.append(f"--date_analysis_requested {analysis['date_analysis_requested']}")
    args.append(f"--priority {analysis['priority']}")
    args.append(f"--gene_panel {analysis['params']['genepanel']}")

    # Specified CNV files for different capture kits, suppose all samples with the same capture kit
    # for trio analysis
    (capturekit, sequencer_id) = get_sample_info(analysis["samples"][0])
    if "CuCa" in capturekit:
        cnv_results = [
            "data/cnv/*cnv.vcf",
            "data/cnv/*best.score.log",
            "data/cnv/*totallist.txt",
            "data/cnv/*longlist.txt",
            "data/cnv/*shortlist.txt",
            "data/cnv/*coverage.txt",
        ]
        cnv_options = [
            "--cnv_result",
            "--convading_log",
            "--convading_longlist",
            "--convading_shortlist",
            "--convading_totallist",
            "--convading_coverage",
        ]
    elif capturekit == "wgs":
        cnv_results = ["data/cnv/merged*.vcf"]
        cnv_options = ["--cnv_result"]
    else:
        cnv_results = ["data/cnv/*cnv.bed"]
        cnv_options = ["--cnv_result"]

    # Files for trios/singles
    if len(analysis["samples"]) > 1:  # Trio
        main_sample = analysis["params"]["pedigree"]["proband"]["sample"]

        m_basepipe_analysis_name = (
            main_sample + "-DR" if "-DR-" in analysis["name"] else main_sample
        )

        args.append("--type trio")

        args.append(
            f"--gender_proband {analysis['params']['pedigree']['proband']['gender']}"
        )

        trio_analysis_name = analysis["name"].replace(
            "".join(["-", analysis["params"]["genepanel"].replace("_", "-")]), ""
        )

        args.extend(
            assemble_command_line_arguments(
                trio_analysis_name,
                "trio",
                ["data/variantcalling/*final.vcf"],
                ["--final_vcf"],
            )
        )

        args.extend(
            assemble_command_line_arguments(
                trio_analysis_name,
                "trio",
                ["data/mtdna/*filter.chrMT.vcf*"],
                ["--mtdna_vcf"],
            )
        )

        args.extend(
            assemble_command_line_arguments(
                trio_analysis_name,
                "trio",
                ["data/qc/*qc_report.md"],
                ["--qc_report"],
            )
        )

        args.extend(
            assemble_command_line_arguments(
                trio_analysis_name,
                "trio",
                ["data/qc/*qc_result.json"],
                ["--qc_result"],
            )
        )

        args.extend(
            assemble_command_line_arguments(
                trio_analysis_name,
                "trio",
                ["data/qc/*qc_warnings.md"],
                ["--qc_warning"],
            )
        )

        for ped in ["father", "mother"]:
            (capturekit, sequencer_id) = get_sample_info(
                analysis["params"]["pedigree"][ped]["sample"]
            )
            args.append(f"--capture_kit_{ped} {capturekit}")
            args.append(f"--sequencer_id_{ped} {sequencer_id}")
            args.append(
                f"--sample_id_{ped} {analysis['params']['pedigree'][ped]['sample']}"
            )
            basepipe_analysis_name = analysis["params"]["pedigree"][ped]["sample"]
            if "-DR-" in analysis["name"]:
                basepipe_analysis_name = "-".join([basepipe_analysis_name, "DR"])
            args.extend(
                assemble_command_line_arguments(
                    basepipe_analysis_name,
                    "single",
                    ["data/mapping/*bam"],
                    [f"--bam_{ped}"],
                )
            )
            args.extend(
                assemble_command_line_arguments(
                    basepipe_analysis_name,
                    "single",
                    cnv_results,
                    [f"{opt}_{ped}" for opt in cnv_options],
                )
            )
            args.extend(
                assemble_command_line_arguments(
                    basepipe_analysis_name,
                    "single",
                    ["data/cnv/*bigWig"],
                    [f"--bigwig_{ped}"],
                )
            )
            args.extend(
                assemble_command_line_arguments(
                    basepipe_analysis_name,
                    "single",
                    ["data/mtdna/*haplogrep.out"],
                    [f"--mtdna_haplo_{ped}"],
                )
            )

    elif len(analysis["samples"]) == 1:  # Single
        main_sample = analysis["samples"][0]
        args.append("--type single")

        m_basepipe_analysis_name = (
            main_sample + "-DR" if "-DR-" in analysis["name"] else main_sample
        )

        args.extend(
            assemble_command_line_arguments(
                m_basepipe_analysis_name,
                "single",
                ["data/variantcalling/*final.vcf"],
                ["--final_vcf"],
            )
        )

        args.extend(
            assemble_command_line_arguments(
                m_basepipe_analysis_name,
                "single",
                ["data/mtdna/*chrMT.final.vcf.gz"],
                ["--mtdna_vcf"],
            )
        )

        args.extend(
            assemble_command_line_arguments(
                m_basepipe_analysis_name,
                "single",
                ["data/qc/*qc_result.json"],
                ["--qc_result"],
            )
        )

        args.extend(
            assemble_command_line_arguments(
                m_basepipe_analysis_name,
                "single",
                ["data/qc/*qc_report.md"],
                ["--qc_report"],
            )
        )

        args.extend(
            assemble_command_line_arguments(
                m_basepipe_analysis_name,
                "single",
                ["data/qc/*qc_warnings.md"],
                ["--qc_warning"],
            )
        )

    # Files for main sample (proband in trio or single sample)
    (capturekit, sequencer_id) = get_sample_info(main_sample)
    args.append(f"--capture_kit {capturekit}")
    args.append(f"--sample_id {main_sample}")
    args.append(f"--sequencer_id {sequencer_id}")
    args.extend(
        assemble_command_line_arguments(
            m_basepipe_analysis_name,
            "single",
            ["data/mapping/*bam"],
            ["--bam"],
        )
    )
    args.extend(
        assemble_command_line_arguments(
            m_basepipe_analysis_name,
            "single",
            cnv_results,
            cnv_options,
        )
    )
    args.extend(
        assemble_command_line_arguments(
            m_basepipe_analysis_name,
            "single",
            ["data/cnv/*bigWig"],
            ["--bigwig"],
        )
    )
    args.extend(
        assemble_command_line_arguments(
            m_basepipe_analysis_name,
            "single",
            ["data/mtdna/*haplogrep.out"],
            ["--mtdna_haplo"],
        )
    )

    print(" ".join(args))
