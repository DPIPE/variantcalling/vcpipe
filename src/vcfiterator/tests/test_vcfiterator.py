import os
from  vcfiterator import VcfIterator
from vcfiterator.processors import VEPInfoProcessor, SnpEffInfoProcessor, NativeInfoProcessor, CsvAlleleParser, BaseInfoProcessor


def test_vcfiterator():
    vi = VcfIterator(os.path.dirname(os.path.abspath(__file__)) + '/../../report/testdata.vcf')
    assert vi
    vi.addInfoProcessor(VEPInfoProcessor)
    for v in vi.iter():
        pass


def test_abc():
    meta = {'INFO': [{'foo': 5}]}
    v = VEPInfoProcessor(meta)
    s = SnpEffInfoProcessor(meta)
    n = NativeInfoProcessor(meta)
    c = CsvAlleleParser(meta)
    assert isinstance(v, BaseInfoProcessor)
    assert isinstance(s, BaseInfoProcessor)
    assert isinstance(n, BaseInfoProcessor)
    assert isinstance(c, BaseInfoProcessor)