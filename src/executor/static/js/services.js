

executor.factory('Sample', ['$resource',
    function($resource){

        return $resource('api/sample/:id', {}, {
            query: {method:'GET', isArray: false, headers: {'Accept': 'application/vnd.api+json'} }
        });
    }]);


executor.factory('Analysis', ['$resource',
    function($resource){
        return $resource('api/analysis/:id', {}, {
            query: {method: 'GET', isArray: false, headers: {'Accept': 'application/vnd.api+json'} },
            update: {method: 'PUT'},
            create: {method: 'POST'}
        });
    }]);


// includeLog is not used, yet
executor.factory('Analysis_with_logs', ['$resource',
    function($resource){
        return $resource('api/analysis/:id', {}, {
            query: {method: 'GET', isArray: false, headers: {'Accept': 'application/vnd.api+json'}, params: {includeLog: true, include: 'version'} }
        });
    }]);


executor.factory('Version', ['$resource',
    function($resource){
        return $resource('api/version/:id', {}, {
            query: {method: 'GET', headers: {'Accept': 'application/vnd.api+json'} }
        });
    }]);

executor.factory('Dashboard', ['$http',
    function($http){
        return {
            get: function() { return $http({method: 'GET', 'url': 'api/dashboard'}); }
        };
    }]);
