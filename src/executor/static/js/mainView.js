
executor.directive('mainView', function() {
    return {
        restrict: 'E',
        templateUrl: '/static/js/mainView.ngtmpl.html',
        link: function(scope, elem, attrs) {

        },
        controller: function($scope, $rootScope, $location) {
            $rootScope.view = {
                current: ''
            };
            var listenerCleanFn = $rootScope.$on('$locationChangeSuccess', function(e, current, pre) {
              $rootScope.view.current = $location.path();
            });

            $scope.$on('$destroy', function() {
              listenerCleanFn();
            });
        }
    };
});
