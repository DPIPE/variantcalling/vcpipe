

executor.filter('secondsToTime', function () {
    return function (secs) {

        var hours = Math.floor(secs / (60 * 60));

        var divisor_for_minutes = secs % (60 * 60);
        var minutes = Math.floor(divisor_for_minutes / 60);

        var divisor_for_seconds = divisor_for_minutes % 60;
        var seconds = Math.ceil(divisor_for_seconds);

        var str = '';
        if (hours > 0) {
            str += hours + 'h ';
        }
        if (minutes > 0) {
            str += minutes + 'm ';
        }
        if (seconds > 0) {
            str += seconds + 's ';
        }
        return str;
    };
  });

executor.filter('nonNaNDate', function($filter){
    var standardDateFilterFn = $filter('date');
    return function(dateToFormat, format){
        if (isNaN(dateToFormat)) {
            return '';
        }
        return standardDateFilterFn(dateToFormat, format);
    };
});

executor.filter('dateInMillis', function() {
    return function(dateString) {
        return Date.parse(dateString);
    };
});


executor.filter('jsonify', function() {
    return function(obj) {
        return JSON.stringify(obj, undefined, 4);
    };
});


executor.filter('truncate', function () {
    return function (text, length, end) {
        if (isNaN(length))
            length = 10;

        if (end === undefined)
            end = "...";

        if (text.length <= length || text.length - end.length <= length) {
            return text;
        }
        else {
            return String(text).substring(0, length-end.length) + end;
        }

    };
});
