
executor.directive('analysisInfo', function() {
    return {
        restrict: 'E',
        scope: {
            analysis: '='
        },
        templateUrl: '/static/js/widgets/analysisInfo/analysisInfo.ngtmpl.html',
        link: function(scope, elem, attrs) {

        },
        controller: function($scope, $interval, $modal, Analysis, Analysis_with_logs) {

            $scope.getProgressColor = function getProgressColor() {
                status_map = {
                    'COMPLETE': 'success',
                    'FAILED': 'danger',
                    'RUNNING': 'info',
                    'PENDING': 'warning'
                };

                if (angular.isDefined($scope.analysis)) {
                    console.info('status: ' +  $scope.analysis.status);
                    return status_map[$scope.analysis.status];
                }
                return '';
            };

            // Create extra layer to be able to reflect updates inside modal
            $scope.logScope = {
                analysis: null
            };
            
            $scope.$watch('analysis', function() {
                $scope.logScope.analysis = $scope.analysis;
            });
            
            $scope.viewLog = function viewLog() {
                var modalInstance = $modal.open({
                    templateUrl: '/static/js/widgets/analysisInfo/logModal.ngtmpl.html',
                    controller: function ($scope, $modalInstance, $http, analysis) {

                        $scope.updateLog = function updateLog() {
                            var filters = [{name: 'id', op: 'eq', val: analysis.analysis.id}];
                            var opts = {'filter[objects]': JSON.stringify(filters)};
                            Analysis_with_logs.query(opts).$promise.then(function(responseContent) {
                                $scope.analysis_log = responseContent.data[0].attributes.log;
                                $scope.analysis_version = responseContent.included[0];
                                $scope.analysis_properties = responseContent.data[0].attributes.properties;
                                $scope.analysis = responseContent.data[0].attributes;
                            });
                        };
                        var updatePromise = $interval($scope.updateLog, 5000);

                        // Remove autoupdate upon navigating away from page
                        $scope.$on('$destroy', function() {
                            $interval.cancel(updatePromise);
                        });
                        $scope.updateLog();
                        
                        $scope.analysis  = analysis

                        $scope.close = function () {
                            $modalInstance.close();
                        };
                        $scope.admin = {
                            resume: function() {
                                if (confirm("Are you sure you want to RESUME: " + analysis.analysis.attributes.name + "?") === true) {
                                    $http.get('/api/analysis/'+ analysis.analysis.id + '/action/resume?key=12YZzSGHAedfpeMkREDX');
                                    $modalInstance.close();
                                }
                            },
                            delete: function() {
                                if (confirm("Are you sure you want to DELETE: " + analysis.analysis.attributes.name + "?") === true) {
                                    $http.get('/api/analysis/'+ analysis.analysis.id + '/action/delete?key=12YZzSGHAedfpeMkREDX');
                                    $modalInstance.close();
                                }
                            },
                            abort: function() {
                                if (confirm("Are you sure you want to ABORT: " + analysis.analysis.attributes.name + "?") === true) {
                                    $http.get('/api/analysis/'+ analysis.analysis.id + '/action/abort?key=12YZzSGHAedfpeMkREDX');
                                }
                            },
                            deliver: function() {
                                if (confirm("Are you sure you want to DELIVER: " + analysis.analysis.attributes.name + "?") === true) {
                                    $http.get('/api/analysis/'+ analysis.analysis.id + '/action/deliver?key=12YZzSGHAedfpeMkREDX');
                                }
                            },
                            retrieve_analysis: function() {
                                if (confirm("Are you sure you want to RETRIEVE the ANALYSIS from archive: " + analysis.analysis.attributes.name + "?") === true) {
                                    $http.get('/api/analysis/'+ analysis.analysis.id + '/action/retrieve_analysis?key=12YZzSGHAedfpeMkREDX');
                                }
                            },
                            retrieve_sample: function() {
                                if (confirm("Are you sure you want to RETRIEVE the SAMPLE from archive: " + analysis.analysis.samples[0] + "?") === true) {
                                    $http.get('/api/analysis/'+ analysis.analysis.id + '/action/retrieve_sample?key=12YZzSGHAedfpeMkREDX');
                                }
                            }
                        };

                        $scope.translateQC = function translateQC(qc_data, qc_category, qc_key) {
                            switch(qc_key) {
                                case 'Q30':
                                    tr = qc_data['BaseQuality'][1]['q30_bases_pct'].toFixed(2);
                                    break;
                                case 'PCT_PF_READS_ALIGNED[2]':
                                    tr = qc_data['CollectAlignmentSummaryMetrics'][1]['PCT_PF_READS_ALIGNED'][2].toFixed(2);
                                    break;
                                case 'failedRegionCount':
                                    tr = qc_data['LowCoverageRegions'][1]['failed'].length.toString();
                                    break;
                                case 'PCT_10X':
                                    tr = qc_data[qc_category][1]['metrics']['PCT_10X'].toFixed(2);
                                    break;
                                case 'PCT_20X':
                                    tr = qc_data[qc_category][1]['metrics']['PCT_20X'].toFixed(2);
                                    break;
                                case 'MEDIAN_COVERAGE':
                                    tr = qc_data[qc_category][1]['metrics']['MEDIAN_COVERAGE'].toFixed(0);
                                    break;
                                case 'MEAN_COVERAGE':
                                    tr = qc_data[qc_category][1]['metrics']['MEAN_COVERAGE'].toFixed(0);
                                    break;
                                default:
                                    try {
                                        tr = qc_data[qc_category][1][qc_key].toFixed(2);
                                    }
                                    catch(err) {
                                        tr = 'N/A';
                                    }
                                    break;
                            };
                            return tr;
                        };
                    },
                    size: 'lg',
                    resolve: {
                        analysis: function () {
                            console.log($scope.logScope);
                            return $scope.logScope;
                        }
                    }
                });
            };

        }
    };
});
