
executor.directive('healthOverview', function() {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: '/static/js/widgets/healthOverview/healthOverview.ngtmpl.html',
        link: function(scope, elem, attrs) {

        },
        controller: function($scope, $interval, $modal, Dashboard) {

            function updateData() {
                var q = Dashboard.get();
                q.success(function(responseContent) {
                    $scope.data = responseContent;
                });
            }

            // Auto-update every 300 secs
            var updatePromise = $interval(updateData, 300000);

            // Remove autoupdate upon navigating away from page
            $scope.$on('$destroy', function() {
                $interval.cancel(updatePromise);
            });
            updateData();


        }
    };
});
