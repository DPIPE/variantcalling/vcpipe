 // sorting https://jsonapi.org/format/#fetching-sorting
 // &fields\[analysis\]=name
executor.directive('analysisList', function() {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: '/static/js/widgets/analysisList/analysisList.ngtmpl.html',
        link: function(scope, elem, attrs) {

        },
        controller: function($scope, $interval, $modal, Analysis, Version) {


            function getCurrentVersion() {
                var filters = [{
                            name: 'current',
                            op: 'eq',
                            val: true
                }];
                Version.query({'filter[objects]': JSON.stringify(filters)}).$promise.then(function(responseContent) {
                    if (responseContent.data.length) {
                        $scope.version.current = responseContent.data[0].attributes;
                    }
                });

            }

            getCurrentVersion();

            $scope.version = {
                current: null
            };

            $scope.analysis = {
                filter: {
                    text: '',
                    complete: false,
                    failed: false
                },
                numResults: 0,
                curPage: 0,
                analysisData: [],
                analysisConfig: {}
            };

            $scope.$watch('analysis.filter', function() {
                if ($scope.analysis.filter.text !== '' && $scope.analysis.filter.text.length < 2) {
                    return;
                }
                $scope.updateAnalysis();
            }, true);

            $scope.updateAnalysis = function updateAnalysis() {
                var filters = []

                if ($scope.analysis.filter.text !== '') {
                    filters.push({
                        name: 'name',
                        op: 'like',
                        val: '%' + $scope.analysis.filter.text + '%'
                    });
                }
                if ($scope.analysis.filter.status) {
                    filters.push({
                        name: 'status',
                        op: 'eq',
                        val: $scope.analysis.filter.status
                    });
                }
                if ($scope.analysis.filter.type) {
                    if (['annopipe', 'triopipe', 'basepipe','noop'].includes($scope.analysis.filter.type)) {
                        filters.push({
                                name: 'type',
                                op: 'eq',
                                val: $scope.analysis.filter.type
                            });
                    } else if ($scope.analysis.filter.type == 'other') {
                        ['annopipe', 'triopipe', 'basepipe', 'noop'].forEach(function(analysis_type) {
                            filters.push({
                                name: 'type',
                                op: 'neq',
                                val: analysis_type
                            });
                        });
                    }
                }
                var opts = {
                    'filter[objects]': JSON.stringify(filters),
                     sort: ['-inserttime'],
                    'fields[analysis]': 'id,name,status,type,inserttime,starttime,progress_pct,progress_step,duration'
                };
                if ($scope.analysis.curPage > 0) {
                    opts['page[number]'] = $scope.analysis.curPage;
                }

                Analysis.query(opts).$promise.then(function(responseContent) {
                    $scope.analysis.numResults = responseContent.meta.total;
                    $scope.analysis.analysisData = responseContent.data;
                    setupAnalysisConfig(responseContent.data);
                });
            };

            var updatePromise = $interval($scope.updateAnalysis, 5000);

            // Remove autoupdate upon navigating away from page
            $scope.$on('$destroy', function() {
                $interval.cancel(updatePromise);
            });
            $scope.updateAnalysis();


            function getProgress(analysis) {
                status_map = {
                    'COMPLETE': 'success',
                    'FAILED': 'danger',
                    'RUNNING': 'info',
                    'PENDING': 'warning'
                };

                if (angular.isDefined(analysis)) {
                    var bars = [];

                    bars.push({
                        value: 1,
                        type: status_map[analysis.status],
                        status: analysis.status
                    });
                    return bars;

                }
            }

            function setupAnalysisConfig(data) {
                angular.forEach($scope.analysis.analysisData, function(s) {
                    if (!(s.id in $scope.analysis.analysisConfig)) {
                        $scope.analysis.analysisConfig[s.id] = {
                            showTasks: false
                        };
                    }
                    $scope.analysis.analysisConfig[s.id].taskProgress = getProgress(s);
                });
            }

        }
    };
});
