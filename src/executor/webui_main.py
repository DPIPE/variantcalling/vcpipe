import logging
import datetime
import os
from util import killfile
from executor.restapi import RestApi
from settings import settings

logging.basicConfig(
    format="%(asctime)s [%(filename)s] %(levelname)s - %(message)s"
)
logging.getLogger().setLevel(logging.DEBUG)


logFormatter = logging.Formatter("%(asctime)s [%(filename)s] %(levelname)s - %(message)s")
log = logging.getLogger()

logpath = settings['webui']['logs']
if not logpath:
    raise RuntimeError('A valid path to put log files is required. See settings["webui"]["logs"]')
try:
    os.makedirs(logpath)
except OSError:
    pass


#logFile = os.path.join(logpath, 'webui.log')
#fileHandler = logging.handlers.TimedRotatingFileHandler(logFile, when='W0', backupCount=5)
#fileHandler.setFormatter(logFormatter)
#log.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
log.addHandler(consoleHandler)

log.setLevel(logging.DEBUG)
#log.info("Logging to {}".format(logFile))


if __name__ == '__main__':

    host_info = {}

    if 'webui' in settings:

        if 'killfile' not in settings['webui']:
            logging.warning("No killfile is specified in settings. This feature will be disabled!")
        else:
            kill_watcher = killfile.KillFileWatcher(settings['webui']['killfile'])
            kill_watcher.start()
            logging.info("Watching killfile \"{}\". Create this file to kill process.".format(settings['webui']['killfile']))

        host = settings['webui'].get('host')
        if host:
            host_info['host'] = host
        port = settings['webui'].get('port')
        if port:
            host_info['port'] = port

    logging.info("Starting REST API")
    restapi = RestApi(debug=False, **host_info)
    restapi.run()
