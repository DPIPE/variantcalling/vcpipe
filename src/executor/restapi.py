import logging
import os
import datetime
from flask import Flask, request
from flask.json import JSONEncoder
import flask_restless
import sqlalchemy as sa
from sqlalchemy.sql import label
from collections import defaultdict
from util import DB
from executor.model import Sample, Analysis as ModelAnalysis, Version
from executor.engine.analysis import Analysis
from flask.views import MethodView
#from flask.ext.restless import views as RestlessViews


db = DB(os.environ['DB_URL'])

session = db.sessionmaker()

LOCAL_PATH = os.path.dirname(os.path.realpath(__file__))

class AnalysisAdmin(object):

    # Not meant as actual security, just to prevent accidents
    KEY = '12YZzSGHAedfpeMkREDX'

    @staticmethod
    def abort_analysis(analysis_id):
        if request.args.get('key') != AnalysisAdmin.KEY:
            raise RuntimeError("You must provide a valid key to perform this operation.")
        analysis = session.query(ModelAnalysis).filter(ModelAnalysis.id == analysis_id).one()

        if analysis.status != 'RUNNING':
            raise RuntimeError("Cannot abort a analysis that is not running")

        analysis.status = 'FAILED'
        session.commit()
        return 'OK', 200

    @staticmethod
    def resume_analysis(analysis_id):
        if request.args.get('key') != AnalysisAdmin.KEY:
            raise RuntimeError("You must provide a valid key to perform this operation.")
        analysis = session.query(ModelAnalysis).filter(ModelAnalysis.id == analysis_id).one()

        # Resuming an analysis that have processes running is not supported
        if analysis.status == 'RUNNING':
            raise RuntimeError("Cannot resume a running analysis")

        analysis.resume()
        session.commit()
        return 'OK', 200

    @staticmethod
    def delete_analysis(analysis_id):
        if request.args.get('key') != AnalysisAdmin.KEY:
            raise RuntimeError("You must provide a valid key to perform this operation.")
        analysis = session.query(ModelAnalysis).filter(ModelAnalysis.id == analysis_id).one()

        if analysis.status == 'RUNNING':
            raise RuntimeError("Cannot delete a running analysis")

        # Remove samples if only connected to this analysis
        analysis_sharing_samples = session.query(ModelAnalysis).join(ModelAnalysis.samples).filter(
            Sample.id.in_([s.id for s in analysis.samples]),
            ModelAnalysis.id != analysis.id
        ).all()
        if not analysis_sharing_samples:
            for s in analysis.samples:
                session.delete(s)

        Analysis._delete_analysis_result_folder(analysis)

        session.delete(analysis)
        session.commit()
        return 'OK', 200

    @staticmethod
    def deliver_analysis(analysis_id):
        if request.args.get('key') != AnalysisAdmin.KEY:
            raise RuntimeError("You must provide a valid key to perform this operation.")
        analysis = session.query(ModelAnalysis).filter(ModelAnalysis.id == analysis_id).one()

        # Delivering an analysis that have processes running is not supported
        if analysis.status == 'RUNNING':
            raise RuntimeError("Cannot deliver a running analysis")

        Analysis._deliver(analysis)

        session.commit()
        return 'OK', 200

    @staticmethod
    def retrieve_analysis(analysis_id):
        if request.args.get('key') != AnalysisAdmin.KEY:
            raise RuntimeError("You must provide a valid key to perform this operation.")
        analysis = session.query(ModelAnalysis).filter(ModelAnalysis.id == analysis_id).one()
        Analysis._retrieve_analysis(analysis)
        session.commit()
        return 'OK', 200

    @staticmethod
    def retrieve_sample(analysis_id):
        if request.args.get('key') != AnalysisAdmin.KEY:
            raise RuntimeError("You must provide a valid key to perform this operation.")
        analysis = session.query(ModelAnalysis).filter(ModelAnalysis.id == analysis_id).one()
        Analysis._retrieve_sample(analysis)
        session.commit()
        return 'OK', 200

class Dashboard(MethodView):

    def get(self):

        # Get PENDING count
        pending_count = session.query(ModelAnalysis.status).filter(ModelAnalysis.status == 'PENDING').count()

        # Get RUNNING count
        running_count = session.query(ModelAnalysis.status).filter(ModelAnalysis.status == 'RUNNING').count()

        # Get processed last 30 days
        last_30_days_count = session.query(ModelAnalysis.starttime).filter(
            sa.or_(
                ModelAnalysis.status == 'COMPLETE',
                ModelAnalysis.status == 'FAILED'
            ),
            ModelAnalysis.starttime >= datetime.date.today() - datetime.timedelta(days=30)
        ).count()

        # Get total processed
        total_count = session.query(ModelAnalysis.starttime).filter(
            sa.or_(
                ModelAnalysis.status == 'COMPLETE',
                ModelAnalysis.status == 'FAILED'
            )
        ).count()

        return {
            'pending_count': pending_count,
            'running_count': running_count,
            'last_30_days_count': last_30_days_count,
            'total_count': total_count
        }


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date):
            return obj.isoformat()
        else:
            JSONEncoder.default(self, obj)


def get_single_analysis_postprocessor(result=None, **kw):
    """Accepts a single argument, `result`, which is the dictionary
    representation of the requested instance of the model.

    """
    pass


def get_many_analysis_postprocessor(result=None, filters=None, sort=None, **kw):
    """Accepts two arguments, `result`, which is the dictionary
    representation of the JSON response which will be returned to the
    client, and `search_params`, which is a dictionary containing the
    search parameters for the request (that produced the specified
    `result`).

    """
    # skip some fields for performance reason
    result.pop('log', None)
    result.pop('properties', None)
    pass

class RestApi(object):

    def __init__(self, host='127.0.0.1', port=1234, debug=False):
        self.app = Flask('Executor', static_folder=LOCAL_PATH+'/static', static_url_path='/static')
        self.app.json_encoder = CustomJSONEncoder
        self.debug = debug
        self.host = host
        self.port = port
        self.manager = flask_restless.APIManager(self.app, session=session)
        self.setup_resources()

    def root(self):
        return self.app.send_static_file('html/index.html')



    def setup_resources(self):

        # Custom REST endpoints
        self.app.add_url_rule('/api/dashboard/', view_func=Dashboard.as_view('dashboard'))
        self.app.add_url_rule('/api/analysis/<int:analysis_id>/action/abort', 'abort analysis', AnalysisAdmin.abort_analysis)
        self.app.add_url_rule('/api/analysis/<int:analysis_id>/action/resume', 'resume analysis', AnalysisAdmin.resume_analysis)
        self.app.add_url_rule('/api/analysis/<int:analysis_id>/action/delete', 'delete analysis', AnalysisAdmin.delete_analysis)
        self.app.add_url_rule('/api/analysis/<int:analysis_id>/action/deliver', 'deliver analysis', AnalysisAdmin.deliver_analysis)
        self.app.add_url_rule('/api/analysis/<int:analysis_id>/action/retrieve_analysis', 'retrieve analysis', AnalysisAdmin.retrieve_analysis)
        self.app.add_url_rule('/api/analysis/<int:analysis_id>/action/retrieve_sample', 'retrieve sample', AnalysisAdmin.retrieve_sample)

        self.manager.create_api(Sample, methods=['GET'], page_size=20)
        self.manager.create_api(ModelAnalysis, methods=['GET', 'POST'],
                                page_size=20,
                                postprocessors={
                                    # https://flask-restless-ng.readthedocs.io/en/latest/customizing.html#request-preprocessors-and-postprocessors
                                    'GET_RESOURCE': [get_single_analysis_postprocessor],
                                    'GET_COLLECTION': [get_many_analysis_postprocessor],
                                })
        # Note! This function must be called at most once for each model for which you wish to create a ReSTful API. Its behavior (for now) is undefined if called more than once.
        # https://flask-restless-ng.readthedocs.io/en/latest/api.html#flask_restless.APIManager.create_api_blueprint
        # self.manager.create_api(ModelAnalysis, methods=['GET', 'POST'], collection_name='analysis_with_logs')
        self.manager.create_api(Version, methods=['GET'])


        # root
        self.app.add_url_rule('/', view_func=self.root, methods=['GET'])

    def run(self):
        logging.info("Running on {}:{}".format(self.host, self.port))
        self.app.run(host=self.host, port=self.port, debug=self.debug)
