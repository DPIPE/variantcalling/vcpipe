import datetime
from sqlalchemy import Column, Sequence, Integer, DateTime, Boolean
from sqlalchemy.dialects.postgresql import JSON

from executor.model import Base


class Version(Base):
    __tablename__ = "version"

    id = Column(Integer, Sequence("id_version_seq"), primary_key=True)
    version = Column(JSON(), nullable=False)
    inserttime = Column(DateTime, nullable=False, default=datetime.datetime.now)
    current = Column(Boolean(), nullable=False, default=False)
    production = Column(Boolean(), nullable=False, default=False)
    is_checked = Column(Boolean(), nullable=False, default=False)
    properties = Column(JSON())

    def __repr__(self):
        return "<Version('%s', '%s', '%s')>" % (self.id,
                                                self.version,
                                                self.current)
