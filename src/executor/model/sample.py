import datetime
from sqlalchemy import Column, Sequence, Integer, String, DateTime
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.schema import Index

from executor.model import Base


class Sample(Base):
    __tablename__ = "sample"

    id = Column(Integer, Sequence("id_sample_seq"), primary_key=True)
    name = Column(String(), nullable=False)
    path = Column(String(), nullable=False)
    inserttime = Column(DateTime, nullable=False, default=datetime.datetime.now)
    config = Column(String())
    description = Column(String())
    original = Column(JSON())
    properties = Column(JSON())

    __table_args__ = (Index("ix_samplename", "name", unique=True), )

    def __repr__(self):
        return "<Sample('%s', '%s', '%s', '%s')>" % (self.id,
                                                     self.inserttime,
                                                     self.name,
                                                     self.path)
