import os
from sqlalchemy.ext.declarative import declarative_base

from util import DB


db = DB(os.environ['DB_URL'], pool_size=5, pool_max_overflow=60, pool_timeout=120)


class CustomBase(object):

    def to_dict(self, include=None, exclude=None):
        # an SQLAlchemy class
        data = dict()
        for c in (k for k in list(self.__dict__.keys()) if not k.startswith('_')):
            if include:
                if c not in include:
                    continue
            if exclude:
                if c in exclude:
                    continue
            data[c] = self.__getattribute__(c)
        return data


Base = declarative_base(cls=CustomBase)  # NB! Use this Base instance always.


from .sample import Sample
from .analysis import Analysis
from .version import Version

# Base.metadata.create_all(db.engine)
