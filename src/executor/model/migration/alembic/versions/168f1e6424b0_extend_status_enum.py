"""extend status enum

Revision ID: 168f1e6424b0
Revises:
Create Date: 2017-06-02 12:08:27.887440

"""
from alembic import op
import sqlalchemy as sa

# Solution inspired from https://stackoverflow.com/questions/14845203/altering-an-enum-field-using-alembic

# revision identifiers, used by Alembic.
revision = '168f1e6424b0'
down_revision = None
branch_labels = None
depends_on = None

old_options = ['INVALID',
               'DEPENDENCIES',
               'PENDING',
               'RUNNING',
               'FAILED',
               'COMPLETE']
new_options = old_options + ['QCFAIL',
                             'QCSTOP']

old_type = sa.Enum(*old_options, name='analysis_status')
new_type = sa.Enum(*new_options, name='analysis_status')
tmp_type = sa.Enum(*new_options, name='_analysis_status')

analysis = sa.sql.table('analysis',
    sa.Column('status', new_type,
    default='PENDING',
    nullable=False))

def upgrade():
    op.execute('ALTER TYPE analysis_status RENAME TO tmp_analysis_status')
    new_type.create(op.get_bind())
    op.execute('ALTER TABLE analysis ALTER COLUMN status ' +
               'TYPE analysis_status USING status::text::analysis_status')
    op.execute('DROP TYPE tmp_analysis_status')

def downgrade():
    op.execute(analysis.update().where(analysis.c.status=='QCFAIL').values(status='FAILED'))
    op.execute(analysis.update().where(analysis.c.status=='QCSTOP').values(status='FAILED'))
    op.execute('ALTER TYPE analysis_status RENAME TO tmp_analysis_status')
    new_type.create(op.get_bind())
    op.execute('ALTER TABLE analysis ALTER COLUMN status ' +
               'TYPE analysis_status USING status::text::analysis_status')
    op.execute('DROP TYPE tmp_analysis_status')
