import datetime
from sqlalchemy import Column, Sequence, Integer, String, Enum, \
                       DateTime, ForeignKey, Table, Text
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import relationship
from sqlalchemy.ext.mutable import MutableDict

from base import Base



# association table
sample_analysis = Table(
    'sample_analysis',
    Base.metadata,
    Column('sample_id', Integer, ForeignKey('sample.id')),
    Column('analysis_id', Integer, ForeignKey('analysis.id'))
)


class Analysis(Base):
    __tablename__ = "analysis"

    id = Column(Integer, Sequence("id_analysis_seq"), primary_key=True)
    name = Column(String())
    type = Column(String())
    path = Column(String(), nullable=False)
    result_path = Column(String())
    description = Column(String())
    progress_pct = Column(Integer())  # Progress in pct
    progress_step = Column(String())  # Display name for current step in analysis
    status = Column(Enum(*['INVALID',
                           'DEPENDENCIES',
                           'PENDING',
                           'RUNNING',
                           'FAILED',
                           'COMPLETE'],
                         name='analysis_status'),
                    default='PENDING',
                    nullable=False)
    command = Column(String())
    exitcode = Column(Integer())
    inserttime = Column(DateTime(), default=datetime.datetime.now)
    starttime = Column(DateTime())
    duration = Column(Integer())
    original = Column(MutableDict.as_mutable(JSON()))
    properties = Column(MutableDict.as_mutable(JSON()))
    log = Column(String(), default='')
    samples = relationship('Sample', secondary=sample_analysis, backref='analysis')
    version_id = Column(Integer, ForeignKey('version.id'), nullable=False)
    version = relationship('Version')

    def __repr__(self):
        return "<Analysis('%d', '%s','%s', '%s')>" % (self.id,
                                                      self.name,
                                                      self.command,
                                                      self.status)

    def reset(self):
        self.exitcode = None
        self.result_path = None
        self.starttime = None
        self.duration = None
        self.properties = dict()
        self.status = 'PENDING'
        self.progress_pct = None
        self.progress_step = None
        self.log = ''
