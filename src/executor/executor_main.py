import os
import logging
from logging import handlers
import signal
import datetime
from settings import settings
from util import LocalVersion, DB
from executor.engine.poller import Poller
from executor.engine.task import SampleImporterTask, AnalysisImporterTask, AnalysisExecutorTask
from executor.model import Version
from util import killfile

logFormatter = logging.Formatter("%(asctime)s [%(filename)s] %(levelname)s - %(message)s")
log = logging.getLogger()

logpath = settings['executor']['logs']
if not logpath:
    raise RuntimeError('A valid path to put log files is required. See settings["executor"]["logs"]')
try:
    os.makedirs(logpath)
except OSError:
    pass

logFile = os.path.join(logpath, 'executor.log')
fileHandler = logging.handlers.TimedRotatingFileHandler(logFile, when='W0', backupCount=5)
fileHandler.setFormatter(logFormatter)
log.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
log.addHandler(consoleHandler)

log.setLevel(logging.DEBUG)
log.info("Logging to {}".format(logFile))


"""
Executor engine
"""


def handle_pdb(sig, frame):
    import pdb
    pdb.Pdb().set_trace(frame)


def update_version(session):
    lv = LocalVersion()
    local_version = lv.getVersion()
    db_versions = session.query(Version).all()
    exists = any(d.version == local_version for d in db_versions)
    if not exists:
        for d in db_versions:
            d.current = False
        insert_version = Version()
        insert_version.version = local_version
        insert_version.current = True
        insert_version.production = all(l.get('production') for l in list(local_version.values()))
        logging.info("Updated database version with: {}".format(str(local_version)))
        session.add(insert_version)
        session.commit()


def start_poller(session):

    p = Poller(session)

    tasks = [
        SampleImporterTask,
        AnalysisImporterTask,
        AnalysisExecutorTask
    ]

    for task in tasks:
        p.addTask(task)

    p.start()


if __name__ == '__main__':

    signal.signal(signal.SIGUSR1, handle_pdb)  # For debugging

    if 'killfile' not in settings['executor']:
        logging.warning("No killfile is specified in settings. This feature will be disabled!")
    else:
        kill_watcher = killfile.KillFileWatcher(settings['executor']['killfile'])
        kill_watcher.start()
        logging.info("Watching killfile \"{}\". Create this file to kill process.".format(settings['executor']['killfile']))

    db = DB(os.environ['DB_URL'], pool_size=3, pool_max_overflow=30, pool_timeout=120)
    session = db.sessionmaker()

    logging.info("Checking version information in database")
    update_version(session)
    logging.info('\n' + LocalVersion().formatVersion())

    logging.info("Starting executor engine")
    start_poller(session)
