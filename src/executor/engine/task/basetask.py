import abc


class BaseTask(object, metaclass=abc.ABCMeta):

    name = 'BaseTask'

    def __init__(self, session):
        self.session = session

    @abc.abstractmethod
    def init(self):
        pass

    @abc.abstractmethod
    def run(self):
        pass
