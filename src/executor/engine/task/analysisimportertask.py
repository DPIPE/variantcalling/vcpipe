import logging
from settings import settings
from executor.engine.iterators import ConfigIterator
from executor.engine.task import BaseTask
from executor.engine.analysis import Analysis
from executor.model import Analysis as ModelAnalysis

log = logging.getLogger(__name__)


class AnalysisImporterTask(BaseTask):

    name = 'AnalysisImporterTask'

    def __init__(self, session):
        self.session = session
        self.analysis = Analysis(session)

    def init(self):
        super(AnalysisImporterTask, self).init()

    def _checkForNewAnalysis(self):
        it = ConfigIterator(settings['base_analysis_path'], '.analysis')
        for path, config_file, config in it.iter():
            try:
                if not self.analysis.exists(config):
                    a = self.analysis.create(path, config_file, config)
                    self.session.add(a)
            except Exception:
                log.exception("Got exception while importing/creating new analysis: {0}\n{1}".format(path, config))

    def _checkDependencies(self):
        dep_analyses = self.session.query(ModelAnalysis).filter(ModelAnalysis.status == 'DEPENDENCIES').all()
        for da in dep_analyses:
            try:
                if self.analysis.checkDependencies(da):
                    self.analysis.fill(da)

            except Exception:
                log.exception("Got exception while checking dependencies for analysis: {0}".format(da))

    def run(self):
        self._checkForNewAnalysis()
        self.session.commit()
        self._checkDependencies()
        self.session.commit()
