import datetime
import os
import threading
import logging
from settings import settings
from executor.engine.task import BaseTask
from executor.engine import Command
from executor.model import Analysis, Version
from sqlalchemy import Integer

log = logging.getLogger(__name__)

PRIORITY_ORDER = [3, 2, 1, None]  # None for missing 'priority' in .analysis file
TYPE_ORDER = ['annopipe', 'triopipe', 'basepipe', 'noop']


class AnalysisExecutorTask(BaseTask):

    name = 'AnalysisExecutorTask'

    def __init__(self, session):
        self.session = session

    def _getRunningAnalyses(self):
        return self.session.query(Analysis).filter_by(status="RUNNING").all()

    def _failRunningAnalyses(self):
        """
        No analyses can be in RUNNING state upon startup.
        If they are, the executor was probably killed abruptly.
        """
        running_analysis = self._getRunningAnalyses()
        for a in running_analysis:
            a.status = 'FAILED'
        self.session.commit()

    def init(self):
        super(AnalysisExecutorTask, self).init()
        self._failRunningAnalyses()

    def _issuePendingAnalyses(self):
        """
        ordered pending analyses by priority and analysis type
        """
        priority_pending = []
        # priority order: urgent > high > normal
        for priority in PRIORITY_ORDER:
            # analysis type order: annopipe > triopipe > basepipe
            for atype in TYPE_ORDER:
                _pending = self.session.query(Analysis).filter(
                    Analysis.status == 'PENDING',
                    # missing priority will be lowest priority
                    Analysis.original['priority'].astext.cast(Integer) == priority,
                    Analysis.type == atype
                ).all()
                if _pending:
                    priority_pending = priority_pending + _pending

        if priority_pending:
            self._issueAnalyses(priority_pending)

    def _createResultPath(self, analysis):
        if not analysis.result_path:
            analysis.result_path = os.path.join(
                'result', datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
            )
        result_path = os.path.join(
            settings['base_analysis_path'], analysis.path, analysis.result_path
        )
        try:
            os.makedirs(result_path)
        except OSError:
            log.exception("Couldn't create result path")

    def _prepareAnalysis(self, analysis):
        self._createResultPath(analysis)
        # Set version again to make sure the version match with currently running process
        analysis.version = self.session.query(Version).filter(Version.current == True).one()
        analysis.status = 'RUNNING'
        self.session.commit()

    def _issueAnalyses(self, analyses):
        """
        Issues all pending analyses.

        The actual execution of analyses happens in a separate process, as a safeguard in case the
        parent dies or is aborted. We want the task to be able to finish and write the logs to the
        database regardless.

        :param analyses: The pending analyses which should be started.
        :type analyses: list(Analysis)
        """
        c = Command()
        for analysis in analyses:
            try:
                max_threads = settings.get('executor', {}).get('max_threads', 20)
                # +1 due to executor itself counting as one thread
                if threading.active_count() >= max_threads+1:
                    log.debug(
                        f"Thread count reached threshold of {max_threads}: "
                        f"Postponing any further runs (this is normally not an error)."
                    )
                    break
                self._prepareAnalysis(analysis)
                c.launch(analysis)
            except Exception:
                log.exception("Exception occured while launching analysis:")

    def run(self):
        self._issuePendingAnalyses()
        self.session.rollback()
