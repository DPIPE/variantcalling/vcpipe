import logging

from settings import settings
from executor.model import Sample
from executor.engine.iterators import ConfigIterator
from executor.engine.task import BaseTask

logging.basicConfig()
log = logging.getLogger(__name__)

class SampleImporterTask(BaseTask):

    name = 'SampleImporterTask'

    def __init__(self, session):
        self.session = session

    def init(self):
        super(SampleImporterTask, self).init()

    def _checkForNewSamples(self):
        it = ConfigIterator(settings['base_sample_path'], '.sample')
        for path, config_file, config in it.iter():
            sample_data = {
                'path': path,
                'name': config['name'],
                'config': config_file,
                'original': config
            }
            if not self.session.query(Sample).filter(Sample.name == config['name']).count():
                s = Sample(**sample_data)
                self.session.add(s)
                log.info("Inserted new sample: {0}".format(s))
                self.session.commit()

    def run(self):
        self._checkForNewSamples()
