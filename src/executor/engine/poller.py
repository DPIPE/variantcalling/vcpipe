import sys
import datetime
import time
import logging
import threading

from settings import settings


log = logging.getLogger(__name__)


class Poller(object):

    def __init__(self, session):
        self.tasks = list()
        self.session = session
        self.thread_count = 0

    def addTask(self, task):
        self.tasks.append(task(self.session))

    def logThreadCount(self):
        current_thread_count = threading.active_count()
        if current_thread_count != self.thread_count:
            self.thread_count = current_thread_count
            # -1 due to not counting exector itself
            log.debug("Active threads: {0}".format(threading.active_count() - 1))

    def start(self):

        for task in self.tasks:
            task.init()

        while True:
            try:
                for task in self.tasks:
                    try:
                        task.run()
                    except Exception:
                        log.exception("Got exception while running task {0}:".format(task.name))

                self.logThreadCount()
                time.sleep(settings.get('executor', {}).get('poll_interval', 5))

            except Exception:
                log.exception("Exception occured in main loop")
            except KeyboardInterrupt:
                log.info("Manually shutdown (KeyboardInterrupt) at {0}".format(str(datetime.datetime.now())))
                sys.exit()
