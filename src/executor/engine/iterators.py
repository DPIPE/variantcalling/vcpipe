import os
import json
import logging
import glob

logging.basicConfig()
log = logging.getLogger(__name__)


class ConfigIterator(object):

    def __init__(self, base_path, ext):
        self.base_path = base_path
        self.ext = ext

    def _loadConfig(self, path):
        with open(path) as fd:
            config = json.load(fd)
        return config

    def iter(self):
        """
        Iterates over files in self.base_path, reading any files
        matching the extension as JSON, returning the path,
        filename and json data.
        """

        # Get all files in subfolders (one level) matching our extension
        for glob_res in glob.iglob(os.path.join(self.base_path, '*', '*' + self.ext)):
            try:
                config_base = os.path.dirname(glob_res)
                # Check if the directory has 'READY' file
                if not os.path.exists(os.path.join(config_base, 'READY')):
                    continue
                config = self._loadConfig(glob_res)

                # Always reference everything by relative paths
                rel_path = os.path.relpath(config_base, self.base_path)
                config_file = os.path.basename(glob_res)
                yield (rel_path, config_file, config)

            except Exception as e:
                print(config_base)
                print(glob_res)
                log.exception("Error loading config: {}\n{}".format(glob_res, str(e)))
