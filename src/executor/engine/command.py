import time
import os
import fcntl
import threading
import signal
import subprocess
import logging
import datetime
import traceback
import codecs
from settings import settings
from executor.model import db, Analysis as ModelAnalysis
from executor.engine.analysis import Analysis


log = logging.getLogger()


class AnalysisProcess(object):
    """
    Represents one analysis process, i.e. launching the actual command
    given for the analysis on the system.

    It has the following responsibilities:
    * Running and monitoring the command in a subprocess
    * Logging it's output (combined stdout and stderr) to two places: log files specified in the
      analysis and into the log field of the analysis itself (in DB)
    * Update the duration of current process (seconds since start)
    * Poll for a PROGRESS file in the process' cwd,
      and update the progress fields in the DB accordingly.
    * If the analysis' status is changed to something else than "RUNNING",
      the process will be terminated.
    """

    def __init__(self, analysis):

        # Create new session since we're in a new thread
        self.session = db.sessionmaker()

        # We should not share the ModelAnalysis object between threads, so get a new one
        self.analysis = self.session.query(ModelAnalysis).filter(ModelAnalysis.id == analysis.id).one()

    def _createEnv(self, analysis):
        env_base = {
            'VCP_ANALYSIS_CONFIG': os.path.abspath(os.path.join(settings['base_analysis_path'], analysis.path, analysis.name + '.analysis')),
            'VCP_ANALYSIS_PATH': os.path.abspath(os.path.join(settings['base_analysis_path'], analysis.path)),
            'VCP_ANALYSIS_NAME': analysis.name,
            'VCP_ANALYSIS_PROJECT': analysis.original.get('project', None) or '-'.join(analysis.name.split('-')[:2]),
            'VCP_ANALYSIS_RESULT': os.path.abspath(os.path.join(settings['base_analysis_path'], analysis.path, analysis.result_path)),
            'VCP_IMPORT_DB': os.path.abspath(os.path.join(settings['base_analysis_path'], analysis.path, analysis.result_path, 'result.json')),
        }
        for idx, sample in enumerate(analysis.samples):
            env_base.update({
                'VCP_SAMPLE{}_CONFIG'.format(idx): os.path.abspath(os.path.join(settings['base_sample_path'], sample.path, sample.name + '.sample')),
                'VCP_SAMPLE{}_PATH'.format(idx): os.path.abspath(os.path.join(settings['base_sample_path'], sample.path)),
                'VCP_SAMPLE{}_NAME'.format(idx): sample.name
            })

        if 'read_only_perm' in settings:
            env_base['VCP_READ_ONLY_PERM'] = settings['read_only_perm']

        if 'read_only_group' in settings:
            env_base['VCP_READ_ONLY_GROUP'] = settings['read_only_group']

        if 'writable_perm' in settings:
            env_base['VCP_WRITABLE_PERM'] = settings['writable_perm']

        if 'writable_group' in settings:
            env_base['VCP_WRITABLE_GROUP'] = settings['writable_group']

        return env_base

    def run(self):
        log.info("Starting analysis {}".format(self.analysis.name))

        try:
            self._setup()

            env = os.environ.copy()
            env.update(settings.get('env', dict()))
            env.update(self._createEnv(self.analysis))

            self._preCmd()
            process = subprocess.Popen(self.analysis.command,
                                       shell=True,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT,
                                       cwd=self.cwd,
                                       env=env,
                                       preexec_fn=os.setsid)

            with codecs.open(self.stdout_path, 'a', 'utf-8') as stdout_fd:
                for stdout_line in self._poll_stdout(process):  # yields until process is done
                    if stdout_line is not None:
                        self._updateLog(stdout_line, stdout_fd)
                    self._updateDuration()
                    self._updateProgress()
                    self.session.commit()
                    if self._shouldAbort():
                        self._abortProcess(process)

            process.wait()
            exitcode = process.returncode

            log.info("Analysis {0} finished with errorcode {1}".format(self.analysis, exitcode))

            if exitcode == 0:
                self.analysis.status = "COMPLETE"
            else:
                self.analysis.status = "FAILED"
            self.analysis.exitcode = exitcode

            self._postCmd()

            self.session.commit()

        except Exception:
            log.exception("An error occured while running analysis:")
            try:
                self.session.rollback()
                analysis = self.session.query(ModelAnalysis).filter(ModelAnalysis.id == self.analysis.id).one()
                analysis.status = "FAILED"
                analysis.log += "[EXECUTOR] An error occured while running analysis\n"
                analysis.log += traceback.format_exc()
                self.session.commit()
            except Exception:
                log.exception("Something went wrong while writing analysis error information.")

    def _setup(self):
        self.analysis.status = "RUNNING"
        self.analysis.starttime = datetime.datetime.now()
        self.session.commit()

        self.cwd = os.path.join(settings['base_analysis_path'],
                                self.analysis.path,
                                self.analysis.result_path)

        log_path = os.path.join(self.cwd, 'logs')
        try:
            os.makedirs(log_path)
        except Exception:
            # log.exception("Couldn't create log path")
            pass

        self.stdout_path = os.path.join(log_path, '_'.join([
            str(self.analysis.id),
            self.analysis.name,
            'stdout.log'
        ]))

    @staticmethod
    def _readNonBlocking(output):
        fd = output.fileno()
        fl = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
        try:
            return output.readline()
        except Exception:
            return None

    @staticmethod
    def _poll_stdout(process):

        def fetch_line():
            line = True
            while line:
                line = AnalysisProcess._readNonBlocking(process.stdout)
                yield line

        while process.poll() is None:
            for line in fetch_line():
                yield line
            time.sleep(5)
        # Get final output after process exit
        for line in fetch_line():
            yield line

    def _preCmd(self):
        # Run precmd
        analysis_cls = Analysis
        analysis_cls.preCmd(self.analysis)

    def _postCmd(self):
        analysis_cls = Analysis
        analysis_cls.postCmd(self.analysis)

    def _updateLog(self, line, fd):
        self.analysis.log += line.decode('utf-8')
        fd.write(line.decode('utf-8'))
        fd.flush()

    def _updateDuration(self):
        duration = (datetime.datetime.now() - self.analysis.starttime).total_seconds()
        self.analysis.duration = duration

    def _updateProgress(self):
        """
        Tries open a PROGRESS file in process' cwd.
        The file should be in format '12\tSome message', where
        the first part is progress in percent (integer) and the second part
        a message. The changes are added to the analysis object's progress
        fields.
        """
        progress_path = os.path.join(self.cwd, 'PROGRESS')
        if os.path.exists(progress_path):
            try:
                with open(progress_path) as fd:
                    progress = fd.read()
                    progress_pct, progress_step = progress.split('\t', 1)
                try:
                    progress_pct = int(progress_pct)
                    self.analysis.progress_pct = progress_pct
                    self.analysis.progress_step = progress_step
                except ValueError:
                    pass
            # Ignore all errors silently, this isn't critical functionality
            except Exception:
                pass

    def _shouldAbort(self):
        return self.analysis.status != 'RUNNING'

    def _abortProcess(self, process):
        log.info("Analysis {} status has changed from RUNNING, aborting process...".format(
            self.analysis.name
        ))

        # Stop proccess and all children (by terminating the process group)
        try:
            os.killpg(os.getpgid(process.pid), signal.SIGTERM)
        except Exception():
            pass

        raise RuntimeError("Analysis status changed from RUNNING, aborting... (aborted by user?)")


class Command(object):

    def __init__(self):
        pass

    def launch(self, analysis):

        def thread_wrapper(analysis):
            # Is run inside thread
            AnalysisProcess(analysis).run()

        try:
            log.info("Launching analysis {0}".format(analysis))

            t = threading.Thread(name=analysis.name, target=thread_wrapper, args=(analysis,))
            t.start()
        except Exception:
            log.error("Exception occured inside threaded process.")
            traceback.print_exc()
