import abc


from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from executor.engine.analysis import MissingSampleException
from executor.model import Sample, Analysis, Version


class BaseAnalysis(object, metaclass=abc.ABCMeta):
    """
    Represents on analysis type in the system. Use as a base class to implement
    your own analysis type. One analysis type is responsible for creating the new
    analysis objects in the database, setting all the required data for said objects
    and checking whether the dependencies for this analysis are met.

    The basic flow works as follows:

    1. create() is called. Return an executor.model.Analysis object, filled with the
       data you have available.
    2. checkDependencies() is called. Check the dependencies you want and return True
       if all are met. One common thing to check is whether the connected samples have been imported.
    3. fill() is called. Here you fill in the rest of the information for the analysis. In this stage
       you know that any samples will be available, so you can query for them if neccessary.

    preCmd() and postCmd() are called by the AnalysisExector before/after running the process.
    The methods will be run in the process thread, and are meant as a way to pre-/postprocess
    files/info and having the related Analysis object easily available.
    """
    name = 'BaseAnalysis'

    def __init__(self, session):
        self.session = session

    def _getSamples(self, config):
        samples = []
        for sample_id in BaseAnalysis._getSampleIds(config):
            try:
                sample = self.session.query(Sample).filter(Sample.name == sample_id).one()
            except (NoResultFound, MultipleResultsFound):
                raise MissingSampleException("Sample '{}' is either missing or have multiple entries in DB for analysis '{}'".format(sample_id, self.name))
            samples.append(sample)

        if len(samples) < 1:
            raise MissingSampleException("Sample(s) are missing for analysis {0}". format(self.name))

        return samples

    @staticmethod
    def _getSampleIds(config):
        sample_ids = config.get('samples', [])
        if not isinstance(sample_ids, list):
            raise RuntimeError("'samples' must be a list")
        return sample_ids

    def _getTemplate(self, path, config):
        version = self.session.query(Version).filter(Version.current == True).one()
        template = {
            'name': config['name'],
            'type': config['type'],
            'original': config,
            'status': 'DEPENDENCIES',
            'path': path,
            'version': version
        }

        return template

    def exists(self, config):
        return bool(self.session.query(Analysis).filter(Analysis.name == config.get('name')).count())

    @staticmethod
    def preCmd(analysis):
        """
        Run in process thread by Command module before executing cmd.
        You can modify the passed in analysis object, changes will be
        commited by calling method.
        """
        pass

    @staticmethod
    def postCmd(analysis):
        """
        Run in process thread by Command module after executing cmd.
        You can modify and read from the passed in analysis object,
        changes will be commited by calling method.
        Examples are moving files to final location or importing data
        metrics from the run into Analysis object.
        """
        pass

    @abc.abstractmethod
    def create(self, path, config_file, config):
        """
        Creates and returns a executor.model.Analysis object,
        filled with the base values. See self._getTemplate() for
        a baseline template to use.
        """
        pass

    @abc.abstractmethod
    def checkDependencies(self, analysis):
        """
        Will be called frequently until it returns True. Checks the dependencies
        for this analysis, e.g. that all samples are imported or whatever else
        you need to know before finalizing the analysis object.
        """
        pass

    @abc.abstractmethod
    def fill(self, analysis):
        """
        Called after checkDependencies() returned True. Fills in whatever
        information left filling it, for instance, if your command relies
        on knowing information from the connected samples, you can here query
        the samples and update the analysis' command. See self._getSamples()
        for helper on getting the Sample objects.

        Usually you'd want to take care of the following:
        - Set analysis.command
        - Load the sample objects and set analysis.samples
        - Set analysis.status = 'PENDING'
        - Set analysis.status = 'INVALID' if something goes wrong
        """
        pass

    @staticmethod
    def getMetricOptions():
        """
        Returns available statistics metrics for this analysis.
        """
        pass

    @staticmethod
    def getMetrics(session, metric, capturekit):
        """
        Returns data for requested metric.

        Format:
        TBD
        """
        pass
