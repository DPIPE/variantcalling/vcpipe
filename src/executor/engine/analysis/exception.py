
class MissingSampleException(Exception):
    pass

class AnalysisError(Exception):
    pass
