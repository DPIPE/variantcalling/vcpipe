import glob
import json
import shutil
import os
import logging
import subprocess
import imp

from settings import settings
from executor.engine.analysis import BaseAnalysis, MissingSampleException
from executor.model import Analysis as ModelAnalysis
from pipeline.util.check_qc_result import check_qc_result

log = logging.getLogger(__name__)


class Analysis(BaseAnalysis):

    name = ''

    @staticmethod
    def preCmd(analysis):
        """
        - Import QC settings (not the data, just the thresholds) into Analysis object in DB.
        - The Capture Kit is read from the linked sample metadata. If not found, it is WGS.
        """
        qc_settings = {}
        capturekit = analysis.samples[0].original.get('capturekit', 'wgs')
        is_tumor = analysis.original.get('specialized_pipeline') == 'TUMOR'
        analysis_settings_file = capturekit + '_TUMOR' if is_tumor else capturekit
        analysis_settings = settings.get_settings(analysis_settings_file)
        is_dragen = analysis.original.get('specialized_pipeline') == 'Dragen'
        for pipeline_name, params in analysis_settings.items():
            if is_dragen:
                qc_key = 'qc_dragen'
            else:
                qc_key = 'qc'
            qc_settings.update(params.get(qc_key, {}))

        analysis.properties = {'qc_settings': qc_settings, 'analysis_settings_file': analysis_settings_file}

    @staticmethod
    def postCmd(analysis):
        """
        - Import our JSON results into Analysis object in DB.
        """
        if analysis.type == "noop":
            analysis.log += "Analysis type 'noop' passes QC even though it produces no QC data\n"
            return

        if analysis.status != 'COMPLETE':
            return

        analysis_qc_path = os.path.join(
            settings['base_analysis_path'],
            analysis.path,
            analysis.result_path,
            'data/qc',
            '*.qc_result.json'
        )

        analysis.log += "Importing analysis JSON result data into database...\n"
        try:
            analysis_qc_file = glob.glob(analysis_qc_path)[0]
        except IndexError:
            analysis.log += "Couldn't import JSON results into database. File not found at: {}\n".format(analysis_qc_path)
            analysis.status = "FAILED"
            return

        with open(analysis_qc_file) as fd:
            qc = json.load(fd)
            properties_copy = dict(analysis.properties)
            properties_copy.update({'qc': qc})
            analysis.properties = properties_copy

        if not check_qc_result(qc):
            analysis.status = "QCFAIL"

    def create(self, path, config_file, config):
        super(Analysis, self).create(path, config_file, config)

        template = self._getTemplate(path, config)
        a = ModelAnalysis(**template)
        self.name = template['name']

        return a

    def checkDependencies(self, analysis):
        super(Analysis, self).checkDependencies(analysis)
        analysis_config_path = os.path.join(
            settings['base_analysis_path'],
            analysis.path,
            '.'.join([analysis.name, 'analysis'])
        )
        args = {
            'analysis_config_path': analysis_config_path,
        }

        dependency_script = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            '../../../pipeline',
            analysis.type + '-dependency'
        )

        try:
            dep_source = imp.load_source('dependency', dependency_script)
        except IOError:
            log.exception("Dependency script not found for '{}' analysis type, in config {}".format(analysis.type, analysis_config_path))
            return False

        try:
            dep = dep_source.dependency(**args)
        except Exception:
            return False

        try:
            _ = self._getSamples(analysis.original)
        except MissingSampleException:
            log.exception("Samples for '{}' not recorded in DB even if the dependency check detected READY files".format(analysis.name))
            return False

        return dep

    def fill(self, analysis):
        super(Analysis, self).fill(analysis)

        try:
            config = analysis.original
            command = [
                config['type'],
                '--analysis',
                '"{}"'.format(os.path.join(settings['base_analysis_path'],
                                           analysis.path,
                                           analysis.path + '.analysis'))
            ]
            if 'cores' in config.get('params', {}):
                command.append('--cores')
                command.append('{}'.format(config['params']['cores']))
            analysis.command = ' '.join(command)
            if 'samples' in config:
                samples = self._getSamples(config)
                analysis.samples = samples
            analysis.status = 'PENDING'
            log.info("Analysis is imported and set to PENDING: {0}".format(str(analysis)))
        except Exception:
            analysis.status = 'INVALID'
            analysis.log = "Something went wrong when filling in analysis. See executor log for more details.\n"
            log.exception("Something went wrong when filling in analysis\n")

    @staticmethod
    def _deliver(analysis):
        """force delivery of analysis even if qc fail"""

        log.info("Delivering / Running post Cmd")
        result_path = os.path.join(
            settings["base_analysis_path"], analysis.path, analysis.result_path
        )
        qc_result = os.path.join(
            result_path,
            "data/qc",
            "*.qc_result.json",
        )

        args = {
            "export_priority": analysis.original.get("export_priority", 1),
            "platform": analysis.original.get("platform"),
            "qc_result": qc_result,
            "result_path": result_path,
            "analysis_name": analysis.name,
            "capturekit": analysis.samples[0].original.get("capturekit", "wgs"),
            "pipeline_type": analysis.type,
            "ignore_qc": True,
        }

        postcmd_script_basename = (
            "pipeline-post"
            if analysis.type in ["basepipe", "triopipe"]
            else analysis.type + "-post"
        )
        postcmd_script = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "../../../pipeline", postcmd_script_basename
        )
        try:
            post = imp.load_source("postcmd", postcmd_script)
            post.postcmd(**args)

        except IOError:
            log.exception("Dependency script not found: {}".format(postcmd_script))

    @staticmethod
    def _retrieve_analysis(analysis):
        """
        Rsync analysis folder from archive (durable) to runfolder (cluster)
        Avoid overwritting the runfolder.
        """
        log.info("Retrieving analysis from archive")
        runfolder_analysis_path = os.path.join(
            settings['base_analysis_path'],
            analysis.path
        )
        archive_analysis_path = os.path.join(
            settings['archive_analysis_path'],
            analysis.path
        )
        if os.path.isdir(runfolder_analysis_path):
            log.exception("Cannot retrieve analysis from archives: folder already exists: {}".format(runfolder_analysis_path))
            return
        if not os.path.isdir(archive_analysis_path):
            log.exception("Cannot retrieve analysis from archives: folder does not exists in archive: {}".format(archive_analysis_path))
            return
        cmd = [
            'rsync',
            '-vHrltDO',
            '--chmod=ug+rwX,o+rX',
            archive_analysis_path,
            settings['base_analysis_path']
        ]
        log.info("Retrieve analysis: {}".format(' '.join(cmd)))
        _ = subprocess.Popen(cmd,
                                   shell=False,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)

    @staticmethod
    def _retrieve_sample(analysis):
        """
        Rsync sample folder from archive (durable) to runfolder (cluster)
        Avoid overwritting the runfolder.
        """
        log.info("Retrieving sample from archive")
        for sample in analysis.original['samples']:
            runfolder_sample_path = os.path.join(
                settings['base_sample_path'],
                sample
            )
            archive_sample_path = os.path.join(
                settings['archive_sample_path'],
                sample
            )
            if os.path.isdir(runfolder_sample_path):
                log.exception("Cannot retrieve sample from archives: folder already exists: {}".format(runfolder_sample_path))
                return
            if not os.path.isdir(archive_sample_path):
                log.exception("Cannot retrieve sample from archives: folder does not exists in archive: {}".format(archive_sample_path))
                return
            cmd = [
                'rsync',
                '-vHrltDO',
                '--chmod=ug+rwX,o+rX',
                archive_sample_path,
                settings['base_sample_path']
            ]
            log.info("Retrieve sample: {}".format(' '.join(cmd)))
            _ = subprocess.Popen(cmd,
                                       shell=False,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT)

    @staticmethod
    def _delete_analysis_result_folder(analysis):
        """
        Delete the analysis run folder on disk
        """
        if analysis.result_path:
            try:
                analysis_result_path = os.path.join(
                    settings['base_analysis_path'],
                    analysis.path,
                    analysis.result_path
                )

                log.info("Deleting the analysis result folder on disk: {}".format(analysis_result_path))
                shutil.rmtree(analysis_result_path)
                log.info("Finished deleting {}".format(analysis_result_path))
            except Exception:
                log.error("Failed deleting the result folder of {}".format(analysis.path))
        else:
            log.info("No result folder for '{}'. This is normal if missing dependencies prevented the analysis to be run".format(analysis.path))
