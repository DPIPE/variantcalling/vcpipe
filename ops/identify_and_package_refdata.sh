#!/usr/bin/env bash

# Find version from datasets.json and download from DO

set -e

THIS_SCRIPT=${BASH_SOURCE[0]}
DIR="$( cd "$( dirname "${THIS_SCRIPT}" )" && pwd )"

all_args=("$@")

echo "Running ${THIS_SCRIPT} with " "${all_args[@]}"

parent_folder=$1
kind=$2
name_path=$3
docker_image=$4

name=$(echo ${name_path} | cut -d'|' -f1)
dataset_paths=$(echo ${name_path} | cut -d'|' -f2)
version=$(cat ${DIR}/datasets.json | jq ".[\"${kind}\"].kinds[\"${name}\"].version" | sed 's/"//g')

echo "Will download ${kind} ${name} version ${version} with paths ${dataset_paths}"
${DIR}/tar-refdata.sh ${parent_folder} ${kind} ${name} ${version} ${dataset_paths} ${docker_image} --nature ${kind} --kind-id ${name} --version ${version}
