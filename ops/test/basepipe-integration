#!/bin/bash

# basepipe.nf needs location of analyses and samples; create symlinks as the location varies depending on tests

set -ETeu -o pipefail

trap "chown -R ${HOSTUID}:${HOSTGID} /output /result /analyses /tmp;" EXIT

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Running $( basename "${BASH_SOURCE[0]}" ) in ${DIR} as $(whoami)"

capture_kit=$1
specialized=$2

export NXF_HOME=/tmp
# need to set a var that's usually set on the cluster nodes, else the process fails in CI
export SCRATCH=/tmp

declare -A required_dirs
required_dirs[/analyses]="analysis metadata (like in the .analysis file)"
required_dirs[/output]="where to save the pipeline results"
required_dirs[/result]="where to save the summary file from the regression test"
required_dirs[/run-refdata]="reference data, e.g. capture kits, downloaded from DigitalOcean"
required_dirs[/run-testdata]="test data, e.g. fastqc, metadata, dwnloaded from DigitalOcean"
required_dirs[/samples]="sample metadata (like in the .sample file)"
required_dirs[/tmp]="temporary files created by nextflow (if NFX_HOME is set to /tmp)"

declare missing_requirement=false
for dirname in "${!required_dirs[@]}" ; do
  if [[ ! -d "${dirname}" ]] ; then
    echo "Directory '${dirname}' not found."
    missing_requirement=true
  fi
done
if ${missing_requirement} ; then
  echo "The following directories are required:"
  for dirname in "${!required_dirs[@]}" ; do
    echo "${dirname}:\t${required_dirs["${dirname}"]}"
  done
  echo "Mount them in the Docker container using option '-v'"
  exit 1
fi

if [ -z "${specialized}" ]
  then
    export SPECIALIZED_PIPELINE=""
    echo "No argument supplied, testing non-special pipeline"
else
    export SPECIALIZED_PIPELINE="${specialized}"
    echo "Using supplied special pipeline '${SPECIALIZED_PIPELINE}'"
fi


if [ -z "$capture_kit" ]
  then
    export CAPTURE_KIT=CuCaV2
    echo "No argument supplied, testing default capture kit ${CAPTURE_KIT}"
else
    export CAPTURE_KIT=$capture_kit
    echo "Using supplied capture kit ${CAPTURE_KIT}"
    echo "(Note that, at this stage, no check has been done whether it exists)"
fi

export NEXTFLOW_CONFIG=/vcpipe/config/nextflow.config
export SYSTEM=docker_ci

# set test settings
export TEST_SETTINGS=integration


# choose sample
if [[ $CAPTURE_KIT = "CuCaV2" ]]; then
  if [[ "${SPECIALIZED_PIPELINE}x" == "TUMORx" ]]; then
    ANALYSIS_NAME=Diag-EKG200302-TUMORSomaticMultiplex131100
  else
    ANALYSIS_NAME=Diag-EKG200203-NA12878N6
  fi
  PLATFORM=target
elif [[ $CAPTURE_KIT = "agilent_sureselect_v05" ]]; then
  ANALYSIS_NAME=Diag-excap180-NA12878N6
  PLATFORM=wes-hiseq
elif [[ $CAPTURE_KIT = "wgs" ]]; then
  ANALYSIS_NAME=Diag-ValidationWGS3-HG002C2-PM
  PLATFORM=wgs-hiseq
else
  echo "Unknown capture kit '${CAPTURE_KIT}', aborting"
  echo "base pipeline: FAILURE!"
  exit 1
fi

# setup paths
ln -s /run-testdata/${PLATFORM}/samples/${ANALYSIS_NAME} /samples
cp -R /run-testdata/${PLATFORM}/analyses/${ANALYSIS_NAME} /analyses

export VCP_ANALYSIS_RESULT=/analyses/${ANALYSIS_NAME}
export VCP_ANALYSIS_CONFIG=${VCP_ANALYSIS_RESULT}/${ANALYSIS_NAME}.analysis

# remove the existing results
rm -rf "/preprocessed/singles/${ANALYSIS_NAME}" || true

# run pipeline
cd $VCP_ANALYSIS_RESULT # location of nextflow cache/work
/vcpipe/exe/pipeline/basepipe --analysis ${VCP_ANALYSIS_CONFIG}

if [[ $? == 0 ]]; then
  echo "base pipeline: SUCCESS!"
else
  echo "base pipeline: FAILURE!"
  exit 1
fi
