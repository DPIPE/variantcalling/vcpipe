#!/bin/bash

set -euf -o pipefail

THIS_SCRIPT=${BASH_SOURCE[0]}
DIR="$( cd "$( dirname "${THIS_SCRIPT}" )" && pwd )"

source ${DIR}/../../exe/setup.source

CMD="exe/annotation/exCopyDepth  /bundle/testdata/preprocessed/singles/Diag-excap1-NA12878/data/mapping/Diag-excap1-NA12878.bam  --background /bundle/fake-sensitive-db/exCopyDepth/cnv-background-excap-non-sensitive.rds --output exCopyDepth-test.cnv.raw.bed
"
echo "Will run ${CMD}"
$CMD