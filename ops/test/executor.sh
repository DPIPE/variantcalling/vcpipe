#!/bin/bash

# Tests the executor by copying sample and analysis files from the run-testdata folder into the folder that the
# executor is watching. After some time the executer API is queried for the result of those analysis.


# input:
# - analyses

set -euf -o pipefail

THIS_SCRIPT=${BASH_SOURCE[0]}
DIR="$( cd "$( dirname "${THIS_SCRIPT}" )" && pwd )"
echo "Running `basename ${THIS_SCRIPT}` in ${DIR} as $(whoami)"

trap "echo 'Changing permission on exit of executor'; chmod -R a+w ${DIR}/../.. /output/logs /analyses" EXIT

if [[ ! -d /run-refdata || ! -d /run-testdata || ! -d /analyses || ! -d /samples ]]; then
  echo "One or more required directories are missing: /run-refdata, /analyses"
  echo "Mount them in the Docker container using -v ..:/.."
  echo "/run-refdata Data from Digital Ocean, like capture kits. Downloaded to CI runner's /storage"
  echo "/analyses:   metadata for the analysis (like the .analysis file)"
  exit 1
fi

CI_TESTDATA=/analyses/executor
TESTDATA_ROOT=/run-testdata

source ${DIR}/create-analysis.sh

echo "Cleaning ${CI_TESTDATA}"
rm -rf ${CI_TESTDATA}/*

function assert() {
  name=$1
  expected=$2

  echo "Running assert: ${name} ${expected}"

  response=$(curl --get --silent  -H "Accept: application/vnd.api+json" --output -  -d "filter[objects]=[{\"name\":\"name\",\"op\":\"eq\",\"val\":\"${name}\"}]" http://127.0.0.1:5000/api/analysis)
  if [[ "${response}" == "" ]]; then
    echo "No response from API"
    exit 1
  fi
  status_raw=$(echo ${response} | awk 'match($0,/"status": "[A-Z]+"/)  {print substr($0, RSTART,RLENGTH)}')
  total_raw=$(echo ${response} | awk 'match($0,/"total": [0-9]+/)  {print substr($0, RSTART,RLENGTH)}')
  echo "checking number of entries"
  echo ${total_raw} | ( grep '"total": 1' || (echo "Expected a single result/total: 1 in api response for ${name}, was ${total_raw}"; exit 1))
  echo "checking value of status"
  echo ${status_raw} | ( grep ${expected} || (echo "Analysis status was not as expected for ${name}: expected ${expected} (actual: ${status_raw})"; exit 1))
}

function assert_no_analysis() {
  name=$1
  echo "Running assert there are no match for ${name}"

  response=$(curl --get --silent  -H "Accept: application/vnd.api+json" --output -  -d "filter[objects]=[{\"name\":\"name\",\"op\":\"eq\",\"val\":\"${name}\"}]" http://127.0.0.1:5000/api/analysis)
  if [[ "${response}" == "" ]]; then
    echo "No response from API"
    exit 1
  fi

  total_raw=$(echo ${response} | awk 'match($0,/"total": [0-9]+/)  {print substr($0, RSTART,RLENGTH)}')
  echo ${total_raw} | ( grep '"total": 0' || (echo "Expected no result/total: 0 for ${name} in api response ${total_raw}"; exit 1))
}


function wait_for_executor() {
sleep ${1:-6} # give executor time to update, defaults to 4 sec if no argument
}

(function f() {
name=$1
missing_dotsample_but_has_dotanalysis ${name}
#pingit
wait_for_executor
assert $name "DEPENDENCIES"
};
 f "Diag-$RANDOM"
 )

(function f() {
name=$1
create_readyanalysis_but_not_readysample ${name}
#pingit
wait_for_executor
assert $name "DEPENDENCIES"
};
 f "Diag-$RANDOM"
)

(function f() {
name=$1
missing_dotsample_but_has_dotanalysis ${name}
#pingit
wait_for_executor
assert $name "DEPENDENCIES"
};
 f "Diag-$RANDOM"
)

echo "Test OK"

(function f() {
name=$1
missing_dotanalysis_but_has_dotsample ${name}
wait_for_executor
assert_no_analysis $name
};
 f "Diag-$RANDOM"
 )

echo "Test OK"

(function f() {
name=$1
has_both_ready_files_but_missing_dotsample ${name}
wait_for_executor 10
assert $name "DEPENDENCIES"
};
f "Diag-$RANDOM"
 )

echo "Test OK"

(function f() {
name=$1
has_ready_files_and_dotanalysis_and_dotsample_but_missing_fastq ${name}
wait_for_executor 20
assert $name "DEPENDENCIES"
};
 # We don't detect missing fastq files until analysis is actually started. So skip this tests
 # f "Diag-$RANDOM"
)

echo "Test OK"

(function f() {
name=$1
complete_sample_and_analysis ${name}
wait_for_executor 15
assert $name "COMPLETE"
};
 f "Diag-$RANDOM"
)

echo "Test OK"

# encode the uri if wget is used:
# node
# d = [{"name":"name","op":"eq","val":"Diag-14900"}]; encodeURIComponent(JSON.stringify(d))
# d = [{"name":"name","op":"eq","val":"Diag-14900"},{"name":"type","op":"eq","val":"basepipe"}]; encodeURIComponent(JSON.stringify(d))
# { filters: [ { name: 'name', op: 'eq', val: 'Diag-14900' } ] }
# encodeURIComponent(JSON.stringify(d))
# ->> '%7B%22filters%22%3A%5B%7B%22name%22%3A%22name%22%2C%22op%22%3A%22eq%22%2C%22val%22%3A%22xx%22%7D%5D%2C%22single%22%3Atrue%7D'
# ?filter\[objects\]=...
# &fields\[analysis\]=name
