#!/usr/bin/env bash

# terms:
# dotanalysis is the .analysis file
# dotsample is the .sample file
# ready is the READY marker file in the analysis and sample folders

function pingit() {
/bin/ping -c 3 -w 2 localhost
}

function info() {
   echo "Preparing ${1} for ${2}"
}


function missing_dotsample_but_has_dotanalysis() {
   name=$1
   info "missing_dotsample_but_has_dotanalysis" ${name}
   mkdir -p ${CI_TESTDATA}/analyses/${name}
   cat ${TESTDATA_ROOT}/target/analyses/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.analysis |  sed -e "s/Diag-EKG200203-NA12878N6/${name}/g" > ${CI_TESTDATA}/analyses/${name}/${name}.analysis
   touch ${CI_TESTDATA}/analyses/${name}/READY
   info "DONE" $name
}

function create_readyanalysis_but_not_readysample() {
   name=$1
   info "create_readyanalysis_but_not_readysample" $name
   mkdir -p ${CI_TESTDATA}/samples/${name}
   mkdir -p ${CI_TESTDATA}/analyses/${name}
   cat ${TESTDATA_ROOT}/target/analyses/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.analysis |  sed -e "s/Diag-EKG200203-NA12878N6/${name}/g" -e "s/basepipe/noop/" > ${CI_TESTDATA}/analyses/${name}/${name}.analysis
   cat ${TESTDATA_ROOT}/target/samples/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.sample |  sed "s/Diag-EKG200203-NA12878N6/${name}/g" > ${CI_TESTDATA}/samples/${name}/${name}.sample
   touch ${CI_TESTDATA}/analyses/${name}/READY
   info "DONE" $name
}


function missing_dotanalysis_but_has_dotsample() {
   name=$1
   info "missing_dotanalysis_but_has_dotsample" ${name}
   mkdir -p ${CI_TESTDATA}/samples/${name}
   cat ${TESTDATA_ROOT}/target/samples/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.sample |  sed "s/Diag-EKG200203-NA12878N6/${name}/g" > ${CI_TESTDATA}/samples/${name}/${name}.sample
   touch ${CI_TESTDATA}/samples/${name}/READY
   info "DONE" $name
}

function has_both_ready_files_but_missing_dotsample() {
   name=$1
   info "has_both_ready_files_but_missing_dotsample" $name
   mkdir -p ${CI_TESTDATA}/samples/${name}
   mkdir -p ${CI_TESTDATA}/analyses/${name}
   cat ${TESTDATA_ROOT}/target/analyses/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.analysis |  sed -e "s/Diag-EKG200203-NA12878N6/${name}/g" > ${CI_TESTDATA}/analyses/${name}/${name}.analysis
   touch ${CI_TESTDATA}/analyses/${name}/READY
   touch ${CI_TESTDATA}/samples/${name}/READY
   info "DONE" $name
}

function has_ready_files_and_dotanalysis_and_dotsample_but_missing_fastq() {
   name=$1
   info "has_ready_files_and_dotanalysis_and_dotsample_but_missing_fastq" $name
   mkdir -p ${CI_TESTDATA}/samples/${name}
   mkdir -p ${CI_TESTDATA}/analyses/${name}
   cat ${TESTDATA_ROOT}/target/analyses/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.analysis |  sed -e "s/Diag-EKG200203-NA12878N6/${name}/g" > ${CI_TESTDATA}/analyses/${name}/${name}.analysis
   cat ${TESTDATA_ROOT}/target/samples/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.sample |  sed "s/Diag-EKG200203-NA12878N6/${name}/g" > ${CI_TESTDATA}/samples/${name}/${name}.sample
   touch ${CI_TESTDATA}/analyses/${name}/READY
   touch ${CI_TESTDATA}/samples/${name}/READY
   info "DONE" $name
}

function complete_sample_and_analysis() {
   name=$1
   info "complete_sample_and_analysis" $name
   mkdir -p ${CI_TESTDATA}/samples/${name}
   mkdir -p ${CI_TESTDATA}/analyses/${name}
   cat ${TESTDATA_ROOT}/target/analyses/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.analysis |  sed -e "s/Diag-EKG200203-NA12878N6/${name}/g" -e "s/basepipe/noop/" > ${CI_TESTDATA}/analyses/${name}/${name}.analysis
   cat ${TESTDATA_ROOT}/target/samples/Diag-EKG200203-NA12878N6/Diag-EKG200203-NA12878N6.sample |  sed "s/Diag-EKG200203-NA12878N6/${name}/g" > ${CI_TESTDATA}/samples/${name}/${name}.sample
   touch ${CI_TESTDATA}/analyses/${name}/READY
   touch ${CI_TESTDATA}/samples/${name}/READY
   info "DONE" $name
}
