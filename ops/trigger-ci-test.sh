#!/usr/bin/env bash

# Trigger a Gitlab CI execution using the Gitlab API

# The version of vcpipe is taken from the last line of release-history.csv.
# Can also be given using variables, see example below:
# GITLAB_TOKEN_VCPIPE=.. VCPIPE_BRANCH=.. GENEPANELS_BRANCH=.. ops/trigger-ci-test.sh annopipe <boolean> <boolean>
# The first boolean: run regression (default true)
# The second boolean: create genepanel browser (default true)

set -e

SERVER=https://gitlab.com
API=${SERVER}/api/v4/projects/42924648/trigger/pipeline


THIS_SCRIPT=${BASH_SOURCE[0]}
DIR="$( cd "$( dirname "${THIS_SCRIPT}" )" && pwd )"

if [[ -z "${GITLAB_TOKEN_VCPIPE}" ]]; then
  echo "Access to the Gitlab API requires a token set in environment variable GITLAB_TOKEN_VCPIPE"
  exit 1
else
  api_token="${GITLAB_TOKEN_VCPIPE}"
fi

pushd ${DIR}/.. > /dev/null

vcpipe=${VCPIPE_BRANCH}

genepanels=${GENEPANELS_BRANCH:-master}

pipeline_type=${1:-"annopipe"}
run_regression=${2:-"false"}
create_genepanel_browser=${3:-"false"}

ANS=`read  -p "$(echo -e "Start running ${pipeline_type} at ${SERVER}\n" \
                          "using version '${vcpipe}' (regression=${run_regression}, create browser=${create_genepanel_browser})\n" \
                          "genepanels = ${genepanels}\n" \
                          "> [yN]?: ")" answer; echo ${answer:-N}`
if [[ ${ANS} == "y" ]];
 then
   true
  else
   echo "Skipping"; exit 0;
fi


curl -X POST \
     -F token=${api_token} \
     -F ref=${vcpipe} \
     -F "variables[GENEPANELS_BRANCH]=${genepanels}"\
     -F "variables[PIPELINE_TYPE]=${pipeline_type}"\
     -F "variables[RUN_REGRESSION]=${run_regression}" \
     -F "variables[CREATE_GENEPANEL_BROWSER]=${create_genepanel_browser}" \
     ${API}

popd > /dev/null
