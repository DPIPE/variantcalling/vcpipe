#!/usr/bin/env bash

# Download dataset from DO and put in a tar
# Skips download if *-<CHECKSUM>.tar exists. Can create an empty file if a download for some reason is not wanted.

set -e

THIS_SCRIPT=${BASH_SOURCE[0]}
DIR="$( cd "$( dirname "${THIS_SCRIPT}" )" && pwd )"

# The last argument might contain spaces
all_args=("$@")
parent_folder=$1     # where to download files and create tar file
kind=$2              # needed in naming the tar file as 'name' can be ambigous
refdata_name=$3      # names the tar file and contributes to checksum
refdata_version=$4   # names the tar file and contributes to checksum
refdata_path=$5      # paths (bash brace expansion expression) to put in tar and where to find DATA_READY for checking version
docker_image=$6      # docker images used to download data from DO
rest_args=("${all_args[@]:6}")
refdata_options=$(echo "${rest_args[@]}") # options to control what to download

version_slug=$(echo ${refdata_version} | sed 's|/|-|g')
name_slug=$(echo ${refdata_name} | sed 's|/|-|g')

checksum=$(echo ${refdata_name} ${refdata_version} | md5sum | awk '{print $1}')

pushd ${parent_folder}

# exit if file dataset already exits on disk
if ls -l *-${checksum}.tar; then
  echo "Dataset ${refdata_name} exists. Won't re-download"
  exit 0
else
  echo "Will delete files with version info"
  rm -f ${refdata_path}/DATA_READY
  echo "Downloading data from Digital Ocean using docker image ${docker_image}: ${refdata_options}"
  docker run --user=$(id -u):$(id -g) --rm -v ${parent_folder}:/opt/vcpipe-refdata/data ${docker_image} vcpipe_refdata --spaces-key ${AMG_DO_SPACE_KEY} --spaces-secret ${AMG_DO_SPACE_SECRET} --verbose download ${refdata_options}
  # make sure version is correct:
  #find ${refdata_path}/DATA_READY | xargs -I '{}' awk '/^version/ {print $2}' | grep ${refdata_version}
  # TODO: how is a mismatching version handled?
  tar cvf ${kind}_${name_slug}_${version_slug}-${checksum}.tar $(eval echo ${refdata_path}) --remove-files
  echo "tar done"
fi


