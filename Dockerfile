FROM centos:centos7.9.2009 AS base

################## METADATA ######################


COPY ./CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo
RUN yum clean all


LABEL io.ousamg.base_image="centos:centos7.9.2009"
LABEL io.ousamg.version="1"
LABEL io.ousamg.about.summary="Bioinf pipeline testing"
LABEL io.ousamg.about.home="http://gitlab.com/ousamg/apps/vcpipe"
LABEL MAINTAINER="OUSAMG <diag-bioinf@medisin.uio.no>"

##################################################

ENV SINGULARITY_VERSION=3.7.1


ENV TERM=xterm-256color

RUN yum -y update &&  \
    yum -y install \
       make \
       gcc  \
       gcc-c++ \
       squashfs-tools \
       libarchive-devel \
       zlib \
       libffi-devel zlib-devel \
       bzip2-devel \
       openssl-devel \
       wget \
       epel-release \
       python-devel \
       libcurl-devel \
       readline-devel \
       time \
       perl-CGI \
       perl-DBI \
       perl-Time-HiRes \
       sqlite-devel \
       rpm-build \
       xz-devel \
       gcc-gfortran \
       which

FROM base AS python

# Python
RUN cd /tmp && export PYTHON_VERSION="3.6.8" && \
    wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz && \
    tar xvfz Python-${PYTHON_VERSION}.tgz && \
    cd Python-${PYTHON_VERSION} && \
    ./configure --prefix=/usr/local LFLAGS="-lgcov --coverage" LDFLAGS="-Wl,-rpath /usr/local/lib" && \
    make altinstall && \
    rm -rf /tmp/Python-Python-${PYTHON_VERSION}.tgz* && unset PYTHON_VERSION

RUN cd /tmp &&  ln -sv /usr/local/bin/python3 /usr/local/bin/python

FROM python AS core-tools

# Git
RUN yum  -y install http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-1.noarch.rpm && yum -y install git

# Singularity
# See https://github.com/sylabs/singularity/blob/master/INSTALL.md
RUN yum groupinstall -y 'Development Tools' && \
    yum install -y epel-release && \
    yum install -y openssl-devel libuuid-devel golang \
  libseccomp-devel squashfs-tools cryptsetup

RUN go version

RUN ls -la  /tmp

RUN wget https://github.com/apptainer/singularity/releases/download/v${SINGULARITY_VERSION}/singularity-${SINGULARITY_VERSION}.tar.gz && \
    rpmbuild -tb singularity-${SINGULARITY_VERSION}.tar.gz && \
    rpm -ivh ~/rpmbuild/RPMS/x86_64/singularity-${SINGULARITY_VERSION}-1.el7.x86_64.rpm && \
    rm -rf ~/rpmbuild singularity-${SINGULARITY_VERSION}*.tar.gz


# Java
RUN yum -y install java-1.8.0-openjdk-devel

# Postgres
RUN yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
RUN yum -y install centos-release-scl
RUN yum install -y llvm-toolset-7-clang
RUN yum -y install postgresql14-server postgresql14 postgresql14-devel

# JQ (package not found when trying to install further up)
RUN yum -y install jq

ENV PATH=$PATH:/usr/pgsql-14/bin

# Perl modules
RUN yum -y install \
    perl-HTTP-Tiny.noarch \
    perl-Archive-Extract.noarch \
    perl-Archive-Zip.noarch \
    perl-Compress-Raw-Zlib.x86_64 \
    perl-CPAN \
    perl-Digest-MD5


# Nextflow
# See https://www.nextflow.io/docs/latest/getstarted.html
ENV NXF_VER=21.10.6

RUN cd /usr/local/bin && \
    wget -qO- https://get.nextflow.io | /bin/bash && \
    chmod 755 /usr/local/bin/nextflow


FROM core-tools AS python-libs

ENV LANG="en_US.utf8" \
    LC_ALL="en_US.utf8"
# https://github.com/pypa/pipenv/issues/178
RUN mkdir -p /dist/.venv
COPY ./Pipfile.lock /dist/Pipfile.lock
COPY ./Pipfile /dist/Pipfile

# create an empty .venv folder in /dist ?
RUN cd /dist/  && \
    PIPENV_VENV_IN_PROJECT="1"  python3.6 -m venv vcpipe-python && \
    /dist/vcpipe-python/bin/pip install --no-cache-dir pipenv==2020.8.13 && \
    /dist/vcpipe-python/bin/pipenv sync --dev

WORKDIR /vcpipe

ENV PATH=/dist/vcpipe-python/bin:/dist/.venv/bin:/vcpipe/exe:${PATH} \
    PYTHONPATH=/vcpipe/src \
    VIRTUAL_ENV=/dist/.venv \
    PIPENV_VENV_IN_PROJECT="1"


#RUN mkdir -p /run-testdata/{samples,analyses,preprocessed/{singles,trios}}
#RUN chmod -R 777 /run-testdata
RUN mkdir -p /{samples,analyses,preprocessed/{singles,trios}}
RUN chmod -R 777 /{samples,analyses,preprocessed/{singles,trios}}

RUN chmod -R 777 /var
# this might not be relevant to us, since we run most of our tests as 'docker exec ...'
# extra arguments to 'docker run ...' are appended after this entrypoint script:
#RUN echo "#!/bin/bash" >> /entrypoint.sh && \
#    cd /vcpipe && \
#    echo "source $(pipenv --venv)/bin/activate" >> /entrypoint.sh && \
 #   echo 'exec "$@"' >> /entrypoint.sh && \
#    chmod +x /entrypoint.sh
#ENTRYPOINT ["/entrypoint.sh"]

#Remove the one at the top of the script once it is possible to build the image with python3
ENV SETTINGS=/vcpipe/config/settings-docker_ci.json
