# Release notes

## v2.0

### Bug
- [AMG-1318] - trioGenderTest assertion error when pedigree mismatches
- [AMG-1319] - Interpretation PDF files missing + refract minimal_no_reads
- [AMG-1323] - WGS trio does not Join Variant call all chromosomes

### Task
- [AMG-472] - Usage of phenotypes files should be make explicit
- [AMG-740] - Custom bundle (including explicit genepanel path) for custom analyses
- [AMG-743] - Allow analyses to refer to samples in the file archive
- [AMG-959] - Pipeline allow resume
- [AMG-972] - Allow request of multiple gene panels
- [AMG-983] - Intermediary trio pipeline
- [AMG-989] - Fix QC ui
- [AMG-1003] - Integration / regression test for split pipelines
- [AMG-1010] - Request anno post-analysis with new genepanel in web UI
- [AMG-1012] - Insert HaplotypeCaller "debug" BAM in annopipe
- [AMG-1013] - "Post" anno pipelines: get required files with "pull" instead of "push"
- [AMG-1017] - Consolidate all base pipelines into one
- [AMG-1052] - Pipeline dependencies
- [AMG-1117] - fix QC ui (part 2)
- [AMG-1122] - remove "reset" functionality
- [AMG-1123] - "delete" actually deletes the analysis runfolder on disk
- [AMG-1126] - rebase 739-split onto dev
- [AMG-1131] - Change preprocessed structure to single/ and trio/
- [AMG-1132] - Create "staging" area in TSD with test database
- [AMG-1133] - Update nextflow config for ressource allocation
- [AMG-1180] - Improve UI responsiveness
- [AMG-1190] - Modify nightly rsync script
- [AMG-1224] - Add fingerprinting to output folder
- [AMG-1225] - Add raw QC metrics to preprocessed output
- [AMG-1288] - Copy *all* files from basepipe to preprocessed
- [AMG-1301] - Compute sensitivity of vcpipe-2.0
- [AMG-1357] - split base/trio/anno in UI
- [AMG-1375] - Update calling_region for new MasterGenePanel and Av7, CuCaV2
- [AMG-1378] - XLS report improvements
- [AMG-1381] - Script to display the exon coordinates from the genepanel region file

### Sub-task
- [AMG-967] - Refract analysis
- [AMG-974] - Split QC
- [AMG-978] - Re-implement QC fail in executor
- [AMG-981] - Split genome
- [AMG-987] - Re-implement deliver anyway
- [AMG-995] - report.txt and warnings.txt missing from Target
- [AMG-996] - Forward capturekit from target to target-anno
- [AMG-1053] - Consolidate QC and config: one file per Capture Kit
- [AMG-1113] - Sort alphabetically in trio Excel report
- [AMG-1129] - compressed g.vzf.gz for split pipelines
- [AMG-1130] - [qc] trioGenderTest Refract in split pipeline
- [AMG-1135] - Restructure preprocessed folder
- [AMG-1136] - Delete old pipeline files
- [AMG-1157] - Add preprocessed data to bundle
- [AMG-1181] - Forward logs to preprocessed
- [AMG-1296] - EKG, EHG: variant call on (extended) capture kit
- [AMG-1310] - Bug: missing rsID
- [AMG-1312] - trio: output region annotated vcf without freq filtering
- [AMG-1374] - pipeline support for Av7 and CuCaV2

### Epic
- [AMG-739] - Split variant calling and annotation into separate pipelines

### Story
- [AMG-1045] - Consolidate annotation pipelines
- [AMG-1046] - Decide on archival architecture (incl. fetch preprocessed data)
- [AMG-1295] - Review results of vcpipe-2.0 vs vcpipe-1.12


## v1.13

- [AMG-1346] - DBUpdate: ClinVar update 2018Jun
- [AMG-1347] - DBUpdate: HGMD update 2018Q1

## v1.12.2

### Bug

- [AMG-1293] - "unclassified" (filtered out) variants does not handle multiple transcripts

## v1.12.1

### Bug

- [AMG-1291] - [trio][report] recessive variants filtered out: if more than 2 OR if no p_value

## v1.12

### Bug

- [AMG-1191] - vcpipe-executer json raw_decode expecting delimiter error
- [AMG-1216] - Deliver trio results to the proband excap on TSD interpretation

### Task

- [AMG-905] - trio coverage report 90% --> 100%
- [AMG-917] - Add filter for UTR variants in trio pipeline
- [AMG-1170] - Speed up exome_report creation
- [AMG-1223] - report: Trio: Add sample number and excap number for parents
- [AMG-1025] - Add genpanel as column in Excel report

## v1.11.3

- [AMG-1146] - MigraForst-v03 gene panel fails coverage_report_exome.py with KeyError: 'inheritance'
- [AMG-1119] - EKG: 20 reads, not 40 for coverage warning

## v1.11.2

- [AMG-1116] - missing VEP_CONSEQUENCE crashes the exome report

## v1.11.1

- [AMG-1097] - EKG: Coverage report aggregate xls, new phenotype file format
- [AMG-1103] - Split AD field not supported by GATK process in QC

## v1.11

### Bug

- [AMG-663] - Decompose doesn't split AD, PL fields
- [AMG-1009] - EKG: Coverage report aggregate xls, fails regex
- [AMG-1038] - POLG: variant not found in Trio, but was found in (previous) excap
- [AMG-1049] - EKG: Coverage report aggregate xls, float/string conversion
- [AMG-1060] - Trio gender test: undetermined zone
- [AMG-1063] - Allow missing phenotype entry (use default 'N/A' inheritance pattern)
- [AMG-1065] - Bug: decompose doesn't split AD, PL fields
- [AMG-1088] - Split AD,PL fields crashes bcbio-variation

### Task

- [AMG-842] - Adapt no of variants and low coverage regions in QC check in target pipeline for cardio samples
- [AMG-932] - Zip the g.vcf file from WGS pipeline
- [AMG-973] - Add column in XLS report: find all other gene panels where this gene is included
- [AMG-1002] - dbupdate: HGMD update 2017.3
- [AMG-1057] - EKG: Coverage report aggregate xls: change minimal-no-reads
- [AMG-1066] - EKG: Add low coverage regions to warnings.txt
- [AMG-976] - Allow additional columns in phenotype file
- [AMG-1041] - Annotate VCF with extra phenotype columns
- [AMG-1042] - Display extra phenotype columns in XLS interpretation
- [AMG-1043] - Merge Metabolsk phenotype with extra columns
- [AMG-1064] - dbupdate: ClinVar update 2018Jan
- [AMG-1078] - dbupdate: HGMD update 2017.4

### Story

- [AMG-975] - Variant file (XLS) to filter on phenotype
- [AMG-1007] - Requests from lab
- [AMG-1047] - Investigate: Filtus GENE_SYMBOL_INFO not a bug

## v1.10

### Bug

- [AMG-948] - [AND17-5970] trio: POLG variants from parents in XLS
- [AMG-958] - Abort analysis does not kill child processes

### Task

- [AMG-916] - EKG: consolidate coverage reports from all samples in one batch
- [AMG-917] - Add filter for UTR variants
- [AMG-953] - Update gnomAD update scripts due to source URL changed
- [AMG-961] - Update annotation databases for 2017Q3 (vcpipe config)

## v1.9.4

### Bug

- [AMG-946] - Assign gene symbol to variants before Filtus does not work on borderline indels - fix comparison bug
- [AMG-955] - MSH2 script crashed because variable MAX missing value

## v1.9.3

### Bug

- [AMG-946] - Assign gene symbol to variants before Filtus does not work on borderline indels
- [AMG-910] - fix bug variants within special genes were skipped filtering unconditionally

## v1.9.2

### Bug

- [AMG-943] - nextflow missing files fix for all pipelines

## v1.9.1

### Bug

- [AMG-936] - Target pipeline crashed on cardio samples because of "e||a" process

## v1.9

### Bug

- [AMG-931] - cluster jobs not terminating properly

### Task

- [AMG-882] - Metrics UI improvements
- [AMG-900] - Allow exome pipeline to skip SNP fingerprinting test
- [AMG-914] - Filter PMS2 variants before import to ella
- [AMG-920] - vali: adapt executor, nextflow and SLURM config
- [AMG-923] - Add more special genes in trio pipeline
- [AMG-929] - CNV for trios in Excel report sheet
- [AMG-930] - [EKG]: QC increasing the number of low coverage regions
- [AMG-904] - Upgrade vcpdl to nextflow 0.25.6
- [AMG-906] - CNV for trio-excap

## v1.8.1

### Bug

- [AMG-903] - Upgrade nextflow | vcpipe

## v1.8

### Task

- [AMG-511] - Update sops on every release
- [AMG-615] - De novo variants from proband will be filtered out in case the variants in parents are Mosaic variants
- [AMG-815] - Coverage of TrioPU_v03 on Agilent SureSelect capture
- [AMG-835] - Adapt Trio pipeline for exome capture kit
- [AMG-836] - QC params fro Trio pipeline using exome capture
- [AMG-863] - update exome regression test with new total number of filtered variants
- [AMG-871] - XLS report: add "CNV sjekk" checkbox + comment
- [AMG-870] - Adapt webui QC tab for trio-excap
- [AMG-814] - Trio with exome capture technology
- [AMG-741] - Let exome regression test run PU panel and add gene panel checks
- [AMG-837] - Add HG Trio sequenced with exome capture to bundle
- [AMG-846] - Add missing transcript for CDKN2A in panel EKGSanger
- [AMG-852] - Create new panel TrioPU_V03
- [AMG-869] - Add gene MCIDAS to PCD
- [AMG-868] - Add capture kit region files to bundle: Agilent SureSelect Clinical Research Exome V2

## v1.7.2
### Bug
- [AMG-853] - trio variant filtering missing SAS subpopulations
- [AMG-854] - gnomAD genomes filtering with subpopulations not effective
- [AMG-855] - trio filtering with inDB not working

### Task
- [AMG-856] - trio filtering with AN also
- [AMG-843] - variant filtering rules for Excel report (*fixed Trio filtering*)

## v1.7.1

### Task
- [AMG-843] - variant filtering rules for Excel report (*fixed Excap and Target filtring only, Trio filtering not fixed yet*)

## v1.7

### Bug

[AMG-537] - Recessive variants wrongly filtered in Trio pipeline
[AMG-748] - Fix webui QC
[AMG-783] - VEP does not provide NM_ annotations for POMGNT2, ZBTB20, ERMARD
[AMG-787] - Implement the same frequency filtering strategy on trio pipeline as that of exome pipeline
[AMG-821] - Write migration wrapper for alembic

### Task

[AMG-465] - Fetch ID of OMIM entry for gene
[AMG-472] - Usage of phenotypes files should be make explicit
[AMG-580] - Add allele freq from GNOMAD
[AMG-647] - Adapt pipeline to analyse cardio samples
[AMG-749] - Skip taqman check from target.nf for cardio samples
[AMG-755] - Add database status QCFAIL / QCSTOP
[AMG-771] - XLS Rapport: checklist for phenotype check
[AMG-778] - Update inDB
[AMG-779] - Update CNV sensitive-DB
[AMG-780] - Update annotation databases for 2017Q2
[AMG-580] - Add allele freq from GNOMAD
[AMG-645] - Add cardio capture kit into vcpipe-bundle
[AMG-747] - Move cancer and cardio contamination test region file to individual capture kit folder
[AMG-756] - Create test data for Cardio capture kit
[AMG-767] - Create a refGene file with version transcripts
[AMG-780] - Update annotation databases for 2017Q2
[AMG-791] - Add Omim gene ID to Kreft_v01
[AMG-808] - Rename cancer genepanel with capitalization
[AMG-499] - Add and adapt all QC checks for WGS data
[AMG-500] - Adapt the executor/automation system to WGS
[AMG-594] - Add wgs test data to bundle
[AMG-775] - Write result of test in fingerprinting output
[AMG-777] - Adapt QC gui display to WGS
[AMG-594] - Add wgs test data to bundle

### Epic

[AMG-644] - Prepare target pipeline to analyse cardio samples

### Story

[AMG-370] - Use latest available genepanel version by default for new samples
[AMG-485] - WGS GATK3 pipeline
[AMG-744] - Requests from lab for 1.7
[AMG-759] - add gnomAD to vcpipe-bundle
[AMG-759] - add gnomAD to vcpipe-bundle

## v1.6.1

### Task

- [AMG-575] - Facilitate copying of analysis to interpretation

## v1.6

### Bug
- [AMG-696] - indbcreator.py some batches might be ignored silently
- [AMG-709] - pipeline vcpipe1.5 hangs and dies
- [AMG-710] - regression test fails

### Task
- [AMG-681] - Add annotation for pseudogenes
- [AMG-724] - Excel report: extend rules for unclassified variants
- [AMG-725] - Excel report: not rapported class (ir: ikke rapportert)
- [AMG-682] - Add pseudogene annotation to XLS report

## v1.5.1

### Bug

- [AMG-712] - properties MutableDict in SQLAlchemy does not update on TSD

## v1.5

### Bug

- AMG-537 - Recessive variants wrongly filtered in Trio pipeline
- AMG-587 - merge_list_bed in exome-regression takes capture region twice as input
- AMG-591 - duplicate lines in XLS report for POLG
- AMG-592 - Variants in overlapping genes appear twice
- AMG-605 - Permissions not set properly in interpretation folder
- AMG-611 - Duplicate gene symbols in XLS sheet
- AMG-669 - Bad VCF output for splice annotation
- AMG-673 - XLS report missing in exome (in dev)
- AMG-676 - Target copy_interpretation fails (in dev branch)
- AMG-683 - Fix inheritance code in genepanel Migraforst
- AMG-691 - vcpipe, exome_report.py, TypeError when filtering by AF_OUSWES
- AMG-695 - report: wrong keys for inDB
- AMG-698 - XLS wrong display in AC, AF, Hom, Het and indications

### Task

- AMG-55 - Create flowchart of diagnostic processes
- AMG-138 - Compare trio pipeline calls to GIAB trio calls
- AMG-309 - CNV: Create XLS report
- AMG-470 - CNV: write documentation
- AMG-479 - CNV: in-house DB: use M,F vs M,F,P
- AMG-520 - SOP: update "Storage and Security" with PATHS
- AMG-553 - Have regression testing use latest vcpdl and bundle
- AMG-585 - Convert existing genepanels to new format
- AMG-586 - Rename genepanel CorticalMalf to MigraForst
- AMG-590 - CNV: R packages in vcpdl
- AMG-595 - Write script to convert duplicate files on TSD/durable with hardlinks
- AMG-600 - filelock exporter: transfer analyses first
- AMG-612 - Update genepanel KFM with more genes
- AMG-637 - Only give detail indications from in-house db for low frequency variants in the variant file
- AMG-641 - Optimize splice prediction
- AMG-651 - Add POLG to genepanel TrioPU
- AMG-653 - Create new genepanel Lever
- AMG-659 - Update genepanel Ciliopati with more genes
- AMG-660 - Rename cancer genepanels
- AMG-662 - inDB descriptions and XLS column names
- AMG-668 - Update PCD with more genes
- AMG-672 - XLS column order
- AMG-674 - Add fake cnv calls/background to git-lfs
- AMG-675 - Sort XLS report with 1. Inheritance, 2. Gene
- AMG-678 - Merge vcpipe-v1.4.5 into dev
- AMG-679 - Add coverage template worksheet to target excel report
- AMG-686 - vcpipe, fixing merge conflicts reintroduced deleted snippet in another branch

### Sub-task

- AMG-100 - Verify correct linking between procedures in ehaandbok
- AMG-408 - Run Illumina HiSeqX WGS sample with GATK3 pipeline for benchmarking
- AMG-431 - Make tests use latest vcpipe bundle and bin
- AMG-474 - CNV: write CNV specs
- AMG-475 - CNV: write SOP for updating the sensitive-db
- AMG-486 - Adapt alignment and refinement alignment process (genome_mapping) to WGS data
- AMG-487 - Parallellisation (on chr) of HaplotypeCaller
- AMG-496 - Adapt tsd-importer to import WGS data to TSD
- AMG-497 - Adapt/Add VQSR to whole genome SNPs and INDELs
- AMG-498 - Adapt annotation to WGS data analysis
- AMG-563 - Update annotation data for 2017Q1
- AMG-564 - Update in-house DB for 2017Q1
- AMG-569 - Remove all 'u' variants from "Filtered variants" in excel report (exome/target)
- AMG-579 - Add link to ClinVar
- AMG-581 - Trio extrasheet for POLG variants
- AMG-677 - Add url link to GNOMAD

### Story
- AMG-22 - Migrate to garage for all large-file/long-term storage
- AMG-426 - Fix regression/CI tests
- AMG-476 - CNV: exome pipeline
- AMG-477 - CNV: handle chromosome X
- AMG-562 - Requests from lab for 2017Q1
- AMG-575 - Have pipeline-exome use `--analysis` instead of `--sample` and `--genePanel`
- AMG-576 - Pipelines are transcript-agnostic (master target panel)
- AMG-577 - Improve QC

## v1.4.5

### Changes

- AMG-667 - Change config search strategy to improve executor import performance
- AMG-378 - Automatic filtering on ExAC subgroups

## v1.4.4

### Bug fixes

- AMG-624 - Normalization gives wrong frequency for inDB in certain conditions
- AMG-632 - Adapt pipelines (annotation and reports) to new inDB
- AMG-629 - Refractor inDB creator
- AMG-633 - Update inDB

## v1.4.3

### Changes

- AMG-567 Change XLS report field order
- AMG-279 - Adding pylint or flake8 to CI to detect errors
- AMG-523 - Create ansible recipe for vt and vcfanno
- AMG-548 - Change ExAC filtering in trio pipeline
- AMG-552 - Host forks of vcfanno and vt & Update vcpdl to use those
- AMG-555 - Make a binary build of vcfanno
- AMG-567 - Change XLS report field order
- AMG-20 - Establish alternate storage/tracking for vcpipe-{bin|bundle}
- AMG-379 - Report/variantfil should include <100% genes only

### Bug fixes

- AMG-554 - Apply "vcfanno step changes description text" solution to own fork on github/ousamg
- AMG-463 - Allow blank lines and # in sample list used for coverage

## v1.4.2

### Changes

- AMG-541 Increase variant threshold filtering to 0.05 for inDB
- AMG-538 EXaC annotation converts `-0` to `-`
- AMG-540 vcfanno step changes description text

## v1.4.1

### Bug fixes

- AMG-525 Fix TSD cluster options
- AMG-528 Fix R/3.3.2 not loaded in /cluster

## v1.4

### Changes

- AMG-426 improved Unit Testing
- AMG-426 Adapt/Fix target/exome regression test to new version of GIAB data (v3.3)
- AMG-426 Continuous Integration
- Diverse OPS improvements
- AMG-463 Allow blank lines and # in sample list used for coverage
- AMG-368 Add PCT_TARGET_BASES_10X in the QC check in all pipelines
- AMG-483 Set undetermined zone for gender test in trio pipeline and the pipeline will not be crashed for failed gender check
- AMG-510 Updated Filtus
- Add markdown output support to coverage report
- Remove ESP6500 from class 1 filtration
- Excel report:
    - Add ExAC subpopulations
    - Remove ESP6500
    - Trio: Mark uncertain denovo candidates as '?'
- Coverage report:
    - Add markdown output support
    - Support both norwegian and english
    - Add genepanel version to reports
- Annotation databases update  

    |**Database**|**Old Version**|**New Version**|
    |---|---|---|
    |**ExAC**|0.3.0|0.3.1|
    |**HGMD**|Jan. 2013|Feb. 2016|
    |**ClinVar**|Dec. 2014|Oct. 2016|
    |**inDB**|Jun. 2015|Oct. 2016|

#### Genepanels
- PU (nytt) (AMG-451)
- BevegForst (nytt) (AMG-373)
- Immunsvikt (nytt) (AMG-458, AMG-80)
- Craniofacial (nytt) (AMG-103)
- Corticale malformasjoner (nytt) (AMG-310, AMG-462)
- EEogPU: endret fenotyper for ARID1B og SMARCA4 (AMG-283)
- Bindevev (oppdatering) (AMG-278)

### Features

- AMG-265 CNV calling in trio pipeline with exCopyDepth and cnvScan
- AMG-353 Add, scale, standardise annotation with vt and vcfanno
- AMG-455 sensitive database handling


## v1.3.7

### Bug fixes
- [fingerprinting] Fix bug in awk/nextflow
- [fingerprinting] Fix bug in argparser

## v1.3.6

## Changes
- Lab has changed TAQMAN to 16 SNPs
- [fingerprinting] Fingerpriting list for calling HTS is now derived from TAQ
- [fingerprinting] Compare Genotype now allows for multiple sets of options (backward compatible)

## v1.3.5

### Bug fixes
- Fix Nextflow Anaconda path

### Changes
- Copy files, not symlinks, to interpretations

## v1.3.4

### Changes
- [config] Change production area for TSD
- [report] Add genepanel versions to coverage reports
- [report] Only show genes with <90% coverage in coverage report for Immunsvikt
- [report] Mark uncertain denovo variants as '?' in trio excel report
- [report] Remove ESP6500 from excel reports and from filtering
- [pipeline] Merge genepanel regions with capturekit regions in pipelines
- [report] Reorder ExAC subpopulations
- [report] Split gene and inheritance information
- [report] Add subpopulations from ExAC to reports

### Bug fixes
- [report] Fix bug in trio excel report when no header

## v1.3.0

target pipeline

### Bug fixes:

- Fix Sanger verify AlleleDepth criterion when GT is 0/0 (in case of Trios)

### Changes:

- [QC] Adapt SNP fingerprinting scripts with new Taqman card
- [QC] Adapt QC for target sequencing samples
- [bundle] Add all seven cancer associated genepanel
- [bundle] Add region files for cancer customer design capture kit
- [bundle] Add high confident calls from GIAB
- [pipeline] Move SNP fingerprinting checks at the end of the pipeline
- [pipeline] For executor of target pipeline, in order to use ELLA, it also copies some result files to analyses-report folder.

### Features:

- target pipeline

### Known issues:

- regression test for cancer can not fully run in docker

## v1.2.1

Hotfix release.

### Bug fixes:
- Fix missing title in trio report

## v1.2.0

### Bug fixes:
- Fix missing description for some genepanels in excel coverage report

### Changes:
- Add/update 5 genepanels in vcpipe-bundle

## v1.2.1

Hotfix release.

### Bug fixes:
- Fix missing title in trio report

## v1.2.0

### Bug fixes:
- Fix missing description for some genepanels in excel coverage report

### Changes:
- Add/update 5 genepanels in vcpipe-bundle

## v1.1.8

Hotfix release.

### Changes:

- Modify trio coverage report

## v1.1.7

Hotfix release.

### Bug fixes:

- Fix SLURM time

## v1.1.6

Hotfix release.

### Bug fixes:

- Fix coverage report

## v1.1.5

Hotfix release.

### Changes:

- Modify coverage report

### Bug fixes:

- Fix in-house database annotation
- Fix interpretation folder name
- Fix add BAM files to interpretation

## v1.1.4

Hotfix release.

### Bug fixes:

- Fix SLURM memory
- Fix missing import grp
- [bundle] Fix rename capture kit (illumina_trusightone_v01)


## v1.1.2

Hotfix release.

### Bug fixes:

- Previous release didn't include all the fixes it should. This release includes all fixes from 1.1.1.
- [exome] Fix bug when changing the groups of exported files.


## v1.1.1

Hotfix release fixing various issues.

### Bug fixes:

- [qc] Fix bug in coverage_qc script when bam file having no low coverage entries would lead to crash.
- [qc] Change nonsense threshold for exome pipeline.
- [webui] Update flask-restless to newest version. Fixes bug when filtering in the UI leading to permanent error in backend.


### Known issues:

None


## v1.1.0

### Bug fixes:

- [splice] fix REFSEQ accession no
- [webui] fix mixed up Complete<->Failed in overview plot

### Changes:

- [report] Update excel report
- [report] Update coverage report
- [bundle] Add Ektodermal Dysplasi genepanel
- [QC] Adapt QC for trio
- [pipeline] Output HaplotypeCaller bam file
- [pipeline] pre Check functions
- [pipeline] Add new annotation fields for ExAC r3
- [executor] Add support for setting groups when copying files
- [splice] refractor splice module
- [webui] Improvements
- [bundle] Add trio PU genepanel

### Features:

- trio pipeline using Filtus
- in-house database
- ops: Docker, Makefiles, CI
- automated gene panels

### Known issues:

- WebUI may fail on certain filters


## v1.0.2

### Changes:

- [exome pipeline] Fix issue were AD = [0, 0] would crash the excel report.
- [exome pipeline] Fix bug where fingerprint files would not be stored when fingerprint match fails.

### Known issues:

None

## v1.0.1

### Changes:

- Add version information and title to interpretation report in exome pipeline

### Known issues:

None


## v1.0.0

Initial release of the vcpipe pipeline automation system.

### Features:

- Automation system automatically processing incoming samples/analyses, with full version tracking and logging to file system and internal database.
- Web dashboard for showing analysis progress, logs and ability to schedule a rerun of failed analyses.
- Exome pipeline, running either locally or on SLURM cluster

See the documentation for usage and explanation of the format for the sample/analysis packages.

### Known issues:

None
