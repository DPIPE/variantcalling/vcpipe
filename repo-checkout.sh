#!/usr/bin/env bash

# Checking out a revision from a git repository, using separate (working) folders for each branch/tag/sha
#
# Note! Does not checkout out the full LFS files, only the pointer files. vcpipe-bundle has it's own script to
# pull the full LFS files.
#
# Prerequisite: a bare repo already on disk. See usage below.
#
# List worktrees: git -C ${REPO_BARE_ROOT} worktree list

set -euf -o pipefail


usage() {
    echo ""
    echo "USAGE: WORKTREE_ROOT=... REPO_BARE_ROOT=... ./repo-checkout.sh v2.1"
    echo "to checkout branch/tag/sha v2.1 into it's own folder below WORKTREE_ROOT"
    echo ""
    echo "Requirement: A bare clone of the repo already on disk:"
    echo " git -C the-parent-folder-of-REPO_BARE_ROOT clone --bare https://gitlab.com/ousamg/data/<repo.git>"
    echo ""
    echo "Add token to clone without username/password:  https://gitlab-ci-token:<your token>@gitlab.com/.."
    echo ""
    echo "Use https: (as opposed to git:) as GIT LFS only supports https:."
    echo ""
    echo "To list worktrees:"
    echo " git -C REPO_BARE_ROOT worktree list"
    echo "To show disk usage:"
    echo " du -sh REPO_BARE_ROOT"
    
}

if [[ "${#}" -ne 1 || "x${WORKTREE_ROOT}" == "x"  || "x${REPO_BARE_ROOT}" == "x" ]]; then
    echo "Need exactly one argument: tag/branch/sha and variables WORKTREE_ROOT and REPO_BARE_ROOT set."
    usage
    exit 1
fi

if [[ ! -d "${REPO_BARE_ROOT}" ]]; then
    echo "There is no bare repo at ${REPO_BARE_ROOT}. Please create one."
    usage
    exit 1
fi

rev=${1}

echo "Checking out '${rev}' into ${WORKTREE_ROOT}/${rev}"


# Enter bare repo:
cd ${REPO_BARE_ROOT}
git fetch --tags --prune origin ${rev}:${rev}
if [ ! -d "${WORKTREE_ROOT}/${rev}" ];
then
    echo "Will create a worktree and checkout for ${rev}";
    mkdir -p ${WORKTREE_ROOT}
    GIT_LFS_SKIP_SMUDGE=1 git worktree add ${WORKTREE_ROOT}/${rev} ${rev}; # no checkout of LFS, in case repo has such files
else
    echo "Already checked out ${rev} into ${WORKTREE_ROOT}/${rev}";
fi

# Enter working directory:
cd ${WORKTREE_ROOT}/${rev}
git --no-pager show --quiet --format="%H, , %ai: %s%n" HEAD
git reset --hard ${rev} && git clean -f && GIT_LFS_SKIP_SMUDGE=1 git pull origin ${rev}
git --no-pager show --quiet --format="%H, , %ai: %s%n" HEAD
