#!/bin/bash

set -euf -o pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

BRANCH=$(git -C "${DIR}" rev-parse --symbolic-full-name --abbrev-ref HEAD)
REVISION=$(git -C "${DIR}" rev-parse --short HEAD)

FILENAME="vcpipe-DEVELOPMENT-$BRANCH-$REVISION.tgz"

echo "Exporting repo to $FILENAME"

CMD="git -C "${DIR}" archive --prefix vcpipe/ --output $FILENAME $BRANCH"
$CMD

echo "Done!"
