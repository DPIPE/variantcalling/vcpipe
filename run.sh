#!/bin/sh
echo "Container root process running. Will terminate when 'docker stop' or similar"
# Spin until we receive a 15=SIGTERM (e.g. from `docker stop`), 2=SIGINT, 3=SIGQUIT
trap 'echo "Terminating run.sh"; exit 143' 2 3 15 # exit = 128 + 15 (SIGTERM)
tail -f /dev/null & wait ${!}