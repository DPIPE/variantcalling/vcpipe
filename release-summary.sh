#!/bin/bash

# Utility script with multiple uses:
# - prints a summary of changes between the two latest pipeline releases
# - list all releases
# - print "previous|latest" version of vcpipe and genepanels

# This script is assumed to be run in the working directory of vcpipe,
# so no variable is needed for the vcpipe repo.

#set -euf -o pipefail

action=$1

outputfilename=$1

function summary_heading() {
  echo "============================================================"
  echo "Pipeline changes"
  echo "============================================================"
  echo ""
}

function latest_vcpipe() {
  second_to_last $1  | awk '{print $2}'
}

function previous_vcpipe() {
  last $1 | awk '{print $2}'
}

function second_to_last() { # ignore lines starting with # and blank lines (no fields, NF)
 file=$1
 awk '/^#/ {next} NF {print $0}' $file | tail -n2| head -n1
}

function last() {
 file=$1
 awk '/^#/ {next} NF {print $0}' $file | tail -n1
}


function make_summary()  {
  label=$1
  repo=$2
  from_to=$3

  FROM=$(echo $from_to | cut -d'|' -f1)
  TO=$(echo $from_to | cut -d'|' -f2)

  if [[ "${FROM}" == "${TO}" ]]
  then
    echo ""
    echo "------------------------------------------------------------"
    echo "No changes in ${label} ($FROM)"
    echo "------------------------------------------------------------"
  else
    # list binary files (trick is to compary with an empty tree,  SHA1("tree 0\0")):
    git --no-pager -C "${repo}" diff --numstat 4b825dc642cb6eb9a060e54bf8d69288fbee4904 "${FROM}" -- | grep "^-" | cut -f3  > binary-files-temp.txt
    git --no-pager -C "${repo}" diff --numstat 4b825dc642cb6eb9a060e54bf8d69288fbee4904 "${TO}" --   | grep "^-" | cut -f3 >> binary-files-temp.txt

    echo ""
    echo "------------------------------------------------------------"
    echo "Changes in ${label} between ${FROM} and ${TO}"
    echo "------------------------------------------------------------"
    echo "Jira issues mentioned in commits:"
    git -C "${repo}" log -i --grep="LA-[0-9]\+" --grep="AMG-[0-9]\+" "${FROM}...${TO}" | (grep --only-matching -i -e "amg-[0-9]\+" -e "la-[0-9]\+" || true) | tr '[:lower:]' '[:upper:]' | sort | uniq
    echo ""
    echo "Files changed:"
    echo ""
    echo "To see diff run 'git diff --word-diff $FROM $TO -- <filename>'";
    echo "------------------------------"
    git --no-pager -C "${repo}" diff --name-only $FROM $TO | \
       while read a; do
         if [[ $(grep "${a}" binary-files-temp.txt) ]];
           then
             echo "${a} (binary file)"
           else
             echo "${a}";
         fi;
       done
    echo ""
    git --no-pager -C "${repo}" diff --shortstat "${FROM}" "${TO}"
  fi
}

if [[ ${action} == 'summary' ]];
then
  summary_heading
  make_summary "vcpipe" $(pwd) $(./release-summary.sh vcpipe-change)
elif [[ ${action} == 'vcpipe-change' ]];
then
  l=$(latest_vcpipe release-history.csv)
  p=$(previous_vcpipe release-history.csv)
  echo "$l|$p"
elif [[ ${action} == 'list' ]];
then
   awk 'NF {print $0}' release-history.csv | column -t
else
  echo "unknown action"
  exit 1
fi
