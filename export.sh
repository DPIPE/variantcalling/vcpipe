#!/bin/bash

set -euf -o pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "######################"
echo "#  vcpipe  exporter  #"
echo "######################"

usage() {
    echo "Usage: export.sh [-h Show help] [-n revision_descr] [-o output_dir] [-r revision] [...]

                 A descriptive name for the revision may be provided via the '-n' option. If none is
                 provided the revision itself will be inserted in the archive name instead.
                 If no 'revision' is provided the _current_ repository revision will be exported.
                 If no further arguments are provided, the entire repository will be exported.
    "
}

revision=HEAD

while getopts "hn:o:r:" opt; do
    case "$opt" in
    h)
        usage
        exit 0
        ;;
    n)
        meaningful_revision=${OPTARG}
        ;;
    o)
        output_dir=${OPTARG}
        ;;
    r)
        revision=${OPTARG}
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

shift $((OPTIND-1))

if [[ "${#}" -eq 0 ]]; then
    PATHS="";
    FILEPATH_ENDING=""
    echo "exporting entire repository"
else
    PATHS="${@:1}"
    FILEPATH_ENDING="-partial"
    echo "exporting the following paths: ${PATHS[*]}"
fi


# Set to $DIR if not set manually
[[ -z ${output_dir+x} ]] && output_dir="${DIR}"

# Get absolute path to `output_dir`
output_dir_abs="$(realpath "${output_dir}")"

revision_hash=$(git -C "${DIR}" rev-parse --short=8 ${revision})

FILE_COMMON="vcpipe-${meaningful_revision:-$revision}-${revision_hash}${FILEPATH_ENDING}"
ARCHIVE_FILE="${FILE_COMMON}.tgz"
ARCHIVE_META_FILE="${FILE_COMMON}.txt"
ARCHIVE_SHA_FILE="${FILE_COMMON}.sha1"

echo "Exporting repo to ${output_dir_abs}/${ARCHIVE_FILE}"

git -C "${DIR}" archive \
    --output "${output_dir_abs}/${ARCHIVE_FILE}" \
    --prefix vcpipe/ \
    --verbose \
    $revision \
    $PATHS \
    2> /dev/null

echo "Release ${FILE_COMMON}" > ${output_dir_abs}/${ARCHIVE_META_FILE}
echo "-------------------------------------" >> ${output_dir_abs}/${ARCHIVE_META_FILE}

# Enter output_dir so sha1sum's output includes no path!!
pushd ${output_dir_abs} > /dev/null

echo "Run 'sha1sum -c ${ARCHIVE_SHA_FILE}' in the archive file directory to verify its content." \
    >> ${ARCHIVE_META_FILE}
sha1sum ${ARCHIVE_FILE} | tee -a ${ARCHIVE_META_FILE} > ${ARCHIVE_SHA_FILE}
file ${ARCHIVE_FILE} >> ${ARCHIVE_META_FILE}
echo >> ${ARCHIVE_META_FILE}
echo "Generated $(date) on $(hostname) ($(uname -a))" >> ${ARCHIVE_META_FILE}

# Exit output_dir
popd > /dev/null

echo "Done!"