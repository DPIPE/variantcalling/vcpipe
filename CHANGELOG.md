# Changelog

## 6.2 (2023-03-30)
 - Issue #7: new location for genepanels. Format of panels changed as they are created by the Gene Panel Builder
 - Issue #4: Add variables for parental CNV_VCF files in annopipe
 - Issue #5: Don't create per-base.bed.gz as it's not used
 - Issue #10: Bug: basepipe fails for HG002 due to lacking variable
-  Issue #11: Bug: QC outside threshold limits still passes as OK

## 6.1.1 (2023-01-24)
 - don't change ownership or permission of files copied to preprocessed. TSD's cluster often fails on 'chown'
	
## 6.1 (2022-12-08)


 - git backed config file defining svtools 0.1.4
 - use rolling logging in executor
 - msh2 caller for WGS samples
 - PMS2 pseudegene masking for WGS samples
 - Add contamination fraction filter to reduce sensitivity of the caller
 - removed old, unused settings file config/nextflow-test.config
