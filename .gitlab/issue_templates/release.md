# Release vX.X.X

Update this issue while working on the release.

## Release content

vcpipe vX.X.X

Singularity images:

## Prepare
 - [ ] Agree on scope
 - [ ] Create milestone
 - [ ] Link all issues to this release issue and add them to the milestone
 - [ ] Endringskontroll (EK) if relevant (only for significant source code changes, not hotfixes). See attachment of ‘HTS Bioinf - Process for updates to production pipelines (93783)’ in eHåndbok.
 - [ ] Create a folder on TSD to document testing (usually `/ess/p22/data/durable/development/tests`, refer`to it in EK)
 - [ ] Pre-announce to the public (diag-bioinf at UiO, diag-lab at UiO, kvalitet at OUS, other relevant units/users) if needed
 - [ ] Create release branch. For major releases, branch out from `dev`. For hotfixes, branch out from `master`.

## Testing
- [ ] Manual testing: mention any testing done either locally or in the staging environment on TSD/NSC, including the ELLA staging instance.
- [ ] QC: check coverage/precision/specificity for an NA sample (not needed for hotfixes)

## Create release artifact

- [ ] Merge all relevant features into the release branch
- [ ] Update release notes (`doc/releasenotes/README.md`) in the release branch
- [ ] Merge the release branch into both `dev` and `master`
- [ ] Tag on `master` ([vX.X.X](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Releases-and-production-system-updates/Process-for-updates-to-production-pipelines/#versioning-and-hotfixes)), this can be done simultanously as creating a release (see next point)
- [ ] Create a release via the repo's [Releases](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/releases) page ([GitLab guide](https://docs.gitlab.com/ee/user/project/releases/))
- [ ] Run the export script with the tag as revision, see `export.sh -h` in vcpipe 

## Deploy
- [ ] Endringskontroll approved (not for hotfix)
- [ ] Transfer the files created by `export.sh` to TSD archive: `{production}/sw/archive directory`  
- [ ] Unpack files on TSD using `sw/deploy.sh` 
- [ ] Transfer files to NSC
- [ ] Unpack files on NSC (server beta) using `sw/deploy.sh`

## Announcement
- [ ] Post user announcement in the Teams [OPS channel](https://dpipe.gitlab.io/docs/docs-pages/docs-store/docs/Production-routines/Use-of-Teams-ops-channel/). To reduce noise, this is not needed for hotfix releases if the users are already informed about the in-place bug-fixes.

```
Pipeline ble oppdatert ____. Nedenfor er endringene kort beskrevet.

Oppsummering
------------------
vcpipe: vX.X.X-rel 
Singularity images: ...

link to GitLab milestone: 

```
