## Description

Fixes issue: 

## Checklist

- [ ] Endringskontroll written
- [ ] This MR has a description (including any risks) giving the reviewer context
- [ ] Tests have been done (see below, please state how changes has been tested)

## Tests perfomed 

**Pipeline**
- [ ] Run of wgs basepipe in staging 
- [ ] Run of wgs triopipe in staging 
- [ ] Run of wgs single annopipe in staging 
- [ ] Run of wgs trio annopipe in staging 
- [ ] Run of excap basepipe in staging 
- [ ] Run of excap annopipe in staging 
- [ ] Run of EKG basepipe in staging 
- [ ] Run of EKG annopipe in staging 
- [ ] Sensitivity and specificity of control samples

**Import to ELLA**
- [ ] Imported into ELLA: WGS
- [ ] Imported into ELLA: excap
- [ ] Imported into ELLA: EKG

**webui and executor**
- [ ] irrelevant 
- [ ] executor/web starts and stops
- [ ] webui: list analyses using filters (type, status, substring)
- [ ] webui: abort/resume/delete/force-delivery
- [ ] webui: log shown
- [ ] webui: analyses status updated (pending, running, error, done, missing deps)
- [ ] webui: version info shown
- [ ] killfile stops executor/webui