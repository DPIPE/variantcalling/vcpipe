#!/bin/bash

# this script runs all integration / regression test pairs in parallel
# make sure to run `make build` before running this script


if [[ "$#" -eq 0 ]] ; then
    echo "no tests specified."
    exit 0
fi

for test in "$@" ; do
    case "${test}" in
        cuca-base)
            #basepipe - CuCaV1
            screen -dm -S bc bash -c "
            export \
                CAPTURE_KIT=CuCaV1;
            make basepipe-integration &> CuCaV1_BasePipe_Int.log;
            make basepipe-regression &> CuCaV1_BasePipe_Reg.log;
            "
            ;;
        cuca-anno)
            #annopipe - CuCaV1
            screen -dm -S ac bash -c "
            export \
                CAPTURE_KIT=CuCaV1 \
                GENE_PANEL=KREFT17_v01;
            make annopipe-integration &> CuCaV1_AnnoPipe_Int.log;
            make annopipe-regression &> CuCaV1_AnnoPipe_Reg.log;
            "
            ;;
        wes-base)
            #basepipe - agilent_sureselect_v05 
            screen -dm -S ba bash -c "
            export \
                CAPTURE_KIT=agilent_sureselect_v05;
            make basepipe-integration &> Agilent_BasePipe_Int.log;
            make basepipe-regression &> Agilent_BasePipe_Reg.log;
            "
            ;;
        wes-trio)
            #triopipe
            screen -dm -S t bash -c "
            export \
                CAPTURE_KIT=agilent_sureselect_v05;
            make triopipe-integration &> Agilent_TrioPipe_Int.log;
            make triopipe-regression &> Agilent_TrioPipe_Reg.log;
            "
            ;;
        wes-anno)
            #annopipe - agilent_sureselect_v05
            screen -dm -S aa bash -c "
            export \
                CAPTURE_KIT=agilent_sureselect_v05 \
                GENE_PANEL=PU_v02;
            make annopipe-integration &> Agilent_AnnoPipe_Int.log;
            make annopipe-regression &> Agilent_AnnoPipe_Reg.log;
            "
            ;;
        wgs-base)
            #basepipe - wgs
            screen -dm -S ba bash -c "
            export \
                CAPTURE_KIT=wgs \
                SPECIALIZED_PIPELINE=dragen;
            make basepipe-integration &> WGS_BasePipe_Int.log;
            make basepipe-regression &> WGS_BasePipe_Reg.log;
            "
            ;;
        wgs-trio)
            #triopipe
            screen -dm -S t bash -c "
            export \
                CAPTURE_KIT=wgs \
                SPECIALIZED_PIPELINE=dragen;
            make triopipe-integration &> WGS_TrioPipe_Int.log;
            make triopipe-regression &> WGS_TrioPipe_Reg.log;
            "
            ;;
        wgs-anno)
            #annopipe - wgs
            screen -dm -S aa bash -c "
            export \
                CAPTURE_KIT=wgs \
                GENE_PANEL=PU_v02 \
                SPECIALIZED_PIPELINE=dragen;
            make annopipe-integration &> WGS_AnnoPipe_Int.log;
            make annopipe-regression &> WGS_AnnoPipe_Reg.log;
            "
            ;;
        *)
            echo "unknown test: '${test}'"
            exit 1
            ;;
    esac
done
