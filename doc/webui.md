#WebUI

The WebUI shows an interactive view of the database used by the executor. You can here see the status of samples/analysis, relaunch failed analysis and inspect logs.

You can run it by using the following command:

`SETTINGS=/path/to/settings.json DB_URL="postgres://user:pass@localhost/dbname" exe/webui`