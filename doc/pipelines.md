# Pipelines

The framework is built to allow any number of pipelines.
They are implemented using Nextflow.io, and take a sample metadata file as input.
Normally the pipelines are launched by the executor, upon detection up a new analysis.

## Analysis types

In the standard offer, three types of analyses are offered:
- basepipe: performs mapping, variant calling and QC
- triopipe: performs join calling
- annopipe: performs annotation and reporting

For a single sample, two analysis folders and metadata would have been created: one basepipe and one annopipe (which depends on the genepanel).
For a trio, five analysis folders and metadata would have been created: three basepipe (one for each of the members), one triopipe (for joining them) and one annopipe.

### Manual run

You can run a pipeline manually by running:

`SETTINGS=/path/to/settings.json exe/pipeline/basepipe --analysis /path/to/my_analysis.analysis`

## How to create new pipeline

Since an analysis in the system consists of a single entry point (command), your pipeline must run the whole analysis from start to end in one step.

Under `src/pipeline` create:
- `mypipe.nf`: a nextflow pipeline
- `mypipe-dependency`: python script for checking dependencies
- `mypipe-post`: python script for calling Quality Control and delivering files
- a symlink `lib` to `../lib` for utility classes
Under `exe/pipeline` create an entry point script `mypipe`.

No modification of the executor is necessary.
