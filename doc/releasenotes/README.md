---
title: Latest release
---

# Release notes: Latest releases

|Major versions|Minor versions|
|:--|:--|
|[v8.0.0](#version-800)|[v8.0.1](#version-801)|[v8.0.2](#version-801)|[v8.1.0](#version-810)||[v8.1.1](#version-811)|[v8.1.2](#version-812)|[v8.1.3](#version-813)|[v8.1.4](#version-814)|
|[v7.0](#version-70)|[v7.0.1](#version-701), [v7.0.2](#version-702)|

## Version 8.1.4

Release date 18.03.2025

This release contains the following fixed issues:
- [#36](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/36) Add cnv_sv output to dragen.nf
- [#48](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/48) Force delivery function for annopipe should be removed
- [#65](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/65) Release templates should be updated
- [#87](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/87) Hardlink instead of copy pipeline result files
- [#88](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/88) Disable vcpipe CI pipeline
- [#90](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/90) Switch to use v4.2.1 set for the GIAB control samples

## Version 8.1.3

Release date 06.02.2025

This release contains the following fixed issues:
- [#84](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/84) make mt variant vcf file optional for EKG and exome annopipe analysis

## Version 8.1.2

Release date 31.01.2025

This release contains the following fixed issues:
- [#28](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/28) Make mtDNA VCF available for downstream annotation in annopipe
- [#69](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/69) Changes to mount into Steps
- [#81](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/81) Allow NA24385 (HG002) to trigger hap.py running in basepipe
- [#82](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/82) Update the config files to access Dragen reference genome file for Dragen v4.3.6
- [#83](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/83) Edit nextflow.config to gave dragen trio a lower priority

## Version 8.1.1

Release date 04.12.2024

This release contains the following fixed issues:
- [#76](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/76) Show QC report for parents in ELLA
- [#77](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/77) Update the nextflow.config to allow longer processing time for bigger samples
- [#79](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/79) Do not manually call MSH2 variant when Dragen already called

## Version 8.1.0

Release date 18.11.2024

This release contains the following fixed issues:
- [#49](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/49) New version of svtools

## Version 8.0.2

Release date 06.09.2024

This release contains the following hotfixes:
- [#46](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/46) Hap.py crashes with vcpipe version8
- [#52](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/52) Reanalyses of old samples attempt the GATK pipeline
- [#56](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/56) Remove module load singularity
- [#57](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/57) Reinstall psycopg2 and other python libraries for Python3.9
- [#59](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/59) Add ora reference genome in the pipeline, so dragen can read ora files

## Version 8.0.1

Release date 21.06.2024

This release contains the following hotfixes:
- [#45](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/45) Correct instructions of export script
- [#51](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/51) Fix 'force delivery'
- [#62](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/62) Fix decompression of genozip files
- [#63](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/63) Fix lack of Q30 information 

## Version 8.0.0

Release date 19.06.2024
- [#17](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/17) Avoid file-system lookups for input data in nextflow
- [#18](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/18) Stub dragen processes
- [#21](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/21) Adapt vcpipe to compressed FASTQ files
- [#32](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/32) Fixing basepipe and triopipe for the new pipelines used by Steps
- [#38](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/38) Use Hetzner for CI
- [#39](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/39) Fixing CI test on hetzner - pipeline/config

## Version 7.0.2

Release date 23.01.2024
- [#27](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/27) Fix fingerprinting check bug which gives false mismatches
- [#31](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/31) Update TSD dragen nextflow config

## Version 7.0.1

Release date 06.10.2023
- [#25](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/25) Fix bug which make TUMOR samples fail pipeline
- [#26](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/26) Fix Ella importing fails due to two `*.analysis` files in the incoming folder

## Version 7.0

Release date 11.07.2023
- [#23](https://gitlab.com/DPIPE/variantcalling/vcpipe/-/issues/23) Bug fix: hap.py process in triopipe and CNV file names in annopipe and un-used variables from msh2.bash